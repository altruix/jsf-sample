import co.altruix.smarturlrotator.IWebPageContentsReader;
import co.altruix.smarturlrotator.ItemAvailabilityChecker;
import org.junit.Test;

import java.io.IOException;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ItemAvailabilityCheckerTest {
    @Test
    public void testOnAvailableItem() throws IOException {
        final IWebPageContentsReader webPageContentsReader = mock(IWebPageContentsReader.class);

        when(webPageContentsReader.readWebPageContent(anyString())).thenReturn("bla-bla-bla");

        final ItemAvailabilityChecker objectUnderTest =
                new ItemAvailabilityChecker(webPageContentsReader);

        assertThat(objectUnderTest.isItemSold("http://someurl.com")).isFalse();
    }

    @Test
    public void testOnSoldItem() throws IOException {
        final IWebPageContentsReader webPageContentsReader = mock(IWebPageContentsReader.class);

        when(webPageContentsReader.readWebPageContent(anyString())).thenReturn
                (ItemAvailabilityChecker.ITEM_SOLD_MARKER);

        final ItemAvailabilityChecker objectUnderTest =
                new ItemAvailabilityChecker(webPageContentsReader);

        assertThat(objectUnderTest.isItemSold("http://someurl.com")).isTrue();

    }

}
