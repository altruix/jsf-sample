import co.altruix.smarturlrotator.IEmailSender;
import co.altruix.smarturlrotator.ISimpleProductUrlReader;
import co.altruix.smarturlrotator.RedirectInfo;
import co.altruix.smarturlrotator.SmartUrlRotatorServlet;
import co.altruix.smarturlrotator.StartupListener;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import java.util.LinkedList;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class StartupListenerTest {
    @Test
    public void testNPE()
    {
        final StartupListener objectUnderTest = new StartupListener();

        assertThat(Whitebox.getInternalState(objectUnderTest, "emailSender")).isNotNull();
    }

    @Test
    public void testEmailSending()
    {
        final ServletContext context = mock(ServletContext.class);

        final ServletContextEvent servletContextEvent = mock(ServletContextEvent.class);
        when(servletContextEvent.getServletContext()).thenReturn(context);

        final IEmailSender emailSender = mock(IEmailSender.class);

        final ISimpleProductUrlReader simpleProductUrlReader = mock(ISimpleProductUrlReader.class);

        final StartupListener objectUnderTest = new StartupListener(emailSender,
                simpleProductUrlReader);

        objectUnderTest.contextInitialized(servletContextEvent);

        verify(emailSender, times(1)).sendEMail(StartupListener.SUBJECT, StartupListener.BODY,
                StartupListener.RECIPIENT, context);
    }

    @Test
    public void testInitializationOfRedirectList()
    {
        final ServletContext context = mock(ServletContext.class);

        final ServletContextEvent servletContextEvent = mock(ServletContextEvent.class);
        when(servletContextEvent.getServletContext()).thenReturn(context);

        final IEmailSender emailSender = mock(IEmailSender.class);

        final ISimpleProductUrlReader simpleProductUrlReader = mock(ISimpleProductUrlReader.class);

        final StartupListener objectUnderTest = new StartupListener(emailSender,
                simpleProductUrlReader);

        objectUnderTest.contextInitialized(servletContextEvent);

        verify(context, times(1)).setAttribute(SmartUrlRotatorServlet.REDIRECTS,
                new LinkedList<RedirectInfo>());
    }
}
