import co.altruix.smarturlrotator.IItemAvailabilityChecker;
import co.altruix.smarturlrotator.ProductUrlInfo;
import co.altruix.smarturlrotator.UpdateProductAvailabilityJob;
import org.junit.Test;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import javax.servlet.ServletContext;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UpdateProductAvailabilityJobTest {

    public static final String URL_1 = "http://de.dawanda.com/product/57012499-Strumpfband-Elfenbein-mit-Masche";
    public static final String URL_2 = "http://de.dawanda.com/product/61238155-Strumpfband-Blau-und-Silber";
    public static final String URL_3 = "http://de.dawanda.com/product/48755990-Strumpfband-Pfirsich";

    @Test
    public void test() throws JobExecutionException, IOException {
        final IItemAvailabilityChecker itemAvailabilityChecker =
                mock(IItemAvailabilityChecker.class);

        when(itemAvailabilityChecker.isItemSold(URL_1)).thenReturn(true);
        when(itemAvailabilityChecker.isItemSold(URL_2)).thenReturn(false);

        final UpdateProductAvailabilityJob objectUnderTest =
                new UpdateProductAvailabilityJob(itemAvailabilityChecker);

        final JobExecutionContext jobExecutionContext = mock(JobExecutionContext.class);

        final List<ProductUrlInfo> productData = new LinkedList<ProductUrlInfo>();

        productData.add(new ProductUrlInfo(URL_1, true));
        productData.add(new ProductUrlInfo(URL_2, true));
        productData.add(new ProductUrlInfo(URL_3, false));

        final ServletContext context = mock(ServletContext.class);

        when(context.getAttribute(UpdateProductAvailabilityJob.PRODUCT_DATA)).thenReturn(productData);

        final JobDataMap jobDataMap = mock(JobDataMap.class);

        when(jobDataMap.get(UpdateProductAvailabilityJob.SERVLET_CONTEXT)).thenReturn(context);

        final JobDetail jobDetail = mock(JobDetail.class);

        when(jobDetail.getJobDataMap()).thenReturn(jobDataMap);

        when(jobExecutionContext.getJobDetail()).thenReturn(jobDetail);



        objectUnderTest.execute(jobExecutionContext);



        // Let's say there are 2 products:
        // First was available, but now is sold
        // Second was available and is still available

        // URL_3 is sold already, so itemAvailabilityChecker shouldn't even check it.
        //verify(itemAvailabilityChecker.isItemSold(URL_3), never());
        verify(itemAvailabilityChecker, never()).isItemSold(URL_3);

        final List<ProductUrlInfo> newProductData = new LinkedList<ProductUrlInfo>();

        newProductData.add(new ProductUrlInfo(URL_1, false));
        newProductData.add(new ProductUrlInfo(URL_2, true));
        newProductData.add(new ProductUrlInfo(URL_3, false));

        verify(context, times(1)).setAttribute(UpdateProductAvailabilityJob.PRODUCT_DATA,
                newProductData);
    }

    @Test
    public void testNpe() throws JobExecutionException, IOException {
        final IItemAvailabilityChecker itemAvailabilityChecker =
                mock(IItemAvailabilityChecker.class);

        when(itemAvailabilityChecker.isItemSold(URL_1)).thenReturn(true);
        when(itemAvailabilityChecker.isItemSold(URL_2)).thenReturn(false);

        final UpdateProductAvailabilityJob objectUnderTest =
                new UpdateProductAvailabilityJob(itemAvailabilityChecker);

        final JobExecutionContext jobExecutionContext = mock(JobExecutionContext.class);

        final ServletContext context = mock(ServletContext.class);

        when(context.getAttribute(UpdateProductAvailabilityJob.PRODUCT_DATA)).thenReturn(null);

        final JobDataMap jobDataMap = mock(JobDataMap.class);

        when(jobDataMap.get(UpdateProductAvailabilityJob.SERVLET_CONTEXT)).thenReturn(context);

        final JobDetail jobDetail = mock(JobDetail.class);

        when(jobDetail.getJobDataMap()).thenReturn(jobDataMap);

        when(jobExecutionContext.getJobDetail()).thenReturn(jobDetail);



        objectUnderTest.execute(jobExecutionContext);

        // TODO: Continue here
    }

}
