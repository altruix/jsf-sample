package co.altruix.smarturlrotator;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.ee.servlet.QuartzInitializerListener;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import java.util.Date;
import java.util.LinkedList;

import static org.quartz.DateBuilder.evenMinuteDate;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class StartupListener implements ServletContextListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(StartupListener.class);
    public static final String SUBJECT = "smart-url-rotator started";
    public static final String BODY = "I'm up and running!";
    public static final String RECIPIENT = "dp@altruix.co";
    private final IEmailSender emailSender;
    private final ISimpleProductUrlReader simpleProductUrlReader;

    public StartupListener()
    {
        this(new EmailSender(), new SimpleProductUrlReader());
    }

    public StartupListener(final IEmailSender aEmailSender,
                           final ISimpleProductUrlReader aSimpleProductUrlReader)
    {
        emailSender = aEmailSender;
        simpleProductUrlReader = aSimpleProductUrlReader;
    }

    @Override
    public void contextInitialized(final ServletContextEvent aServletContextEvent) {

        final ServletContext context = aServletContextEvent.getServletContext();
        context.setAttribute(SmartUrlRotatorServlet.REDIRECTS,
                new LinkedList<RedirectInfo>());

        emailSender.sendEMail(SUBJECT, BODY, RECIPIENT, context);

        context.setAttribute(UpdateProductAvailabilityJob.PRODUCT_DATA,
                simpleProductUrlReader.readProductUrls(context));

        final StdSchedulerFactory factory = (StdSchedulerFactory) aServletContextEvent
                .getServletContext()
                .getAttribute(QuartzInitializerListener.QUARTZ_FACTORY_KEY);

        if (factory != null)
        {
            setupScheduledJobs(context, factory);
        }

    }

    private void setupScheduledJobs(final ServletContext aContext,
                                    final StdSchedulerFactory aFactory) {
        try {
            final Scheduler scheduler = aFactory.getScheduler();

            final JobDetail job = newJob(UpdateProductAvailabilityJob.class)
                    .build();
            Trigger trigger = newTrigger()
                    .withIdentity("trigger1", "group1")
                    .startAt(evenMinuteDate(new Date()))
                    .withSchedule(simpleSchedule().withIntervalInMinutes(2).repeatForever())
                    .build();

            job.getJobDataMap().put("emailSender", emailSender);
            job.getJobDataMap().put("servletContext", aContext);

            scheduler.scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            LOGGER.error("", e);
        }
    }

    @Override
    public void contextDestroyed(final ServletContextEvent aServletContextEvent) {
    }
}
