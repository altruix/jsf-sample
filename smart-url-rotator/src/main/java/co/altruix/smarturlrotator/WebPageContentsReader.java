package co.altruix.smarturlrotator;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;

public class WebPageContentsReader implements IWebPageContentsReader {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(WebPageContentsReader.class);

    @Override
    public String readWebPageContent(final String aUrl) throws IOException {
        final URL url = new URL(aUrl);

        final String result = Resources.toString(url, Charsets.UTF_8);

        LOGGER.error("result: " + result);

        return result;
    }
}
