package co.altruix.smarturlrotator;

import javax.servlet.ServletContext;

public interface IEmailSender {
    void sendEMail(final String aSubject, final String aBody,
                   final String aRecipient, final ServletContext aContext);
}
