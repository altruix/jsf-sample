package co.altruix.smarturlrotator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class SimpleProductUrlReader implements ISimpleProductUrlReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleProductUrlReader.class);

    @Override
    public List<ProductUrlInfo> readProductUrls(final ServletContext aContext) {
        final List<ProductUrlInfo> result = new LinkedList<ProductUrlInfo>();
        final String productUrls = aContext.getInitParameter(ProductUrlReader.PRODUCT_URLS);

        final StringTokenizer tokenizer = new StringTokenizer(productUrls,
                System.getProperty("line.separator"));

        while (tokenizer.hasMoreTokens())
        {
            final String curUrl = tokenizer.nextToken();
            result.add(new ProductUrlInfo(curUrl,  true));
        }

        return result;
    }

}
