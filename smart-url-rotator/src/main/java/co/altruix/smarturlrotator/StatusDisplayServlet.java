package co.altruix.smarturlrotator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class StatusDisplayServlet extends HttpServlet {
    @Override
    protected void doGet(final HttpServletRequest aRequest, final HttpServletResponse aResponse)
            throws ServletException, IOException {
        final StringBuilder builder = new StringBuilder();

        builder.append("<h1>Status</h1>");

        builder.append("<h2>Original URLs</h2>");
        builder.append("<table>");
        builder.append("<tr>");
        builder.append("<td>Status</td>");
        builder.append("<td>URL</td>");
        builder.append("</tr>");

        final List<ProductUrlInfo> products = (List<ProductUrlInfo>) getServletContext().getAttribute
                (UpdateProductAvailabilityJob.PRODUCT_DATA);

        for (final ProductUrlInfo curProductUrlInfo : products)
        {
            builder.append("<tr>");
            builder.append("<td>");

            if (curProductUrlInfo.isAvailable())
            {
                builder.append("Available");
            }
            else
            {
                builder.append("Sold");
            }

            builder.append("</td>");
            builder.append("<td>");
            builder.append("<a href=");
            builder.append(curProductUrlInfo.getUrl());
            builder.append(">");
            builder.append(curProductUrlInfo.getUrl());
            builder.append("</a>");
            builder.append("</td>");
            builder.append("</tr>");
        }

        builder.append("</table>");

        builder.append("<h2>Redirects</h2>");
        builder.append("<pre>");

        final List<RedirectInfo> redirectInfos = (List<RedirectInfo>) getServletContext().getAttribute
                (SmartUrlRotatorServlet.REDIRECTS);

        for (final RedirectInfo curRedirectInfo : redirectInfos)
        {
            builder.append(curRedirectInfo.getRedirectionTime().toString());
            builder.append("  ");
            builder.append(curRedirectInfo.getTargetUrl());
            builder.append("  ");
            builder.append(curRedirectInfo.getReferer());
            builder.append(System.getProperty("line.separator"));
        }

        builder.append("</pre>");

        aResponse.getWriter().write(builder.toString());
    }
}
