package co.altruix.smarturlrotator;

public class ProductUrlInfo {
    private String url;
    private boolean available;

    public ProductUrlInfo(final String aUrl, final boolean aAvailable) {
        url = aUrl;
        available = aAvailable;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String aUrl) {
        url = aUrl;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(final boolean aAvailable) {
        available = aAvailable;
    }

}
