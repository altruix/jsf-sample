package co.altruix.smarturlrotator;

import javax.servlet.ServletContext;
import java.util.List;

public interface ISimpleProductUrlReader {
    List<ProductUrlInfo> readProductUrls(ServletContext aContext);
}
