package co.altruix.smarturlrotator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class ProductUrlReader {
    public static final String PRODUCT_URLS = "ProductUrls";
}
