package co.altruix.smarturlrotator;

import java.util.Date;

public class RedirectInfo {
    private Date redirectionTime;
    private String targetUrl;
    private String referer;

    public Date getRedirectionTime() {
        return redirectionTime;
    }

    public void setRedirectionTime(final Date aRedirectionTime) {
        redirectionTime = aRedirectionTime;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(final String aTargetUrl) {
        targetUrl = aTargetUrl;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(final String aReferer) {
        referer = aReferer;
    }

    public RedirectInfo(final Date aRedirectionTime, final String aTargetUrl,
                        final String aReferer) {
        redirectionTime = aRedirectionTime;
        targetUrl = aTargetUrl;
        referer = aReferer;
    }


}
