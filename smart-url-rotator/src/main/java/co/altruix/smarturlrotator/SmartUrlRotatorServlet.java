package co.altruix.smarturlrotator;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class SmartUrlRotatorServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(SmartUrlRotatorServlet.class);

    public static final String REDIRECTS = "Redirects";

    private final IEmailSender emailSender;

    private final Random random = new Random();

    public SmartUrlRotatorServlet()
    {
        this(new EmailSender());
    }

    public SmartUrlRotatorServlet(IEmailSender aEmailSender) {
        emailSender = aEmailSender;
    }


    @Override
    protected void doGet(final HttpServletRequest aRequest, final HttpServletResponse aResponse)
            throws ServletException, IOException {
        final Predicate<ProductUrlInfo> predicate = new Predicate<ProductUrlInfo>() {
            @Override
            public boolean apply(final ProductUrlInfo aProductUrlInfo) {
                return aProductUrlInfo.isAvailable();
            }
        };

        final List<ProductUrlInfo> products = (List<ProductUrlInfo>) getServletContext().getAttribute
                (UpdateProductAvailabilityJob.PRODUCT_DATA);

        final List<ProductUrlInfo> availableProducts = Lists.newArrayList(Iterables.filter(
                products, predicate));

        if (availableProducts.size() > 0)
        {
            int randomNum = random.nextInt((availableProducts.size() - 1 - 0) + 1);

            final String url = availableProducts.get(randomNum).getUrl().trim();

            final List<RedirectInfo> oldRedirects = (List<RedirectInfo>) getServletContext()
                    .getAttribute(REDIRECTS);

            final String referer = aRequest.getHeader("referer");

            oldRedirects.add(new RedirectInfo(new Date(), url, referer));

            getServletContext().setAttribute(REDIRECTS, oldRedirects);

            aResponse.sendRedirect(url);
        }
        else
        {
            // TODO: Send an e-mail to dp@altruix.co that all items have been sold

            aResponse.sendRedirect("http://de.dawanda.com/shop/Nastasi");
        }
    }
}
