package co.altruix.smarturlrotator;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class UpdateProductAvailabilityJob implements Job {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(UpdateProductAvailabilityJob.class);

    public static final String SERVLET_CONTEXT = "servletContext";
    public static final String PRODUCT_DATA = "productData";
    private IItemAvailabilityChecker itemAvailabilityChecker;

    public UpdateProductAvailabilityJob()
    {
        this(new ItemAvailabilityChecker());
    }

    public UpdateProductAvailabilityJob(final IItemAvailabilityChecker aItemAvailabilityChecker)
    {
        itemAvailabilityChecker = aItemAvailabilityChecker;
    }

    @Override
    public void execute(final JobExecutionContext aJobExecutionContext) throws
            JobExecutionException {
        final JobDataMap data = aJobExecutionContext.getJobDetail().getJobDataMap();

        final IEmailSender emailSender = (IEmailSender) data.get("emailSender");
        final ServletContext servletContext = (ServletContext) data.get(SERVLET_CONTEXT);

        if (emailSender != null)
        {
            emailSender.sendEMail("Scheduled task", "Test", "dp@altruix.co", servletContext);
        }

        final List<ProductUrlInfo> productData = (List<ProductUrlInfo>)
                servletContext.getAttribute(PRODUCT_DATA);

        if (productData == null)
        {
            return;
        }

        final List<ProductUrlInfo> newProductData = new LinkedList<ProductUrlInfo>();

        for (final ProductUrlInfo curProductUrlInfo : productData)
        {
            if (curProductUrlInfo.isAvailable())
            {
                try
                {
                    boolean stillAvailable = !itemAvailabilityChecker.isItemSold(curProductUrlInfo
                            .getUrl().trim());

                    LOGGER.debug("URL: " + curProductUrlInfo.getUrl() + ", " +
                            "available: " + stillAvailable);

                    newProductData.add(new ProductUrlInfo(curProductUrlInfo.getUrl(), stillAvailable));
                } catch (IOException e) {
                    LOGGER.error("", e);
                }
            }
            else
            {
                newProductData.add(new ProductUrlInfo(curProductUrlInfo.getUrl(), false));
            }
        }

        servletContext.setAttribute(PRODUCT_DATA, newProductData);
    }
}
