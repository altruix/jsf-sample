package co.altruix.smarturlrotator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ItemAvailabilityChecker implements IItemAvailabilityChecker {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(ItemAvailabilityChecker.class);

    public static final String ITEM_SOLD_MARKER =
            "Dieses einzigartige Produkt ist momentan leider nicht verfügbar.";
    private final IWebPageContentsReader webPageContentsReader;

    public ItemAvailabilityChecker()
    {
        this(new WebPageContentsReader());
    }

    public ItemAvailabilityChecker(
            final IWebPageContentsReader aWebPageContentsReader) {
        webPageContentsReader = aWebPageContentsReader;
    }

    @Override
    public boolean isItemSold(final String aUrl) throws IOException {
        LOGGER.error("In isItemSold, webPageContentsReader: " + webPageContentsReader);

        final String text = webPageContentsReader.readWebPageContent(aUrl);

        LOGGER.error("text: " + text);

        return text.contains(ITEM_SOLD_MARKER);
    }
}
