package co.altruix.smarturlrotator;

import java.io.IOException;

public interface IItemAvailabilityChecker {
    boolean isItemSold(String aUrl) throws IOException;
}
