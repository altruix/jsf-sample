package co.altruix.smarturlrotator;

import java.io.IOException;
import java.net.MalformedURLException;

public interface IWebPageContentsReader {
    String readWebPageContent(final String aUrl) throws IOException;
}
