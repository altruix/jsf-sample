package co.altruix;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class InwtApplicationTest {
    @Test
    public void testNpe()
    {
        final InwtApplication app = new InwtApplication();

        app.transactionStart(app, null);
        assertThat(InwtApplication.getInstance()).isNotNull();
    }
}
