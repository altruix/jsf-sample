package co.altruix.inwt.recommendationtab;

import com.vaadin.data.Property;
import com.vaadin.ui.Button;
import com.vaadin.ui.TextArea;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UrlTextAreaValueChangeListenerTest {
    @Test
    public void testValueChangeEmptyText()
    {
        valueChangeTestLogic(" ", false);

    }
    @Test
    public void testValueChangeNonEmptyText()
    {
        valueChangeTestLogic("http://95.85.28.148:8080/inwt-mobile/f/index.xhtml", true);
    }

    private void valueChangeTestLogic(final String aTextAreaValue,
                                      final boolean aExpectedButtonEnabledness) {
        /**
         * Prepare
         */
        final TextArea textArea = mock(TextArea.class);
        when(textArea.getValue()).thenReturn(aTextAreaValue);

        final Button button = mock(Button.class);

        final UrlTextAreaValueChangeListener objectUnderTest = new UrlTextAreaValueChangeListener
                (textArea, button);
        /**
         * Run method under test
         */
        objectUnderTest.valueChange(mock(Property.ValueChangeEvent.class));

        /**
         * Verify
         */
        verify(button).setEnabled(aExpectedButtonEnabledness);
    }
}
