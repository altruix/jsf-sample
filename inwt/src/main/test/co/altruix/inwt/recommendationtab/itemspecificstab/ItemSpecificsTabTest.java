package co.altruix.inwt.recommendationtab.itemspecificstab;

import org.junit.Test;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.mock;

public class ItemSpecificsTabTest {
    public static final String EMAIL = "dp@altruix.co";

    @Test
    public void testInitUi()
    {
        /**
         * Prepare
         */
        final ItemSpecificsTab objectUnderTest = spy(new ItemSpecificsTab(true));

        /**
         * Run method under test
         */
        objectUnderTest.initUi();


        /**
         * Verify
         */
        verify(objectUnderTest).addComponent(isA(ItemSpecificsControlPane.class));
        verify(objectUnderTest).addComponent(isA(ItemSpecificsTablePane.class));
    }

    @Test
    public void testUserChanged()
    {
        /**
         * Prepare
         */
        final ItemSpecificsTab objectUnderTest = spy(new ItemSpecificsTab(true));

        final ItemSpecificsTablePane tablePane = mock(ItemSpecificsTablePane.class);
        doReturn(tablePane).when(objectUnderTest).createTablePane();

        final ItemSpecificsControlPane controlPane = mock(ItemSpecificsControlPane.class);
        doReturn(controlPane).when(objectUnderTest).createControlPane();

        /**
         * Run method under test
         */
        objectUnderTest.initUi();
        objectUnderTest.userChanged(EMAIL);

        /**
         * Verify
         */
        verify(controlPane).userChanged(EMAIL);
    }

    @Test
    public void testInitUi2()
    {
        /**
         * Prepare
         */
        final ItemSpecificsTab objectUnderTest = spy(new ItemSpecificsTab(true));

        final ItemSpecificsControlPane controlPane = mock(ItemSpecificsControlPane.class);
        doReturn(controlPane).when(objectUnderTest).createControlPane();

        final ItemSpecificsTablePane tablePane = mock(ItemSpecificsTablePane.class);
        doReturn(tablePane).when(objectUnderTest).createTablePane();

        /**
         * Run method under test
         */
        objectUnderTest.initUi();

        /**
         * Verify
         */
        verify(controlPane).setTablePane(tablePane);
    }
}
