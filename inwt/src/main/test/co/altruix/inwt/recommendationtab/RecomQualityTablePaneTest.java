package co.altruix.inwt.recommendationtab;

import com.vaadin.ui.Table;
import org.junit.Test;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class RecomQualityTablePaneTest {
    @Test
    public void testTableIsAdded()
    {
        /**
         * Prepare
         */
        final RecomQualityTablePane objectUnderTest = spy(new RecomQualityTablePane(true));

        /**
         * Run method under test
         */
        objectUnderTest.initUi();

        /**
         * Verify
         */
        verify(objectUnderTest).addComponent(isA(Table.class));
    }
}
