package co.altruix.inwt.recommendationtab;

import com.vaadin.ui.Button;
import com.vaadin.ui.TextArea;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.mock;

public class NewRecommendatonBatchPanelTest {
    public static final List<String> EXPECTED_RESULT = Arrays.asList(new String[]{"A", "B", "C"});

    @Test
    public void testTextToLineList()
    {
        final NewRecommendatonBatchPanel objectUnderTest = new NewRecommendatonBatchPanel();

        assertThat(objectUnderTest.textToLineList("A\nB\nC")).isEqualTo(EXPECTED_RESULT);
        assertThat(objectUnderTest.textToLineList("A\rB\rC")).isEqualTo(EXPECTED_RESULT);
        assertThat(objectUnderTest.textToLineList("A\r\nB\r\nC")).isEqualTo(EXPECTED_RESULT);
    }

    @Test
    public void testSubmitButtonIsAdded()
    {
        /**
         * Prepare
         */
        final NewRecommendatonBatchPanel objectUnderTest = spy(new NewRecommendatonBatchPanel
                (true));

        /**
         * Run method under test
         */
        objectUnderTest.initUi();

        /**
         * Verify
         */
        verify(objectUnderTest).addComponent(isA(Button.class));
    }

    @Test
    public void testAddingOfValueChangeListener()
    {
        /**
         * Prepare
         */
        final NewRecommendatonBatchPanel objectUnderTest = spy(new NewRecommendatonBatchPanel
                (true));
        final TextArea textArea = mock(TextArea.class);

        doReturn(textArea).when(objectUnderTest).createTextArea();

        /**
         * Run method under test
         */
        objectUnderTest.initUi();


        /**
         * Verify
         */
        verify(textArea).addListener(isA(UrlTextAreaValueChangeListener.class));
    }
}
