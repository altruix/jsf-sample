package co.altruix.inwt.recommendationtab.itemspecificstab;

import co.altruix.inwt.messages.GetAspectStatisticsResponse;
import co.altruix.inwt.model.EbayAspectStatistic;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.ui.Table;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static co.altruix.inwt.recommendationtab.itemspecificstab.ItemSpecificsTablePane.ASPECT_NAME;
import static co.altruix.inwt.recommendationtab.itemspecificstab.ItemSpecificsTablePane.ASPECT_VALUE;
import static co.altruix.inwt.recommendationtab.itemspecificstab.ItemSpecificsTablePane.NUMBER_OF_DISLIKED_IMAGES;
import static co.altruix.inwt.recommendationtab.itemspecificstab.ItemSpecificsTablePane.NUMBER_OF_LIKED_IMAGES;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ItemSpecificsTablePaneTest {

    @Test
    public void testInitUi()
    {
        /**
         * Prepare
         */
        final ItemSpecificsTablePane objectUnderTest = spy(new ItemSpecificsTablePane(true));

        final Table statisticsTable = mock(Table.class);
        doReturn(statisticsTable).when(objectUnderTest).createStatisticsTable();

        final BeanContainer<String,EbayAspectStatistic> statisticsData = mock(BeanContainer.class);
        doReturn(statisticsData).when(objectUnderTest).createStatisticsData();

        /**
         * Run method under test
         */
        objectUnderTest.initUi();

        /**
         * Verify
         */
        verify(statisticsData).setBeanIdProperty(ASPECT_VALUE);
        verify(statisticsTable).setImmediate(true);
        verify(objectUnderTest).addComponent(statisticsTable);
    }

    @Test
    public void testDisplayData()
    {
        /**
         * Prepare
         */
        final ItemSpecificsTablePane objectUnderTest = spy(new ItemSpecificsTablePane(true));

        final Table statisticsTable = mock(Table.class);
        doReturn(statisticsTable).when(objectUnderTest).createStatisticsTable();

        final BeanContainer<String,EbayAspectStatistic> statisticsData = mock(BeanContainer.class);
        doReturn(statisticsData).when(objectUnderTest).createStatisticsData();

        final EbayAspectStatistic stat1 = mock(EbayAspectStatistic.class);
        final EbayAspectStatistic stat2 = mock(EbayAspectStatistic.class);

        final List<EbayAspectStatistic> stats = new LinkedList<EbayAspectStatistic>();
        stats.add(stat1);
        stats.add(stat2);

        final GetAspectStatisticsResponse response = mock(GetAspectStatisticsResponse.class);
        when(response.getStatistics()).thenReturn(stats);

        objectUnderTest.initUi();

        /**
         * Run method under test
         */
        objectUnderTest.displayData(response);

        /**
         * Verify
         */
        verify(statisticsData).removeAllItems();
        verify(statisticsData).addBean(stat1);
        verify(statisticsData).addBean(stat2);
        verify(statisticsTable).requestRepaint();
    }

    @Test
    public void testCreateStatisticsTable()
    {
        /**
         * Prepare
         */
        final ItemSpecificsTablePane objectUnderTest = new ItemSpecificsTablePane(true);
        objectUnderTest.initUi();
        /**
         * Run method under test
         */
        final Table statisticsTable = objectUnderTest.createStatisticsTable();

        /**
         * Verify
         */
        assertThat(statisticsTable.getVisibleColumns()).isEqualTo(new
                Object[]{ASPECT_NAME, ASPECT_VALUE, NUMBER_OF_LIKED_IMAGES,
                NUMBER_OF_DISLIKED_IMAGES});
    }
}
