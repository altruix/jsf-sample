package co.altruix.inwt.recommendationtab;

import co.altruix.inwt.model.RecomQualityInfo;
import org.joda.time.DateTime;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RecomQualityDayColumnGeneratorTest {
    @Test
    public void test()
    {
        final RecomQualityDayColumnGenerator objectUnderTest = new RecomQualityDayColumnGenerator
                ("");

        final RecomQualityInfo data = mock(RecomQualityInfo.class);

        when(data.getFirstRecomCreationDateTimeUtc()).thenReturn(new DateTime(2014, 11, 16, 16,
                54, 32));

        assertThat(objectUnderTest.mapRecomQualityInfoToValue(data)).isEqualTo("16.11.2014");
    }
}
