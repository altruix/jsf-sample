package co.altruix.inwt.recommendationtab;

import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.SubmitRecommendationBatchRequest;
import co.altruix.inwt.messages.SubmitRecommendationBatchResponse;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

// TODO: Delete this class
public class NewRecommendationBatchWindowTest {

    public static final String PRODUCT_URL_1 = "http://www.ebay.com/itm/New-Nice-Charm-Rhinestone-Pearl-Crystal-Bear-Golden-Chain-Coin-Bracelet-Bangle-/380958804608?pt=Fashion_Jewelry&hash=item58b2ea0a80";
    public static final String PRODUCT_URL_2 = "http://www.ebay.com/itm/Hot-New-Design-Fashion-Charm-Blue-Resin-Statement-Gemstone-Dangle-Stud-Earrings-/400776883607?pt=Fashion_Jewelry&hash=item5d5029ed97";
    public static final String PRODUCT_URL_3 = "http://www.ebay.com/itm/Broche-ancienne-fiole-a-parfum-breloques-/400791505559?pt=LH_DefaultDomain_71&hash=item5d51090a97";
    public static final String RECIPIENT_EMAIL = "recipient@inwt.info";
    public static final String SUBMITTER_EMAIL = "submitter@inwt.info";

    @Test
    public void testSubmitButtonPressed()
    {
        /**
         * Prepare
         */
        final NewRecommendationBatchWindow objectUnderTest = spy(new NewRecommendationBatchWindow
                (RECIPIENT_EMAIL));

        objectUnderTest.addProductUrlTableItem(PRODUCT_URL_1);
        objectUnderTest.addProductUrlTableItem(PRODUCT_URL_2);
        objectUnderTest.addProductUrlTableItem(PRODUCT_URL_3);

        final RequestSender<SubmitRecommendationBatchRequest,SubmitRecommendationBatchResponse>
                requestSender = mock(RequestSender.class);

        doReturn(requestSender).when(objectUnderTest).
                createSubmitRecommendationBatchRequestSender();

        doReturn(SUBMITTER_EMAIL).when(objectUnderTest).getSubmitterEmail();

        doAnswer(new Answer() {
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final SubmitRecommendationBatchRequest request =
                        (SubmitRecommendationBatchRequest) invocation.getArguments()[0];

                assertThat(request).isNotNull();
                assertThat(request.getRecommenationRecipientEmail()).isEqualTo(RECIPIENT_EMAIL);
                assertThat(request.getSubmitterEmail()).isEqualTo(SUBMITTER_EMAIL);
                assertThat(request.getProductUrls()).contains(PRODUCT_URL_1, PRODUCT_URL_2,
                        PRODUCT_URL_3);
                assertThat(request.getProductUrls().size()).isEqualTo(3);

                return null;
            }
        }).when(requestSender).sendRequest(isA(SubmitRecommendationBatchRequest.class));

        /**
         * Run method under test
         */
        objectUnderTest.submitButtonPressed();

        /**
         * Verify
         */
        verify(requestSender).sendRequest(isA(SubmitRecommendationBatchRequest.class));
        verify(objectUnderTest).close();
    }

}