package co.altruix.inwt.recommendationtab.itemspecificstab;

import co.altruix.inwt.messages.GetAspectStatisticsResponse;
import co.altruix.inwt.messages.GetAspectsResponse;
import co.altruix.inwt.messages.GetUserProductCategoriesResponse;
import co.altruix.inwt.model.EbayAspect;
import co.altruix.inwt.model.EbayProductCategory;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static co.altruix.inwt.recommendationtab.itemspecificstab.ItemSpecificsControlPane.CATEGORY_NAMES;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ItemSpecificsControlPaneTest {
    public static final String EMAIL = "dp@altruix.co";
    public static final String CATEGORY_ID = "31331";
    public static final String ASPECT = "Rigging";

    @Test
    public void testInitUi1()
    {
        /**
         * Prepare
         */
        final ItemSpecificsControlPane objectUnderTest = spy(new ItemSpecificsControlPane(true));

        doAnswer(new Answer() {
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final Button button = (Button) invocation.getArguments()[0];

                assertThat(button.isEnabled()).isFalse();

                return null;
            }
        }).when(objectUnderTest).addComponent(isA(Button.class));

        /**
         * Run method under test
         */
        objectUnderTest.initUi();

        /**
         * Verify
         */
        verify(objectUnderTest, times(2)).addComponent(isA(ComboBox.class));
        verify(objectUnderTest).addComponent(isA(Button.class));
    }

    @Test
    public void testUserChanged1()
    {
        /**
         * Prepare
         */
        final ItemSpecificsControlPane objectUnderTest = spy(new ItemSpecificsControlPane(true));

        final BeanContainer<String,EbayProductCategory> productCategoryData =
                mock(BeanContainer.class);
        final BeanContainer<String,EbayAspect> aspectData = mock(BeanContainer.class);

        doReturn(productCategoryData).when(objectUnderTest).createProductCategoryData();
        doReturn(aspectData).when(objectUnderTest).createAspectData();

        final Button button = mock(Button.class);

        doReturn(button).when(objectUnderTest).createGetStatisticsButton();

        final ComboBox productCategoryComboBox = mock(ComboBox.class);
        final ComboBox aspectComboBox = mock(ComboBox.class);

        doReturn(productCategoryComboBox).when(objectUnderTest).createProductCategoryComboBox();
        doReturn(aspectComboBox).when(objectUnderTest).createAspectComboBox();

        objectUnderTest.initUi();

        /**
         * Run method under test
         */
        objectUnderTest.userChanged(null);

        /**
         * Verify
         */
        verify(productCategoryData).removeAllItems();
        verify(aspectData).removeAllItems();
        verify(button, times(2)).setEnabled(false);
        verify(productCategoryComboBox).requestRepaint();
        verify(aspectComboBox).requestRepaint();
    }

    @Test
    public void testUserChanged2()
    {
        /**
         * Prepare
         */
        final ItemSpecificsControlPane objectUnderTest = spy(new ItemSpecificsControlPane(true));

        objectUnderTest.initUi();

        final GetUserProductCategoriesResponse getUserProductCategoriesResponse = mock
                (GetUserProductCategoriesResponse.class);
        doReturn(getUserProductCategoriesResponse).when(objectUnderTest)
                .sendGetUserProductCategoriesRequest(EMAIL);

        /**
         * Run method under test
         */
        objectUnderTest.userChanged(EMAIL);

        /**
         * Verify
         */
        verify(objectUnderTest).updateUi(EMAIL);
    }

    @Test
    public void testUpdateUi()
    {
        /**
         * Prepare
         */
        final ItemSpecificsControlPane objectUnderTest = spy(new ItemSpecificsControlPane(true));

        final BeanContainer<String,EbayProductCategory> productCategoryData =
                mock(BeanContainer.class);
        final BeanContainer<String,EbayAspect> aspectData = mock(BeanContainer.class);

        doReturn(productCategoryData).when(objectUnderTest).createProductCategoryData();
        doReturn(aspectData).when(objectUnderTest).createAspectData();

        final EbayProductCategory category1 = mock(EbayProductCategory.class);
        final EbayProductCategory category2 = mock(EbayProductCategory.class);

        final List<EbayProductCategory> categories = new LinkedList<EbayProductCategory>();

        categories.add(category1);
        categories.add(category2);

        final GetUserProductCategoriesResponse response = mock(GetUserProductCategoriesResponse.class);
        when(response.getUserProductCategories()).thenReturn(categories);

        doReturn(response).when(objectUnderTest).sendGetUserProductCategoriesRequest(EMAIL);

        objectUnderTest.initUi();

        /**
         * Run method under test
         */
        objectUnderTest.updateUi(EMAIL);

        /**
         * Verify
         */
        verify(productCategoryData).removeAllItems();
        verify(productCategoryData).addBean(category1);
        verify(productCategoryData).addBean(category2);
        verify(aspectData).removeAllItems();
    }

    @Test
    public void testInitUi2()
    {
        /**
         * Prepare
         */
        final ItemSpecificsControlPane objectUnderTest = spy(new ItemSpecificsControlPane(true));

        final ComboBox productCategoryComboBox = mock(ComboBox.class);

        doReturn(productCategoryComboBox).when(objectUnderTest).createProductCategoryComboBox();

        /**
         * Run method under test
         */
        objectUnderTest.initUi();

        /**
         * Verify
         */
        verify(productCategoryComboBox).addListener(isA(Property.ValueChangeListener.class));
    }

    @Test
    public void testProductCategoryComboBoxValueChanged()
    {
        /**
         * Prepare
         */
        final ItemSpecificsControlPane objectUnderTest = spy(new ItemSpecificsControlPane(true));

        final ComboBox productCategoryComboBox = mock(ComboBox.class);

        when(productCategoryComboBox.getValue()).thenReturn(CATEGORY_ID);

        doReturn(productCategoryComboBox).when(objectUnderTest).createProductCategoryComboBox();

        final EbayAspect aspect1 = createEbayAspect("Aspect 1");
        final EbayAspect aspect2 = createEbayAspect("Aspect 2");

        final GetAspectsResponse aspectsResponse = spy(new GetAspectsResponse());
        aspectsResponse.setAspects(Arrays.asList(new EbayAspect[]{aspect1, aspect2}));

        doReturn(aspectsResponse).when(objectUnderTest).sendAspectsRequest(CATEGORY_ID);

        final BeanContainer<String, EbayAspect> aspectData = mock(BeanContainer.class);

        doReturn(aspectData).when(objectUnderTest).createAspectData();

        final ComboBox aspectComboBox = mock(ComboBox.class);

        doReturn(aspectComboBox).when(objectUnderTest).createAspectComboBox();

        objectUnderTest.initUi();

        /**
         * Run method under test
         */
        objectUnderTest.productCategoryComboBoxValueChanged();

        /**
         * Verify
         */
        verify(aspectData).removeAllItems();
        verify(aspectData).addBean(aspect1);
        verify(aspectData).addBean(aspect2);
        verify(aspectComboBox).requestRepaint();
    }

    protected EbayAspect createEbayAspect(final String aName) {
        final EbayAspect aspect = new EbayAspect();
        aspect.setName(aName);
        aspect.setProductCategory(CATEGORY_ID);
        return aspect;
    }

    @Test
    public void testGetStatisticsButtonPressed()
    {
        /**
         * Prepare
         */
        final ItemSpecificsControlPane objectUnderTest = spy(new ItemSpecificsControlPane(true));

        doReturn(CATEGORY_ID).when(objectUnderTest).getProductCategoryId();
        doReturn(ASPECT).when(objectUnderTest).getAspectName();

        final GetUserProductCategoriesResponse getUserProductCategoriesResponse = mock
                (GetUserProductCategoriesResponse.class);
        doReturn(getUserProductCategoriesResponse).when(objectUnderTest).
                sendGetUserProductCategoriesRequest(EMAIL);

        objectUnderTest.initUi();

        objectUnderTest.userChanged(EMAIL);

        final GetAspectStatisticsResponse response = mock(GetAspectStatisticsResponse.class);

        doReturn(response).when(objectUnderTest).sendGetAspectStatisticsRequest(CATEGORY_ID,
                ASPECT, EMAIL);

        final IItemSpecificsTablePane tablePane = mock(IItemSpecificsTablePane.class);

        objectUnderTest.setTablePane(tablePane);

        /**
         * Run method under test
         */
        objectUnderTest.getStatisticsButtonPressed();

        /**
         * Verify
         */
        verify(tablePane).displayData(response);
    }

    @Test
    public void testCreateProductCategoryComboBox()
    {
        /**
         * Prepare
         */
        final ItemSpecificsControlPane objectUnderTest = new ItemSpecificsControlPane(true);

        /**
         * Run method under test
         */
        final ComboBox actualResult = objectUnderTest.createProductCategoryComboBox();

        /**
         * Verify
         */
        assertThat(actualResult).isNotNull();
        assertThat(actualResult.getItemCaptionPropertyId()).isEqualTo(CATEGORY_NAMES);
    }

    @Test
    public void testProductCategoryComboBoxValueChanged2()
    {
        /**
         * Prepare
         */
        final ItemSpecificsControlPane objectUnderTest = spy(new ItemSpecificsControlPane(true));

        final ComboBox comboBox = mock(ComboBox.class);
        when(comboBox.getValue()).thenReturn("SomeCategoryId");

        doReturn(comboBox).when(objectUnderTest).createProductCategoryComboBox();

        objectUnderTest.initUi();

        final GetAspectsResponse response = mock(GetAspectsResponse.class);
        when(response.getAspects()).thenReturn(new LinkedList<EbayAspect>());

        doReturn(response).when(objectUnderTest).
                sendAspectsRequest("SomeCategoryId");

        /**
         * Run method under test
         */
        objectUnderTest.productCategoryComboBoxValueChanged();

        /**
         * Verify
         */
        verify(objectUnderTest).sendAspectsRequest("SomeCategoryId");
    }

    @Test
    public void testCreateAspectData()
    {
        /**
         * Prepare
         */
        final ItemSpecificsControlPane objectUnderTest = new ItemSpecificsControlPane(true);

        /**
         * Run method under test
         */
        final BeanContainer<String, EbayAspect> actualResult = objectUnderTest.createAspectData();

        /**
         * Verify
         */
        final EbayAspect aspect = new EbayAspect();
        aspect.setName("SomeName");
        actualResult.addBean(aspect);
    }

    @Test
    public void testGetStatsButtonIsEnabledAfterSelectionOfAspect()
    {
        /**
         * Prepare
         */
        final ItemSpecificsControlPane objectUnderTest = spy(new ItemSpecificsControlPane(true));

        final ComboBox aspectComboBox = mock(ComboBox.class);
        doReturn(aspectComboBox).when(objectUnderTest).createAspectComboBox();

        /**
         * Run method under test
         */
        objectUnderTest.initUi();

        /**
         * Verify
         */
        verify(aspectComboBox).addListener(isA(Property.ValueChangeListener.class));
    }

    @Test
    public void testAspectComboBoxValueChanged()
    {
        /**
         * Prepare
         */
        final ItemSpecificsControlPane objectUnderTest = spy(new ItemSpecificsControlPane(true));

        final Button getStatisticsButton = mock(Button.class);
        doReturn(getStatisticsButton).when(objectUnderTest).createGetStatisticsButton();

        objectUnderTest.initUi();

        /**
         * Run method under test
         */
        objectUnderTest.aspectComboBoxValueChanged();

        /**
         * Verify
         */
        verify(getStatisticsButton).setEnabled(false);
        verify(getStatisticsButton).setEnabled(true);
    }
}