package co.altruix.inwt.recommendationtab;

import co.altruix.inwt.recommendationtab.itemspecificstab.ItemSpecificsTab;
import org.junit.Test;

import static co.altruix.inwt.recommendationtab.UserTabSheet.PAST_RECOMMENDATION_RESULTS;
import static co.altruix.inwt.recommendationtab.UserTabSheet.RECOM_QUALITY;
import static co.altruix.inwt.recommendationtab.UserTabSheet.PRODUCT_CATEGORIES;
import static co.altruix.inwt.recommendationtab.UserTabSheet.ITEM_SPECIFICS;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class UserTabSheetTest {
    @Test
    public void testRecomQualityTabIsAdded()
    {
        /**
         * Prepare
         */
        final IUserComboBoxListener userComboBoxListener = mock(IUserComboBoxListener.class);
        final UserTabSheet objectUnderTest = spy(new UserTabSheet(userComboBoxListener, true));

        /**
         * Run method under test
         */
        objectUnderTest.initUi(userComboBoxListener);

        /**
         * Verify
         */
        verify(objectUnderTest).addTab(isA(PastImagesTab.class), eq(PAST_RECOMMENDATION_RESULTS));
        verify(userComboBoxListener).addUserChangeSelectionListener(isA(PastImagesTab.class));

        verify(objectUnderTest).addTab(isA(RecomQualityTab.class), eq(RECOM_QUALITY));
        verify(userComboBoxListener).addUserChangeSelectionListener(isA(RecomQualityTab.class));

        verify(objectUnderTest).addTab(isA(ProductCategoriesTab.class), eq(PRODUCT_CATEGORIES));
        verify(userComboBoxListener).addUserChangeSelectionListener(isA(ProductCategoriesTab.class));

        verify(objectUnderTest).addTab(isA(ItemSpecificsTab.class), eq(ITEM_SPECIFICS));
        verify(userComboBoxListener).addUserChangeSelectionListener(isA(ItemSpecificsTab.class));
    }
}
