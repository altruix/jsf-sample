package co.altruix.inwt.recommendationtab;

import org.junit.Test;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class UserPanelTest {
    @Test
    public void testInitGuiAddsTabSheet()
    {
        /**
         * Prepare
         */
        final UserPanel objectUnderTest = spy(new UserPanel(true));

        doNothing().when(objectUnderTest).fillUsersComboBox();

        /**
         * Run method under test
         */
        objectUnderTest.initUi();

        /**
         * Verify
         */
        verify(objectUnderTest).addComponent(isA(UserTabSheet.class));
    }
}
