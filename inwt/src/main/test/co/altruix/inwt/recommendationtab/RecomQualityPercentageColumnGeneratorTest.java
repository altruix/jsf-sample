package co.altruix.inwt.recommendationtab;

import co.altruix.inwt.model.RecomQualityInfo;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RecomQualityPercentageColumnGeneratorTest {
    @Test
    public void test()
    {
        final RecomQualityPercentageColumnGenerator objectUnderTest = new
                RecomQualityPercentageColumnGenerator("someColumn");

        final RecomQualityInfo dataTuple = mock(RecomQualityInfo.class);
        when(dataTuple.getNumberOfHotRecoms()).thenReturn(6);
        when(dataTuple.getNumberOfRecoms()).thenReturn(10);

        assertThat(objectUnderTest.mapRecomQualityInfoToValue(dataTuple)).isEqualTo("60%");
    }
}
