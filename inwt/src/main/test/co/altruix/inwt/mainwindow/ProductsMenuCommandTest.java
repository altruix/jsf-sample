package co.altruix.inwt.mainwindow;

import co.altruix.inwt.productstab.ProductTab;
import com.vaadin.ui.MenuBar;
import org.junit.Test;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ProductsMenuCommandTest {
    @Test
    public void test()
    {
        /**
         * Prepare
         */
        final IInwtWindow parentWindow = mock(IInwtWindow.class);
        final ProductsMenuCommand objectUnderTest = spy(new ProductsMenuCommand(null,
                parentWindow));
        final ProductTab productTab = mock(ProductTab.class);

        doReturn(productTab).when(objectUnderTest).createProductTab();

        /**
         * Run method under test
         */
        objectUnderTest.menuSelected(mock(MenuBar.MenuItem.class));

        /**
         * Verify
         */
        verify(parentWindow).setMainArea(productTab);
    }
}
