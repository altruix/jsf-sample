package co.altruix.inwt.mainwindow;

import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Panel;
import org.junit.Test;

import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.mock;

public class InwtWindowTest {
    @Test
    public void testInitGuiSetMenuBar()
    {
        /**
         * Prepare
         */
        final InwtWindow objectUnderTest = spy(new InwtWindow());

        /**
         * Run method under test
         */
        objectUnderTest.initUI();

        /**
         * Verify
         */
        verify(objectUnderTest).addComponent(isA(MenuBar.class));
    }

    @Test
    public void testMenuContainsProductsAndUserItems()
    {
        /**
         * Prepare
         */
        final InwtWindow objectUnderTest = spy(new InwtWindow());

        final MenuBar menuBar = mock(MenuBar.class);

        doReturn(menuBar).when(objectUnderTest).createMenuBar();

        /**
         * Run method under test
         */
        objectUnderTest.initUI();

        /**
         * Verify
         */
        verify(menuBar).addItem(eq(InwtWindow.MENU_PRODUCTS),isA(ProductsMenuCommand.class));
        verify(menuBar).addItem(eq(InwtWindow.MENU_USERS),isA(UsersMenuCommand.class));
    }

    @Test
    public void testInitUI()
    {
        /**
         * Prepare
         */
        final InwtWindow objectUnderTest = spy(new InwtWindow());

        final MenuBar menuBar = mock(MenuBar.class);

        doReturn(menuBar).when(objectUnderTest).createMenuBar();

        final Panel panel = mock(Panel.class);

        doReturn(panel).when(objectUnderTest).createPanel();

        /**
         * Run method under test
         */
        objectUnderTest.initUI();

        /**
         * Verify
         */
        verify(objectUnderTest).addComponent(panel);
    }
}
