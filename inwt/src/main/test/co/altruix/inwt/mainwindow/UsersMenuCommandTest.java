package co.altruix.inwt.mainwindow;

import co.altruix.inwt.recommendationtab.UserPanel;
import com.vaadin.ui.MenuBar;
import org.junit.Test;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class UsersMenuCommandTest {
    @Test
    public void test()
    {
        /**
         * Prepare
         */
        final IInwtWindow parentWindow = mock(IInwtWindow.class);
        final UsersMenuCommand objectUnderTest = spy(new UsersMenuCommand(parentWindow));
        final UserPanel recommendationTab  = mock(UserPanel.class);

        doReturn(recommendationTab).when(objectUnderTest).createRecommendationTab();

        /**
         * Run method under test
         */
        objectUnderTest.menuSelected(mock(MenuBar.MenuItem.class));

        /**
         * Verify
         */
        verify(parentWindow).setMainArea(recommendationTab);
    }
}
