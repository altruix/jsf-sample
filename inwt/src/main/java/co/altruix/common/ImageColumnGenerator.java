package co.altruix.common;

import co.altruix.InwtApplication;
import com.vaadin.data.util.BeanItem;
import com.vaadin.terminal.StreamResource;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public abstract class ImageColumnGenerator<C> {
    private static final Logger LOGGER = LoggerFactory.getLogger(
            ImageColumnGenerator.class);
    public final static String IMAGE_FIELD = "image";

    public Object generateCell(final Table aTable, final Object aItemId, final Object aColumnId) {
        if (!IMAGE_FIELD.equals(aColumnId)) {
            return null;
        }
        final BeanItem<C> beanItem = (BeanItem<C>)aTable.getItem(aItemId);
        final byte[] imageData = getImageData(beanItem);
        final String fileName = getFileName(beanItem);

        final StreamResource streamResource = new StreamResource(new StreamResource.StreamSource() {
            public InputStream getStream() {
                return new ByteArrayInputStream(imageData);
            }
        }, fileName, InwtApplication.getInstance());

        LOGGER.debug("imageResource: " + streamResource);
        final Embedded embedded = new Embedded("", streamResource);
        final HorizontalLayout layout = new HorizontalLayout();

        layout.addComponent(embedded);

        return layout;
    }

    protected abstract String getFileName(final BeanItem<C> aBeanItem);

    protected abstract byte[] getImageData(final BeanItem<C> aBeanItem);
}
