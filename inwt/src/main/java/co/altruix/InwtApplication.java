/*
 * Copyright 2009 IT Mill Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package co.altruix;

import co.altruix.inwt.mainwindow.InwtWindow;
import co.altruix.inwt.LoginWindow;
import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.LoginRequest;
import co.altruix.inwt.messages.LoginResponse;
import com.vaadin.Application;
import com.vaadin.service.ApplicationContext;
import com.vaadin.terminal.gwt.server.WebApplicationContext;
import com.vaadin.ui.Window;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;

/**
 * The Application's "main" class
 */
@SuppressWarnings("serial")
public class InwtApplication extends Application implements ApplicationContext.TransactionListener
{
    private static final Logger LOGGER = LoggerFactory.getLogger(InwtApplication.class);
    private static ThreadLocal<InwtApplication> currentApplication =
            new ThreadLocal<InwtApplication> ();

    private String currentUser = null;

    private Window window;

    public static String getServerUrl() {
        final ServletContext servletContext =
                ((WebApplicationContext) getInstance().getContext()).
                        getHttpSession().getServletContext();
        return servletContext.getInitParameter(InwtWindow.INWT_SERVER_URL);
    }

    @Override
    public void init()
    {
        getContext ().addTransactionListener ( this );
        setMainWindow ( new LoginWindow() );
    }

    public void authenticate( String login, String password) throws Exception
    {
        if (credentialsCorrect(login, password))
        {
            currentUser = login;
            loadProtectedResources();
            return;
        }

        throw new Exception("Login failed!");
    }

    private boolean credentialsCorrect(final String aLogin, final String aPassword) {
        final LoginRequest request = new LoginRequest();

        request.setEmail(aLogin);
        request.setPassword(aPassword);

        final RequestSender<LoginRequest,LoginResponse> requestSender = new
                RequestSender<LoginRequest, LoginResponse>(LoginResponse.class, "Login",
                InwtApplication.getServerUrl());

        final LoginResponse response = requestSender.sendRequest(request);

        return response.isSuccess();
    }

    private void loadProtectedResources ()
    {
        setMainWindow ( new InwtWindow());
    }

    public void transactionStart(final Application aApplication, final Object o) {
        if ( aApplication == InwtApplication.this )
        {
            currentApplication.set ( this );
        }
    }

    public void transactionEnd(final Application aApplication, final Object o) {
        if ( aApplication == InwtApplication.this )
        {
            currentApplication.set ( null );
            currentApplication.remove ();
        }
    }

    public static InwtApplication getInstance()
    {
        return currentApplication.get ();


    }

    public String getCurrentUser() {
        return currentUser;
    }
}
