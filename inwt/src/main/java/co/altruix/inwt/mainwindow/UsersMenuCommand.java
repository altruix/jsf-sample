package co.altruix.inwt.mainwindow;

import co.altruix.inwt.recommendationtab.UserPanel;
import com.vaadin.ui.MenuBar;

public class UsersMenuCommand implements MenuBar.Command {
    private final IInwtWindow window;

    public UsersMenuCommand(final IInwtWindow aWindow) {
        window = aWindow;
    }

    public void menuSelected(final MenuBar.MenuItem aMenuItem) {
        final UserPanel panel = createRecommendationTab();
        window.setMainArea(panel);
    }

    protected UserPanel createRecommendationTab() {
        return new UserPanel();
    }
}
