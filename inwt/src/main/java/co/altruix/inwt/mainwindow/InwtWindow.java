package co.altruix.inwt.mainwindow;

import com.vaadin.ui.Layout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Window;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InwtWindow extends Window implements IInwtWindow {
    private static final Logger LOGGER = LoggerFactory.getLogger(InwtWindow.class);
	private static final long serialVersionUID = 1L;
    public static final String INWT_SERVER_URL = "inwt:server-url";
    public static final String MENU_PRODUCTS = "Products";
    public static final String MENU_USERS = "Users";

    private Panel contentArea;

    private Layout curComponent;

    public InwtWindow()
    {
        super("INWT");
        initUI();
    }

    protected void initUI() {
        final MenuBar menuBar = createMenuBar();

        menuBar.addItem(MENU_PRODUCTS,
                new ProductsMenuCommand(this, this));
        menuBar.addItem(MENU_USERS,
                new UsersMenuCommand(this));

        addComponent(menuBar);
        setImmediate(true);

        contentArea = createPanel();

        addComponent(contentArea);
    }

    protected Panel createPanel() {
        return new Panel();
    }

    protected MenuBar createMenuBar() {
        return new MenuBar();
    }

    public void setMainArea(final Layout aComponent)
    {
        contentArea.setContent(aComponent);
    }
}
