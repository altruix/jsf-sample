package co.altruix.inwt.mainwindow;

import co.altruix.inwt.productstab.ProductTab;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Window;

public class ProductsMenuCommand implements MenuBar.Command {
    private final Window mainWindow;
    private final IInwtWindow parentWindow;

    public ProductsMenuCommand(final Window aMainWindow, final IInwtWindow aParentWindow) {
        mainWindow = aMainWindow;
        parentWindow = aParentWindow;
    }


    public void menuSelected(final MenuBar.MenuItem aMenuItem) {
        final ProductTab panel = createProductTab();

        parentWindow.setMainArea(panel);
    }

    protected ProductTab createProductTab() {
        return new ProductTab(mainWindow);
    }
}
