package co.altruix.inwt.mainwindow;

import com.vaadin.ui.Layout;

public interface IInwtWindow {

    void setMainArea(Layout aComponent);
}
