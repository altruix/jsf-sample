package co.altruix.inwt.recommendationtab;

import co.altruix.common.ImageColumnGenerator;
import co.altruix.inwt.model.UserProductImageRating;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Table;

public class UserProductImageRatingImageColumnGenerator extends
        ImageColumnGenerator<UserProductImageRating> implements Table.ColumnGenerator {
    @Override
    protected String getFileName(final BeanItem<UserProductImageRating> aBeanItem) {
        return aBeanItem.getBean().getImage().getFileName();
    }

    @Override
    protected byte[] getImageData(final BeanItem<UserProductImageRating> aBeanItem) {
        return aBeanItem.getBean().getImage().getImageData();
    }
}
