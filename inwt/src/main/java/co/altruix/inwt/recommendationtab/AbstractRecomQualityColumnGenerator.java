package co.altruix.inwt.recommendationtab;

import co.altruix.inwt.model.RecomQualityInfo;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Table;

public abstract class AbstractRecomQualityColumnGenerator implements Table.ColumnGenerator {
    private final String columnName;
    public AbstractRecomQualityColumnGenerator(final String aColumnName)
    {
        columnName = aColumnName;
    }
    public Object generateCell(final Table aTable, final Object aItemId, final Object aColumnId) {
        if (!columnName.equals(aColumnId)) {
            return null;
        }
        final BeanItem<RecomQualityInfo> beanItem =
                (BeanItem<RecomQualityInfo>)aTable.getItem(aItemId);

        return mapRecomQualityInfoToValue(beanItem.getBean());
    }

    protected abstract Object mapRecomQualityInfoToValue(final RecomQualityInfo aRecomQualityInfo);
}
