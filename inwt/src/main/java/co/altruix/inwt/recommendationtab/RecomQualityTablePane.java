package co.altruix.inwt.recommendationtab;

import co.altruix.InwtApplication;
import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.GetRecomQualityRequest;
import co.altruix.inwt.messages.GetRecomQualityResponse;
import co.altruix.inwt.model.RecomQualityInfo;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

public class RecomQualityTablePane extends VerticalLayout implements IUserChangeSelectionListener {
    public static final String DAY = "Day";
    public static final String RECOM_QUALITY = "Recom quality";
    private Table recomQualityTable;
    private BeanContainer<byte[],RecomQualityInfo> recomQualityData;

    RecomQualityTablePane(final boolean aForTestingPurposesOnly)
    {
        super();
    }

    public RecomQualityTablePane()
    {
        super();
        initUi();
    }

    protected void initUi() {
        recomQualityData = new BeanContainer<byte[], RecomQualityInfo>(RecomQualityInfo.class);
        recomQualityData.setBeanIdProperty("firstRecomCreationDateTimeUtc");
        recomQualityTable = new Table("Recommendation quality", recomQualityData);

        recomQualityTable.setImmediate(true);

        recomQualityTable.addGeneratedColumn(DAY,
                new RecomQualityDayColumnGenerator(DAY));
        recomQualityTable.addGeneratedColumn(RECOM_QUALITY,
                new RecomQualityPercentageColumnGenerator(RECOM_QUALITY));

        addComponent(recomQualityTable);
    }

    public void userChanged(final String aNewUserEmail) {
        final GetRecomQualityRequest request = new GetRecomQualityRequest();
        request.setUserEmail(aNewUserEmail);
        request.setRecommenderEmail(InwtApplication.getInstance().getCurrentUser());

        final RequestSender<GetRecomQualityRequest,GetRecomQualityResponse> sender = new
                RequestSender<GetRecomQualityRequest,
                        GetRecomQualityResponse>(GetRecomQualityResponse.class,
                GetRecomQualityRequest.SERVICE_NAME,
                InwtApplication.getServerUrl());

        final GetRecomQualityResponse response = sender.sendRequest(request);

        recomQualityData.removeAllItems();

        for (final RecomQualityInfo curQualityInfo : response.getRecomQualityInfos()) {
            recomQualityData.addBean(curQualityInfo);
        }
        recomQualityTable.requestRepaint();
    }
}
