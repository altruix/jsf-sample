package co.altruix.inwt.recommendationtab.itemspecificstab;

import co.altruix.inwt.messages.GetAspectStatisticsResponse;
import co.altruix.inwt.model.EbayAspectStatistic;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;

public class ItemSpecificsTablePane extends HorizontalLayout implements IItemSpecificsTablePane {
    public static final String ASPECT_NAME = "aspectName";
    public static final String ASPECT_VALUE = "value";
    public static final String NUMBER_OF_LIKED_IMAGES = "numberOfLikedImages";
    public static final String NUMBER_OF_DISLIKED_IMAGES = "numberOfDislikedImages";
    private Table statisticsTable;
    private BeanContainer<String,EbayAspectStatistic> statisticsData;

    ItemSpecificsTablePane(final boolean aForTestingPurposesOnly)
    {
        super();
    }

    public ItemSpecificsTablePane()
    {
        super();
        initUi();
    }

    protected void initUi() {
        statisticsData = createStatisticsData();
        statisticsData.setBeanIdProperty(ASPECT_VALUE);

        statisticsTable = createStatisticsTable();
        statisticsTable.setImmediate(true);

        addComponent(statisticsTable);
    }

    protected Table createStatisticsTable() {
        final Table table = new Table("Aspect statistics", statisticsData);

        table.setVisibleColumns(new Object[]{ASPECT_NAME, ASPECT_VALUE, NUMBER_OF_LIKED_IMAGES,
                NUMBER_OF_DISLIKED_IMAGES});

        return table;
    }

    protected BeanContainer<String, EbayAspectStatistic> createStatisticsData() {
        return new BeanContainer<String, EbayAspectStatistic>(EbayAspectStatistic.class);
    }

    public void displayData(final GetAspectStatisticsResponse aData) {
        statisticsData.removeAllItems();

        for (final EbayAspectStatistic curStatistic : aData.getStatistics())
        {
            statisticsData.addBean(curStatistic);
        }

        statisticsTable.requestRepaint();
    }
}
