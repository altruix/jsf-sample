package co.altruix.inwt.recommendationtab;

import co.altruix.InwtApplication;
import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.GetLikedProductCategoriesRequest;
import co.altruix.inwt.messages.GetLikedProductCategoriesResponse;
import co.altruix.inwt.model.EbayProductCategoryPopularityInfo;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import java.util.List;

public class ProductCategoriesTablePane extends VerticalLayout implements IUserChangeSelectionListener {
    private Table productCategoriesTable;
    private BeanContainer<byte[],EbayProductCategoryPopularityInfo> productCategoriesData;

    public ProductCategoriesTablePane()
    {
        super();
        initUi();
    }

    private void initUi() {
        productCategoriesData = new BeanContainer<byte[], EbayProductCategoryPopularityInfo>(
                EbayProductCategoryPopularityInfo.class);
        productCategoriesData.setBeanIdProperty("categoryId");
        productCategoriesTable = new Table("Liked product categories", productCategoriesData);

        productCategoriesTable.setImmediate(true);

        addComponent(productCategoriesTable);
    }

    public void userChanged(final String aNewUserEmail) {
        final GetLikedProductCategoriesRequest request = new GetLikedProductCategoriesRequest();
        request.setUserEmail(aNewUserEmail);

        final RequestSender<GetLikedProductCategoriesRequest,GetLikedProductCategoriesResponse> sender = new
                RequestSender<GetLikedProductCategoriesRequest,
                        GetLikedProductCategoriesResponse>(GetLikedProductCategoriesResponse.class,
                GetLikedProductCategoriesRequest.SERVICE_NAME,
                InwtApplication.getServerUrl());

        final GetLikedProductCategoriesResponse response = sender.sendRequest(request);

        productCategoriesData.removeAllItems();

        final List<EbayProductCategoryPopularityInfo> popularityData = response.getPopularityData();

        for (final EbayProductCategoryPopularityInfo curPopularityInfo : popularityData) {
            productCategoriesData.addBean(curPopularityInfo);
        }
        productCategoriesTable.requestRepaint();
    }
}
