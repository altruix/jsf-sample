package co.altruix.inwt.recommendationtab;

import com.vaadin.data.Property;
import com.vaadin.ui.Button;
import com.vaadin.ui.TextArea;
import org.apache.commons.lang3.StringUtils;

public class UrlTextAreaValueChangeListener implements Property.ValueChangeListener {
    private final TextArea textArea;
    private final Button button;

    public UrlTextAreaValueChangeListener(final TextArea aTextArea, final Button aButton) {
        textArea = aTextArea;
        button = aButton;
    }

    public void valueChange(final Property.ValueChangeEvent aValueChangeEvent) {
        final boolean textAreaValueBlank = StringUtils.isBlank((String)textArea.getValue());

        button.setEnabled(!textAreaValueBlank);
    }
}
