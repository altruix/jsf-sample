package co.altruix.inwt.recommendationtab;

import co.altruix.inwt.model.RecomQualityInfo;

import java.text.MessageFormat;

public class RecomQualityDayColumnGenerator  extends AbstractRecomQualityColumnGenerator {
    public RecomQualityDayColumnGenerator(final String aColumnName) {
        super(aColumnName);
    }

    @Override
    protected Object mapRecomQualityInfoToValue(final RecomQualityInfo aRecomQualityInfo) {
        return MessageFormat.format("{0,date,dd.MM.yyyy}",
                aRecomQualityInfo.getFirstRecomCreationDateTimeUtc().toDate());
    }
}
