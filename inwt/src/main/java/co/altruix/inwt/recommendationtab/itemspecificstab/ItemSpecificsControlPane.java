package co.altruix.inwt.recommendationtab.itemspecificstab;

import co.altruix.InwtApplication;
import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.GetAspectStatisticsRequest;
import co.altruix.inwt.messages.GetAspectStatisticsResponse;
import co.altruix.inwt.messages.GetAspectsRequest;
import co.altruix.inwt.messages.GetAspectsResponse;
import co.altruix.inwt.messages.GetUserProductCategoriesRequest;
import co.altruix.inwt.messages.GetUserProductCategoriesResponse;
import co.altruix.inwt.model.EbayAspect;
import co.altruix.inwt.model.EbayProductCategory;
import co.altruix.inwt.recommendationtab.IUserChangeSelectionListener;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ItemSpecificsControlPane extends HorizontalLayout implements
        IUserChangeSelectionListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(ItemSpecificsControlPane.class);
    public static final String CATEGORY_NAMES = "categoryNames";
    private ComboBox productCategoryComboBox;
    private BeanContainer<String, EbayProductCategory> productCategoryData;
    private BeanContainer<String, EbayAspect> aspectData;
    private ComboBox aspectComboBox;
    private Button getStatisticsButton;
    private String userEmail;
    private IItemSpecificsTablePane tablePane;

    ItemSpecificsControlPane(boolean aForTestingPurposesOnly) {
        super();
    }

    public ItemSpecificsControlPane() {
        super();
        initUi();
    }

    protected void initUi() {
        productCategoryData = createProductCategoryData();
        aspectData = createAspectData();

        productCategoryComboBox = createProductCategoryComboBox();
        productCategoryComboBox.setImmediate(true);

        productCategoryComboBox.addListener(new Property.ValueChangeListener() {
            public void valueChange(final Property.ValueChangeEvent aValueChangeEvent) {
                productCategoryComboBoxValueChanged();
            }
        });

        aspectComboBox = createAspectComboBox();
        aspectComboBox.setImmediate(true);

        aspectComboBox.addListener(new Property.ValueChangeListener() {
            public void valueChange(final Property.ValueChangeEvent aValueChangeEvent) {
                aspectComboBoxValueChanged();
            }
        });

        getStatisticsButton = createGetStatisticsButton();
        getStatisticsButton.setImmediate(true);
        getStatisticsButton.setEnabled(false);
        getStatisticsButton.addListener(new Button.ClickListener() {
            public void buttonClick(final Button.ClickEvent aClickEvent) {
                getStatisticsButtonPressed();
            }
        });

        addComponent(productCategoryComboBox);
        addComponent(aspectComboBox);
        addComponent(getStatisticsButton);
    }

    protected void aspectComboBoxValueChanged() {
        getStatisticsButton.setEnabled(true);
    }

    protected void productCategoryComboBoxValueChanged() {
        final String categoryId = (String) productCategoryComboBox.getValue();

        final GetAspectsResponse aspectsResponse =
                sendAspectsRequest(categoryId);

        aspectData.removeAllItems();

        for (final EbayAspect curAspect : aspectsResponse.getAspects())
        {
            aspectData.addBean(curAspect);
        }
        aspectComboBox.requestRepaint();
    }

    protected GetAspectsResponse sendAspectsRequest(final String aCategoryId) {
        final GetAspectsRequest request = new GetAspectsRequest();
        request.setUserEmail(userEmail);
        request.setProductCategoryId(aCategoryId);

        final RequestSender<GetAspectsRequest, GetAspectsResponse> requestSender = new
                RequestSender(GetAspectsResponse.class, GetAspectsRequest.SERVICE_NAME,
                        InwtApplication.getServerUrl());

        return requestSender.sendRequest(request);
    }

    protected ComboBox createAspectComboBox() {
        return new ComboBox("Aspect", aspectData);
    }

    protected ComboBox createProductCategoryComboBox() {
        final ComboBox comboBox = new ComboBox("Product category", productCategoryData);

        comboBox.setItemCaptionPropertyId(CATEGORY_NAMES);

        return comboBox;
    }

    protected Button createGetStatisticsButton() {
        return new Button("Get stats");
    }

    protected BeanContainer<String, EbayAspect> createAspectData() {
        final BeanContainer<String, EbayAspect> container = new BeanContainer<String, EbayAspect>(
                EbayAspect.class);

        container.setBeanIdProperty("name");

        return container;
    }

    protected BeanContainer<String, EbayProductCategory> createProductCategoryData() {
        final BeanContainer<String, EbayProductCategory> result = new
                BeanContainer<String, EbayProductCategory>(EbayProductCategory.class);

        result.setBeanIdProperty("categoryId");

        return result;
    }

    protected void getStatisticsButtonPressed() {
        final String productCategoryId = getProductCategoryId();
        final String aspectName = getAspectName();
        
        final GetAspectStatisticsResponse response = sendGetAspectStatisticsRequest
                (productCategoryId, aspectName, userEmail);

        tablePane.displayData(response);
    }

    protected String getAspectName() {
        final String aspectId = (String) aspectComboBox.getValue();
        final EbayAspect aspect = aspectData.getItem(aspectId).getBean();
        return aspect.getName();
    }

    protected String getProductCategoryId() {

        final EbayProductCategory selectedCategory =
                productCategoryData.getItem(productCategoryComboBox.getValue()).getBean();
        return selectedCategory.getCategoryId();
    }

    protected GetAspectStatisticsResponse sendGetAspectStatisticsRequest(
            final String aProductCategoryId,
            final String aAspectName, final String aUserEmail) {
        final GetAspectStatisticsRequest request = new GetAspectStatisticsRequest();

        request.setProductCategoryId(aProductCategoryId);
        request.setUserEmail(aUserEmail);
        request.setAspectName(aAspectName);

        final RequestSender<GetAspectStatisticsRequest, GetAspectStatisticsResponse>
                requestSender = new RequestSender
                (GetAspectStatisticsResponse.class,
                        GetAspectStatisticsRequest.SERVICE_NAME,
                        InwtApplication.getServerUrl());


        return requestSender.sendRequest(request);
    }

    public void userChanged(final String aNewUserEmail) {
        if (StringUtils.isNotBlank(aNewUserEmail)) {
            updateUi(aNewUserEmail);
        }
        else {
            getStatisticsButton.setEnabled(false);
            productCategoryData.removeAllItems();
            aspectData.removeAllItems();
            aspectComboBox.removeAllItems();

            productCategoryComboBox.requestRepaint();
            aspectComboBox.requestRepaint();
        }
    }

    protected void updateUi(final String aNewUserEmail) {
        final GetUserProductCategoriesResponse categoriesResponse =
                sendGetUserProductCategoriesRequest(aNewUserEmail);

        productCategoryData.removeAllItems();

        for (final EbayProductCategory curCategory : categoriesResponse.getUserProductCategories())
        {
            productCategoryData.addBean(curCategory);
        }

        aspectData.removeAllItems();

        userEmail = aNewUserEmail;
    }

    protected GetUserProductCategoriesResponse sendGetUserProductCategoriesRequest(
            final String aNewUserEmail) {
        final GetUserProductCategoriesRequest request = new GetUserProductCategoriesRequest();
        request.setUserEmail(aNewUserEmail);

        final RequestSender<GetUserProductCategoriesRequest, GetUserProductCategoriesResponse>
                requestSender = new RequestSender
                (GetUserProductCategoriesResponse.class,
                        GetUserProductCategoriesRequest.SERVICE_NAME,
                        InwtApplication.getServerUrl());

        return requestSender.sendRequest(request);
    }

    public void setTablePane(final IItemSpecificsTablePane aTablePane) {
        tablePane = aTablePane;
    }
}
