package co.altruix.inwt.recommendationtab;

import co.altruix.InwtApplication;
import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.SubmitRecommendationBatchRequest;
import co.altruix.inwt.messages.SubmitRecommendationBatchResponse;
import com.vaadin.ui.Button;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;

import java.util.List;

import static co.altruix.InwtApplication.getServerUrl;
import static co.altruix.inwt.messages.SubmitRecommendationBatchRequest.SERVICE_NAME;
import static java.lang.System.lineSeparator;
import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringUtils.split;

public class NewRecommendatonBatchPanel extends Panel implements IUserChangeSelectionListener {
    private TextArea urls;
    private String curUserEmail;

    NewRecommendatonBatchPanel(boolean aForTestingOnly)
    {
        super("New recommendation batch");
    }

    public NewRecommendatonBatchPanel()
    {
        super("New recommendation batch");


        initUi();
    }

    protected void initUi() {
        final Button submitButton = new Button("Submit");

        urls = createTextArea();
        urls.setImmediate(true);
        urls.setWidth("100%");
        urls.addListener(new UrlTextAreaValueChangeListener(urls, submitButton));
        addComponent(urls);


        submitButton.setImmediate(true);
        submitButton.setEnabled(false);
        submitButton.addListener(new Button.ClickListener() {
            public void buttonClick(final Button.ClickEvent aClickEvent) {
                submitReccommendationBatchButtonPressed();
            }
        });

        addComponent(submitButton);
    }

    protected TextArea createTextArea() {
        return new TextArea("Enter URLs of new recom. products here");
    }

    protected String getSubmitterEmail() {
        return InwtApplication.getInstance().getCurrentUser();
    }


    private void submitReccommendationBatchButtonPressed() {
        final SubmitRecommendationBatchRequest request = new SubmitRecommendationBatchRequest();
        request.setRecommenationRecipientEmail(curUserEmail);
        request.setSubmitterEmail(getSubmitterEmail());


        request.setProductUrls(textToLineList((String)urls.getValue()));

        final RequestSender<SubmitRecommendationBatchRequest,SubmitRecommendationBatchResponse>
                requestSender = createSubmitRecommendationBatchRequestSender();

        requestSender.sendRequest(request);

        urls.setValue("");
    }

    protected List<String> textToLineList(final String aUrls) {
        return asList(split(aUrls, lineSeparator()));
    }

    protected RequestSender<SubmitRecommendationBatchRequest, SubmitRecommendationBatchResponse>
    createSubmitRecommendationBatchRequestSender() {
        return new RequestSender<SubmitRecommendationBatchRequest,
                SubmitRecommendationBatchResponse>(SubmitRecommendationBatchResponse.class,
                SERVICE_NAME, getServerUrl());
    }

    public void userChanged(final String aNewUserEmail) {
        curUserEmail = aNewUserEmail;
    }
}
