package co.altruix.inwt.recommendationtab;

import co.altruix.InwtApplication;
import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.SubmitRecommendationBatchRequest;
import co.altruix.inwt.messages.SubmitRecommendationBatchResponse;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

import static co.altruix.InwtApplication.getServerUrl;
import static co.altruix.inwt.messages.SubmitRecommendationBatchRequest.SERVICE_NAME;

// TODO: Delete this class
public class NewRecommendationBatchWindow extends Window {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(NewRecommendationBatchWindow.class);

    private final TextField productUrlTextField;
    private final Table productUrlTable;
    private final String recomRecipientEmail;

    public NewRecommendationBatchWindow(final String aRecomRecipientEmail)
    {
        super();
        recomRecipientEmail = aRecomRecipientEmail;

        final VerticalLayout layout = new VerticalLayout();

        productUrlTable = new Table("Product URLs");
        productUrlTable.addContainerProperty("Product URL", String.class, null);
        productUrlTable.setImmediate(true);
        productUrlTable.setWidth("500px");
        layout.addComponent(productUrlTable);

        final HorizontalLayout addProductUrlPanel = new HorizontalLayout();

        productUrlTextField = new TextField("Enter new product URL here");
        final Button addProductUrlButton = new Button("Add");

        addProductUrlButton.addListener(new Button.ClickListener() {
            public void buttonClick(final Button.ClickEvent aClickEvent) {
                addProductUrlButtonPressed();
            }
        });

        addProductUrlPanel.addComponent(productUrlTextField);
        addProductUrlPanel.addComponent(addProductUrlButton);

        layout.addComponent(addProductUrlPanel);

        final Button submitButton = new Button("Submit batch proposal");
        submitButton.addListener(new Button.ClickListener() {
            public void buttonClick(final Button.ClickEvent aClickEvent) {
                submitButtonPressed();
            }
        });

        layout.addComponent(submitButton);
        layout.setSizeFull();

        addComponent(layout);

        setWidth("500px");
        setHeight("500px");
    }

    protected void submitButtonPressed() {
        final SubmitRecommendationBatchRequest request = new SubmitRecommendationBatchRequest();
        request.setRecommenationRecipientEmail(recomRecipientEmail);
        request.setSubmitterEmail(getSubmitterEmail());
        request.setProductUrls(productTableUrlsToList(productUrlTable));

        final RequestSender<SubmitRecommendationBatchRequest,SubmitRecommendationBatchResponse>
                requestSender = createSubmitRecommendationBatchRequestSender();

        requestSender.sendRequest(request);

        close();
    }

    /**
     * For testing
     */
    @Override
    protected void close() {
        super.close();
    }

    protected String getSubmitterEmail() {
        return InwtApplication.getInstance().getCurrentUser();
    }

    protected RequestSender<SubmitRecommendationBatchRequest, SubmitRecommendationBatchResponse>
        createSubmitRecommendationBatchRequestSender() {
        return new RequestSender<SubmitRecommendationBatchRequest,
        SubmitRecommendationBatchResponse>(SubmitRecommendationBatchResponse.class,
        SERVICE_NAME, getServerUrl());
    }

    private List<String> productTableUrlsToList(final Table aProductUrlTable) {
        final List<String> productTableUrls = new LinkedList<String>();

        for (final Object curItem : aProductUrlTable.getItemIds())
        {
            final String curUrl = (String)curItem;

            productTableUrls.add(curUrl);
        }

        return productTableUrls;
    }

    private void addProductUrlButtonPressed() {
        final String url = (String) productUrlTextField.getValue();

        LOGGER.debug("url: " + url);

        if (StringUtils.isNotEmpty(url) && (!productUrlTable.containsId(url)))
        {
            addProductUrlTableItem(url);
            productUrlTextField.setValue("");

            productUrlTable.requestRepaint();
        }
    }

    protected void addProductUrlTableItem(final String aUrl) {
        productUrlTable.addItem(new Object[]{aUrl}, aUrl);
    }
}
