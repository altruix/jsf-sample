package co.altruix.inwt.recommendationtab;

import co.altruix.InwtApplication;
import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.GetUsersRequest;
import co.altruix.inwt.messages.GetUsersResponse;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.VerticalLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserPanel extends VerticalLayout {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserPanel.class);

    private final ComboBox userComboBox = new ComboBox("Please select the user");

    /**
     * For testing purposes only
     */
    UserPanel(boolean aTest)
    {
        super();
    }

    public UserPanel()
    {
        super();

        initUi();
    }

    protected void initUi() {
        final IUserComboBoxListener listener = new UserComboBoxListener(userComboBox);

        userComboBox.setImmediate(true);
        userComboBox.addListener(listener);

        final UserTabSheet tabSheet = new UserTabSheet(listener);

        fillUsersComboBox();
        addComponent(userComboBox);

        setDebugId("recommendationTab");

        setImmediate(true);

        addComponent(tabSheet);

        final NewRecommendatonBatchPanel newRecommendatonBatchPanel = new
                NewRecommendatonBatchPanel();

        listener.addUserChangeSelectionListener(newRecommendatonBatchPanel);

        addComponent(newRecommendatonBatchPanel);

        // TODO: Test this
    }

    protected void fillUsersComboBox() {
        final GetUsersRequest request = new GetUsersRequest();
        request.setRequesterEmail(InwtApplication.getInstance().getCurrentUser());

        final RequestSender<GetUsersRequest,GetUsersResponse> sender = new
                RequestSender<GetUsersRequest,
                        GetUsersResponse>(GetUsersResponse.class,
                GetUsersRequest.SERVICE_NAME,
                InwtApplication.getServerUrl());

        final GetUsersResponse response = sender.sendRequest(request);

        for (final String curEmail : response.getUserEmails())
        {
            userComboBox.addItem(curEmail);
        }
    }
}
