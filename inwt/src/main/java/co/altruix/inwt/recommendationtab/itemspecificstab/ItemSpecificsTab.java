package co.altruix.inwt.recommendationtab.itemspecificstab;

import co.altruix.inwt.recommendationtab.IUserChangeSelectionListener;
import com.vaadin.ui.VerticalLayout;

public class ItemSpecificsTab extends VerticalLayout implements IUserChangeSelectionListener {
    private ItemSpecificsControlPane controlPane;
    private ItemSpecificsTablePane tablePane;

    ItemSpecificsTab(boolean aForTestingPurposesOnly)
    {
        super();
    }

    public ItemSpecificsTab()
    {
        super();
        initUi();
    }

    protected void initUi() {
        controlPane = createControlPane();
        tablePane = createTablePane();

        controlPane.setTablePane(tablePane);

        addComponent(controlPane);
        addComponent(tablePane);
    }

    protected ItemSpecificsTablePane createTablePane() {
        return new ItemSpecificsTablePane();
    }

    protected ItemSpecificsControlPane createControlPane() {
        return new ItemSpecificsControlPane();
    }


    public void userChanged(final String aNewUserEmail) {
        controlPane.userChanged(aNewUserEmail);
    }
}
