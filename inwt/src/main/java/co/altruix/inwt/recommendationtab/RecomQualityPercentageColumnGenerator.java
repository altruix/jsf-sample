package co.altruix.inwt.recommendationtab;

import co.altruix.inwt.model.RecomQualityInfo;

import java.text.MessageFormat;

public class RecomQualityPercentageColumnGenerator extends AbstractRecomQualityColumnGenerator {

    public RecomQualityPercentageColumnGenerator(final String aColumnName) {
        super(aColumnName);
    }

    @Override
    protected Object mapRecomQualityInfoToValue(final RecomQualityInfo aRecomQualityInfo) {
        final double numberOfHotItems = aRecomQualityInfo.getNumberOfHotRecoms();
        final double totalNumberOfItems = aRecomQualityInfo.getNumberOfRecoms();
        final double percentage = numberOfHotItems / totalNumberOfItems;

        return MessageFormat.format("{0,number,#.##%}", percentage);
    }
}
