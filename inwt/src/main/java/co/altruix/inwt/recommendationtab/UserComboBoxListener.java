package co.altruix.inwt.recommendationtab;

import com.vaadin.data.Property;
import com.vaadin.ui.ComboBox;

import java.util.LinkedList;
import java.util.List;

public class UserComboBoxListener implements IUserComboBoxListener {
    private final ComboBox userComboBox;
    private final List<IUserChangeSelectionListener> userSelectionChangeListeners =
            new LinkedList<IUserChangeSelectionListener>();

    public UserComboBoxListener(final ComboBox aUserComboBox) {
        userComboBox = aUserComboBox;
    }

    public void addUserChangeSelectionListener(final IUserChangeSelectionListener aListener)
    {
        userSelectionChangeListeners.add(aListener);
    }

    public void valueChange(final Property.ValueChangeEvent aValueChangeEvent) {
        final Object uncastNewUserEmail = userComboBox.getValue();

        if (uncastNewUserEmail instanceof String)
        {
            final String newUserEmail = (String) uncastNewUserEmail;

            for (final IUserChangeSelectionListener curListener : userSelectionChangeListeners)
            {
                curListener.userChanged(newUserEmail);
            }
        }
        // TODO: Test this
    }
}
