package co.altruix.inwt.recommendationtab;

import com.vaadin.ui.HorizontalLayout;

public class ProductCategoriesTab extends HorizontalLayout implements IUserChangeSelectionListener {
    private final ProductCategoriesTablePane tablePane = new ProductCategoriesTablePane();

    public ProductCategoriesTab()
    {
        super();
        initUi();
    }

    protected void initUi() {
        addComponent(tablePane);
    }

    public void userChanged(final String aNewUserEmail) {
        tablePane.userChanged(aNewUserEmail);
    }
}
