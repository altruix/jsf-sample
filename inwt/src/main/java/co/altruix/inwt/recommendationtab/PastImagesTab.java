package co.altruix.inwt.recommendationtab;

import co.altruix.InwtApplication;
import co.altruix.common.ImageColumnGenerator;
import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.GetUserImageRatingsRequest;
import co.altruix.inwt.messages.GetUserImageRatingsResponse;
import co.altruix.inwt.model.UserProductImageRating;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import java.util.List;

public class PastImagesTab extends VerticalLayout implements IUserChangeSelectionListener {
    private Table recomImagesTable;
    private BeanContainer<byte[],UserProductImageRating> recomImagesData;

    public PastImagesTab()
    {
        super();

        recomImagesData = new BeanContainer<byte[],
                UserProductImageRating>(UserProductImageRating.class);
        recomImagesData.setBeanIdProperty("image");

        recomImagesTable = new Table("Past recommendation results", recomImagesData);
        recomImagesTable.addGeneratedColumn(ImageColumnGenerator.IMAGE_FIELD,
                new UserProductImageRatingImageColumnGenerator());
        recomImagesTable.setImmediate(true);
        recomImagesTable.setVisibleColumns(new Object[]{ImageColumnGenerator.IMAGE_FIELD,
                "rating"});
        recomImagesTable.setColumnWidth(ImageColumnGenerator.IMAGE_FIELD, 1000);
        recomImagesTable.setWidth("100%");

        addComponent(recomImagesTable);
    }

    private void loadPastRecommendationResults(final String aNewUser) {
        final List<UserProductImageRating> ratings = fetchRatings(aNewUser);

        recomImagesData.removeAllItems();

        for (final UserProductImageRating curRating : ratings)
        {
            recomImagesData.addBean(curRating);
        }
    }

    private List<UserProductImageRating> fetchRatings(final String aNewUser) {
        final GetUserImageRatingsRequest request = new GetUserImageRatingsRequest();
        request.setUserEmail(aNewUser);

        final RequestSender<GetUserImageRatingsRequest,GetUserImageRatingsResponse> requestSender
                = new RequestSender(GetUserImageRatingsResponse.class,
                GetUserImageRatingsRequest.SERVICE_NAME, InwtApplication.getServerUrl());

        return requestSender.sendRequest(request).getRatings();
    }



    public void userChanged(final String aNewUserEmail) {
        loadPastRecommendationResults(aNewUserEmail);
    }
}
