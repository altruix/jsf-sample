package co.altruix.inwt.recommendationtab;

import com.vaadin.data.Property;

public interface IUserComboBoxListener extends Property.ValueChangeListener  {
    void addUserChangeSelectionListener(IUserChangeSelectionListener aListener);
}
