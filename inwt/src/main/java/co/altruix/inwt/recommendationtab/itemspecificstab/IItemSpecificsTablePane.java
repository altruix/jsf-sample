package co.altruix.inwt.recommendationtab.itemspecificstab;

import co.altruix.inwt.messages.GetAspectStatisticsResponse;

public interface IItemSpecificsTablePane {
    void displayData(final GetAspectStatisticsResponse aData);
}
