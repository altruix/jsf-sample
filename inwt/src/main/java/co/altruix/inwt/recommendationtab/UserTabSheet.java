package co.altruix.inwt.recommendationtab;

import co.altruix.inwt.recommendationtab.itemspecificstab.ItemSpecificsTab;
import com.vaadin.ui.TabSheet;

public class UserTabSheet extends TabSheet {
    public static final String RECOM_QUALITY = "Recom. quality";
    public static final String PAST_RECOMMENDATION_RESULTS = "Past recommendation results";
    public static final String PRODUCT_CATEGORIES = "Product categories";
    public static final String ITEM_SPECIFICS = "Item specifics";

    UserTabSheet(final IUserComboBoxListener aUserComboBoxListener,
                 final boolean aForTestingPurposesOnly)
    {
        super();
    }

    public UserTabSheet(final IUserComboBoxListener aUserComboBoxListener)
    {
        super();

        initUi(aUserComboBoxListener);
    }

    protected void initUi(final IUserComboBoxListener aUserComboBoxListener) {
        final RecomQualityTab recomQualityTab = new RecomQualityTab();
        aUserComboBoxListener.addUserChangeSelectionListener(recomQualityTab);
        addTab(recomQualityTab, RECOM_QUALITY);

        final ProductCategoriesTab productCategoriesTab = new ProductCategoriesTab();
        aUserComboBoxListener.addUserChangeSelectionListener(productCategoriesTab);
        addTab(productCategoriesTab, PRODUCT_CATEGORIES);

        final PastImagesTab pastImagesTab = new PastImagesTab();
        aUserComboBoxListener.addUserChangeSelectionListener(pastImagesTab);
        addTab(pastImagesTab, PAST_RECOMMENDATION_RESULTS);

        final ItemSpecificsTab itemSpecificsTab = new ItemSpecificsTab();
        aUserComboBoxListener.addUserChangeSelectionListener(itemSpecificsTab);
        addTab(itemSpecificsTab, ITEM_SPECIFICS);
    }
}
