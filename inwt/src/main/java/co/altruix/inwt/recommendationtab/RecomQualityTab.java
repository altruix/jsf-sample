package co.altruix.inwt.recommendationtab;

import com.vaadin.ui.HorizontalLayout;

public class RecomQualityTab extends HorizontalLayout implements IUserChangeSelectionListener {
    private final RecomQualityChartPane chartPane = new RecomQualityChartPane();
    private final RecomQualityTablePane tablePane = new RecomQualityTablePane();

    RecomQualityTab(boolean aForTestingPurposesOnly)
    {
        super();
    }

    public RecomQualityTab()
    {
        super();
        initUi();
    }

    private void initUi() {
        addComponent(chartPane);
        addComponent(tablePane);
    }

    public void userChanged(final String aNewUserEmail) {
        chartPane.userChanged(aNewUserEmail);
        tablePane.userChanged(aNewUserEmail);
    }
}
