package co.altruix.inwt.recommendationtab;

public interface IUserChangeSelectionListener {
    void userChanged(final String aNewUserEmail);
}
