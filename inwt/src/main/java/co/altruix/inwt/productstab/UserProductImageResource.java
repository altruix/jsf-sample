package co.altruix.inwt.productstab;

import co.altruix.inwt.model.UserProductImage;
import com.vaadin.Application;
import com.vaadin.terminal.ApplicationResource;
import com.vaadin.terminal.DownloadStream;
import com.vaadin.terminal.ParameterHandler;
import com.vaadin.terminal.Resource;
import com.vaadin.terminal.URIHandler;

import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.Map;

public class UserProductImageResource implements ApplicationResource, Resource, URIHandler,
        ParameterHandler {
    private final UserProductImage userProductImage;
    private final Application application;

    public UserProductImageResource(final UserProductImage aUserProductImage,
                                    final Application aApplication) {
        userProductImage = aUserProductImage;
        application = aApplication;
    }

    public String getMIMEType() {
        return userProductImage.getMimeType();
    }

    public DownloadStream getStream() {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(userProductImage
                .getImageData());
        final DownloadStream downloadStream = new DownloadStream(byteArrayInputStream,
                userProductImage.getMimeType(), userProductImage.getFileName());
        return downloadStream;
    }

    public Application getApplication() {
        return application;
    }

    public String getFilename() {
        return userProductImage.getFileName();
    }

    public long getCacheTime() {
        return 0;
    }

    public int getBufferSize() {
        return userProductImage.getImageData().length;
    }

    public void handleParameters(final Map<String, String[]> aStringMap) {

    }

    public DownloadStream handleURI(final URL aURL, final String s) {
        return getStream();
    }
}
