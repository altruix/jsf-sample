package co.altruix.inwt.productstab;

import co.altruix.inwt.model.UserProductImage;
import com.vaadin.Application;
import com.vaadin.ui.Window;

public class UserProductImageBean extends UserProductImage {
    private UserProductImageResource imageResource;

    private UserProductImageBean()
    {
    }

    public UserProductImageResource getImageResource() {
        return imageResource;
    }

    public static UserProductImageBean create(final UserProductImage aUserProductImage,
                                              final Application aApplication,
                                              final Window aWindow)
    {
        final UserProductImageBean result = new UserProductImageBean();

        result.setImageData(aUserProductImage.getImageData());
        result.setUserProductId(aUserProductImage.getUserProductId());
        result.setCreatorEmail(aUserProductImage.getCreatorEmail());
        result.setMimeType(aUserProductImage.getMimeType());
        result.setFileName(aUserProductImage.getFileName());
        result.setUserProductImageId(aUserProductImage.getUserProductImageId());
        result.imageResource = new UserProductImageResource(aUserProductImage, aApplication);

        aWindow.addParameterHandler(result.imageResource);
        aWindow.addURIHandler(result.imageResource);

        return result;
    }
}
