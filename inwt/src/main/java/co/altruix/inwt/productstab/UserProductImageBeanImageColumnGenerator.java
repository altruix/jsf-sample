package co.altruix.inwt.productstab;

import co.altruix.common.ImageColumnGenerator;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Table;

public class UserProductImageBeanImageColumnGenerator
        extends ImageColumnGenerator<UserProductImageBean> implements Table.ColumnGenerator {

    protected String getFileName(final BeanItem<UserProductImageBean> aBeanItem) {
        return aBeanItem.getBean().getFileName();
    }

    protected byte[] getImageData(final BeanItem<UserProductImageBean> aBeanItem) {
        return aBeanItem.getBean().getImageData();
    }
}
