package co.altruix.inwt.productstab;

import com.vaadin.ui.Upload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ImageUploader implements Upload.Receiver {
    private static final Logger LOGGER = LoggerFactory.getLogger(ImageUploader.class);

    private ByteArrayOutputStream outputStream;
    private String fileName;
    private String mimeType;

    public OutputStream receiveUpload(final String aFileName, final String aMimeType) {
        outputStream = null;

        outputStream = new ByteArrayOutputStream();

        fileName = aFileName;
        mimeType = aMimeType;

        return outputStream;
    }

    public byte[] getUploadedFileContents() {
        if (outputStream != null)
        {
            return outputStream.toByteArray();
        }
        return new byte[0];
    }

    public void freeMemory() {
        if (outputStream != null)
        {
            try {
                outputStream.close();
            } catch (final IOException exception) {
                LOGGER.error("", exception);
            }
        }
    }

    public String getFileName() {
        return fileName;
    }

    public String getMimeType() {
        return mimeType;
    }
}
