package co.altruix.inwt.productstab;

import co.altruix.inwt.model.UserProductImage;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static com.vaadin.terminal.StreamResource.StreamSource;

public class ImageSource implements StreamSource {
    private final UserProductImage userProductImage;

    public ImageSource(final UserProductImage aUserProductImage) {
        userProductImage = aUserProductImage;
    }

    public InputStream getStream() {
        return new ByteArrayInputStream(userProductImage.getImageData());
    }
}
