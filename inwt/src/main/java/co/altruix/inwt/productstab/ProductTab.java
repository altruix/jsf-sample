package co.altruix.inwt.productstab;

import co.altruix.InwtApplication;
import co.altruix.common.ImageColumnGenerator;
import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.CreateProductRequest;
import co.altruix.inwt.messages.CreateProductResponse;
import co.altruix.inwt.messages.DeleteImageRequest;
import co.altruix.inwt.messages.DeleteImageResponse;
import co.altruix.inwt.messages.GetUserProductImagesRequest;
import co.altruix.inwt.messages.GetUserProductImagesResponse;
import co.altruix.inwt.messages.GetUserProductsRequest;
import co.altruix.inwt.messages.GetUserProductsResponse;
import co.altruix.inwt.messages.UploadImageRequest;
import co.altruix.inwt.messages.UploadImageResponse;
import co.altruix.inwt.model.UserProductImage;
import co.altruix.inwt.model.UserProductInfo;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanContainer;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.Action;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ProductTab extends HorizontalLayout {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductTab.class);

    public static final String PRODUCT_IMAGES_TABLE_WIDTH = "1000px";
    private final Window window;
    private BeanContainer<byte[],UserProductInfo> productData;
    private ListSelect productsList;
    private VerticalLayout productDetailsPanel;
    private TextField productNameTextField;
    private UserProductInfo curUserProductInfo;
    private Table productImagesTable;
    private BeanContainer<byte[],UserProductImageBean> productImageData;
    public static final String DELETE = "Delete";
    private static final Action DELETE_ACTION = new Action(DELETE);

    public ProductTab(final Window aWindow)
    {
        super();
        window = aWindow;

        final VerticalLayout productListPanel = createProductsPanel();

        addComponent(productListPanel);

        final Button createNewProductButton = new Button("Create new product");

        createNewProductButton.addListener(new Button.ClickListener() {
            public void buttonClick(final Button.ClickEvent aClickEvent) {
                createNewProductButtonPressed();
            }
        });

        productListPanel.addComponent(createNewProductButton);

        productDetailsPanel = new VerticalLayout();

        productNameTextField = new TextField("Name: ");
        productDetailsPanel.addComponent(productNameTextField);

        final Button saveButton = new Button("Save");
        saveButton.addListener(new Button.ClickListener() {
            public void buttonClick(final Button.ClickEvent aClickEvent) {
                saveProductDataButtonPressed();
            }
        });
        productDetailsPanel.addComponent(saveButton);

        productImageData = new BeanContainer<byte[],
                UserProductImageBean>(UserProductImageBean.class);
        productImageData.setBeanIdProperty("userProductImageId");

        productImagesTable = new Table("Product images", productImageData);
        productImagesTable.addGeneratedColumn(ImageColumnGenerator.IMAGE_FIELD,
                new UserProductImageBeanImageColumnGenerator());
        productImagesTable.setImmediate(true);
        // productImagesTable.setSizeFull();
        productImagesTable.setVisibleColumns(new Object[]{ImageColumnGenerator.IMAGE_FIELD});
        productImagesTable.setColumnWidth(ImageColumnGenerator.IMAGE_FIELD, 1000);
        productImagesTable.setWidth(PRODUCT_IMAGES_TABLE_WIDTH);
        // productImagesTable.setPageLength(3);

        productImagesTable.addActionHandler(new Action.Handler() {
            public Action[] getActions(final Object aTarget, final Object aSender) {
                return new Action[] {DELETE_ACTION};
            }

            public void handleAction(Action aAction, Object aSender, Object aTarget) {
                LOGGER.debug("handleAction");
                LOGGER.debug("handleAction - " + aAction.getCaption());

                final BeanItem<UserProductImageBean> beanItem = (BeanItem<UserProductImageBean>)
                        productImagesTable.getItem(aTarget);
                LOGGER.debug("beanItem - " + beanItem);
                LOGGER.debug("beanItem.getBean().getFileName(): " + beanItem.getBean().getFileName
                        ());

                final DeleteImageRequest request = new DeleteImageRequest();
                request.setImageId(beanItem.getBean().getUserProductImageId());

                final RequestSender<DeleteImageRequest,DeleteImageResponse> sender = new
                        RequestSender<DeleteImageRequest,
                                DeleteImageResponse>(DeleteImageResponse.class,
                        DeleteImageRequest.SERVICE_NAME,
                        InwtApplication.getServerUrl());

                sender.sendRequest(request);

                LOGGER.debug("DeleteImageRequest sent");
            }
        });

        final VerticalLayout imageUploadPanel = createImageUploadPanel();

        productDetailsPanel.addComponent(imageUploadPanel);
        productDetailsPanel.addComponent(productImagesTable);

        addComponent(productDetailsPanel);

        setImmediate(true);
    }

    private VerticalLayout createImageUploadPanel() {
        final VerticalLayout imageUploadPanel = new VerticalLayout();
        final Label uploadResultLabel = new Label();
        final ImageUploader receiver = new ImageUploader();
        final Upload upload = new Upload("Upload a file", receiver);


        upload.addListener(new Upload.SucceededListener() {
            public void uploadSucceeded(Upload.SucceededEvent event) {
                byte[] fileData = receiver.getUploadedFileContents();

                receiver.freeMemory();

                if (curUserProductInfo != null)
                {
                    // Send upload request to the server here
                    final UploadImageRequest request = new UploadImageRequest();
                    request.setCreatorEmail(curUserProductInfo.getAuthorEmail());
                    request.setData(fileData);
                    request.setFileName(receiver.getFileName());
                    request.setMimeType(receiver.getMimeType());
                    request.setUserProductId(curUserProductInfo.getProductId());

                    final RequestSender<UploadImageRequest,UploadImageResponse> requestSender = new
                            RequestSender<UploadImageRequest, UploadImageResponse>(
                            UploadImageResponse.class, UploadImageRequest.SERVICE_NAME,
                            InwtApplication.getServerUrl());

                    requestSender.sendRequest(request);
                }
            }
        });


        imageUploadPanel.addComponent(uploadResultLabel);
        imageUploadPanel.addComponent(upload);

        return imageUploadPanel;
    }

    private void saveProductDataButtonPressed() {
        getWindow().showNotification("Not implemented");
    }

    public void updateProductsPanel() {
        final GetUserProductsRequest request = new GetUserProductsRequest();
        request.setUserEmail(InwtApplication.getInstance().getCurrentUser());

        final RequestSender<GetUserProductsRequest,GetUserProductsResponse> sender = new
                RequestSender<GetUserProductsRequest,
                        GetUserProductsResponse>(GetUserProductsResponse.class,
                GetUserProductsRequest.SERVICE_NAME,
                InwtApplication.getServerUrl());

        final GetUserProductsResponse response = sender.sendRequest(request);

        final List<UserProductInfo> products = response.getUserProducts();

        productData.removeAllItems();

        for (final UserProductInfo curUserProductInfo : products)
        {
            productData.addBean(curUserProductInfo);
        }
    }

    private VerticalLayout createProductsPanel()
    {
        final VerticalLayout productsPanel = new VerticalLayout();

        productData = new BeanContainer<byte[],UserProductInfo>(UserProductInfo.class);
        productData.setBeanIdProperty("productId");

        productsList = new ListSelect("Products", productData);

        productsList.setItemCaptionMode(AbstractSelect.ITEM_CAPTION_MODE_EXPLICIT);
        productsList.setItemCaptionPropertyId("name");
        productsList.setImmediate(true);

        productsList.addListener(new Property.ValueChangeListener() {
            public void valueChange(final Property.ValueChangeEvent aValueChangeEvent) {
                productSelectionChanged();
            }
        });

        productsPanel.addComponent(productsList);

        return productsPanel;
    }

    private void productSelectionChanged() {
        final Object productId = productsList.getValue();

        if (productId == null)
        {
            curUserProductInfo = null;
            disableProductDetailsPanel();
            return;
        }

        final BeanItem<UserProductInfo> beanItem = productData.getItem(productId);
        final UserProductInfo userProductInfo = beanItem.getBean();

        curUserProductInfo = userProductInfo;
        updateProductDetails(userProductInfo);


    }

    private void updateProductDetails(final UserProductInfo aUserProductInfo) {
        productDetailsPanel.setVisible(true);

        productNameTextField.setValue(aUserProductInfo.getName());



        final GetUserProductImagesRequest request = new GetUserProductImagesRequest();
        request.setUserProductId(aUserProductInfo.getProductId());

        final RequestSender<GetUserProductImagesRequest,GetUserProductImagesResponse> sender = new
                RequestSender<GetUserProductImagesRequest, GetUserProductImagesResponse>
                (GetUserProductImagesResponse.class, GetUserProductImagesRequest.SERVICE_NAME,
                        InwtApplication.getServerUrl());

        final GetUserProductImagesResponse response = sender.sendRequest(request);

        final List<UserProductImage> userProductImages = response.getUserImages();
        productImageData.removeAllItems();

        for (final UserProductImage curImage : userProductImages)
        {
            productImageData.addBean(UserProductImageBean.create(curImage,
                    InwtApplication.getInstance(), window));
        }

        productImagesTable.setColumnWidth(ImageColumnGenerator.IMAGE_FIELD, 1000);
        productImagesTable.setWidth(PRODUCT_IMAGES_TABLE_WIDTH);
        productImagesTable.requestRepaint();
        // productImagesTable.requestRepaint();
        // productImagesTable.setWidth("100%");
        // productImagesTable.setColumnWidth(UserProductImageBeanImageColumnGenerator.IMAGE_FIELD, 1000);

        //productImagesTable.requestRepaint();
        //productImagesTable.setColumnWidth(UserProductImageBeanImageColumnGenerator.IMAGE_FIELD, 1000);
    }

    private void disableProductDetailsPanel() {
        productDetailsPanel.setVisible(false);
    }

    private void createNewProductButtonPressed() {
        final Window productWindow = new Window("Product");
        // let's give it a size (optional)
        productWindow.setWidth("300px");
        productWindow.setHeight("200px");

        // Configure the windws layout; by default a VerticalLayout
        VerticalLayout layout = (VerticalLayout) productWindow.getContent();
        layout.setMargin(true);
        layout.setSpacing(true);
        // make it fill the whole window
        layout.setSizeFull();

        // Add some content; a label and a close-button
        Label message = new Label("This is a positioned window");
        productWindow.addComponent(message);

        final TextField nameTextField = new TextField("Name: ");
        productWindow.addComponent(nameTextField);

        final TextArea descriptionTextArea = new TextArea("Description: ");
        productWindow.addComponent(descriptionTextArea);

        Button createButton = new Button("Close", new Button.ClickListener() {
            // inline click-listener
            public void buttonClick(Button.ClickEvent event) {
                // close the window by removing it from the parent window
                (productWindow.getParent()).removeWindow(productWindow);

                LOGGER.debug("name: " + nameTextField.getValue());
                LOGGER.debug("description: " + descriptionTextArea.getValue());

                final String serverUrl = InwtApplication.getServerUrl();

                final CreateProductRequest request = new CreateProductRequest();

                request.setName(""+nameTextField.getValue());
                request.setDescription(""+descriptionTextArea.getValue());
                request.setCreatorEmail(InwtApplication.getInstance().getCurrentUser());

                final RequestSender<CreateProductRequest,CreateProductResponse> sender = new
                        RequestSender<CreateProductRequest,
                                CreateProductResponse>(CreateProductResponse.class,
                        CreateProductRequest.SERVICE_NAME,
                        serverUrl);

                sender.sendRequest(request);

                updateProductsPanel();
            }
        });
        // The components added to the window are actually added to the window's
        // layout; you can use either. Alignments are set using the layout
        layout.addComponent(createButton);
        layout.setComponentAlignment(createButton, Alignment.BOTTOM_RIGHT);

        if (productWindow.getParent() == null) {
            // Open the subwindow by adding it to the parent
            // window
            getWindow().addWindow(productWindow);
        }

        // Center the window
        productWindow.center();
    }

}
