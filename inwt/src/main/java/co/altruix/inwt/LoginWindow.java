package co.altruix.inwt;

import co.altruix.InwtApplication;
import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.SignUpRequest;
import co.altruix.inwt.messages.SignUpResponse;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginWindow extends Window {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginWindow.class);

    private Button loginButton;
    private TextField loginEmail = new TextField("E-Mail:");
    private PasswordField loginPassword = new PasswordField("Password:");

    public LoginWindow() {
        super("INWT");
        setName("loginEmail");
        initUI();
    }

    private void initUI() {
        loginButton = new Button("Login");

        addComponent(new Label("<b>INWT</b>", Label.CONTENT_XHTML));

        addComponent(new Label("INWT allows authors of crafts to get feedback about their " +
                "existing and planned products from potential buyers",
                Label.CONTENT_XHTML));

        addComponent(new Label("<b>Login</b>", Label.CONTENT_XHTML));
        addComponent(new Label("If you have an account already, please use the form below to " +
                "loginEmail.",
                Label.CONTENT_XHTML));

        addComponent(new Label("Please loginEmail in order to use the application"));
        addComponent(new Label());
        addComponent(loginEmail);
        addComponent(loginPassword);
        addComponent(loginButton);

        loginButton.addListener(new Button.ClickListener() {
            public void buttonClick(Button.ClickEvent event) {
                try {
                    InwtApplication.getInstance().authenticate((String) loginEmail.getValue(),
                            (String) loginPassword.getValue());
                    open(new ExternalResource(InwtApplication.getInstance().getURL()));
                } catch (Exception e) {
                    showNotification(e.toString());
                    e.printStackTrace();
                }
            }
        });


        addComponent(new Label("<b>Register</b>", Label.CONTENT_XHTML));
        addComponent(new Label("If you are new to INWT, please enter your e-mail address and " +
                "press the <b>Register</b>. Your loginPassword will be sent to you via e-mail",
                Label.CONTENT_XHTML));

        addComponent(new Label("Please loginEmail in order to use the application"));
        addComponent(new Label());

        final Button registerButton = new Button("Register");
        final TextField registerEmail = new TextField("E-Mail:");

        registerButton.addListener(new Button.ClickListener() {

            public void buttonClick(final Button.ClickEvent aClickEvent) {
                registerUser(registerEmail.getValue().toString());

                getWindow().showNotification(
                        "INWT",
                        "An e-mail with a password was sent you. If it didn't arrive within a few" +
                                " minutes, please contact dp@altruix.co");
            }
        });

        addComponent(registerEmail);
        addComponent(registerButton);
    }

    private void registerUser(final String aEmail) {
        final SignUpRequest request = new SignUpRequest();

        request.setEmail(aEmail);


        final RequestSender<SignUpRequest,SignUpResponse> requestSender = new
                RequestSender<SignUpRequest, SignUpResponse>(SignUpResponse.class, "SignUp",
                InwtApplication.getServerUrl());

        requestSender.sendRequest(request);
    }
}
