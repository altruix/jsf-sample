package co.altruix.inwt.common;

import co.altruix.inwt.messages.GetRecomQualityRequest;
import co.altruix.inwt.messages.GetRecomQualityResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class RequestSenderTest {
    @Test
    public void testMapperCreation()
    {
        final RequestSender<GetRecomQualityRequest,GetRecomQualityResponse> sender = new
                RequestSender<GetRecomQualityRequest,
                        GetRecomQualityResponse>(GetRecomQualityResponse.class,
                GetRecomQualityRequest.SERVICE_NAME,
                "");

        final ObjectMapper objectMapper = sender.createObjectMapper();

        assertThat(objectMapper.getDeserializationConfig().isEnabled(DeserializationFeature
                .FAIL_ON_UNKNOWN_PROPERTIES)).isFalse();
    }

    @Test
    public void testMapperCreation2()
    {
        final RequestSender<GetRecomQualityRequest,GetRecomQualityResponse> sender = spy(new
                RequestSender<GetRecomQualityRequest,
                        GetRecomQualityResponse>(GetRecomQualityResponse.class,
                GetRecomQualityRequest.SERVICE_NAME,
                ""));

        final ObjectMapper mapper = mock(ObjectMapper.class);
        doReturn(mapper).when(sender).createVirginObjectMapper();

        sender.createObjectMapper();

        verify(mapper).registerModule(isA(JodaModule.class));
    }
}
