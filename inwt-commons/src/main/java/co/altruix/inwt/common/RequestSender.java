package co.altruix.inwt.common;

import co.altruix.inwt.messages.InwtRequest;
import co.altruix.inwt.messages.InwtResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class RequestSender<RequestType extends InwtRequest, ResponseType extends InwtResponse> {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestSender.class);
    public static final String REQUEST = "request";
    private final String serverUrl;
    private final String serviceName;
    private final Class<ResponseType> responseClass;

    public RequestSender(final Class<ResponseType> aResponseClass, final String aServiceName,
                         final String aServerUrl) {
        responseClass = aResponseClass;
        serverUrl = aServerUrl;
        serviceName = aServiceName;
    }

    public ResponseType sendRequest(final RequestType aRequest)
    {
        final ObjectMapper mapper = createObjectMapper();

        String json = null;

        try {
            json = mapper.writeValueAsString(aRequest);

            final CloseableHttpClient httpclient = HttpClients.createDefault();

            HttpPost post = new HttpPost(serverUrl + serviceName);

            final List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair(REQUEST, json));

            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            final CloseableHttpResponse response2 = httpclient.execute(post);

            final ResponseType response = mapper.readValue(response2.getEntity().getContent(),
                    responseClass);

            EntityUtils.consume(response2.getEntity());

            return response;
        } catch (final JsonProcessingException exception) {
            LOGGER.error("", exception);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("", e);
        } catch (IOException e) {
            LOGGER.error("", e);
        }

        return null;
    }

    protected ObjectMapper createObjectMapper() {
        final ObjectMapper mapper = createVirginObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.registerModule(new JodaModule()); // TODO: Test this

        return mapper;
    }

    protected ObjectMapper createVirginObjectMapper() {
        return new ObjectMapper();
    }
}
