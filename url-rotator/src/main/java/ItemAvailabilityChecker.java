import com.google.common.base.Charsets;
import com.google.common.io.Resources;

import java.io.IOException;
import java.net.URL;

public class ItemAvailabilityChecker implements IItemAvailabilityChecker {
    @Override
    public boolean isItemSold(final String aUrl) throws IOException {
        final URL url = new URL(aUrl);
        final String text = Resources.toString(url, Charsets.UTF_8);

        return text.contains("Dieses einzigartige Produkt ist momentan leider nicht verfügbar.");
    }
}
