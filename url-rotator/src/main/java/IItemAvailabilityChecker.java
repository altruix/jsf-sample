import java.io.IOException;

public interface IItemAvailabilityChecker {
    boolean isItemSold(final String aUrl) throws IOException;
}
