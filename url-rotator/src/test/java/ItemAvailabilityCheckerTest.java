import org.junit.Test;

import java.io.IOException;

import static org.fest.assertions.api.Assertions.*;

public class ItemAvailabilityCheckerTest {
    @Test
    public void testOnAvailableItems() throws IOException {
        final IItemAvailabilityChecker objectUnderTest = new ItemAvailabilityChecker();

        assertThat(objectUnderTest.isItemSold("http://de.dawanda.com/product/57012499-Strumpfband-Elfenbein-mit-Masche")).isFalse();
        assertThat(objectUnderTest.isItemSold("http://de.dawanda.com/product/61238155-Strumpfband-Blau-und-Silber")).isFalse();
        assertThat(objectUnderTest.isItemSold("http://de.dawanda.com/product/48755990-Strumpfband-Pfirsich")).isFalse();
        assertThat(objectUnderTest.isItemSold("http://de.dawanda.com/product/60370411-Strumpfband-mit-Masche")).isFalse();
    }

    @Test
    public void testOnSoldItems() throws IOException {
        final IItemAvailabilityChecker objectUnderTest = new ItemAvailabilityChecker();

        assertThat(objectUnderTest.isItemSold("http://de.dawanda.com/product/58099767-Strumpfband-Blau")).isTrue();
        assertThat(objectUnderTest.isItemSold("http://de.dawanda.com/product/60368271-Strumpfband-Blau")).isTrue();
        assertThat(objectUnderTest.isItemSold("http://de.dawanda.com/product/58100195-Strumpfband-Schwarz-und-Weiss")).isTrue();
        assertThat(objectUnderTest.isItemSold("http://de.dawanda.com/product/57010403-Strumpfband-Champaign-Vintage")).isTrue();
    }

}
