package co.altruix.inwt.messages;

public class LoginResponse extends InwtResponse {
	private boolean success = false;
	
	public LoginResponse()
	{
		
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(final boolean aSuccess) {
		this.success = aSuccess;
	}
}
