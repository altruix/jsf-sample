package co.altruix.inwt.messages;

import co.altruix.inwt.model.EbayAspectStatistic;

import java.util.List;

public class GetAspectStatisticsResponse extends InwtResponse {
    private List<EbayAspectStatistic> statistics;

    public List<EbayAspectStatistic> getStatistics() {
        return statistics;
    }

    public void setStatistics(final List<EbayAspectStatistic> aStatistics) {
        statistics = aStatistics;
    }
}
