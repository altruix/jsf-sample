package co.altruix.inwt.messages;

public class CreateProductResponse extends InwtResponse {
    private boolean success = false;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(final boolean aSuccess) {
        success = aSuccess;
    }
}
