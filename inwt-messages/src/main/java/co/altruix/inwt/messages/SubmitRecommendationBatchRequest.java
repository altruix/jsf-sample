package co.altruix.inwt.messages;

import java.util.List;

public class SubmitRecommendationBatchRequest extends InwtRequest {
    @SuppressWarnings("unused")
    public final static String SERVICE_NAME = "SubmitRecommendationBatch";

    private List<String> productUrls;

    private String submitterEmail;

    private String recommenationRecipientEmail;

    public List<String> getProductUrls() {
        return productUrls;
    }

    public void setProductUrls(final List<String> aProductUrls) {
        productUrls = aProductUrls;
    }

    public String getSubmitterEmail() {
        return submitterEmail;
    }

    public void setSubmitterEmail(final String aSubmitterEmail) {
        submitterEmail = aSubmitterEmail;
    }

    public String getRecommenationRecipientEmail() {
        return recommenationRecipientEmail;
    }

    public void setRecommenationRecipientEmail(final String aRecommenationRecipientEmail) {
        recommenationRecipientEmail = aRecommenationRecipientEmail;
    }
}
