package co.altruix.inwt.messages;

public class SignUpRequest extends InwtRequest {
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(final String aEmail) {
		this.email = aEmail;
	}
}
