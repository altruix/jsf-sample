package co.altruix.inwt.messages;

public class GetRecomQualityRequest extends InwtRequest {
    public final static String SERVICE_NAME = "GetRecomQuality";

    private String userEmail;

    private String recommenderEmail;

    public String getRecommenderEmail() {
        return recommenderEmail;
    }

    public void setRecommenderEmail(final String aRecommenderEmail) {
        recommenderEmail = aRecommenderEmail;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(final String aUserEmail) {
        userEmail = aUserEmail;
    }
}
