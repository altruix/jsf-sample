package co.altruix.inwt.messages;

public class NewImageAvailabilityRequest extends InwtRequest {
    public final static String SERVICE_NAME = "NewImageAvailability";

    private String userEmail;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(final String aUserEmail) {
        userEmail = aUserEmail;
    }

}
