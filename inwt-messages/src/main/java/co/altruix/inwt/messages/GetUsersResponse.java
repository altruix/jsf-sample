package co.altruix.inwt.messages;

import java.util.List;

public class GetUsersResponse extends InwtResponse {
    private List<String> userEmails;

    public List<String> getUserEmails() {
        return userEmails;
    }

    public void setUserEmails(final List<String> aUserEmails) {
        userEmails = aUserEmails;
    }
}
