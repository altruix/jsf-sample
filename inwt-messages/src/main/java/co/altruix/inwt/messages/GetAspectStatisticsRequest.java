package co.altruix.inwt.messages;

public class GetAspectStatisticsRequest extends InwtRequest {
    public final static String SERVICE_NAME = "GetAspectStatistics";

    private String userEmail;
    private String productCategoryId;
    private String aspectName;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(final String aUserEmail) {
        userEmail = aUserEmail;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(final String aProductCategoryId) {
        productCategoryId = aProductCategoryId;
    }

    public String getAspectName() {
        return aspectName;
    }

    public void setAspectName(final String aAspectName) {
        aspectName = aAspectName;
    }
}
