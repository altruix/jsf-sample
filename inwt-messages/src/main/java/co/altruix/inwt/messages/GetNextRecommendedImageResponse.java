package co.altruix.inwt.messages;

import co.altruix.inwt.model.UserProductImage;

public class GetNextRecommendedImageResponse extends InwtResponse {
    private RecommendationResult result;
    private UserProductImage image;

    public UserProductImage getImage() {
        return image;
    }

    public void setImage(final UserProductImage aImage) {
        image = aImage;

    }
    public RecommendationResult getResult() {
        return result;
    }

    public void setResult(final RecommendationResult aResult) {
        result = aResult;
    }
}
