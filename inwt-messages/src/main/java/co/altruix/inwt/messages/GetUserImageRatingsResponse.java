package co.altruix.inwt.messages;

import co.altruix.inwt.model.UserProductImageRating;

import java.util.List;

public class GetUserImageRatingsResponse extends InwtResponse {
    private List<UserProductImageRating> ratings;

    public List<UserProductImageRating> getRatings() {
        return ratings;
    }

    public void setRatings(final List<UserProductImageRating> aRatings) {
        ratings = aRatings;
    }
}
