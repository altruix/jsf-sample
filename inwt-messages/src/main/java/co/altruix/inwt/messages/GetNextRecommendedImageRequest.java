package co.altruix.inwt.messages;

@SuppressWarnings("unused")
public class GetNextRecommendedImageRequest extends InwtRequest {

    @SuppressWarnings("unused")
    public final static String SERVICE_NAME = "GetNextRecommendedImage";

    private String userEmail;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(final String aUserEmail) {
        userEmail = aUserEmail;
    }
}
