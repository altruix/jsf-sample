package co.altruix.inwt.messages;

import co.altruix.inwt.model.EbayProductCategory;

import java.util.List;

public class GetUserProductCategoriesResponse extends InwtResponse {
    private List<EbayProductCategory> userProductCategories;

    public List<EbayProductCategory> getUserProductCategories() {
        return userProductCategories;
    }

    public void setUserProductCategories(final List<EbayProductCategory> aUserProductCategories) {
        userProductCategories = aUserProductCategories;
    }
}
