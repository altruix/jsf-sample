package co.altruix.inwt.messages;

import co.altruix.inwt.model.UserProductImage;

import java.util.List;

public class GetUserProductImagesResponse extends InwtResponse {
    private List<UserProductImage> userImages;

    public List<UserProductImage> getUserImages() {
        return userImages;
    }

    public void setUserImages(final List<UserProductImage> aUserImages) {
        userImages = aUserImages;
    }
}
