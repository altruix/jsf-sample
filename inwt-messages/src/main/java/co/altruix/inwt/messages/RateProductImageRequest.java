package co.altruix.inwt.messages;

public class RateProductImageRequest extends InwtRequest{
    @SuppressWarnings("unused")
    public final static String SERVICE_NAME = "RateProductImage";

    private String email;
    private byte[] productId;
    private byte[] imageId;
    private ImageRating rating;

    public byte[] getProductId() {
        return productId;
    }

    public void setProductId(final byte[] aProductId) {
        productId = aProductId;
    }

    public byte[] getImageId() {
        return imageId;
    }

    public void setImageId(final byte[] aImageId) {
        imageId = aImageId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String aEmail) {
        email = aEmail;
    }

    public ImageRating getRating() {
        return rating;
    }

    public void setRating(final ImageRating aRating) {
        rating = aRating;
    }
}
