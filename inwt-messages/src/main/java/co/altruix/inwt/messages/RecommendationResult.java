package co.altruix.inwt.messages;

public enum RecommendationResult {
    NEW_IMAGE_AVAILABLE,
    NO_NEW_IMAGES
}
