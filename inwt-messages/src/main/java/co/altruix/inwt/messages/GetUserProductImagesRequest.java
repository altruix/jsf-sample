package co.altruix.inwt.messages;

@SuppressWarnings("unused")
public class GetUserProductImagesRequest extends InwtRequest {
    @SuppressWarnings("unused")
    public final static String SERVICE_NAME = "GetUserProductImages";

    private byte[] userProductId;

    public byte[] getUserProductId() {
        return userProductId;
    }

    public void setUserProductId(final byte[] aUserProductId) {
        userProductId = aUserProductId;
    }
}
