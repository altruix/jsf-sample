package co.altruix.inwt.messages;

public class GetLikedProductCategoriesRequest extends InwtRequest {
    public final static String SERVICE_NAME = "GetLikedProductCategories";

    private String userEmail;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(final String aUserEmail) {
        userEmail = aUserEmail;
    }
}
