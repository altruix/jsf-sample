package co.altruix.inwt.messages;

public class SignUpResponse extends InwtResponse {
	private SignUpResult result = SignUpResult.INITIAL;

	public SignUpResponse(final SignUpResult aResult)
	{
		result = aResult;
	}
	
	public SignUpResponse() {
	}

	public SignUpResult getResult() {
		return result;
	}

	public void setResult(final SignUpResult aResult) {
		this.result = aResult;
	}	
}
