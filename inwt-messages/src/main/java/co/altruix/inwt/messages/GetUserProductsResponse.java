package co.altruix.inwt.messages;

import co.altruix.inwt.model.UserProductInfo;

import java.util.List;

@SuppressWarnings("unused")
public class GetUserProductsResponse extends InwtResponse {
    private List<UserProductInfo> userProducts;

    public List<UserProductInfo> getUserProducts() {
        return userProducts;
    }

    public void setUserProducts(final List<UserProductInfo> aUserProducts) {
        userProducts = aUserProducts;
    }
}
