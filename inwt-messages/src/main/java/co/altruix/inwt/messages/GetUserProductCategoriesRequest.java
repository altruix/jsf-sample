package co.altruix.inwt.messages;

public class GetUserProductCategoriesRequest extends InwtRequest {
    public final static String SERVICE_NAME = "GetUserProductCategories";

    private String userEmail;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(final String aUserEmail) {
        userEmail = aUserEmail;
    }
}
