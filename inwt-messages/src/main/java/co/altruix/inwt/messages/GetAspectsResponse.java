package co.altruix.inwt.messages;

import co.altruix.inwt.model.EbayAspect;

import java.util.List;

public class GetAspectsResponse extends InwtResponse {
    private List<EbayAspect> aspects;

    public List<EbayAspect> getAspects() {
        return aspects;
    }

    public void setAspects(final List<EbayAspect> aAspects) {
        aspects = aAspects;
    }
}
