package co.altruix.inwt.messages;

public class RateProductImageResponse extends InwtResponse {
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(final boolean aSuccess) {
        success = aSuccess;
    }
}
