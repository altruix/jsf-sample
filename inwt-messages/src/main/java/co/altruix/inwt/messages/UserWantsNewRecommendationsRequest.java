package co.altruix.inwt.messages;

public class UserWantsNewRecommendationsRequest extends InwtRequest {
    public final static String SERVICE_NAME = "UserWantsNewRecommendations";

    private String userEmail;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(final String aUserEmail) {
        userEmail = aUserEmail;
    }
}
