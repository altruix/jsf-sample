package co.altruix.inwt.messages;

public class GetUsersRequest extends InwtRequest {
    @SuppressWarnings("unused")
    public final static String SERVICE_NAME = "GetUsers";

    private String requesterEmail;

    public String getRequesterEmail() {
        return requesterEmail;
    }

    public void setRequesterEmail(final String aRequesterEmail) {
        requesterEmail = aRequesterEmail;
    }
}
