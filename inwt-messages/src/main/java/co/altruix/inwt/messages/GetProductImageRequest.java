package co.altruix.inwt.messages;

public class GetProductImageRequest extends InwtRequest {
    private String email = "";

    public GetProductImageRequest()
    {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String aEmail) {
        email = aEmail;
    }
}
