package co.altruix.inwt.messages;

public class GetUserImageRatingsRequest extends InwtRequest {
    public final static String SERVICE_NAME = "GetUserImageRatings";

    private String userEmail;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(final String aUserEmail) {
        userEmail = aUserEmail;
    }
}
