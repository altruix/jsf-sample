package co.altruix.inwt.messages;

@SuppressWarnings("unused")
public class UploadImageRequest extends InwtRequest {
    @SuppressWarnings("unused")
    public final static String SERVICE_NAME = "UploadImage";

    private String creatorEmail;
    private String fileName;
    private String mimeType;
    private byte[] userProductId;
    private byte[] data;

    public byte[] getUserProductId() {
        return userProductId;
    }

    public void setUserProductId(final byte[] aUserProductId) {
        userProductId = aUserProductId;
    }

    public String getCreatorEmail() {
        return creatorEmail;
    }

    public void setCreatorEmail(final String aCreatorEmail) {
        creatorEmail = aCreatorEmail;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(final String aFileName) {
        fileName = aFileName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(final String aMimeType) {
        mimeType = aMimeType;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(final byte[] aData) {
        data = aData;
    }
}
