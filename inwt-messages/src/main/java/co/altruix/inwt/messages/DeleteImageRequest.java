package co.altruix.inwt.messages;

@SuppressWarnings("unused")
public class DeleteImageRequest extends InwtRequest {
    @SuppressWarnings("unused")
    public final static String SERVICE_NAME = "DeleteImage";

    private byte[] imageId;

    public byte[] getImageId() {
        return imageId;
    }

    public void setImageId(final byte[] aImageId) {
        imageId = aImageId;
    }

}
