package co.altruix.inwt.messages;

public class CreateProductRequest extends InwtRequest {
    @SuppressWarnings("unused")
    public final static String SERVICE_NAME = "CreateProduct";
    private String name;
    private String description;
    private String creatorEmail;

    public String getName() {
        return name;
    }

    public void setName(final String aName) {
        name = aName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String aDescription) {
        description = aDescription;
    }

    public String getCreatorEmail() {
        return creatorEmail;
    }

    public void setCreatorEmail(final String aCreatorEmail) {
        creatorEmail = aCreatorEmail;
    }
}
