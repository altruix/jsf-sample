package co.altruix.inwt.messages;

@SuppressWarnings("unused")
public class DeleteImageResponse extends InwtResponse {
    private boolean success = false;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(final boolean aSuccess) {
        success = aSuccess;
    }
}
