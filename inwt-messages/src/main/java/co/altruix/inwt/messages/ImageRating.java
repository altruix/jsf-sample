package co.altruix.inwt.messages;

public enum ImageRating {
    HOT,
    NOT,
    UNKNOWN
}
