package co.altruix.inwt.messages;

import co.altruix.inwt.model.RecomQualityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GetRecomQualityResponse extends InwtResponse {
    private List<RecomQualityInfo> recomQualityInfos;

    public List<RecomQualityInfo> getRecomQualityInfos() {
        return recomQualityInfos;
    }

    public void setRecomQualityInfos(final List<RecomQualityInfo> aRecomQualityInfos) {
        recomQualityInfos = aRecomQualityInfos;
    }
}
