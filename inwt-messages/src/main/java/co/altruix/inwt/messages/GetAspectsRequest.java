package co.altruix.inwt.messages;

public class GetAspectsRequest extends InwtRequest {
    public final static String SERVICE_NAME = "GetAspects";

    private String userEmail;
    private String productCategoryId;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(final String aUserEmail) {
        userEmail = aUserEmail;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(final String aProductCategoryId) {
        productCategoryId = aProductCategoryId;
    }
}
