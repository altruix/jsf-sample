package co.altruix.inwt.messages;

public class NewImageAvailabilityResponse extends InwtResponse {
    private RecommendationResult result;

    public RecommendationResult getResult() {
        return result;
    }

    public void setResult(final RecommendationResult aResult) {
        result = aResult;
    }
}
