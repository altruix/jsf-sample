package co.altruix.inwt.messages;

public class GetNextImageToShowResponse extends InwtResponse {
    private boolean imageAvailable;
    private byte[] productId;
    private byte[] imageId;
    private byte[] image;

    public boolean isImageAvailable() {
        return imageAvailable;
    }

    public void setImageAvailable(final boolean aImageAvailable) {
        imageAvailable = aImageAvailable;
    }

    public byte[] getProductId() {
        return productId;
    }

    public void setProductId(final byte[] aProductId) {
        productId = aProductId;
    }

    public byte[] getImageId() {
        return imageId;
    }

    public void setImageId(final byte[] aImageId) {
        imageId = aImageId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(final byte[] aImage) {
        image = aImage;
    }
}
