package co.altruix.inwt.messages;

public class GetProductImageResponse extends InwtResponse {
    private String viewItemUrl;

    private byte[] image;

    public GetProductImageResponse()
    {

    }

    public String getViewItemUrl() {
        return viewItemUrl;
    }

    public void setViewItemUrl(final String aViewItemUrl) {
        viewItemUrl = aViewItemUrl;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(final byte[] aImage) {
        image = aImage;
    }
}
