package co.altruix.inwt.messages;

import co.altruix.inwt.model.EbayProductCategoryPopularityInfo;

import java.util.List;

public class GetLikedProductCategoriesResponse extends InwtResponse {
    private List<EbayProductCategoryPopularityInfo> popularityData;

    public List<EbayProductCategoryPopularityInfo> getPopularityData() {
        return popularityData;
    }

    public void setPopularityData(final List<EbayProductCategoryPopularityInfo> aPopularityData) {
        popularityData = aPopularityData;
    }
}
