package co.altruix.inwt.model;

public enum UserBehaviourEvent {
    REQUESTED_NEW_RECOMMENDATIONS,
    LOGIN_ATTEMPT,
    IMAGE_RATING
}
