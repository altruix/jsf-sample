package co.altruix.inwt.model;

public class UserProductInfo {
    private String authorEmail;
    private byte[] productId;
    private String name;
    private String description;

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(final String aAuthorEmail) {
        authorEmail = aAuthorEmail;
    }

    public byte[] getProductId() {
        return productId;
    }

    public void setProductId(final byte[] aProductId) {
        productId = aProductId;
    }

    public String getName() {
        return name;
    }

    public void setName(final String aName) {
        name = aName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String aDescription) {
        description = aDescription;
    }
}
