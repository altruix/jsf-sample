package co.altruix.inwt.model;

public class EbayAspect {
    private String productCategory;
    private String name;

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(final String aProductCategory) {
        productCategory = aProductCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(final String aName) {
        name = aName;
    }
}
