package co.altruix.inwt.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.joda.time.DateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RecomQualityInfo {

    @JsonIgnoreProperties(ignoreUnknown = true)
    private DateTime firstRecomCreationDateTimeUtc;

    @JsonIgnoreProperties(ignoreUnknown = true)
    private DateTime lastRecomCreationDateTimeUtc;

    private int numberOfRecoms;
    private int numberOfHotRecoms;

    public RecomQualityInfo()
    {
        this(null, null, 0, 0);
    }

    public RecomQualityInfo(final DateTime aFirstRecomCreationDateTimeUtc,
                            final DateTime aLastRecomCreationDateTimeUtc, final int aNumberOfRecoms,
                            final int aNumberOfHotRecoms) {
        firstRecomCreationDateTimeUtc = aFirstRecomCreationDateTimeUtc;
        lastRecomCreationDateTimeUtc = aLastRecomCreationDateTimeUtc;
        numberOfRecoms = aNumberOfRecoms;
        numberOfHotRecoms = aNumberOfHotRecoms;
    }

    public DateTime getFirstRecomCreationDateTimeUtc() {
        return firstRecomCreationDateTimeUtc;
    }

    public void setFirstRecomCreationDateTimeUtc(final DateTime aFirstRecomCreationDateTimeUtc) {
        firstRecomCreationDateTimeUtc = aFirstRecomCreationDateTimeUtc;
    }

    public DateTime getLastRecomCreationDateTimeUtc() {
        return lastRecomCreationDateTimeUtc;
    }

    public void setLastRecomCreationDateTimeUtc(final DateTime aLastRecomCreationDateTimeUtc) {
        lastRecomCreationDateTimeUtc = aLastRecomCreationDateTimeUtc;
    }

    public int getNumberOfRecoms() {
        return numberOfRecoms;
    }

    public void setNumberOfRecoms(final int aNumberOfRecoms) {
        numberOfRecoms = aNumberOfRecoms;
    }

    public int getNumberOfHotRecoms() {
        return numberOfHotRecoms;
    }

    public void setNumberOfHotRecoms(final int aNumberOfHotRecoms) {
        numberOfHotRecoms = aNumberOfHotRecoms;
    }
}
