package co.altruix.inwt.model;

import java.util.LinkedList;
import java.util.List;

public class EbayProductCategoryPopularityInfo {
    private String categoryId;
    private List<String> categoryNames = new LinkedList<String>();
    private int numberOfLikedImages;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(final String aCategoryId) {
        categoryId = aCategoryId;
    }

    public List<String> getCategoryNames() {
        return categoryNames;
    }

    public void setCategoryNames(final List<String> aCategoryNames) {
        categoryNames = aCategoryNames;
    }

    public int getNumberOfLikedImages() {
        return numberOfLikedImages;
    }

    public void setNumberOfLikedImages(final int aNumberOfLikedImages) {
        numberOfLikedImages = aNumberOfLikedImages;
    }

    public void appendCategoryName(final String aName)
    {
        if (!categoryNames.contains(aName))
        {
            categoryNames.add(aName);
        }
    }

    public void incrementNumberOfLikedImages(final int aIncrease)
    {
        numberOfLikedImages += aIncrease;
    }
}
