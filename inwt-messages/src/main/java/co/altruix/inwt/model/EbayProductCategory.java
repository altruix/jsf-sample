package co.altruix.inwt.model;

import java.util.LinkedList;
import java.util.List;

public class EbayProductCategory {
    private String categoryId;
    private List<String> categoryNames = new LinkedList<String>();

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(final String aCategoryId) {
        categoryId = aCategoryId;
    }

    public List<String> getCategoryNames() {
        return categoryNames;
    }

    public void setCategoryNames(final List<String> aCategoryNames) {
        categoryNames = aCategoryNames;
    }

}
