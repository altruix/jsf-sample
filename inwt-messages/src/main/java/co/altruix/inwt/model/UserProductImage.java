package co.altruix.inwt.model;

public class UserProductImage {
    private byte[] userProductImageId;
    private byte[] imageData;
    private byte[] userProductId;
    private String fileName;
    private String creatorEmail;
    private String mimeType;

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(final byte[] aImageData) {
        imageData = aImageData;
    }

    public byte[] getUserProductId() {
        return userProductId;
    }

    public void setUserProductId(final byte[] aUserProductId) {
        userProductId = aUserProductId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(final String aFileName) {
        fileName = aFileName;
    }

    public String getCreatorEmail() {
        return creatorEmail;
    }

    public void setCreatorEmail(final String aCreatorEmail) {
        creatorEmail = aCreatorEmail;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(final String aMimeType) {
        mimeType = aMimeType;
    }

    public byte[] getUserProductImageId() {
        return userProductImageId;
    }

    public void setUserProductImageId(final byte[] aUserProductImageId) {
        userProductImageId = aUserProductImageId;
    }
}
