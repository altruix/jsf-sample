package co.altruix.inwt.model;

import co.altruix.inwt.messages.ImageRating;

public class UserProductImageRating {
    private UserProductImage image;
    private ImageRating rating;

    public UserProductImage getImage() {
        return image;
    }

    public void setImage(final UserProductImage aImage) {
        image = aImage;
    }

    public ImageRating getRating() {
        return rating;
    }

    public void setRating(final ImageRating aRating) {
        rating = aRating;
    }
}
