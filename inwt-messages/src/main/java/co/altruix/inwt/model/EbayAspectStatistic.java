package co.altruix.inwt.model;

public class EbayAspectStatistic {
    private EbayAspect aspect;
    private String value;
    private int numberOfLikedImages;
    private int numberOfDislikedImages;

    public EbayAspect getAspect() {
        return aspect;
    }


    public void setAspect(final EbayAspect aAspect) {
        aspect = aAspect;
    }

    public int getNumberOfLikedImages() {
        return numberOfLikedImages;
    }

    public void setNumberOfLikedImages(final int aNumberOfLikedImages) {
        numberOfLikedImages = aNumberOfLikedImages;
    }

    public String getAspectName()
    {
        return aspect.getName();
    }

    public void setAspectName(final String aName)
    {

    }

    public int getNumberOfDislikedImages() {
        return numberOfDislikedImages;
    }

    public void setNumberOfDislikedImages(final int aNumberOfDislikedImages) {
        numberOfDislikedImages = aNumberOfDislikedImages;
    }

    public void incrementNumberOfLikedImages()
    {
        numberOfLikedImages++;
    }

    public void incrementNumberOfDisklikedImages()
    {
        numberOfDislikedImages++;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String aValue) {
        value = aValue;
    }
}
