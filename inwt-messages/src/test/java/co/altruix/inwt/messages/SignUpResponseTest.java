package co.altruix.inwt.messages;

import org.junit.Test;
import static org.fest.assertions.api.Assertions.assertThat;

public class SignUpResponseTest {
	/**
	 * 
	 * <pre>
	 * 07-31 22:37:08.342: D/SignUpActivity(15914): responseAsString: {"result":"SUCCESS"}
	 * 07-31 22:37:08.373: E/SignUpActivity(15914): com.fasterxml.jackson.databind.JsonMappingException: 
	 * No suitable constructor found for type [simple type, class 
	 * co.altruix.inwt.messages.SignUpResponse]: can not instantiate from JSON object 
	 * (need to add/enable type information?)
	 * </pre>
	 */
	@Test
	public void testDefaultConstructorExists()
	{
		final SignUpResponse objectUnerTest = new SignUpResponse();
		assertThat(objectUnerTest.getResult()).isEqualTo(SignUpResult.INITIAL);
	}
}
