package co.altruix.inwt.mobile;

import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.GetNextRecommendedImageRequest;
import co.altruix.inwt.messages.GetNextRecommendedImageResponse;
import co.altruix.inwt.messages.RateProductImageRequest;
import co.altruix.inwt.messages.RecommendationResult;
import co.altruix.inwt.mobile.util.Util;
import co.altruix.inwt.model.UserProductImage;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.mockito.verification.VerificationMode;
import org.primefaces.component.graphicimage.GraphicImage;

import javax.faces.component.UIComponent;

import static co.altruix.inwt.messages.ImageRating.HOT;
import static co.altruix.inwt.mobile.MainView.HOT_BUTTON_COMPONENT_ID;
import static co.altruix.inwt.mobile.MainView.IMAGE_COMPONENT_ID;
import static co.altruix.inwt.mobile.MainView.NOT_BUTTON_COMPONENT_ID;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MainViewTest {

    public static final String AUTHENTICATE_USER_EMAIL = "dp@altruix.co";
    public static final byte[] IMAGE_ID = "123456789012".getBytes();
    public static final byte[] PRODUCT_ID = "223456789012".getBytes();

    @Test
    public void testLoadFirstImageWhenImageIsThere()
    {
        loadFirstImageTestLogic(RecommendationResult.NEW_IMAGE_AVAILABLE, true, times(1), false,
                true);
    }

    @Test
    public void testLoadFirstImageWhenNoImageIsThere()
    {
        loadFirstImageTestLogic(RecommendationResult.NO_NEW_IMAGES, false, never(), true,
                false);
    }

    protected void loadFirstImageTestLogic(final RecommendationResult aNewImageAvailable,
                                           final boolean aImageVisible,
                                           final VerificationMode aSetCurrentImageDataInvocations,
                                           final boolean aNoImagesAvailableLabelVisible,
                                           final boolean aHotNotButtonsVisible) {
        /**
         * Prepare
         */
        final MainView objectUnderTest = spy(new MainView());

        final RequestSender requestSender = mock(RequestSender.class);

        final GetNextRecommendedImageResponse response = mock(GetNextRecommendedImageResponse.class);

        when(response.getResult()).thenReturn(aNewImageAvailable);
        final UserProductImage userProductImage = new UserProductImage();

        userProductImage.setUserProductImageId(IMAGE_ID);
        userProductImage.setUserProductId(PRODUCT_ID);

        when(response.getImage()).thenReturn(userProductImage);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final GetNextRecommendedImageRequest request = (GetNextRecommendedImageRequest)
                        invocation.getArguments()[0];

                assertThat(request).isNotNull();
                assertThat(request.getUserEmail()).isEqualTo(AUTHENTICATE_USER_EMAIL);

                return response;
            }
        }).when(requestSender).sendRequest(isA(GetNextRecommendedImageRequest.class));

        doReturn(requestSender).when(objectUnderTest).createGetNextRecommendedImageRequestSender();

        final GraphicImage image = mock(GraphicImage.class);

        doReturn(image).when(objectUnderTest).findComponent(IMAGE_COMPONENT_ID);

        final UIComponent hotButton = mock(UIComponent.class);
        doReturn(hotButton).when(objectUnderTest).findComponent(HOT_BUTTON_COMPONENT_ID);

        final UIComponent notButton = mock(UIComponent.class);
        doReturn(notButton).when(objectUnderTest).findComponent(NOT_BUTTON_COMPONENT_ID);

        doReturn(AUTHENTICATE_USER_EMAIL).when(objectUnderTest).getEmail();

        /**
         * Run method under test
         */
        objectUnderTest.loadFirstImage();

        /**
         * Verify
         */
        verify(objectUnderTest).createGetNextRecommendedImageRequestSender();
        verify(requestSender).sendRequest(isA(GetNextRecommendedImageRequest.class));
        verify(response).getResult();
        verify(objectUnderTest).findComponent(IMAGE_COMPONENT_ID);
        verify(image).setRendered(aImageVisible);
        verify(objectUnderTest, aSetCurrentImageDataInvocations).setCurrentImageData(eq(response));
        verify(hotButton).setRendered(aHotNotButtonsVisible);
        verify(notButton).setRendered(aHotNotButtonsVisible);

        verify(objectUnderTest, aSetCurrentImageDataInvocations).setImageId(IMAGE_ID);
        verify(objectUnderTest, aSetCurrentImageDataInvocations).setProductId(PRODUCT_ID);
    }

    @Test
    public void testHotButtonAction()
    {
        /**
         * Prepare
         */
        final MainView objectUnderTest = spy(new MainView());

        doNothing().when(objectUnderTest).sendRecommendationResult(true);
        doNothing().when(objectUnderTest).fetchNextImage();

        doReturn(AUTHENTICATE_USER_EMAIL).when(objectUnderTest).getEmail();

        /**
         * Run method under test
         */
        objectUnderTest.hotButtonAction();

        /**
         * Verify
         */
        verify(objectUnderTest).sendRecommendationResult(true);
        // verify(objectUnderTest).fetchNextImage();
    }

    @Test
    public void testNotButtonAction()
    {
        /**
         * Prepare
         */
        final MainView objectUnderTest = spy(new MainView());

        doNothing().when(objectUnderTest).sendRecommendationResult(false);
        doNothing().when(objectUnderTest).fetchNextImage();

        doReturn(AUTHENTICATE_USER_EMAIL).when(objectUnderTest).getEmail();

        /**
         * Run method under test
         */
        objectUnderTest.notButtonAction();

        /**
         * Verify
         */
        verify(objectUnderTest).sendRecommendationResult(false);
        // verify(objectUnderTest).fetchNextImage();
    }

    @Test
    public void testSendRecommendationResult()
    {
        /**
         * Prepare
         */
        final MainView objectUnderTest = spy(new MainView());

        final RequestSender requestSender = mock(RequestSender.class);

        when(objectUnderTest.createRateProductImageRequestSender()).thenReturn(requestSender);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final RateProductImageRequest request = (RateProductImageRequest)
                        invocation.getArguments()[0];

                assertThat(request.getEmail()).isEqualTo(AUTHENTICATE_USER_EMAIL);
                assertThat(request.getImageId()).isEqualTo(IMAGE_ID);
                assertThat(request.getProductId()).isEqualTo(PRODUCT_ID);
                assertThat(request.getRating()).isEqualTo(HOT);

                return null;
            }
        }).when(requestSender).sendRequest(isA(RateProductImageRequest.class));

        doReturn(AUTHENTICATE_USER_EMAIL).when(objectUnderTest).getEmail();

        /**
         * Run method under test
         */
        objectUnderTest.setImageId(IMAGE_ID);
        objectUnderTest.setProductId(PRODUCT_ID);
        objectUnderTest.sendRecommendationResult(true);

        /**
         * Verify
         */
        verify(objectUnderTest).createRateProductImageRequestSender();
    }
}
