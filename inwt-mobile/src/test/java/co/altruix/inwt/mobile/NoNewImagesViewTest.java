package co.altruix.inwt.mobile;

import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.UserWantsNewRecommendationsRequest;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class NoNewImagesViewTest {

    public static final String EMAIL = "user@provider.com";

    @Test
    public void test()
    {
        /**
         * Prepare
         */
        final NoNewImagesView objectUnderTest = spy(new NoNewImagesView());
        final RequestSender requestSender = mock(RequestSender.class);

        doReturn(requestSender).when(objectUnderTest).createRequestSender();
        doReturn(EMAIL).when(objectUnderTest).getEmail();

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final UserWantsNewRecommendationsRequest request =
                        (UserWantsNewRecommendationsRequest) invocation.getArguments()[0];

                assertThat(request).isNotNull();
                assertThat(request.getUserEmail()).isEqualTo(EMAIL);

                return null;
            }
        }).when(requestSender).sendRequest(isA(UserWantsNewRecommendationsRequest.class));

        /**
         * Run method under test
         */
        objectUnderTest.requestNewImagesButtonAction();

        /**
         * Verify
         */
        verify(objectUnderTest).createRequestSender();
        verify(requestSender).sendRequest(isA(UserWantsNewRecommendationsRequest.class));
    }
}
