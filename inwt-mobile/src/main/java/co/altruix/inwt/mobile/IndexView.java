package co.altruix.inwt.mobile;

import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.LoginRequest;
import co.altruix.inwt.messages.LoginResponse;
import co.altruix.inwt.messages.SignUpRequest;
import co.altruix.inwt.messages.SignUpResponse;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import static co.altruix.inwt.mobile.util.Util.newImagesAvailble;

@ManagedBean(name = IndexView.NAME)
@SessionScoped
public class IndexView {
    public static final String NAME = "index";
    public static final String SERVER_URL = "http://95.85.28.148:8080/inwt-server/inwt/";
    public static final String AUTHENTICATED_USER_EMAIL = "authenticatedUserEmail";
    public static final String VIEW_ID = "index";
    private String registrationEmail;
    private String loginEmail;
    private String password;

    public String loginButtonAction()
    {
        final LoginRequest request = new LoginRequest();

        request.setEmail(loginEmail);
        request.setPassword(password);

        final RequestSender<LoginRequest,LoginResponse> requestSender = new
                RequestSender<LoginRequest, LoginResponse>(LoginResponse.class, "Login",
                SERVER_URL);

        final LoginResponse response = requestSender.sendRequest(request);

        System.out.println("loginButtonAction, response.success: " + response.isSuccess());

        final FacesContext fc = FacesContext.getCurrentInstance();

        if (response.isSuccess())
        {
            fc.getExternalContext().getSessionMap().put(AUTHENTICATED_USER_EMAIL, loginEmail);

            final boolean newImagesAvailble = newImagesAvailble(loginEmail, SERVER_URL);

            System.out.println("loginButtonAction, newImagesAvailble: " + newImagesAvailble);

            if (newImagesAvailble)
            {
                return MainView.NAME;
            }
            else
            {
                return NoNewImagesView.NAME;
            }
        }
        else
        {
            fc.getExternalContext().getSessionMap().put(AUTHENTICATED_USER_EMAIL, "");
            return VIEW_ID;
        }
    }

    public void registerButtonAction(ActionEvent actionEvent)
    {
        System.out.println("registerButtonAction, registrationEmail: " + registrationEmail);

        final SignUpRequest request = new SignUpRequest();

        request.setEmail(registrationEmail);


        final RequestSender<SignUpRequest,SignUpResponse> requestSender = new
                RequestSender<SignUpRequest, SignUpResponse>(SignUpResponse.class, "SignUp",
                SERVER_URL);

        requestSender.sendRequest(request);
    }

    public String getRegistrationEmail() {
        return registrationEmail;
    }

    public void setRegistrationEmail(final String aRegistrationEmail) {
        registrationEmail = aRegistrationEmail;
    }

    public String getLoginEmail() {
        return loginEmail;
    }

    public void setLoginEmail(final String aLoginEmail) {
        loginEmail = aLoginEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String aPassword) {
        password = aPassword;
    }
}
