package co.altruix.inwt.mobile;

import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.GetNextRecommendedImageRequest;
import co.altruix.inwt.messages.GetNextRecommendedImageResponse;
import co.altruix.inwt.messages.RateProductImageRequest;
import co.altruix.inwt.messages.RateProductImageResponse;
import co.altruix.inwt.messages.RecommendationResult;
import co.altruix.inwt.mobile.util.Util;
import org.primefaces.component.graphicimage.GraphicImage;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import java.io.ByteArrayInputStream;

import static co.altruix.inwt.messages.GetNextRecommendedImageRequest.SERVICE_NAME;
import static co.altruix.inwt.messages.ImageRating.HOT;
import static co.altruix.inwt.messages.ImageRating.NOT;
import static co.altruix.inwt.mobile.IndexView.SERVER_URL;

@ManagedBean(name = MainView.NAME)
@SessionScoped
public class MainView {
    public static final String NAME = "main";
    public static final String IMAGE_COMPONENT_ID = "image";
    public static final String NO_IMAGES_AVAILABLE_LABEL_COMPONENT_ID = "noImagesAvailableLabel";
    public static final String HOT_BUTTON_COMPONENT_ID = "hotButton";
    public static final String NOT_BUTTON_COMPONENT_ID = "notButton";
    public static final String GET_NEXT_IMAGE_COMPONENT_ID = "getNextImage";
    private byte[] currentImageData;
    private byte[] productId;
    private byte[] imageId;
    private String imageFileName;

    public void loadFirstImage()
    {
        fetchNextImage();
    }

    protected void fetchNextImage() {
        final String userEmail = getEmail();
        System.out.println("email: " + userEmail);

        final GetNextRecommendedImageRequest request = new GetNextRecommendedImageRequest();
        request.setUserEmail(userEmail);

        final RequestSender<GetNextRecommendedImageRequest, GetNextRecommendedImageResponse>
                requestSender = createGetNextRecommendedImageRequestSender();

        final GetNextRecommendedImageResponse response = requestSender.sendRequest(request);



        final RecommendationResult result = response.getResult();

        final GraphicImage image = (GraphicImage)findComponent(IMAGE_COMPONENT_ID);

        final UIComponent hotButton = findComponent(HOT_BUTTON_COMPONENT_ID);
        final UIComponent notButton = findComponent(NOT_BUTTON_COMPONENT_ID);

        System.out.println("result: " + result);

        if (RecommendationResult.NEW_IMAGE_AVAILABLE.equals(result))
        {

            setCurrentImageData(response);

            image.setRendered(true);
            hotButton.setRendered(true);
            notButton.setRendered(true);
            setProductId(response.getImage().getUserProductId());
            setImageId(response.getImage().getUserProductImageId());
        }
        else
        {
            System.out.println("Disabling controls");

            System.out.println("hotButton: " + hotButton);
            System.out.println("notButton: " + notButton);

            image.setRendered(false);
            hotButton.setRendered(false);
            notButton.setRendered(false);
        }
    }

    public String hotButtonAction()
    {
        System.out.println("hotButtonAction");
        return hotOrNotButtonPresed(true);
    }

    protected String hotOrNotButtonPresed(final boolean aHot) {
        sendRecommendationResult(aHot);

        if (Util.newImagesAvailble(getEmail(),SERVER_URL))
        {
            return NAME;
        }
        else
        {
            return NoNewImagesView.NAME;
        }
    }

    protected void setProductId(final byte[] aProductId) {
        productId = aProductId;
    }


    protected void setImageId(final byte[] aImageId) {
        imageId = aImageId;
    }



    protected void sendRecommendationResult(final boolean aHot) {
        final RateProductImageRequest request = new RateProductImageRequest();
        request.setRating(aHot ? HOT : NOT);
        request.setProductId(productId);
        request.setImageId(imageId);
        request.setEmail(getEmail());

        final RequestSender<RateProductImageRequest, RateProductImageResponse> requestSender =
                createRateProductImageRequestSender();

        requestSender.sendRequest(request);
    }

    protected String getEmail() {
        return Util.getAuthenticatedUserEmail();
    }

    protected RequestSender<RateProductImageRequest, RateProductImageResponse>
                   createRateProductImageRequestSender() {
        return new
                    RequestSender<RateProductImageRequest, RateProductImageResponse>
                    (RateProductImageResponse.class, RateProductImageRequest.SERVICE_NAME,
                            SERVER_URL);
    }

    public String notButtonAction()
    {
        System.out.println("notButtonAction");
        return hotOrNotButtonPresed(false);
    }

    protected void setCurrentImageData(final GetNextRecommendedImageResponse aResponse) {
        imageFileName = aResponse.getImage().getFileName();
        currentImageData = aResponse.getImage().getImageData();
    }

    protected RequestSender<GetNextRecommendedImageRequest, GetNextRecommendedImageResponse>
        createGetNextRecommendedImageRequestSender() {
        return new RequestSender(GetNextRecommendedImageResponse.class,
            SERVICE_NAME, SERVER_URL);
    }

    public StreamedContent currentImage()
    {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the view. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        }
        else {
            String mimeType = null;

            if (imageFileName.toLowerCase().endsWith(".png"))
            {
                mimeType = "image/png";
            }
            else if (imageFileName.toLowerCase().endsWith(".jpeg") || imageFileName.toLowerCase().endsWith(".jpg"))
            {
                mimeType = "image/jpeg";
            }

            // So, browser is requesting the image. Return a real StreamedContent with the image bytes.
            return new DefaultStreamedContent(new ByteArrayInputStream(currentImageData), mimeType);
        }
    }

    public UIComponent findComponent(String id) {

        UIComponent result = null;
        UIComponent root = FacesContext.getCurrentInstance().getViewRoot();
        if (root != null) {
            result = findComponent(root, id);
        }
        return result;

    }

    private UIComponent findComponent(UIComponent root, String id) {

        UIComponent result = null;
        if (root.getId().equals(id))
            return root;

        for (UIComponent child : root.getChildren()) {
            if (child.getId().equals(id)) {
                result = child;
                break;
            }
            result = findComponent(child, id);
            if (result != null)
                break;
        }
        return result;
    }
}
