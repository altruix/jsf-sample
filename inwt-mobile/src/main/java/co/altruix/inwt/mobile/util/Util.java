package co.altruix.inwt.mobile.util;

import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.NewImageAvailabilityRequest;
import co.altruix.inwt.messages.NewImageAvailabilityResponse;
import co.altruix.inwt.messages.RecommendationResult;
import co.altruix.inwt.mobile.IndexView;

import javax.faces.context.FacesContext;

import static co.altruix.inwt.messages.NewImageAvailabilityRequest.SERVICE_NAME;

public final class Util {
    private Util()
    {

    }

    public static boolean newImagesAvailble(final String aUserEmail, final String aServerUrl)
    {
        final NewImageAvailabilityRequest request = new NewImageAvailabilityRequest();
        request.setUserEmail(aUserEmail);

        final RequestSender<NewImageAvailabilityRequest,NewImageAvailabilityResponse>
                requestSender = new
                RequestSender<NewImageAvailabilityRequest, NewImageAvailabilityResponse>(
                NewImageAvailabilityResponse.class, SERVICE_NAME,
                aServerUrl);
        final NewImageAvailabilityResponse response = requestSender.sendRequest(
                request);

        return RecommendationResult.NEW_IMAGE_AVAILABLE.equals(response.getResult());
    }

    public static String getAuthenticatedUserEmail() {
        final FacesContext fc = FacesContext.getCurrentInstance();

        return (String)fc.getExternalContext().getSessionMap().get(IndexView
                .AUTHENTICATED_USER_EMAIL);
    }
}
