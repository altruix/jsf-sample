package co.altruix.inwt.mobile;

import co.altruix.inwt.common.RequestSender;
import co.altruix.inwt.messages.UserWantsNewRecommendationsRequest;
import co.altruix.inwt.messages.UserWantsNewRecommendationsResponse;
import co.altruix.inwt.mobile.util.Util;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = NoNewImagesView.NAME)
@SessionScoped
public class NoNewImagesView {
    public static final String NAME = "nonewimages";

    public void requestNewImagesButtonAction()
    {
        final RequestSender<UserWantsNewRecommendationsRequest,
                UserWantsNewRecommendationsResponse> requestSender = createRequestSender();

        final UserWantsNewRecommendationsRequest request = new UserWantsNewRecommendationsRequest
                ();

        request.setUserEmail(getEmail());

        requestSender.sendRequest(request);
    }

    protected String getEmail() {
        return Util.getAuthenticatedUserEmail();
    }

    protected RequestSender<UserWantsNewRecommendationsRequest,
            UserWantsNewRecommendationsResponse> createRequestSender() {
        return new RequestSender<UserWantsNewRecommendationsRequest,
        UserWantsNewRecommendationsResponse>(UserWantsNewRecommendationsResponse.class,
        UserWantsNewRecommendationsRequest.SERVICE_NAME, IndexView.SERVER_URL);
    }
}
