package co.altruix.inwt.mobile;

import javax.faces.application.ResourceHandler;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class ImageCorruptionPreventionFilter implements Filter {
    @Override
    public void init(final FilterConfig aFilterConfig) throws ServletException {

    }

    @Override
    public void doFilter(final ServletRequest aServletRequest,
                         final ServletResponse aServletResponse,
                         final FilterChain aFilterChain) throws IOException, ServletException {
        System.out.println("aServletRequest instanceof HttpServletRequest: " +
                (aServletRequest instanceof HttpServletRequest));

        if (aServletRequest instanceof HttpServletRequest)
        {
            final HttpServletRequest request = (HttpServletRequest) aServletRequest;

            System.out.println("request.getRequestURI(): " + request.getRequestURI());
            System.out.println("request.getContextPath(): " + request.getContextPath());

            System.out.println("ResourceHandler.RESOURCE_IDENTIFIER: " +
                    ResourceHandler.RESOURCE_IDENTIFIER);

            final String requestURI = request.getRequestURI().toLowerCase();

            if (!requestURI.endsWith("/javax.faces.resource/dynamiccontent.properties"))
            {
                System.out.println("Applying filter chain");
                aFilterChain.doFilter(aServletRequest, aServletResponse);
            }

        }

    }

    @Override
    public void destroy() {

    }
}
