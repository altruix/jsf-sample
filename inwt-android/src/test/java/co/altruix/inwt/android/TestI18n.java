package co.altruix.inwt.android;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import static org.fest.assertions.api.Assertions.assertThat;

public class TestI18n {
	private final String[] I18N_STRING_NAMES = new String[] { "sign_up_label",
			"greeting", "start", "hotButtonText", "notButtonText",
			"noNewProducts", "sign_up_label", "sign_up_email_label",
			"sign_up_email_label2", "sign_up_button_text",
			"sign_up_password_label1", "sign_up_password_label2",
			"login_button_label", "greeting_sign_up_button_label",
			"greeting_login_button" };

	@Test
	public void testRussianTranslation() throws ParserConfigurationException,
			SAXException, XPathExpressionException, IOException {
		i18nTestLogic("ru");
	}

	@Test
	public void testGermanTranslation() throws ParserConfigurationException,
			SAXException, XPathExpressionException, IOException {
		i18nTestLogic("de");
	}

	@Test
	public void testEnglishTranslation() throws ParserConfigurationException,
			SAXException, XPathExpressionException, IOException {
		i18nTestLogic("en");
	}

	private void i18nTestLogic(final String aLanguageCode)
			throws FactoryConfigurationError, ParserConfigurationException,
			SAXException, IOException, XPathExpressionException {
		final File file = new File("res/values-" + aLanguageCode
				+ "/strings.xml");

		assertThat(file.exists()).isTrue();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(file);
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();

		for (final String stringName : I18N_STRING_NAMES) {
			checkString(doc, xpath, stringName);
		}
	}

	private void checkString(final Document aDoc, final XPath aXpath,
			final String aStringName) throws XPathExpressionException {
		XPathExpression expr = aXpath.compile("//string[@name='" + aStringName
				+ "']");

		final String value = (String) expr
				.evaluate(aDoc, XPathConstants.STRING);

		assertThat(value).isNotEmpty();
	}
}
