package co.altruix.inwt.android;

import co.altruix.inwt.android.server.FakeInwtServer;
import co.altruix.inwt.android.server.IInwtServer;
import co.altruix.inwt.android.server.IProductInfo;
import co.altruix.inwt.android.server.IUserInfo;
import co.altruix.inwt.android.server.UserInfo;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class HotOrNotActivity extends Activity {
	private final IInwtServer server = new FakeInwtServer();
	private final IUserInfo userInfo = new UserInfo();
	private IProductInfo currentProduct;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.altruix.R.layout.hot_or_not_activity);
        
        currentProduct = server.getNextProduct(userInfo);
        
        final Button hotButton = (Button) findViewById(co.altruix.R.id.hotButton);
        
        hotButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(final View aView) {
				hotOrNotButtonPressed(true);
			}
		});
        
        final Button notButton = (Button) findViewById(co.altruix.R.id.notButton);
        
        notButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(final View aView) {
				hotOrNotButtonPressed(false);
			}
		});
    }

	protected void hotOrNotButtonPressed(final boolean aHot) {
		server.saveProductChoice(userInfo, currentProduct, aHot);
		
		if (server.newProductsAvailable(userInfo))
		{
			final IProductInfo newProduct = server.getNextProduct(userInfo);
			
			final ImageView imageView = (ImageView) findViewById(co.altruix.R.id.hotOrNotImage);
			
			imageView.setImageResource(newProduct.getImageId());
		}
		else
		{
			final Intent intent = new Intent(this, NoNewProductsActivity.class);
			startActivity(intent);
		}
	}
}
