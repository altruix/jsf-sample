package co.altruix.inwt.android.server;

import java.util.UUID;

public interface IProductInfo {
	UUID getId();
	void setId(final UUID aId);
	
	int getImageId();
	void setImageId(final int aImageId);
}
