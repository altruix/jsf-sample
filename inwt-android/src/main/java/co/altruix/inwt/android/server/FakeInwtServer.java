package co.altruix.inwt.android.server;

public class FakeInwtServer implements IInwtServer {
	private int curImageIndex = 0;

	@Override
	public void saveProductChoice(final IUserInfo aUserInfo,
			final IProductInfo aProduct, final boolean aHot) {
	}

	@Override
	public boolean newProductsAvailable(final IUserInfo aUserInfo) {
		return curImageIndex < 5;
	}

	@Override
	public IProductInfo getNextProduct(final IUserInfo aUserInfo) {
		final IProductInfo productInfo = new ProductInfo();

		if (curImageIndex == 0) {
			productInfo.setImageId(co.altruix.R.drawable.bracelet_01);
		} else if (curImageIndex == 1) {
			productInfo.setImageId(co.altruix.R.drawable.bracelet_02);
		} else if (curImageIndex == 2) {
			productInfo.setImageId(co.altruix.R.drawable.bracelet_03);
		} else if (curImageIndex == 3) {
			productInfo.setImageId(co.altruix.R.drawable.bracelet_04);
		} else if (curImageIndex == 4) {
			productInfo.setImageId(co.altruix.R.drawable.bracelet_05);
		}

		curImageIndex++;

		return productInfo;
	}
}
