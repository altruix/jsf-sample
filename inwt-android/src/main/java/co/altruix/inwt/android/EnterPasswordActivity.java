package co.altruix.inwt.android;

import java.util.concurrent.ExecutionException;
import co.altruix.inwt.messages.LoginRequest;
import co.altruix.inwt.messages.LoginResponse;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import org.apache.commons.lang3.StringUtils;
import co.altruix.R;
import ru.altruix.commons.android.WebServiceTaskHelperFactory;
import co.altruix.inwt.android.login.ILoginAsyncTask;
import co.altruix.inwt.android.login.ILoginAsyncTaskFactory;
import co.altruix.inwt.android.login.LoginAsyncTaskFactory;
import ru.altruix.commons.android.LoggerWrapper;
import ru.altruix.commons.android.ActivityServerUrlStorage;

import android.app.Activity;
import android.os.Bundle;

public class EnterPasswordActivity extends Activity {
	public final static String EMAIL = "EMAIL";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(co.altruix.R.layout.enter_password_activity);

		final Button loginButton = (Button) findViewById(R.id.login_button2);
		loginButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(final View aView) {
				loginButtonPressed();
			}
		});

	}

	protected void loginButtonPressed() {
		final EditText passwordTextField = (EditText) findViewById(R.id.passwordTextField);
		final String password = passwordTextField.getText().toString();

		if (StringUtils.isEmpty(password)) {
			// Show alert
			showAlert("Login", "Please enter a password");
		} else {
			// Send request
			final LoginRequest request = new LoginRequest();
			request.setEmail(getIntent().getStringExtra(EMAIL));
			request.setPassword(password);

			final ILoginAsyncTaskFactory asyncTaskFactory = new LoginAsyncTaskFactory(
					new WebServiceTaskHelperFactory().create(),
					new LoggerWrapper(getClass().getSimpleName()),
					new ActivityServerUrlStorage(this, R.string.server_address));
			final ILoginAsyncTask task = asyncTaskFactory.createAsyncTask();

			try {
				final LoginResponse response = task.sendRequest(request);

				if (response.isSuccess()) {
					final Intent intent = new Intent(this,
							HotOrNotActivity.class);
					startActivity(intent);
				} else {
					showAlert("Login", "Wrong password");
				}
			} catch (final ExecutionException exception) {
				Log.e(getClass().getSimpleName(), "", exception);
				showAlert("Login", "Something went wrong during authentication");
			} catch (final InterruptedException exception) {
				Log.e(getClass().getSimpleName(), "", exception);
				showAlert("Login", "Something went wrong during authentication");
			}
		}
	}

	private void showAlert(final String aTitle, final String aMessage) {
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle("Login");
		alertDialog.setMessage("Please enter a password");
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new Message());
		alertDialog.show();
	}

}
