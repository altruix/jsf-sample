package co.altruix.inwt.android;

import java.util.concurrent.ExecutionException;
import co.altruix.inwt.messages.LoginRequest;
import co.altruix.inwt.messages.LoginResponse;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import org.apache.commons.lang3.StringUtils;
import co.altruix.R;
import ru.altruix.commons.android.WebServiceTaskHelperFactory;
import co.altruix.inwt.android.login.ILoginAsyncTask;
import co.altruix.inwt.android.login.ILoginAsyncTaskFactory;
import co.altruix.inwt.android.login.LoginAsyncTaskFactory;
import ru.altruix.commons.android.LoggerWrapper;
import ru.altruix.commons.android.ActivityServerUrlStorage;

public class LoginActivity extends Activity {
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.altruix.R.layout.login_activity);
        
    }
}
