package co.altruix.inwt.android;

import java.util.concurrent.ExecutionException;

import co.altruix.inwt.messages.SignUpRequest;
import co.altruix.inwt.messages.SignUpResponse;
import co.altruix.inwt.android.signup.ISignUpAsyncTask;
import co.altruix.inwt.android.signup.ISignUpAsyncTaskFactory;
import co.altruix.inwt.android.signup.SignUpAsyncTaskFactory;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import co.altruix.R;
import ru.altruix.commons.android.WebServiceTaskHelperFactory;
import co.altruix.inwt.android.signup.SignUpAsyncTask;
import co.altruix.inwt.messages.SignUpRequest;
import co.altruix.inwt.messages.SignUpResponse;
import ru.altruix.commons.android.WebServiceTaskHelperFactory;
import ru.altruix.commons.android.LoggerWrapper;
import ru.altruix.commons.android.ActivityServerUrlStorage;
import co.altruix.inwt.messages.SignUpResult;

public class SignUpActivity extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(co.altruix.R.layout.sign_up_activity);

		final Button signUpButton = (Button) findViewById(R.id.sign_up_button);
		signUpButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(final View aView) {
				signUpButtonPressed();
			}
		});
	}

	protected void signUpButtonPressed() {
		final EditText emailTextField = (EditText) findViewById(R.id.email_text_field);
		final String email = emailTextField.getText().toString();
		
		final SignUpRequest request = new SignUpRequest();
		request.setEmail(email);

		final ISignUpAsyncTaskFactory asyncTaskFactory = new SignUpAsyncTaskFactory(
				new WebServiceTaskHelperFactory().create(), new LoggerWrapper(
						getClass().getSimpleName()),
				new ActivityServerUrlStorage(this, R.string.server_address));
		final ISignUpAsyncTask task = asyncTaskFactory.createAsyncTask();

		try {
			final SignUpResponse response = task.sendRequest(request);

			Log.i(getClass().getSimpleName(), "Response received");
			Log.i(getClass().getSimpleName(), "Sucess: " + response.getResult());

			if (SignUpResult.SUCCESS.equals(response.getResult()))
			{
				final Intent intent = new Intent(this, EnterPasswordActivity.class);
				intent.putExtra(EnterPasswordActivity.EMAIL, email);
				startActivity(intent);
			}
			else
			{
				AlertDialog alertDialog = new AlertDialog.Builder(this).create();
				alertDialog.setTitle("Signup");
				alertDialog.setMessage("Something went wrong with your registration");
				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new Message());
				alertDialog.show();
			}
		} catch (final ExecutionException exception) {
			Log.e(getClass().getSimpleName(), "", exception);
		} catch (final InterruptedException exception) {
			Log.e(getClass().getSimpleName(), "", exception);
		}
	}
}
