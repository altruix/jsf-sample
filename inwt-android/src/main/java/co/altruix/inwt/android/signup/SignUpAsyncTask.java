package co.altruix.inwt.android.signup;

import ru.altruix.commons.android.IServerUrlStorage;
import ru.altruix.commons.android.ILogger;
import ru.altruix.commons.android.ResponseParser;
import ru.altruix.commons.android.AbstractAsyncTask;
import ru.altruix.commons.android.IWebServiceTaskHelper;
import co.altruix.inwt.messages.SignUpRequest;
import co.altruix.inwt.messages.SignUpResponse;

public class SignUpAsyncTask extends
		AbstractAsyncTask<SignUpRequest, SignUpResponse> implements
		ISignUpAsyncTask {
	private static final String SERVICE_NAME = "SignUp";

	public SignUpAsyncTask(final IWebServiceTaskHelper aHelper,
			final ILogger aLogger, final IServerUrlStorage aServerUrlStorage) {
		super(aHelper, SignUpResponse.class, aLogger,
				new ResponseParser<SignUpResponse>(), aServerUrlStorage,
				SERVICE_NAME);
	}

}
