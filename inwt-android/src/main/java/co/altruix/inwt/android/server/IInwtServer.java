package co.altruix.inwt.android.server;

public interface IInwtServer {

	void saveProductChoice(final IUserInfo aUserInfo, 
			final IProductInfo aProduct, 
			boolean aHot);

	boolean newProductsAvailable(final IUserInfo userInfo);

	IProductInfo getNextProduct(final IUserInfo aUserInfo);	
}
