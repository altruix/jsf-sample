package co.altruix.inwt.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.util.Log;
import java.util.concurrent.ExecutionException;

import ru.altruix.commons.android.WebServiceTaskHelperFactory;
import co.altruix.inwt.android.signup.ISignUpAsyncTask;
import co.altruix.inwt.android.signup.ISignUpAsyncTaskFactory;
import co.altruix.inwt.android.signup.SignUpAsyncTask;
import co.altruix.inwt.android.signup.SignUpAsyncTaskFactory;
import co.altruix.inwt.messages.SignUpRequest;
import co.altruix.inwt.messages.SignUpResponse;
import ru.altruix.commons.android.WebServiceTaskHelperFactory;
import ru.altruix.commons.android.LoggerWrapper;
import ru.altruix.commons.android.ActivityServerUrlStorage;
import co.altruix.R;

public class GreetingActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.altruix.R.layout.greeting_activity);
                
        final Button signUpButton = (Button) findViewById(R.id.greeting_sign_up_button);
        signUpButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(final View aView) {
				signUpButtonPressed();
			}
        });
    }

	protected void signUpButtonPressed() {
		final Intent intent = new Intent(this, SignUpActivity.class);
		startActivity(intent);
	}
}
