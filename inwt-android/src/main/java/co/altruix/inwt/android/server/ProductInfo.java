package co.altruix.inwt.android.server;

import java.util.UUID;

public class ProductInfo implements IProductInfo {
	private UUID id;
	private int imageId;
	
	public int getImageId() {
		return imageId;
	}

	public void setImageId(final int aImageId) {
		this.imageId = aImageId;
	}

	public UUID getId() {
		return id;
	}

	public void setId(final UUID aId) {
		this.id = aId;
	}
}
