package co.altruix.inwt.android.signup;

import ru.altruix.commons.android.ILogger;
import ru.altruix.commons.android.IServerUrlStorage;
import ru.altruix.commons.android.IWebServiceTaskHelper;

public class SignUpAsyncTaskFactory implements ISignUpAsyncTaskFactory {
	private IWebServiceTaskHelper helper;
	private ILogger logger;
	private IServerUrlStorage serverUrlStorage;

	public SignUpAsyncTaskFactory(final IWebServiceTaskHelper aHelper,
			final ILogger aLogger, final IServerUrlStorage aServerUrlStorage) {
		helper = aHelper;
		logger = aLogger;
		serverUrlStorage = aServerUrlStorage;
	}

	@Override
	public ISignUpAsyncTask createAsyncTask() {
		return new SignUpAsyncTask(helper, logger, serverUrlStorage);
	}

}
