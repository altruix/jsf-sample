package co.altruix.inwt.android.signup;

public interface ISignUpAsyncTaskFactory {
	ISignUpAsyncTask createAsyncTask();
}
