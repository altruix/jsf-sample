package co.altruix.inwt.android.signup;

import java.util.concurrent.ExecutionException;

import ru.altruix.commons.android.IRequestSender;
import co.altruix.inwt.messages.SignUpRequest;
import co.altruix.inwt.messages.SignUpResponse;

public interface ISignUpAsyncTask extends IRequestSender<SignUpRequest, SignUpResponse>
{
	SignUpResponse get() throws InterruptedException, ExecutionException;
}
