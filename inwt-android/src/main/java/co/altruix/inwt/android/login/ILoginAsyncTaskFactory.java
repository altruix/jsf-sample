package co.altruix.inwt.android.login;

public interface ILoginAsyncTaskFactory {
	ILoginAsyncTask createAsyncTask();
}
