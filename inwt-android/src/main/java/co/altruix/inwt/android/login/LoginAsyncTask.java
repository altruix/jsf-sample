package co.altruix.inwt.android.login;

import ru.altruix.commons.android.IServerUrlStorage;
import ru.altruix.commons.android.ILogger;
import ru.altruix.commons.android.ResponseParser;
import ru.altruix.commons.android.AbstractAsyncTask;
import ru.altruix.commons.android.IWebServiceTaskHelper;
import co.altruix.inwt.messages.LoginRequest;
import co.altruix.inwt.messages.LoginResponse;

public class LoginAsyncTask extends
		AbstractAsyncTask<LoginRequest, LoginResponse> implements
		ILoginAsyncTask {
	private static final String SERVICE_NAME = "Login";

	public LoginAsyncTask(final IWebServiceTaskHelper aHelper,
			final ILogger aLogger, final IServerUrlStorage aServerUrlStorage) {
		super(aHelper, LoginResponse.class, aLogger,
				new ResponseParser<LoginResponse>(), aServerUrlStorage,
				SERVICE_NAME);
	}

}
