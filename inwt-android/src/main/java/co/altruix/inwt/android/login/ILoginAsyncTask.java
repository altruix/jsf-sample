package co.altruix.inwt.android.login;

import java.util.concurrent.ExecutionException;

import ru.altruix.commons.android.IRequestSender;
import co.altruix.inwt.messages.LoginRequest;
import co.altruix.inwt.messages.LoginResponse;

public interface ILoginAsyncTask extends IRequestSender<LoginRequest, LoginResponse> {
	LoginResponse get() throws InterruptedException, ExecutionException;
}
