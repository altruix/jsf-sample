package co.altruix.inwt.android.login;

import ru.altruix.commons.android.ILogger;
import ru.altruix.commons.android.IServerUrlStorage;
import ru.altruix.commons.android.IWebServiceTaskHelper;

public class LoginAsyncTaskFactory implements ILoginAsyncTaskFactory {
	private final IWebServiceTaskHelper helper;
	private final ILogger logger;
	private final IServerUrlStorage serverUrlStorage;

	public LoginAsyncTaskFactory(final IWebServiceTaskHelper aHelper,
			final ILogger aLogger, final IServerUrlStorage aServerUrlStorage) {
		helper = aHelper;
		logger = aLogger;
		serverUrlStorage = aServerUrlStorage;
	}
	@Override
	public ILoginAsyncTask createAsyncTask() {
		return new LoginAsyncTask(helper, logger, serverUrlStorage);
	}
}
