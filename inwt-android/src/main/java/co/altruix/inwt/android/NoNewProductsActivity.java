package co.altruix.inwt.android;

import android.app.Activity;
import android.os.Bundle;

public class NoNewProductsActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.altruix.R.layout.no_new_products_activity);
    }

}
