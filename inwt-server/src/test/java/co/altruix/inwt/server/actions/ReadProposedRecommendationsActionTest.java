package co.altruix.inwt.server.actions;

import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import co.altruix.inwt.server.services.recomquality.Recom;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import org.joda.time.DateTime;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.List;
import java.util.Map;

import static co.altruix.inwt.messages.ImageRating.HOT;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_CREATION_DATETIME_UTC;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_HOT_NOT_MARKING_DATETIME_UTC;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_SUBMITTER_EMAIL;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED;
import static java.lang.Boolean.TRUE;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.fest.assertions.data.MapEntry.entry;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class ReadProposedRecommendationsActionTest {

    public static final String USER_EMAIL = "user@provider.com";
    public static final String RECOMMENDER_EMAIL = "submitter@provider.com";
    public static final DateTime CREATION_DATE_TIME = new DateTime(2014, 11, 11, 22, 29);
    public static final DateTime HOT_NOR_MARKING_DATETIME = new DateTime(2014, 11, 12, 22, 29);
    public static final Boolean HOT_OR_NOT = Boolean.TRUE;

    @Test
    public void testRunOnMongo() {
        /**
         * Prepare
         */
        final ReadProposedRecommendationsAction objectUnderTest = spy(new
                ReadProposedRecommendationsAction(USER_EMAIL, RECOMMENDER_EMAIL));

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        final DBCollection recoms = mock(DBCollection.class);

        doReturn(recoms).when(objectUnderTest).getCollection(persistenceState);

        final BasicDBObject query = mock(BasicDBObject.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final Map<String, Object> data = (Map<String, Object>) invocation.getArguments()[0];

                assertThat(data).contains(
                        entry(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL,
                                USER_EMAIL),
                        entry(FIELD_COLLECTION_RECOMMENDATIONS_SUBMITTER_EMAIL,
                                RECOMMENDER_EMAIL),
                        entry(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, TRUE));

                return query;
            }
        }).when(objectUnderTest).createQuery(isA(Map.class));

        final BasicDBObject record = mock(BasicDBObject.class);

        final DBCursor cursor = mock(DBCursor.class);
        when(cursor.hasNext()).thenReturn(true).thenReturn(false);
        when(cursor.next()).thenReturn(record);

        doReturn(cursor).when(recoms).find(query);

        final Recom recom = mock(Recom.class);

        doReturn(recom).when(objectUnderTest).recordToRecom(record);

        /**
         * Run method under test
         */
        final List<Recom> actualResult = objectUnderTest.runOnMongo(persistenceState);

        /**
         * Verify
         */
        assertThat(actualResult).contains(recom);
    }

    @Test
    public void testRecordToRecom()
    {
        /**
         * Prepare
         */
        final ReadProposedRecommendationsAction objectUnderTest = spy(new
                ReadProposedRecommendationsAction(USER_EMAIL, RECOMMENDER_EMAIL));

        final BasicDBObject record = mock(BasicDBObject.class);

        when(record.getDate(FIELD_COLLECTION_RECOMMENDATIONS_CREATION_DATETIME_UTC)).thenReturn
                (CREATION_DATE_TIME.toDate());
        when(record.getDate(FIELD_COLLECTION_RECOMMENDATIONS_HOT_NOT_MARKING_DATETIME_UTC)).
                thenReturn(HOT_NOR_MARKING_DATETIME.toDate());
        when(record.getBoolean(FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER)).
                thenReturn(HOT_OR_NOT);

        /**
         * Run method under test
         */
        final Recom actualResult = objectUnderTest.recordToRecom(record);

        /**
         * Verify
         */
        assertThat(actualResult.getCreationDateTimeUtc()).isEqualTo(CREATION_DATE_TIME);
        assertThat(actualResult.getHotNotMarkingDateTimeUtc()).isEqualTo(HOT_NOR_MARKING_DATETIME);
        assertThat(actualResult.getHotOrNot()).isEqualTo(HOT);
    }
}
