package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.RateProductImageRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import ru.altruix.androidprototyping.server.persistence.IPersistence;

import java.io.IOException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RateProductImageTest {

    public static final String EMAIL = "user@provider.com";

    @Test
    public void testLoggingOfUserBehaviour() throws IOException {
        /**
         * Prepare
         */
        final RateProductImage objectUnderTest = spy(new RateProductImage());
        final ObjectMapper objectMapper = mock(ObjectMapper.class);

        doReturn(objectMapper).when(objectUnderTest).createObjectMapper();

        final RateProductImageRequest request = mock(RateProductImageRequest.class);
        when(request.getEmail()).thenReturn(EMAIL);

        when(objectMapper.readValue(anyString(), eq(RateProductImageRequest.class))).thenReturn
                (request);

        final IPersistence persistence = mock(IPersistence.class);

        when(objectUnderTest.getInwtPersistence()).thenReturn(persistence);

        /**
         * Run method under test
         */
        objectUnderTest.rateProductImage("");

        /**
         * Verify
         */
        verify(objectUnderTest).logUserBehaviour(eq(persistence), eq(EMAIL));
    }
}
