package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.GetAspectStatisticsRequest;
import co.altruix.inwt.model.EbayAspectStatistic;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import co.altruix.inwt.server.services.GetAspectStatistics;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mockingDetails;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GetAspectStatisticsActionTest {

    public static final String ASPECT_VALUE_1 = "Aspect value 1";
    public static final String ASPECT_1 = "Aspect 1";
    public static final String ASPECT_2 = "Aspect 2";
    public static final String ASPECT_VALUE_2 = "Aspect value 2";
    public static final String ASPECT_3 = "Aspect 3";
    public static final String PRODUCT_CATEGORY = "Product category";

    @Test
    public void testGetAspectValue()
    {
        /**
         * Prepare
         */
        final GetAspectStatisticsRequest request = mock(GetAspectStatisticsRequest.class);
        final GetAspectStatisticsAction objectUnderTest = spy(
                new GetAspectStatisticsAction(request));

        final BasicDBList aspectList = new BasicDBList();
        aspectList.add(new BasicDBObject(ASPECT_1, ASPECT_VALUE_1));
        aspectList.add(new BasicDBObject(ASPECT_2, ASPECT_VALUE_2));

        /**
         * Run method under test
         */
        final String aspectValue1 = objectUnderTest.getAspectValue(aspectList, ASPECT_1);
        final String aspectValue2 = objectUnderTest.getAspectValue(aspectList, ASPECT_2);
        final String nonExistentAspectValue = objectUnderTest.getAspectValue(aspectList,
                ASPECT_3);
        /**
         * Verify
         */
        assertThat(aspectValue1).isEqualTo(ASPECT_VALUE_1);
        assertThat(aspectValue2).isEqualTo(ASPECT_VALUE_2);
        assertThat(nonExistentAspectValue).isNull();
    }

    @Test
    public void testGetCurStatistic()
    {
        /**
         * Prepare
         */
        final GetAspectStatisticsRequest request = mock(GetAspectStatisticsRequest.class);
        final GetAspectStatisticsAction objectUnderTest = spy(
                new GetAspectStatisticsAction(request));

        final Map<String, EbayAspectStatistic> aStatsByAspectName = new HashMap<String,
                EbayAspectStatistic>();

        /**
         * Run method under test
         */
        final EbayAspectStatistic actualResult1 = objectUnderTest.getCurStatistic
            (aStatsByAspectName, ASPECT_1, ASPECT_VALUE_1,
                    PRODUCT_CATEGORY);

        /**
         * Verify
         */
        assertThat(actualResult1).isNotNull();
        assertThat(actualResult1.getAspectName()).isEqualTo(ASPECT_1);
        assertThat(actualResult1.getAspect()).isNotNull();
        assertThat(actualResult1.getAspect().getName()).isEqualTo(ASPECT_1);
        assertThat(actualResult1.getAspect().getProductCategory()).isEqualTo(PRODUCT_CATEGORY);
        assertThat(actualResult1.getValue()).isEqualTo(ASPECT_VALUE_1);
        assertThat(actualResult1.getNumberOfDislikedImages()).isEqualTo(0);
        assertThat(actualResult1.getNumberOfLikedImages()).isEqualTo(0);

        /**
         * Run method under test
         */
        final EbayAspectStatistic actualResult2 = objectUnderTest.getCurStatistic
                (aStatsByAspectName, ASPECT_1, ASPECT_VALUE_1,
                        PRODUCT_CATEGORY);

        /**
         * Verify
         */
        assertThat(actualResult2).isSameAs(actualResult1);
    }

    @Test
    public void testComposeStatisticsUsesCorrectProductIdList()
    {
        /**
         * Prepare
         */
        final GetAspectStatisticsRequest request = mock(GetAspectStatisticsRequest.class);
        final GetAspectStatisticsAction objectUnderTest = spy(new GetAspectStatisticsAction
            (request));

        final List<ObjectId> productIds = mock(List.class);

        final BasicDBList rawByteArraysList = mock(BasicDBList.class);

        doReturn(rawByteArraysList).when(objectUnderTest).convertObjectIdListToByteArrayList(
                productIds);

        final DBCollection recoms = mock(DBCollection.class);

        doReturn(recoms).when(objectUnderTest).getRecomsCollection(isA(IMongoPersistenceState
                .class));

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final BasicDBObject query = (BasicDBObject) invocation.getArguments()[0];

                final BasicDBObject inClause = (BasicDBObject) query.get(
                        InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID);

                assertThat(inClause.get("$in")).isSameAs(rawByteArraysList);

                return mock(DBCursor.class);
            }
        }).when(recoms).find(isA(BasicDBObject.class));

        /**
         * Run method under test
         */
        objectUnderTest.composeStatistics(productIds, mock(IMongoPersistenceState.class), "", "",
                "");

        /**
         * Verify
         */
        verify(recoms).find(isA(BasicDBObject.class));
    }

    @Test
    public void testConvertObjectIdListToByteArrayList()
    {
        /**
         * Prepare
         */
        final GetAspectStatisticsRequest request = mock(GetAspectStatisticsRequest.class);
        final GetAspectStatisticsAction objectUnderTest = spy(new GetAspectStatisticsAction
                (request));

        final List<ObjectId> productIds = new LinkedList<ObjectId>();
        final ObjectId objectId = new ObjectId("012345678901".getBytes());
        productIds.add(objectId);

        /**
         * Run method under test
         */
        final BasicDBList actualResult =
                objectUnderTest.convertObjectIdListToByteArrayList(productIds);

        /**
         * Verify
         */
        assertThat(actualResult.get(0)).isEqualTo(objectId.toByteArray());
    }

    @Test
    public void testGetProductAspects()
    {
        /**
         * Prepare
         */
        final GetAspectStatisticsRequest request = mock(GetAspectStatisticsRequest.class);
        final GetAspectStatisticsAction objectUnderTest = spy(new GetAspectStatisticsAction
                (request));

        final byte[] productIdAsByteArray = "012345678901".getBytes();

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        final DBCollection userProductsColl = mock(DBCollection.class);
        doReturn(userProductsColl).when(objectUnderTest).getUserProductsCollection
                (persistenceState);

        // final DBObject product = userProductsColl.findOne(query);
        final DBObject product = mock(DBObject.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final BasicDBObject query = (BasicDBObject) invocation.getArguments()[0];

                assertThat(query.get("_id")).isEqualTo(new ObjectId(productIdAsByteArray));

                return product;
            }
        }).when(userProductsColl).findOne(isA(BasicDBObject.class));

        final BasicDBList aspectList = mock(BasicDBList.class);

        when(product.get(InwtPersistenceAction.FIELD_EBAY_ASPECTS)).thenReturn(aspectList);

        /**
         * Run method under test
         */
        final BasicDBList productAspects = objectUnderTest.getProductAspects(productIdAsByteArray,
                persistenceState);

        /**
         * Verify
         */
        assertThat(productAspects).isSameAs(aspectList);
    }
}
