package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.GetUserProductCategoriesRequest;
import co.altruix.inwt.messages.GetUserProductCategoriesResponse;
import co.altruix.inwt.model.EbayProductCategory;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import co.altruix.inwt.server.persistence.MongoPersistence;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import static co.altruix.inwt.server.actions.InwtPersistenceAction.COLLECTION_USER_PRODUCTS;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class GetUserProductCategoriesActionTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(
            GetUserProductCategoriesActionTest.class);
    public static final String NAME_1 = "Name 1";
    public static final String NAME_1A = "Name 1a";
    public static final String NAME_2 = "Name 2";
    public static final String EMAIL = "user@provider.com";
    public static final EbayProductCategoryComparator EBAY_PRODUCT_CATEGORY_COMPARATOR = new EbayProductCategoryComparator();

    @Test
    @Ignore
    public void test()
    {
        /**
         * Prepare
         */
        final MongoPersistence persistence = new MongoPersistence();
        persistence.init();

        final GetUserProductCategoriesRequest request = new GetUserProductCategoriesRequest();
        request.setUserEmail("dp@altruix.co");
        /**
         * Run method under test
         */
        try
        {
            final GetUserProductCategoriesAction objectUnderTest =
                    new GetUserProductCategoriesAction(request);

            persistence.runAction(objectUnderTest);
        } catch (InterruptedException e) {
            LOGGER.error("", e);
        } finally {
            persistence.shutdown();
        }

        /**
         * Verify
         */

    }

    @Test
    public void testRunOnMongo()
    {
        /**
         * Prepare
         */
        final GetUserProductCategoriesRequest request = mock(GetUserProductCategoriesRequest.class);
        when(request.getUserEmail()).thenReturn(EMAIL);

        final GetUserProductCategoriesAction objectUnderTest = spy(
                new GetUserProductCategoriesAction(request));

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        final DBCollection recoms = mock(DBCollection.class);
        doReturn(recoms).when(objectUnderTest).getCollection(persistenceState,
                InwtPersistenceAction.COLLECTION_RECOMMENDATIONS);

        final List<byte[]> productIdBytes = new LinkedList<byte[]>();
        productIdBytes.add("012345678901".getBytes());
        productIdBytes.add("112345678901".getBytes());

        doReturn(productIdBytes).when(objectUnderTest).getProductIdAsByteArrays(recoms,
                EMAIL);

        final BasicDBList productIds = mock(BasicDBList.class);
        doReturn(productIds).when(objectUnderTest).convertByteArrayToObjectId(productIdBytes);

        final BasicDBObject query = mock(BasicDBObject.class);
        doReturn(query).when(objectUnderTest).createQuery(productIds);

        final DBCollection userProducts = mock(DBCollection.class);
        doReturn(userProducts).when(objectUnderTest).getCollection(persistenceState,
                COLLECTION_USER_PRODUCTS);

        final DBCursor cursor = mock(DBCursor.class);
        when(userProducts.find(query)).thenReturn(cursor);

        final BasicDBObject record1 = new BasicDBObject();
        record1.put(InwtPersistenceAction.FIELD_EBAY_PRIMARY_CATEGORY_ID, "1");
        record1.put(InwtPersistenceAction.FIELD_EBAY_PRIMARY_CATEGORY_NAME, NAME_1);

        final BasicDBObject record2 = new BasicDBObject();
        record2.put(InwtPersistenceAction.FIELD_EBAY_PRIMARY_CATEGORY_ID, "1");
        record2.put(InwtPersistenceAction.FIELD_EBAY_PRIMARY_CATEGORY_NAME, NAME_1A);

        final BasicDBObject record3 = new BasicDBObject();
        record3.put(InwtPersistenceAction.FIELD_EBAY_PRIMARY_CATEGORY_ID, "2");
        record3.put(InwtPersistenceAction.FIELD_EBAY_PRIMARY_CATEGORY_NAME, NAME_2);

        when(cursor.hasNext()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn
                (false);
        when(cursor.next()).thenReturn(record1).thenReturn(record2).thenReturn(record3);

        when(userProducts.find(query)).thenReturn(cursor);

        /**
         * Run method under test
         */
        final GetUserProductCategoriesResponse actualResult = objectUnderTest.runOnMongo(
                persistenceState);

        /**
         * Verify
         */
        assertThat(actualResult).isNotNull();
        assertThat(actualResult.getUserProductCategories().size()).isEqualTo(2);

        final EbayProductCategory category1 = new EbayProductCategory();
        category1.setCategoryId("1");
        category1.setCategoryNames(Arrays.asList(new String[]{NAME_1, NAME_1A}));

        final EbayProductCategory category2 = new EbayProductCategory();
        category2.setCategoryId("2");
        category2.setCategoryNames(Arrays.asList(new String[]{NAME_2}));

        assertThat(actualResult.getUserProductCategories()).usingElementComparator(EBAY_PRODUCT_CATEGORY_COMPARATOR)
                .contains(category1);
        assertThat(actualResult.getUserProductCategories()).usingElementComparator(EBAY_PRODUCT_CATEGORY_COMPARATOR)
                .contains(category2);
    }

    private static class EbayProductCategoryComparator implements Comparator<EbayProductCategory> {
        @Override
        public int compare(final EbayProductCategory o1, final EbayProductCategory o2) {
            boolean idEqual = o1.getCategoryId().equals(o2.getCategoryId());
            boolean namesEqual = o1.getCategoryNames().containsAll(
                    o2.getCategoryNames()) &&
    o2.getCategoryNames().containsAll(o1.getCategoryNames());

            return (idEqual && namesEqual) ? 0 : 1;
        }
    }
}
