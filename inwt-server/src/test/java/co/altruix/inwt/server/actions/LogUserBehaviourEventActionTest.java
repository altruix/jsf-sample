package co.altruix.inwt.server.actions;

import co.altruix.inwt.model.UserBehaviourEvent;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.WriteConcern;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Date;
import java.util.Map;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.fest.assertions.data.MapEntry.entry;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class LogUserBehaviourEventActionTest {

    public static final String EMAIL = "user@provider.com";
    public static final UserBehaviourEvent EVENT = UserBehaviourEvent.REQUESTED_NEW_RECOMMENDATIONS;

    @Test
    public void test()
    {
        /**
         * Prepare
         */
        final LogUserBehaviourEventAction objectUnderTest = spy(new LogUserBehaviourEventAction
                (EMAIL, EVENT));
        final DBCollection userBehaviourLog = mock(DBCollection.class);

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);
        doReturn(userBehaviourLog).when(objectUnderTest).getCollection(persistenceState);

        final Date now = CreateProductAction.getCurrentUtcTime();

        doReturn(now).when(objectUnderTest).getCurrentUtcTime();

        final DBObject record = mock(DBObject.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final Map<String,Object> data = (Map<String, Object>) invocation.getArguments()[0];

                assertThat(data).contains(entry(InwtPersistenceAction
                        .FIELD_COLLECTION_USER_BEHAVIOUR_LOG_TIMESTAMP_UTC, now),
                        entry(InwtPersistenceAction.FIELD_COLLECTION_USER_BEHAVIOUR_LOG_USER_EMAIL,
                                EMAIL),
                        entry(InwtPersistenceAction.FIELD_COLLECTION_USER_BEHAVIOUR_LOG_EVENT_TYPE,
                                EVENT.toString()));

                return record;
            }
        }).when(objectUnderTest).createRecord(isA(Map.class));

        /**
         * Run method under test
         */
        objectUnderTest.runOnMongo(persistenceState);

        /**
         * Verify
         */
        verify(userBehaviourLog).insert(WriteConcern.SAFE, record);
    }
}
