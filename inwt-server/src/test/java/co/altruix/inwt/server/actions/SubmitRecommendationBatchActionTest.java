package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.SubmitRecommendationBatchRequest;
import co.altruix.inwt.server.actions.submitrecommendationbatch.IEbayProductDataSaver;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static co.altruix.inwt.server.actions.InwtPersistenceAction.COLLECTION_RECOMMENDATIONS;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.COLLECTION_RECOMMENDATION_LOG;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.COLLECTION_USER_PRODUCTS;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.COLLECTION_USER_PRODUCT_IMAGES;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_CREATION_DATETIME_UTC;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_USER_IMAGE_ID;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATION_LOG_RECIPIENT_EMAIL;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATION_LOG_SUBMITTER_EMAIL;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATION_LOG_TIMESTAMP;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATION_LOG_MESSAGE;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATION_LOG_PRODUCT_URL;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_PRODUCT_ID;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_USER_PRODUCTS_PRODUCT_URL;
import static java.lang.Boolean.FALSE;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.fest.assertions.data.MapEntry.entry;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SubmitRecommendationBatchActionTest {

    public static final String PRODUCT_URL = "http://www.ebay" +
            ".com/itm/Fashion-Hot-Women-Sterling-Silver-Plated-Beads-Ball-Bangle-Cuff" +
            "-Bracelet-Jewelry-/151389560917?pt=Fashion_Jewelry&hash=item233f856055";
    public static final byte[] PRODUCT_ID = "123456789012".getBytes();
    public static final String RECIPIENT_EMAIL = "recipient@inwt.info";
    public static final String SUBMITTER_EMAIL = "submitter@inwt.info";
    public static final Date TIMESTAMP = new DateTime(2014, 10, 19, 23, 21).toDate();
    public static final byte[] IMAGE_ID = "223456789012".getBytes();

    /**
     * Verifies that processProductUrl does not process a URL, which was already rejected by the
     * recipient more than X % of times the product image was presented to her.
     */
    @Test
    public void testProcessProductUrl1()
    {
        final SubmitRecommendationBatchRequest request = mock
                (SubmitRecommendationBatchRequest.class);
        when(request.getRecommenationRecipientEmail()).thenReturn(RECIPIENT_EMAIL);
        when(request.getSubmitterEmail()).thenReturn(SUBMITTER_EMAIL);

        final SubmitRecommendationBatchActionForTesting objectUnderTest = spy(new
                SubmitRecommendationBatchActionForTesting(request,
                mock(IEbayProductDataSaver.class)));
        final List<Recommendation> recommendations = mock(List.class);
        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        doReturn(PRODUCT_ID).when(objectUnderTest).getProductId(PRODUCT_URL, persistenceState);

        doReturn(3).when(objectUnderTest).getNumberOfImpressions(eq(PRODUCT_ID),
                eq(RECIPIENT_EMAIL), eq(persistenceState));
        doReturn(2).when(objectUnderTest).getNumberOfRejections(eq(PRODUCT_ID),
                eq(RECIPIENT_EMAIL), eq(persistenceState));

        doReturn(TIMESTAMP).when(objectUnderTest).getTimeStampUtc();

        doReturn(mock(DBCollection.class)).when(objectUnderTest).getCollection(eq
                (persistenceState), anyString());

        objectUnderTest.processProductUrl(PRODUCT_URL, recommendations, persistenceState);

        verify(objectUnderTest).saveRecommendationLog(eq(PRODUCT_URL), eq(RECIPIENT_EMAIL),
                eq(SUBMITTER_EMAIL), eq(TIMESTAMP), eq("Product rejected because it was presented" +
                " 3.0 times and rejected 2.0 times."), eq(persistenceState));
    }

    /**
     * Verifies that processProductUrl does not reject a product, which was presented too little
     * number of times (hence the rejections are not meaningful yet).
     */
    @Test
    public void testProcessProductUrl2()
    {
        final SubmitRecommendationBatchRequest request = mock
                (SubmitRecommendationBatchRequest.class);
        when(request.getRecommenationRecipientEmail()).thenReturn(RECIPIENT_EMAIL);
        when(request.getSubmitterEmail()).thenReturn(SUBMITTER_EMAIL);

        final SubmitRecommendationBatchActionForTesting objectUnderTest = spy(new
                SubmitRecommendationBatchActionForTesting(request,
                mock(IEbayProductDataSaver.class)));
        final List<Recommendation> recommendations = mock(List.class);

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        doReturn(PRODUCT_ID).when(objectUnderTest).getProductId(PRODUCT_URL, persistenceState);

        doReturn(0).when(objectUnderTest).getNumberOfImpressions(eq(PRODUCT_ID),
                eq(RECIPIENT_EMAIL), eq(persistenceState));
        doReturn(2).when(objectUnderTest).getNumberOfRejections(eq(PRODUCT_ID),
                eq(RECIPIENT_EMAIL), eq(persistenceState));

        doReturn(new LinkedList<byte[]>()).when(objectUnderTest)
                .getImagesOfProductNotPresentedToUserYet(eq(PRODUCT_ID), eq(RECIPIENT_EMAIL),
                        eq(persistenceState));

        doReturn(TIMESTAMP).when(objectUnderTest).getTimeStampUtc();

        objectUnderTest.processProductUrl(PRODUCT_URL, recommendations, persistenceState);

        verify(objectUnderTest, never()).saveRecommendationLog(eq(PRODUCT_URL), eq(RECIPIENT_EMAIL),
                eq(SUBMITTER_EMAIL), eq(TIMESTAMP), eq("Product rejected because it was presented" +
                " 3.0 times and rejected 2.0 times."), eq(persistenceState));
    }

    @Test
    public void testUrlInvalid()
    {
        final SubmitRecommendationBatchRequest request = mock
                (SubmitRecommendationBatchRequest.class);

        final SubmitRecommendationBatchActionForTesting objectUnderTest = new
                SubmitRecommendationBatchActionForTesting(request,
                mock(IEbayProductDataSaver.class));

        assertThat(objectUnderTest.urlInvalid(null)).isTrue();
        assertThat(objectUnderTest.urlInvalid("  ")).isTrue();
        assertThat(objectUnderTest.urlInvalid("abc")).isTrue();
        assertThat(objectUnderTest.urlInvalid("http://www.nebay" +
                ".com/itm/Fashion-Hot-Women-Sterling-Silver-Plated-Beads-Ball-Bangle-Cuff" +
                "-Bracelet-Jewelry-/151389560917?pt=Fashion_Jewelry&hash=item233f856055")).isTrue();
        assertThat(objectUnderTest.urlInvalid("http://www.someweirdshop" +
                ".com/itm/Fashion-Hot-Women-Sterling-Silver-Plated-Beads-Ball-Bangle-Cuff" +
                "-Bracelet-Jewelry-/151389560917?pt=Fashion_Jewelry&hash=item233f856055")).isTrue();
        assertThat(objectUnderTest.urlInvalid(PRODUCT_URL)).isFalse();
    }

    @Test
    public void testGetImagesOfProductNotPresentedToUserYet()
    {
        final SubmitRecommendationBatchRequest request = mock
                (SubmitRecommendationBatchRequest.class);

        final SubmitRecommendationBatchActionForTesting objectUnderTest = spy(new
                SubmitRecommendationBatchActionForTesting(request,
                mock(IEbayProductDataSaver.class)));

        doReturn(mock(DBCollection.class)).when(objectUnderTest).getCollection(isA
                (IMongoPersistenceState.class), eq(COLLECTION_USER_PRODUCT_IMAGES));

        final byte[] productImageId1 = "productImageId1".getBytes();
        final byte[] productImageId2 = "productImageId2".getBytes();
        final byte[] productImageId3 = "productImageId3".getBytes();
        final byte[] productImageId4 = "productImageId4".getBytes();

        final List<byte[]> productImageIds = new LinkedList<byte[]>();
        productImageIds.add(productImageId1);
        productImageIds.add(productImageId2);
        productImageIds.add(productImageId3);
        productImageIds.add(productImageId4);

        final List<byte[]> shownProductImageIds = new LinkedList<byte[]>();
        shownProductImageIds.add(productImageId2);
        shownProductImageIds.add(productImageId4);

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        doReturn(productImageIds).when(objectUnderTest).getProductImageIds(eq(PRODUCT_ID),
                eq(persistenceState));
        doReturn(shownProductImageIds).when(objectUnderTest).getShownProductImageIds(eq
                (PRODUCT_ID), eq(RECIPIENT_EMAIL), eq(persistenceState));
        doReturn(mock(DBCollection.class)).when(objectUnderTest).getCollection(eq
                (persistenceState), anyString());

        final List<byte[]> actualResult = objectUnderTest.getImagesOfProductNotPresentedToUserYet
            (PRODUCT_ID, RECIPIENT_EMAIL, persistenceState);

        assertThat(actualResult).isNotNull();
        assertThat(actualResult).contains(productImageId1);
        assertThat(actualResult).doesNotContain(productImageId2);
        assertThat(actualResult).contains(productImageId3);
        assertThat(actualResult).doesNotContain(productImageId4);
    }

    @Test
    public void testGetProductIdWhenProductExists()
    {
        final IEbayProductDataSaver ebayProductDataSaver = mock(IEbayProductDataSaver.class);
        final SubmitRecommendationBatchRequest request = mock
                (SubmitRecommendationBatchRequest.class);

        final DBObject product = mock(DBObject.class);
        when(product.get("_id")).thenReturn(new ObjectId(PRODUCT_ID));

        final DBCollection userProductsCollection = mock(DBCollection.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                assertThat(invocation.getArguments().length).isEqualTo(1);

                final Object arg = invocation.getArguments()[0];

                assertThat(arg instanceof BasicDBObject).isTrue();

                final BasicDBObject query = (BasicDBObject) arg;

                assertThat(query.get(FIELD_USER_PRODUCTS_PRODUCT_URL)).
                        isEqualsToByComparingFields(PRODUCT_URL);

                return null;
            }
        }).when(userProductsCollection).findOne(isA(BasicDBObject.class));

        when(userProductsCollection.findOne(isA(BasicDBObject.class))).thenReturn(product);

        final DB db = mock(DB.class);
        when(db.getCollection(COLLECTION_USER_PRODUCTS)).thenReturn
                (userProductsCollection);

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);
        when(persistenceState.getDb()).thenReturn(db);

        final SubmitRecommendationBatchActionForTesting objectUnderTest = spy(new
                SubmitRecommendationBatchActionForTesting(request, ebayProductDataSaver));

        /**
         * Run method under test
         */
        final byte[] actualResult = objectUnderTest.getProductId(PRODUCT_URL, persistenceState);

        /**
         * Verify
         */
        verify(userProductsCollection).findOne(isA(BasicDBObject.class));
        assertThat(actualResult).isEqualTo(PRODUCT_ID);
        verify(ebayProductDataSaver, never()).importEbayProduct(anyString(), eq(persistenceState));
    }

    @Test
    public void testGetProductIdWhenProductDoesNotExist()
    {
        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        final IEbayProductDataSaver ebayProductDataSaver = mock(IEbayProductDataSaver.class);
        when(ebayProductDataSaver.importEbayProduct(eq(PRODUCT_URL),
                eq(persistenceState))).thenReturn(PRODUCT_ID);

        final SubmitRecommendationBatchRequest request = mock
                (SubmitRecommendationBatchRequest.class);

        final DBCollection userProductsCollection = mock(DBCollection.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                assertThat(invocation.getArguments().length).isEqualTo(1);

                final Object arg = invocation.getArguments()[0];

                assertThat(arg instanceof BasicDBObject).isTrue();

                final BasicDBObject query = (BasicDBObject) arg;

                assertThat(query.get(FIELD_USER_PRODUCTS_PRODUCT_URL)).
                        isEqualsToByComparingFields(PRODUCT_URL);

                return null;
            }
        }).when(userProductsCollection).findOne(isA(BasicDBObject.class));


        when(userProductsCollection.findOne(isA(BasicDBObject.class))).thenReturn(null);

        final DB db = mock(DB.class);
        when(db.getCollection(COLLECTION_USER_PRODUCTS)).thenReturn
                (userProductsCollection);

        when(persistenceState.getDb()).thenReturn(db);

        final SubmitRecommendationBatchActionForTesting objectUnderTest = spy(new
                SubmitRecommendationBatchActionForTesting(request, ebayProductDataSaver));

        /**
         * Run method under test
         */
        final byte[] actualResult = objectUnderTest.getProductId(PRODUCT_URL, persistenceState);

        /**
         * Verify
         */
        verify(userProductsCollection).findOne(isA(BasicDBObject.class));
        assertThat(actualResult).isEqualTo(PRODUCT_ID);
        verify(ebayProductDataSaver).importEbayProduct(eq(PRODUCT_URL), eq(persistenceState));
    }

    @Test
    public void testGetProductId1()
    {
        /**
         * Prepare
         */
        final SubmitRecommendationBatchRequest request =
                mock(SubmitRecommendationBatchRequest.class);
        final IEbayProductDataSaver ebayProductDataSaver = mock(IEbayProductDataSaver.class);

        final ObjectId objectId = new ObjectId(PRODUCT_ID);

        final DBObject product = mock(DBObject.class);
        when(product.get("_id")).thenReturn(objectId);

        final DBCollection userProductsColl = mock(DBCollection.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final BasicDBObject query = (BasicDBObject)invocation.getArguments()[0];

                assertThat(query.get(FIELD_USER_PRODUCTS_PRODUCT_URL)).isEqualTo(PRODUCT_URL);

                return product;
            }
        }).when(userProductsColl).findOne(isA(BasicDBObject.class));

        final DB db = mock(DB.class);
        when(db.getCollection(COLLECTION_USER_PRODUCTS)).thenReturn(userProductsColl);

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);
        when(persistenceState.getDb()).thenReturn(db);

        final SubmitRecommendationBatchAction objectUnderTest = spy(new
                SubmitRecommendationBatchAction(request, ebayProductDataSaver));

        /**
         * Run method under test
         */
        final byte[] actualResult = objectUnderTest.getProductId(PRODUCT_URL, persistenceState);

        /**
         * Verify
         */
        assertThat(actualResult).isEqualTo(PRODUCT_ID);
        verify(userProductsColl).findOne(isA(BasicDBObject.class));
        verify(ebayProductDataSaver, never()).importEbayProduct(eq(PRODUCT_URL),
                eq(persistenceState));
    }

    @Test
    public void testGetProductId2()
    {
        /**
         * Prepare
         */
        final SubmitRecommendationBatchRequest request =
                mock(SubmitRecommendationBatchRequest.class);
        final IEbayProductDataSaver ebayProductDataSaver = mock(IEbayProductDataSaver.class);


        final DBCollection userProductsColl = mock(DBCollection.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final BasicDBObject query = (BasicDBObject)invocation.getArguments()[0];

                assertThat(query.get(FIELD_USER_PRODUCTS_PRODUCT_URL)).isEqualTo(PRODUCT_URL);

                return null;
            }
        }).when(userProductsColl).findOne(isA(BasicDBObject.class));

        final DB db = mock(DB.class);
        when(db.getCollection(COLLECTION_USER_PRODUCTS)).thenReturn(userProductsColl);

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);
        when(persistenceState.getDb()).thenReturn(db);

        when(ebayProductDataSaver.importEbayProduct(eq(PRODUCT_URL),
                eq(persistenceState))).thenReturn(PRODUCT_ID);


        final SubmitRecommendationBatchAction objectUnderTest = spy(new
                SubmitRecommendationBatchAction(request, ebayProductDataSaver));

        /**
         * Run method under test
         */
        final byte[] actualResult = objectUnderTest.getProductId(PRODUCT_URL, persistenceState);

        /**
         * Verify
         */
        assertThat(actualResult).isEqualTo(PRODUCT_ID);
        verify(userProductsColl).findOne(isA(BasicDBObject.class));
        verify(ebayProductDataSaver).importEbayProduct(eq(PRODUCT_URL),
                eq(persistenceState));
    }
    @Test
    public void testGetNumberOfRejections()
    {
        /**
         * Prepare
         */
        final SubmitRecommendationBatchRequest request = mock(SubmitRecommendationBatchRequest.class);
        final SubmitRecommendationBatchAction objectUnderTest = spy(new
                SubmitRecommendationBatchAction(request));

        final BasicDBObject query = mock(BasicDBObject.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock aInvocation) throws Throwable {
                final Map<String,Object> data = (Map<String,Object>)aInvocation.getArguments()[0];

                assertThat(data.keySet().size()).isEqualTo(4);

                assertThat(data).contains(
                        entry(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL, RECIPIENT_EMAIL),
                        entry(FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID, PRODUCT_ID),
                        entry(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, Boolean.TRUE),
                        entry(FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER,
                                Boolean.FALSE));
                return query;
            }
        }).when(objectUnderTest).createBasicDBObject(isA(Map.class));

        final DBCursor queryResult = mock(DBCursor.class);
        when(queryResult.count()).thenReturn(31331);

        final DBCollection recomColl = mock(DBCollection.class);
        when(recomColl.find(eq(query))).thenReturn(queryResult);

        final DB db = mock(DB.class);
        when(db.getCollection(eq(COLLECTION_RECOMMENDATIONS))).thenReturn(recomColl);

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);
        when(persistenceState.getDb()).thenReturn(db);
        /**
         * Run method under test
         */
         final int actualResult = objectUnderTest.getNumberOfRejections(PRODUCT_ID,
                 RECIPIENT_EMAIL, persistenceState);
        /**
         * Verify
         */
        verify(objectUnderTest).createBasicDBObject(isA(Map.class));
        assertThat(actualResult).isEqualTo(31331);
    }

    @Test
    public void testGetNumberOfImpressions()
    {
        /**
         * Prepare
         */
        final SubmitRecommendationBatchRequest request = mock(SubmitRecommendationBatchRequest.class);
        final SubmitRecommendationBatchAction objectUnderTest = spy(new
                SubmitRecommendationBatchAction(request));

        final BasicDBObject query = mock(BasicDBObject.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock aInvocation) throws Throwable {
                final Map<String,Object> data = (Map<String,Object>)aInvocation.getArguments()[0];

                assertThat(data.keySet().size()).isEqualTo(3);

                assertThat(data).contains(
                        entry(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL, RECIPIENT_EMAIL),
                        entry(FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID, PRODUCT_ID),
                        entry(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, Boolean.TRUE));
                return query;
            }
        }).when(objectUnderTest).createBasicDBObject(isA(Map.class));

        final DBCursor queryResult = mock(DBCursor.class);
        when(queryResult.count()).thenReturn(31331);

        final DBCollection recomColl = mock(DBCollection.class);
        when(recomColl.find(eq(query))).thenReturn(queryResult);

        final DB db = mock(DB.class);
        when(db.getCollection(eq(COLLECTION_RECOMMENDATIONS))).thenReturn(recomColl);

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);
        when(persistenceState.getDb()).thenReturn(db);
        /**
         * Run method under test
         */
        final int actualResult = objectUnderTest.getNumberOfImpressions(PRODUCT_ID,
                RECIPIENT_EMAIL, persistenceState);
        /**
         * Verify
         */
        verify(objectUnderTest).createBasicDBObject(isA(Map.class));
        assertThat(actualResult).isEqualTo(31331);
    }

    @Test
    public void testGetShownProductImageIds()
    {
        /**
         * Prepare
         */
        final SubmitRecommendationBatchRequest request = mock(SubmitRecommendationBatchRequest.class);
        final SubmitRecommendationBatchAction objectUnderTest = spy(new
                SubmitRecommendationBatchAction(request));
        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        final BasicDBObject query = mock(BasicDBObject.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final Map<String,Object> queryMap = (Map<String,Object>)invocation.getArguments()
                        [0];

                assertThat(queryMap.keySet().size()).isEqualTo(3);

                assertThat(queryMap).contains(
                        entry(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL, RECIPIENT_EMAIL),
                        entry(FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID, PRODUCT_ID),
                        entry(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, Boolean.TRUE));

                return query;
            }
        }).when(objectUnderTest).createBasicDBObject(isA(Map.class));

        final BasicDBObject record = mock(BasicDBObject.class);
        when(record.get(FIELD_COLLECTION_RECOMMENDATIONS_USER_IMAGE_ID))
                .thenReturn(IMAGE_ID);

        final DBCursor cursor = mock(DBCursor.class);
        when(cursor.hasNext()).thenReturn(true).thenReturn(false);
        when(cursor.next()).thenReturn(record);

        final DBCollection recomCollection = mock(DBCollection.class);
        when(recomCollection.find(eq(query))).thenReturn(cursor);

        doReturn(recomCollection).when(objectUnderTest).getCollection(eq(persistenceState),
                eq(COLLECTION_RECOMMENDATIONS));

        /**
         * Run method under test
         */
        final List<byte[]> actualResult = objectUnderTest.getShownProductImageIds(
                PRODUCT_ID, RECIPIENT_EMAIL, persistenceState);

        /**
         * Verify
         */
        verify(objectUnderTest).createBasicDBObject(isA(Map.class));
        verify(objectUnderTest).getCollection(eq(persistenceState),
                eq(COLLECTION_RECOMMENDATIONS));
        verify(cursor, times(2)).hasNext();
        verify(cursor).next();
        verify(record).get(eq(FIELD_COLLECTION_RECOMMENDATIONS_USER_IMAGE_ID));
        assertThat(actualResult).isNotNull();
        assertThat(actualResult.size()).isEqualTo(1);
        assertThat(actualResult).contains(IMAGE_ID);
    }

    @Test
    public void testGetProductImageIds()
    {
        /**
         * Prepare
         */
        final SubmitRecommendationBatchRequest request = mock(SubmitRecommendationBatchRequest.class);

        final SubmitRecommendationBatchAction objectUnderTest = spy(new
                SubmitRecommendationBatchAction(request));

        final BasicDBObject query = mock(BasicDBObject.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final Map<String,Object> queryData = (Map<String,Object>)invocation.getArguments
                        ()[0];

                assertThat(queryData).isNotNull();
                assertThat(queryData).contains(entry(
                        FIELD_COLLECTION_USER_PRODUCT_IMAGES_PRODUCT_ID, PRODUCT_ID));

                return query;
            }
        }).when(objectUnderTest).createBasicDBObject(isA(Map.class));


        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        final DBCollection imagesColl = mock(DBCollection.class);

        doReturn(imagesColl).when(objectUnderTest).getCollection(eq(persistenceState),
                eq(COLLECTION_USER_PRODUCT_IMAGES));

        final ObjectId objectId = mock(ObjectId.class);
        when(objectId.toByteArray()).thenReturn(IMAGE_ID);

        final DBObject record = mock(DBObject.class);
        when(record.get("_id")).thenReturn(objectId);

        final DBCursor cursor = mock(DBCursor.class);
        when(cursor.hasNext()).thenReturn(true).thenReturn(false);
        when(cursor.next()).thenReturn(record);

        doReturn(cursor).when(imagesColl).find(eq(query));

        /**
         * Run method under test
         */
        final List<byte[]> actualResult = objectUnderTest.getProductImageIds(PRODUCT_ID,
                persistenceState);
        /**
         * Verify
         */
        assertThat(actualResult).isNotNull();
        assertThat(actualResult).contains(IMAGE_ID);
    }

    @Test
    public void testMakeSureEachProductHasOnly2Images()
    {
        /**
         * Prepare
         */
        final SubmitRecommendationBatchRequest request = mock(SubmitRecommendationBatchRequest.class);

        final SubmitRecommendationBatchAction objectUnderTest = spy(new
                SubmitRecommendationBatchAction(request));


        final byte[] productId1 = "productId1".getBytes();
        final byte[] productId2 = "productId2".getBytes();

        final byte[] imageId1 = "imageId1".getBytes();
        final byte[] imageId2 = "imageId2".getBytes();
        final byte[] imageId3 = "imageId3".getBytes();

        final byte[] imageId4 = "imageId4".getBytes();
        final byte[] imageId5 = "imageId5".getBytes();

        final Recommendation recom1 = new Recommendation(imageId1, productId1);
        final Recommendation recom2 = new Recommendation(imageId2, productId1);
        final Recommendation recom3 = new Recommendation(imageId3, productId1);

        final Recommendation recom4 = new Recommendation(imageId4, productId2);
        final Recommendation recom5 = new Recommendation(imageId5, productId2);

        final List<Recommendation> originalList = new LinkedList<Recommendation>();

        originalList.add(recom1);
        originalList.add(recom2);
        originalList.add(recom3);
        originalList.add(recom4);
        originalList.add(recom5);

        /**
         * Run method under test
         */

        final List<Recommendation> filteredList = objectUnderTest.makeSureEachProductHasOnly2Images(
                originalList);

        /**
         * Verify
         */
        // assertThat(filteredList.size()).isEqualTo(4);
        assertThat(filteredList).contains(recom1);
        assertThat(filteredList).contains(recom2);
        assertThat(filteredList).contains(recom4);
        assertThat(filteredList).contains(recom5);
    }

    @Test
    public void testInsertRecommendations()
    {
        /**
         * Prepare
         */
        final SubmitRecommendationBatchRequest request = mock(SubmitRecommendationBatchRequest.class);

        final SubmitRecommendationBatchAction objectUnderTest = spy(new
                SubmitRecommendationBatchAction(request));

        final byte[] productId1 = "productId1".getBytes();
        final byte[] productId2 = "productId2".getBytes();

        final byte[] imageId1 = "imageId1".getBytes();

        final byte[] imageId4 = "imageId4".getBytes();

        final Recommendation recom1 = new Recommendation(imageId1, productId1);
        final Recommendation recom2 = new Recommendation(imageId4, productId2);

        final List<Recommendation> originalList = new LinkedList<Recommendation>();

        originalList.add(recom1);
        originalList.add(recom2);

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        final DBCollection recomColl = mock(DBCollection.class);

        doReturn(recomColl).when(objectUnderTest).getCollection(eq(persistenceState),
                eq(COLLECTION_RECOMMENDATIONS));

        final BasicDBObject record1 = mock(BasicDBObject.class);
        final BasicDBObject record2 = mock(BasicDBObject.class);

        doReturn(record1).when(objectUnderTest).createRecomRecord(eq(recom1), eq(RECIPIENT_EMAIL),
                eq(SUBMITTER_EMAIL));
        doReturn(record2).when(objectUnderTest).createRecomRecord(eq(recom2), eq(RECIPIENT_EMAIL),
                eq(SUBMITTER_EMAIL));

        /**
         * Run method under test
         */
        objectUnderTest.insertRecommendations(originalList, persistenceState, RECIPIENT_EMAIL,
                SUBMITTER_EMAIL);

        /**
         * Verify
         */
        verify(recomColl).insert(eq(record1));
        verify(recomColl).insert(eq(record2));
    }

    @Test
    public void testCreateRecomRecord()
    {
        /**
         * Prepare
         */
        final SubmitRecommendationBatchRequest request = mock(SubmitRecommendationBatchRequest.class);

        final SubmitRecommendationBatchAction objectUnderTest = spy(new
                SubmitRecommendationBatchAction(request));

        final BasicDBObject record = mock(BasicDBObject.class);

        final Date now = CreateProductAction.getCurrentUtcTime();

        doReturn(now).when(objectUnderTest).getTimeStampUtc();

        final Recommendation recom = new Recommendation(IMAGE_ID, PRODUCT_ID);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final Map<String,Object> data = (Map<String,Object>)invocation.getArguments()[0];

                assertThat(data).isNotNull();
                assertThat(data).contains(
                entry(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL, RECIPIENT_EMAIL),
                entry(FIELD_COLLECTION_RECOMMENDATIONS_USER_IMAGE_ID, IMAGE_ID),
                entry(FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID, PRODUCT_ID),
                entry(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, FALSE),
                entry(FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER, FALSE),
                entry(FIELD_COLLECTION_RECOMMENDATIONS_CREATION_DATETIME_UTC, now));

                return record;
            }
        }).when(objectUnderTest).createBasicDBObject(isA(Map.class));

        /**
         * Run method under test
         */
        final BasicDBObject actualResult = objectUnderTest.createRecomRecord(recom,
                RECIPIENT_EMAIL, SUBMITTER_EMAIL);

        /**
         * Verify
         */
        assertThat(actualResult).isSameAs(record);
        verify(objectUnderTest).createBasicDBObject(isA(Map.class));
    }

    @Test
    public void testSaveRecommendationLog()
    {
        /**
         * Prepare
         */
        final SubmitRecommendationBatchRequest request = mock(SubmitRecommendationBatchRequest.class);

        final SubmitRecommendationBatchAction objectUnderTest = spy(new
                SubmitRecommendationBatchAction(request));

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        final Date now = CreateProductAction.getCurrentUtcTime();

        final BasicDBObject record = mock(BasicDBObject.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final Map<String,Object> data = (Map<String,Object>)invocation.getArguments()[0];

                assertThat(data).isNotNull();
                assertThat(data).contains(
                        entry(FIELD_COLLECTION_RECOMMENDATION_LOG_SUBMITTER_EMAIL, SUBMITTER_EMAIL),
                        entry(FIELD_COLLECTION_RECOMMENDATION_LOG_RECIPIENT_EMAIL, RECIPIENT_EMAIL),
                        entry(FIELD_COLLECTION_RECOMMENDATION_LOG_TIMESTAMP, now),
                        entry(FIELD_COLLECTION_RECOMMENDATION_LOG_MESSAGE, "someMessage"),
                        entry(FIELD_COLLECTION_RECOMMENDATION_LOG_PRODUCT_URL,
                                PRODUCT_URL));
                return record;
            }
        }).when(objectUnderTest).createBasicDBObject(isA(Map.class));

        final DBCollection recomColl = mock(DBCollection.class);

        doReturn(recomColl).when(objectUnderTest).getCollection(eq(persistenceState),
                eq(COLLECTION_RECOMMENDATION_LOG));

        /**
         * Run method under test
         */
        objectUnderTest.saveRecommendationLog(PRODUCT_URL, RECIPIENT_EMAIL, SUBMITTER_EMAIL, now,
         "someMessage", persistenceState);

        /**
         * Verify
         */
        verify(objectUnderTest).createBasicDBObject(isA(Map.class));
        verify(objectUnderTest).getCollection(eq(persistenceState),
                eq(COLLECTION_RECOMMENDATION_LOG));
        verify(recomColl).insert(eq(record));
    }

    @Test
    public void testSubmitterIsSaved()
    {
        /**
         * Prepare
         */
        final SubmitRecommendationBatchRequest request =
                mock(SubmitRecommendationBatchRequest.class);

        final SubmitRecommendationBatchAction objectUnderTest = spy(new
                SubmitRecommendationBatchAction(request));

        /**
         * Run method under test
         */
        final BasicDBObject record = objectUnderTest.createRecomRecord(mock(Recommendation.class),
                RECIPIENT_EMAIL, SUBMITTER_EMAIL);

        /**
         * Verify
         */
        assertThat(record.get(InwtPersistenceAction
                .FIELD_COLLECTION_RECOMMENDATIONS_SUBMITTER_EMAIL)).isEqualTo(SUBMITTER_EMAIL);
    }

    @Test
    @Ignore
    public void test()
    {
        /**
         * Prepare
         */

        /**
         * Run method under test
         */

        /**
         * Verify
         */
    }

}
