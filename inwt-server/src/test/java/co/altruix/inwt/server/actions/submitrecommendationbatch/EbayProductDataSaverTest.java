package co.altruix.inwt.server.actions.submitrecommendationbatch;

import co.altruix.inwt.server.actions.CreateProductAction;
import co.altruix.inwt.server.actions.InwtPersistenceAction;
import co.altruix.inwt.server.actions.SubmitRecommendationBatchActionTest;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.WriteConcern;
import com.mongodb.WriteResult;
import org.bson.types.Binary;
import org.bson.types.ObjectId;
import org.fest.assertions.data.MapEntry;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static co.altruix.inwt.server.actions.InwtPersistenceAction.COLLECTION_USER_PRODUCT_IMAGES;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_CREATOR_EMAIL;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_DATA;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_FILE_NAME;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_IS_DELETED;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_MIME_TYPE;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_PRODUCT_ID;
import static co.altruix.inwt.server.actions.submitrecommendationbatch.EbayProductDataSaver.MIME_TYPE_JPEG;
import static co.altruix.inwt.server.actions.submitrecommendationbatch.EbayProductDataSaver.MIME_TYPE_PNG;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.fest.assertions.data.MapEntry.entry;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EbayProductDataSaverTest {

    public static final byte[] PRODUCT_ID = "123456789012".getBytes();
    public static final String IMAGE_URL = "http://i.ebayimg.com/00/s/NTAwWDUwMA==/z/K5QAAOSwVFlT~U-6/$_1\" +\n" +
            "                \".JPG?set_id=8800005007";

    @Test
    @Ignore
    public void testSunnyDay() {
        final EbayProductDataSaver objectUnderTest = new EbayProductDataSaver();

        objectUnderTest.importEbayProduct(SubmitRecommendationBatchActionTest.PRODUCT_URL,
                mock(IMongoPersistenceState.class));
    }

    @Test
    @Ignore
    public void testSunnyDay2() {
        final EbayProductDataSaver objectUnderTest = new EbayProductDataSaver();

        objectUnderTest.importEbayProduct(
                "http://www.ebay.com/itm/New-Nice-Charm-Rhinestone-Pearl-Crystal-Bear-Golden-Chain-Coin-Bracelet-Bangle-/380958804608?pt=Fashion_Jewelry&hash=item58b2ea0a80",
                mock(IMongoPersistenceState.class));
    }

    @Test
    @Ignore
    public void testSunnyDay3() {
        final EbayProductDataSaver objectUnderTest = new EbayProductDataSaver();

        objectUnderTest.importEbayProduct(
                "http://www.ebay.com/itm/Hot-New-Design-Fashion-Charm-Blue-Resin-Statement-Gemstone-Dangle-Stud-Earrings-/400776883607?pt=Fashion_Jewelry&hash=item5d5029ed97",
                mock(IMongoPersistenceState.class));
    }


    @Test
    public void testExtractItemId() {
        final EbayProductDataSaver objectUnderTest = spy(new EbayProductDataSaver());

        assertThat(objectUnderTest.extractItemId("http://www.ebay" +
                ".com/itm/Fashion-Hot-Women-" + "Sterling-Silver-Plated-Beads-Ball-Bangle-Cuff" +
                "-Bracelet-Jewelry-/151389560917")).isEqualTo("151389560917");
        assertThat(objectUnderTest.extractItemId("http://www.ebay" +
                ".com/itm/Fashion-Hot-Women-" +
                "Sterling-Silver-Plated-Beads-Ball-Bangle-Cuff-Bracelet-Jewelry-/" + "151389560917" +
                "?pt=Fashion_Jewelry&hash=item233f856055")).isEqualTo("151389560917");
    }

    @Test
    public void testSaveProductData() {
        saveProductDataTestLogic(createConvertedCurrentPrice());
    }

    private void saveProductDataTestLogic(final ConvertedCurrentPrice aConvertedCurrentPrice) {
        /**
         * Prepare
         */

        final DB db = mock(DB.class);

        final DBCollection imagesColl = mock(DBCollection.class);

        when(db.getCollection(eq(InwtPersistenceAction.COLLECTION_USER_PRODUCT_IMAGES))).thenReturn
                (imagesColl);

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);
        when(persistenceState.getDb()).thenReturn(db);

        final EbayItemData ebayItemData = createEbayItemData(aConvertedCurrentPrice);

        final EbayProductDataSaver objectUnderTest = spy(new EbayProductDataSaver());
        doReturn(PRODUCT_ID).when(objectUnderTest).saveProductDataExceptImages(eq
                (ebayItemData), eq(persistenceState));


        /**
         * Invoke method under test
         */

        final byte[] newProductId = objectUnderTest.saveProductData(ebayItemData,
                persistenceState);

        /**
         * Verify
         */
        assertThat(newProductId).isEqualTo(PRODUCT_ID);
        verify(objectUnderTest).saveProductDataExceptImages(eq(ebayItemData), eq(persistenceState));
        verify(objectUnderTest).saveProductImages(eq(PRODUCT_ID), eq(ebayItemData.getPictureUrls
                ()), eq(persistenceState));
    }

    private EbayItemData createEbayItemData(final ConvertedCurrentPrice aConvertedCurrentPrice) {

        final List<String> pictureUrls = new LinkedList<String>();
        pictureUrls.add("http://i.ebayimg.com/00/s/NTAwWDUwMA==/z/K5QAAOSwVFlT~U-6/$_1" +
                ".JPG?set_id=8800005007");
        pictureUrls.add(
                "http://i.ebayimg.com/00/s/ODAwWDgwMA==/z/32AAAOSwxCxT9umJ/$_12.JPG?set_id=880000500F");

        final EbayItemData ebayItemData = new EbayItemData();
        ebayItemData.setTitle(
                "Fashion Hot Women Sterling Silver Plated Beads Ball Bangle Cuff Bracelet Jewelry");
        ebayItemData.setConvertedCurrentPrice(aConvertedCurrentPrice);
        ebayItemData.setPrimaryCategoryName("Jewelry & Watches:Fashion Jewelry:Bracelets");
        ebayItemData.setPrimaryCategoryID("50637");
        ebayItemData.setPictureUrls(pictureUrls);
        ebayItemData.setItemId("151389560917");
        ebayItemData.setListingType("FixedPriceItem");
        ebayItemData.setViewItemURLForNaturalSearch(
                "http://www.ebay.com/itm/Fashion-Hot-Women-Sterling-Silver-Plated-Beads-Ball-Bangle-Cuff-Bracelet-Jewelry-/151389560917");
        return ebayItemData;
    }

    private ConvertedCurrentPrice createConvertedCurrentPrice() {
        final ConvertedCurrentPrice convertedCurrentPrice = new ConvertedCurrentPrice();
        convertedCurrentPrice.setAmount(new BigDecimal("1.98"));
        convertedCurrentPrice.setCurrencyId("USD");
        return convertedCurrentPrice;
    }

    @Test
    public void testSaveProductDataExceptImages() {
        saveProductDataExceptImagesTestLogic(createConvertedCurrentPrice(), 198,
                "USD");

    }

    private void saveProductDataExceptImagesTestLogic(
            final ConvertedCurrentPrice aConvertedCurrentPrice, final Integer aExpectedPrice,
            final String aExpectedCurrency) {
        /**
         * Prepare
         */
        final EbayProductDataSaver objectUnderTest = spy(new EbayProductDataSaver());

        final Date now = CreateProductAction.getCurrentUtcTime();
        doReturn(now).when(objectUnderTest).getCurrentUtcTime();

        final BasicDBObject newProduct = mock(BasicDBObject.class);
        when(newProduct.get("_id")).thenReturn(new ObjectId(PRODUCT_ID));

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                assertThat(invocation.getArguments().length).isEqualTo(1);
                assertThat(invocation.getArguments()[0] instanceof Map).isTrue();

                final Map<String, Object> map = (Map<String, Object>) invocation.getArguments()[0];

                assertThat(
                        map.get(InwtPersistenceAction.FIELD_USER_PRODUCTS_CREATOR_EMAIL)).isEqualTo(
                        EbayProductDataSaver.CREATOR_EMAIL);
                assertThat(map.get(InwtPersistenceAction.FIELD_USER_PRODUCTS_NAME)).isEqualTo(
                        "Fashion Hot Women Sterling Silver Plated Beads Ball Bangle Cuff Bracelet Jewelry");
                assertThat(
                        map.get(InwtPersistenceAction.FIELD_USER_PRODUCTS_CREATION_DATETIME_UTC)).isEqualTo(
                        now);
                assertThat(map.get(InwtPersistenceAction
                        .FIELD_USER_PRODUCTS_PRODUCT_URL)).isEqualTo(
                        "http://www.ebay.com/itm/Fashion-Hot-Women-Sterling-Silver-Plated-Beads-Ball-Bangle-Cuff-Bracelet-Jewelry-/151389560917");

                assertThat(map.get(InwtPersistenceAction.FIELD_EBAY_ITEM_ID)).isEqualTo(
                        "151389560917");
                assertThat(
                        map.get(InwtPersistenceAction.FIELD_EBAY_VIEW_ITEM_URL_FOR_NATURAL_SEARCH)).isEqualTo(
                        "http://www.ebay.com/itm/Fashion-Hot-Women-Sterling-Silver-Plated-Beads-Ball-Bangle-Cuff-Bracelet-Jewelry-/151389560917");
                assertThat(map.get(InwtPersistenceAction.FIELD_EBAY_LISTING_TYPE)).isEqualTo(
                        "FixedPriceItem");
                assertThat(map.get(InwtPersistenceAction.FIELD_EBAY_PRIMARY_CATEGORY_ID)).isEqualTo(
                        "50637");
                assertThat(
                        map.get(InwtPersistenceAction.FIELD_EBAY_PRIMARY_CATEGORY_NAME)).isEqualTo(
                        "Jewelry & Watches:Fashion Jewelry:Bracelets");
                assertThat(
                        map.get(InwtPersistenceAction.FIELD_EBAY_CONVERTED_CURRENT_PRICE_AMOUNT)).isEqualTo(
                        aExpectedPrice);
                assertThat(
                        map.get(InwtPersistenceAction.FIELD_EBAY_CONVERTED_CURRENT_PRICE_CURRENCY)).isEqualTo(
                        aExpectedCurrency);
                return newProduct;
            }
        }).when(objectUnderTest).createRecordObject(isA(Map.class));

        final EbayItemData ebayItemData = createEbayItemData(aConvertedCurrentPrice);

        final WriteResult writeResult = mock(WriteResult.class);
        when(writeResult.getUpsertedId()).thenReturn(new ObjectId(PRODUCT_ID));

        final DBCollection productsColl = mock(DBCollection.class);
        when(productsColl.insert(eq(WriteConcern.SAFE), eq(newProduct))).thenReturn(writeResult);


        final DB db = mock(DB.class);
        when(db.getCollection(eq(InwtPersistenceAction.COLLECTION_USER_PRODUCTS))).thenReturn
                (productsColl);

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        when(persistenceState.getDb()).thenReturn(db);

        /**
         * Run method under test
         */
        final byte[] actualResult = objectUnderTest.saveProductDataExceptImages(ebayItemData,
                persistenceState);

        /**
         * Verify
         */
        verify(objectUnderTest).createRecordObject(isA(Map.class));
        assertThat(actualResult).isEqualTo(PRODUCT_ID);
        verify(productsColl).insert(eq(WriteConcern.SAFE), eq(newProduct));
    }

    @Test
    public void testSaveProductImages() {
        /**
         * Prepare
         */
        final Binary imageData = mock(Binary.class);
        final EbayProductDataSaver objectUnderTest = spy(new EbayProductDataSaver());

        final String fileName = "SomeFileName";
        doReturn(fileName).when(objectUnderTest).extractFileName(eq(IMAGE_URL));

        final String mimeType = "image/png";
        doReturn(mimeType).when(objectUnderTest).getMimeType(eq(fileName));

        doReturn(imageData).when(objectUnderTest).readImage(eq(IMAGE_URL));

        final BasicDBObject record = mock(BasicDBObject.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final Map<String,Object> map = (Map<String,Object>)invocation.getArguments()[0];

                assertThat(map).contains(new MapEntry[]{
                        entry(FIELD_COLLECTION_USER_PRODUCT_IMAGES_CREATOR_EMAIL,
                                EbayProductDataSaver.CREATOR_EMAIL),
                        entry(FIELD_COLLECTION_USER_PRODUCT_IMAGES_PRODUCT_ID, PRODUCT_ID),
                        entry(FIELD_COLLECTION_USER_PRODUCT_IMAGES_DATA, imageData),
                        entry(FIELD_COLLECTION_USER_PRODUCT_IMAGES_FILE_NAME, fileName),
                        entry(FIELD_COLLECTION_USER_PRODUCT_IMAGES_MIME_TYPE, mimeType),
                        entry(FIELD_COLLECTION_USER_PRODUCT_IMAGES_IS_DELETED, Boolean.FALSE),
                });
                return record;
            }
        }).when(objectUnderTest).createRecordObject(isA(Map.class));

        final List<String> pictureUrls = new LinkedList<String>();
        pictureUrls.add(IMAGE_URL);

        final DBCollection userProductImagesColl = mock(DBCollection.class);


        final DB db = mock(DB.class);
        when(db.getCollection(COLLECTION_USER_PRODUCT_IMAGES)).thenReturn(userProductImagesColl);

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);
        when(persistenceState.getDb()).thenReturn(db);

        /**
         * Run method under test
         */
        objectUnderTest.saveProductImages(PRODUCT_ID, pictureUrls, persistenceState);

        /**
         * Verify
         */
        verify(objectUnderTest).createRecordObject(isA(Map.class));
    }

    @Test
    public void testExtractFileName()
    {
        /**
         * Prepare
         */
        final EbayProductDataSaver objectUnderTest = spy(new EbayProductDataSaver());

        /**
         * Verify
         */
        assertThat(objectUnderTest.extractFileName("http://i.ebayimg.com/00/s/NTAwWDUwMA==/z/K5QAAOSwVFlT~U-6/$_1" +
                ".JPG?set_id=8800005007")).isEqualTo("_1.JPG");
        assertThat(objectUnderTest.extractFileName("http://i.ebayimg" +
                ".com/00/s/ODAwWDgwMA==/z/32AAAOSwxCxT9umJ/$_12.JPG?set_id=880000500F")).isEqualTo("_12.JPG");
    }

    @Test
    public void testGetMimeType()
    {
        /**
         * Prepare
         */
        final EbayProductDataSaver objectUnderTest = spy(new EbayProductDataSaver());

        /**
         * Verify
         */
        assertThat(objectUnderTest.getMimeType("_1.JPeG")).isEqualTo(MIME_TYPE_JPEG);
        assertThat(objectUnderTest.getMimeType("_1.JPG")).isEqualTo(MIME_TYPE_JPEG);
        assertThat(objectUnderTest.getMimeType("_1.png")).isEqualTo(MIME_TYPE_PNG);
    }

    @Test
    public void testNpeWhenConvertedPriceIsNull()
    {
        saveProductDataTestLogic(null);
    }

    @Test
    public void testSaveProductDataExceptImages2() {
        saveProductDataExceptImagesTestLogic(null, null, null);

    }

    @Test
    public void testBigDecimalToInt()
    {
        /**
         * Prepare
         */
        final EbayProductDataSaver objectUnderTest = spy(new EbayProductDataSaver());

        /**
         * Run method under test
         */

        /**
         * Verify
         */
        assertThat(objectUnderTest.bigDecimalToInt(new BigDecimal("1.98"))).isEqualTo(198);
    }
}
