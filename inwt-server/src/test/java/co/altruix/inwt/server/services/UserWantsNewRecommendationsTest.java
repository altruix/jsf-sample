package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.UserWantsNewRecommendationsRequest;
import co.altruix.inwt.server.email.IEmailSender;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import ru.altruix.androidprototyping.server.persistence.IPersistence;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import java.io.IOException;

import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserWantsNewRecommendationsTest {

    public static final String REQUEST_AS_STRING = "RequestAsString";
    public static final String EMAIL = "user@provider.com";

    @Test
    public void testUserWantsNewRecommendations() throws IOException, InterruptedException,
            MessagingException {
        /**
         * Prepare
         */
        final UserWantsNewRecommendations objectUnderTest = spy(new UserWantsNewRecommendations(
                mock(IEmailSender.class)));

        final UserWantsNewRecommendationsRequest request = mock
                (UserWantsNewRecommendationsRequest.class);
        when(request.getUserEmail()).thenReturn(EMAIL);

        doReturn(request).when(objectUnderTest).readRequest(eq(REQUEST_AS_STRING),
                isA(ObjectMapper.class));

        final IPersistence persistence = mock(IPersistence.class);

        doReturn(persistence).when(objectUnderTest).getInwtPersistence();

        doNothing().when(objectUnderTest).logUserBehaviour(request);

        /**
         * Invoke method under test
         */
        objectUnderTest.userWantsNewRecommendations(REQUEST_AS_STRING);

        /**
         * Verify
         */
        verify(objectUnderTest).logUserBehaviour(request);
    }

    @Test
    public void testSendEmail() throws MessagingException {
        /**
         * Prepare
         */
        final ServletContext context = mock(ServletContext.class);
        final IEmailSender emailSender = mock(IEmailSender.class);
        final UserWantsNewRecommendations objectUnderTest = spy(new UserWantsNewRecommendations
                (emailSender));

        objectUnderTest.setServletContext(context);

        /**
         * Run method under test
         */
        objectUnderTest.sendEmail(EMAIL);

        /**
         * Verify
         */
        verify(emailSender).sendEMail("User user@provider.com wants new recommendations",
                "User user@provider.com wants new recommendations", "dp@altruix.co", context);
    }
}