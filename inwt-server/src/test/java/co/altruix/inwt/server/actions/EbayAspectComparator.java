package co.altruix.inwt.server.actions;

import co.altruix.inwt.model.EbayAspect;

import java.util.Comparator;

class EbayAspectComparator implements Comparator<EbayAspect> {
    @Override
    public int compare(final EbayAspect o1, final EbayAspect o2) {
        if (o1.getName().equals(o2.getName()) && o1.getProductCategory().equals(
                o2.getProductCategory()))
        {
            return 0;
        }

        return -1;
    }
}
