package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.ImageRating;
import co.altruix.inwt.messages.RateProductImageRequest;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Date;
import java.util.Map;

import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_HOT_NOT_MARKING_DATETIME_UTC;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_USER_IMAGE_ID;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.SET;
import static java.lang.Boolean.TRUE;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.fest.assertions.data.MapEntry.entry;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class RateProductImageActionTest {

    public static final byte[] IMAGE_ID = "1".getBytes();
    public static final byte[] PRODUCT_ID = "2".getBytes();

    @Test
    public void testCreateDataMap()
    {
        createDataMapTestLogic(ImageRating.HOT);
        createDataMapTestLogic(ImageRating.NOT);

    }

    private void createDataMapTestLogic(final ImageRating aImageRating) {
        /**
         * Prepare
         */
        final RateProductImageRequest request = new RateProductImageRequest();
        request.setEmail("dp@altruix.co");
        request.setImageId(IMAGE_ID);
        request.setProductId(PRODUCT_ID);
        request.setRating(aImageRating);

        final RateProductImageAction objectUnderTest = spy(new RateProductImageAction(request));

        final Date now = CreateProductAction.getCurrentUtcTime();

        doReturn(now).when(objectUnderTest).getNow();
        /**
         * Run method under test
         */

        final Map<String,Object> actualResult = objectUnderTest.createDataMap(request);

        /**
         * Verify
         */
        assertThat(actualResult).contains(
                entry(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL,"dp@altruix.co"),
                entry(FIELD_COLLECTION_RECOMMENDATIONS_USER_IMAGE_ID, IMAGE_ID),
                entry(FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID, PRODUCT_ID)
        );
    }

    @Test
    public void testRunOnMongo()
    {
        /**
         * Prepare
         */
        final RateProductImageRequest request = mock(RateProductImageRequest.class);
        final RateProductImageAction objectUnderTest = spy(new RateProductImageAction(request));

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        final Map<String, Object> queryMap = mock(Map.class);

        doReturn(queryMap).when(objectUnderTest).createDataMap(eq(request));

        final DBCollection recommendationsColl = mock(DBCollection.class);

        doReturn(recommendationsColl).when(objectUnderTest).getCollection(eq(persistenceState));

        final BasicDBObject query = mock(BasicDBObject.class);

        doReturn(query).when(objectUnderTest).createBasicDBObject(eq(queryMap));

        doReturn(true).when(objectUnderTest).imageRatingToBoolean(eq(request));

        final Date now = CreateProductAction.getCurrentUtcTime();

        doReturn(now).when(objectUnderTest).getNow();

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final BasicDBObject query2 = (BasicDBObject) invocation.getArguments()[0];
                final BasicDBObject update = (BasicDBObject) invocation.getArguments()[1];

                assertThat(query2).isSameAs(query);

                final BasicDBObject setStuff = (BasicDBObject) update.get(SET);

                assertThat(setStuff).isNotNull();

                assertThat(setStuff.get(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED)).isEqualTo
                        (TRUE);
                assertThat(setStuff.get(
                        FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER)).isEqualTo
                        (true);
                assertThat(setStuff.get(
                        FIELD_COLLECTION_RECOMMENDATIONS_HOT_NOT_MARKING_DATETIME_UTC)).isEqualTo
                        (now);

                return null;
            }
        }).when(recommendationsColl).update(eq(query), isA(DBObject.class));

        /**
         * Run method under test
         */
        objectUnderTest.runOnMongo(persistenceState);

        /**
         * Verify
         */
        verify(objectUnderTest).createDataMap(eq(request));
        verify(recommendationsColl).update(eq(query), isA(DBObject.class));
    }
}
