package co.altruix.inwt.server.actions.specifics;

import com.ebay.sdk.ApiContext;
import com.ebay.sdk.call.GetItemCall;
import com.ebay.soap.eBLBaseComponents.ItemType;
import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class EbayAspectDataReaderTest {
    /**
     * Unit test for issue #1. Verifies that no NPE is thrown, if item.getItemSpecifics() is null.
     */
    @Test
    public void testIssue1() throws Exception {
        /**
         * Prepare
         */
        final EbayAspectDataReader objectUnderTest = spy(new EbayAspectDataReader());

        final ApiContext apiContext = mock(ApiContext.class);

        doReturn(apiContext).when(objectUnderTest).createApiContext();

        final GetItemCall getItem = mock(GetItemCall.class);
        doReturn(getItem).when(objectUnderTest).createGetItemCall(apiContext);

        final ItemType item = mock(ItemType.class);
        when(item.getItemSpecifics()).thenReturn(null);

        when(getItem.getItem()).thenReturn(item);

        /**
         * Run method under test
         */
        objectUnderTest.getAspectData("itemId");
        assertThat(objectUnderTest.getNameValueList(item)).isNotNull();
    }
}
