package co.altruix.inwt.server.ebay;

import java.util.List;

import com.ebay.sdk.ApiContext;
import com.ebay.sdk.ApiCredential;
import com.ebay.sdk.call.GetItemCall;
import com.ebay.services.client.ClientConfig;
import com.ebay.services.client.FindingServiceClientFactory;
import com.ebay.services.finding.Aspect;
import com.ebay.services.finding.AspectFilter;
import com.ebay.services.finding.AspectHistogramContainer;
import com.ebay.services.finding.AspectValueHistogram;
import com.ebay.services.finding.Category;
import com.ebay.services.finding.FindItemsAdvancedRequest;
import com.ebay.services.finding.FindItemsAdvancedResponse;
import com.ebay.services.finding.FindItemsByCategoryRequest;
import com.ebay.services.finding.FindItemsByCategoryResponse;
import com.ebay.services.finding.FindItemsByKeywordsRequest;
import com.ebay.services.finding.FindItemsByKeywordsResponse;
import com.ebay.services.finding.FindingServicePortType;
import com.ebay.services.finding.OutputSelectorType;
import com.ebay.services.finding.PaginationInput;
import com.ebay.services.finding.SearchItem;

import com.ebay.soap.eBLBaseComponents.DetailLevelCodeType;
import com.ebay.soap.eBLBaseComponents.ItemType;
import com.ebay.soap.eBLBaseComponents.NameValueListArrayType;
import com.ebay.soap.eBLBaseComponents.NameValueListType;
import com.ebay.soap.eBLBaseComponents.SiteCodeType;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EBayTest {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(EBayTest.class);

	@Test
	@Ignore
	public void testEBay() {
		try {
			final ClientConfig config = new ClientConfig();
			
			config.setApplicationId("DmitriPi-4181-4c46-a8ff-cb6ad365117c");
			
			final FindingServicePortType serviceClient = FindingServiceClientFactory.getServiceClient(config);
			
			final FindItemsByKeywordsRequest request = new FindItemsByKeywordsRequest();
            request.setKeywords("earring");

            PaginationInput pi = new PaginationInput();
            pi.setEntriesPerPage(2);
            request.setPaginationInput(pi);

            FindItemsByKeywordsResponse result = serviceClient.findItemsByKeywords(request);
            
            LOGGER.debug("Ack = "+result.getAck());
            LOGGER.debug("Find " + result.getSearchResult().getCount() + " items." );
            List<SearchItem> items = result.getSearchResult().getItem();
            for(SearchItem item : items) {            	
            	LOGGER.debug("Title: " + item.getTitle());
            	
            	LOGGER.debug("Product ID: " + item.getProductId());
            	
            	LOGGER.debug("Global ID: " + item.getGlobalId());
            	
            	final Category primaryCategory = item.getPrimaryCategory();
            	final Category secondaryCategory = item.getSecondaryCategory();
            	
            	if (primaryCategory != null)
            	{
            		LOGGER.debug("primaryCategory: " + primaryCategory.getCategoryName());
            		LOGGER.debug("categoryId: " + primaryCategory.getCategoryId());
            	}
            	
            	if (secondaryCategory != null)
            	{
            		LOGGER.debug("secondaryCategory: " + secondaryCategory.getCategoryName());
            	}
            	
            	
            }
		} catch (final Exception exception) {
			LOGGER.error("", exception);
		}
	}
	@Test
    @Ignore
	public void testFindItemsByCategory() {
		try {
			final ClientConfig config = new ClientConfig();
			
			config.setApplicationId("DmitriPi-4181-4c46-a8ff-cb6ad365117c");
			
			final FindingServicePortType serviceClient = FindingServiceClientFactory.getServiceClient(config);
			
			final FindItemsByCategoryRequest request = new FindItemsByCategoryRequest();
            request.getOutputSelector().add(OutputSelectorType.ASPECT_HISTOGRAM);
            request.getCategoryId().add("50647");

            PaginationInput pi = new PaginationInput();
            pi.setEntriesPerPage(2);
            request.setPaginationInput(pi);



            final FindItemsByCategoryResponse result = serviceClient.findItemsByCategory(request);
            
            LOGGER.debug("Ack = "+result.getAck());
            LOGGER.debug("Find " + result.getSearchResult().getCount() + " items." );

            final AspectHistogramContainer aspectHistogramContainer =
                    result.getAspectHistogramContainer();

            LOGGER.debug("aspectHistogramContainer (start)");

            final List<Aspect> aspects = aspectHistogramContainer.getAspect();

            for (final Aspect curAspect : aspects)
            {
                LOGGER.debug("curAspect: " + curAspect.getName());

                final StringBuilder builder = new StringBuilder();

                final List<AspectValueHistogram> valueHistogram = curAspect.getValueHistogram();

                for (final AspectValueHistogram curAspectValue : valueHistogram)
                {
                    builder.append(curAspectValue.getValueName());
                    builder.append(", ");
                }

                LOGGER.debug("values: " + builder.toString());
            }

            LOGGER.debug("aspectHistogramContainer (end)");



            List<SearchItem> items = result.getSearchResult().getItem();
            
            for(SearchItem item : items) {
            	LOGGER.debug("Title: " + item.getTitle());
            	
            	LOGGER.debug("Product ID: " + item.getProductId());

                LOGGER.debug("Item ID: " + item.getItemId());

            	LOGGER.debug("Global ID: " + item.getGlobalId());
            	
            	final Category primaryCategory = item.getPrimaryCategory();
            	final Category secondaryCategory = item.getSecondaryCategory();
            	
            	if (primaryCategory != null)
            	{
            		LOGGER.debug("primaryCategory: " + primaryCategory.getCategoryName());
            		LOGGER.debug("categoryId: " + primaryCategory.getCategoryId());
            	}
            	
            	if (secondaryCategory != null)
            	{
            		LOGGER.debug("secondaryCategory: " + secondaryCategory.getCategoryName());
            	}
            	
            	LOGGER.debug("Gallery URL: " + item.getGalleryURL());
            	
            	for (final String url : item.getGalleryPlusPictureURL())
            	{
            		LOGGER.debug("url: " + url);
            	}
            	
            	LOGGER.debug("View item URL: " + item.getViewItemURL());
            }
		} catch (final Exception exception) {
			LOGGER.error("", exception);
		}
	}

    /**
     * Playground for finding out, how I can get product details like material, stone, color etc.
     */
    @Test
    @Ignore
    public void testReadingProductDetails()
    {
        String itemId = "231393253296";
        String itemId2  = "151474934975";

        try
        {
            ApiContext apiContext = new ApiContext();

            final ApiCredential apiCredential = new ApiCredential();
            apiCredential.seteBayToken("AgAAAA**AQAAAA**aAAAAA**ZeZsVA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6AGmYGnCZeKqA6dj6x9nY+seQ**yGoCAA**AAMAAA**KlSsq31aVXHoot0vvcuxIuGSKi5jtqrxkFYO2rm5vs0rFmzlsVUSoKlTRrlrwChl5XRDqH0wYNPhqh+4H1qFFUD1IjOuQWji3uIAI8iQSy2COQgRTWIV2WsbFjOWu6xyB9eGIAIMaBv+yJgPFmFBfP3xBuWnkrMHu4WCHYJwIeQnTDK+05/SOiWTZyFtsnl1xY5SsfkPTW7DTppMZzjw4ocDbbfaYjT9RnHsKA/3JjzHpdz3JBkYhab62LsOeFHn+VtlgA0lXf0i9MYAlvMqAn5ne2Qu7tPSDtrbA/8NgyyUxxJzAQLJOaH1rm6zwiN4U5xDlcNYcjaUyAVFxb+i04e8v5FwtJkqz4RGZZS13Txis9h6WKumgcSJS5Ah9zRU+FQChYzFHZIY1Z01uvbJ4GExXfoCClRfxaEW9pmAgX6SpvdQDKcvNprdYHshtfQ4FmSjzdGuH+EslBXlJ3fxNTf6EoBH6RwJ3ecK5ui7o89CAmiM1xY5Q9CoLKNkeGpSiGJsJubUVahaXZKH4CQCmqg7qRQIaFeh4TRYbgpTw4zNcCAXzpZ5/Zs601Gz1JEBVyJdZmPiK0DoBwHw+alivVcTVgbRJvFgKVgtrH1IUEO2QvxeLf3AgnO4VsZcZGXdf81Nxz0gq3t+9TdY54b45fpsT2R77rzkGXoDtWrVOMkV9m9O7FH8KEOEfIiMba5OwJOfQipsW/jo1/NICarOHvSJ4uaxMK/Bs38fLpq4paOaCRg00rul28P5Jytqx0RP");

            apiContext.setApiCredential(apiCredential);
            // apiContext.setSite(SiteCodeType.US);
            apiContext.setApiServerUrl("https://api.ebay.com/wsapi");

            GetItemCall getItem = new GetItemCall(apiContext);
            getItem.setItemID(itemId);
            getItem.setIncludeItemSpecifics(Boolean.TRUE);
            getItem.addDetailLevel(DetailLevelCodeType.RETURN_ALL);

            final ItemType item = getItem.getItem();

            final NameValueListArrayType itemSpecifics = item.getItemSpecifics();

            final NameValueListType[] nameValueList = itemSpecifics.getNameValueList();

            for (NameValueListType curSpecific : nameValueList) {
                final StringBuilder builder = new StringBuilder();

                builder.append(curSpecific.getName());
                builder.append(": ");

                final String[] values = curSpecific.getValue();

                boolean firstValueAdded = false;

                for (String curValue : values)
                {
                    if (!firstValueAdded)
                    {
                        firstValueAdded = true;
                    }
                    else
                    {
                        builder.append(", ");
                    }
                    builder.append(curValue);
                }

                LOGGER.debug(builder.toString());
            }

            LOGGER.debug("Title: " + item.getTitle());
            LOGGER.debug("Description: " + item.getDescription());

            LOGGER.debug("item: " + item);
        } catch (final Exception exception) {
            LOGGER.error("", exception);
        }

    }

    protected void readDataAboutTheItem(final String aItemId) {


    }

    /**
     * Playground for finding out, how I can get a list of eBay items,
     * which satisfy some criteria (like color, material, etc.)
     */
    @Test
    @Ignore
    public void testQueryingByDetails()
    {
        try {
            final ClientConfig config = new ClientConfig();

            config.setApplicationId("DmitriPi-4181-4c46-a8ff-cb6ad365117c");

            final FindingServicePortType serviceClient = FindingServiceClientFactory.getServiceClient(config);

            final FindItemsAdvancedRequest request = new FindItemsAdvancedRequest();
            request.getCategoryId().add("50647");

            final AspectFilter metal = new AspectFilter();
            metal.setAspectName("Metal");

            // metal.getAspectValueName().add("Silver Plated");
            // metal.getAspectValueName().add("Sterling Silver");

            metal.getAspectValueName().add("Gold Filled");
            metal.getAspectValueName().add("Gold Plated");
            metal.getAspectValueName().add("Multi-Tone Gold");
            metal.getAspectValueName().add("Rose Gold");
            metal.getAspectValueName().add("White Gold");
            metal.getAspectValueName().add("Yellow Gold");

            // request.getAspectFilter().add(metal);

            final AspectFilter mainStone = new AspectFilter();
            mainStone.setAspectName("Main Stone");
            mainStone.getAspectValueName().add("Ruby");
            // mainStone.getAspectValueName().add("Diamond");
            // mainStone.getAspectValueName().add("Diamante");

            request.getAspectFilter().add(mainStone);

            PaginationInput pi = new PaginationInput();
            pi.setEntriesPerPage(2);
            request.setPaginationInput(pi);

            final FindItemsAdvancedResponse result = serviceClient.findItemsAdvanced(request);

            LOGGER.debug("Ack = "+result.getAck());
            LOGGER.debug("Find " + result.getSearchResult().getCount() + " items." );
            List<SearchItem> items = result.getSearchResult().getItem();

            for(SearchItem item : items) {
                LOGGER.debug("Title: " + item.getTitle());

                LOGGER.debug("Product ID: " + item.getProductId());

                LOGGER.debug("Item ID: " + item.getItemId());

                LOGGER.debug("Global ID: " + item.getGlobalId());

                final Category primaryCategory = item.getPrimaryCategory();

                if (primaryCategory != null)
                {
                    LOGGER.debug("primaryCategory: " + primaryCategory.getCategoryName());
                    LOGGER.debug("categoryId: " + primaryCategory.getCategoryId());
                }

                LOGGER.debug("View item URL: " + item.getViewItemURL());
            }
        } catch (final Exception exception) {
            LOGGER.error("", exception);
        }

    }
}

