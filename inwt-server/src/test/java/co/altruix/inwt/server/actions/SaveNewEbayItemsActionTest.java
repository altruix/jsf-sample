package co.altruix.inwt.server.actions;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Arrays;

public class SaveNewEbayItemsActionTest {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SaveNewEbayItemsActionTest.class);

	@Test
	@Ignore
	public void testSavingOfImages() throws UnknownHostException
	{
		final ServerAddress serverAddress = new ServerAddress("95.85.28.148", 27017);
		
		final MongoCredential credential = MongoCredential.createMongoCRCredential("inwtAdmin", 
				"inwt", "2C6wGOVox136JU6V1a2QOjsj0P7WIJTUoF4J6rL1".toCharArray());
		
		MongoClient mongoClient = new MongoClient(serverAddress, Arrays.asList(credential));
		
		DB db = mongoClient.getDB("inwt");

		DBCollection ebayProducts = db.getCollection(FilterExistingEbayProductIdsAction.EBAY_PRODUCT);
		
		final DBCursor cursor = ebayProducts.find();
		
		for (int i=0; (i < 10) && cursor.hasNext(); i++)
		{
			 
			byte[] image = (byte[]) cursor.next().get("image");
			
			// image.getData()
			FileOutputStream fileOutputStream = null;
			try
			{
				fileOutputStream = new FileOutputStream(new File("file" + i + ".jpg"));
				fileOutputStream.write(image);
			} catch (final FileNotFoundException exception) {
				LOGGER.error("", exception);
			} catch (IOException exception) {
				LOGGER.error("", exception);
			}
			finally
			{
				IOUtils.closeQuietly(fileOutputStream);
			}
		}
	}
}
