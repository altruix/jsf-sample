package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.SubmitRecommendationBatchRequest;
import co.altruix.inwt.messages.SubmitRecommendationBatchResponse;
import co.altruix.inwt.server.actions.submitrecommendationbatch.IEbayProductDataSaver;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;

import java.util.Date;
import java.util.List;

public class SubmitRecommendationBatchActionForTesting extends SubmitRecommendationBatchAction {
    public SubmitRecommendationBatchActionForTesting(
            final SubmitRecommendationBatchRequest aRequest,
            final IEbayProductDataSaver aEbayProductDataSaver) {
        super(aRequest, aEbayProductDataSaver);
    }

    @Override
    public SubmitRecommendationBatchResponse runOnMongo(
            final IMongoPersistenceState aPersistenceState) {
        return super.runOnMongo(aPersistenceState);
    }

    @Override
    public void processProductUrl(final String aProductUrl,
                                  final List<Recommendation> aRecommendations,
                                  final IMongoPersistenceState aPersistenceState) {
        super.processProductUrl(aProductUrl, aRecommendations, aPersistenceState);
    }

    @Override
    public void saveRecommendationLog(final String aProductUrl,
                                      final String aRecommenationRecipientEmail,
                                      final String aSubmitterEmail, final Date aDate,
                                      final String aMessage,
                                      final IMongoPersistenceState aPersistenceState) {
        super.saveRecommendationLog(aProductUrl, aRecommenationRecipientEmail, aSubmitterEmail,
                aDate, aMessage, aPersistenceState);
    }

    @Override
    public boolean urlInvalid(final String aProductUrl) {
        return super.urlInvalid(aProductUrl);
    }

    @Override
    public byte[] getProductId(final String aProductUrl,
                               final IMongoPersistenceState aPersistenceState) {
        return super.getProductId(aProductUrl, aPersistenceState);
    }

    @Override
    protected List<byte[]> getImagesOfProductNotPresentedToUserYet(final byte[] aProductId,
                                                                   final String aRecommenationRecipientEmail,
                                                                   final IMongoPersistenceState aPersistenceState) {
        return super.getImagesOfProductNotPresentedToUserYet(aProductId,
                aRecommenationRecipientEmail, aPersistenceState);
    }


}
