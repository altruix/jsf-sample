package co.altruix.inwt.server;

import co.altruix.inwt.server.ebay.EbayCrawler;
import co.altruix.inwt.server.specifics.SpecificsDataAdder;
import org.junit.Test;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import ru.altruix.androidprototyping.server.persistence.IPersistence;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class StartupListenerTest {
    @Test
    public void testStartingOfSpecificsAdderJob() throws SchedulerException {
        /**
         * Prepare
         */
        final StartupListener objectUnderTest = spy(new StartupListener());

        final Scheduler scheduler = mock(Scheduler.class);

        final SchedulerFactory factory = mock(SchedulerFactory.class);
        when(factory.getScheduler()).thenReturn(scheduler);

        doReturn(factory).when(objectUnderTest).createSchedulerFactory();

        final ServletContext servletContext = mock(ServletContext.class);

        final IPersistence persistence = mock(IPersistence.class);

        final WebApplicationContext appContext = mock(WebApplicationContext.class);
        when(appContext.getBean("persistence")).thenReturn(persistence);


        doReturn(appContext).when(objectUnderTest).getAppContext(servletContext);

        final ServletContextEvent servletContextEvent = mock(ServletContextEvent.class);

        when(servletContextEvent.getServletContext()).thenReturn(servletContext);

        final JobDetail specificsDataAdderJob = mock(JobDetail.class);
        doReturn(specificsDataAdderJob).when(objectUnderTest).createSpecificsDataAdderJob
                (persistence);
        final Trigger specificsDataAdderTrigger = mock(Trigger.class);
        doReturn(specificsDataAdderTrigger).when(objectUnderTest).createSpecificsDataAdderTrigger();


        /**
         * Run method under test
         */
        objectUnderTest.contextInitialized(servletContextEvent);

        /**
         * Verify
         */
        verify(scheduler).scheduleJob(specificsDataAdderJob, specificsDataAdderTrigger);
    }

    @Test
    public void testCreateSpecificsDataAdderJob()
    {
        final StartupListener objectUnderTest = new StartupListener();

        final IPersistence persistence = mock(IPersistence.class);

        final JobDetail result = objectUnderTest.createSpecificsDataAdderJob(
                persistence);

        assertThat(SpecificsDataAdder.class.equals(result.getJobClass())).isTrue();
        assertThat(result.getJobDataMap().get(EbayCrawler.PERSISTENCE)).isEqualTo(persistence);
    }

    @Test
    public void testCreateSpecificsDataAdderTrigger()
    {
        final StartupListener objectUnderTest = new StartupListener();

        final Trigger trigger = objectUnderTest.createSpecificsDataAdderTrigger();

        assertThat(trigger).isNotNull();
    }
}
