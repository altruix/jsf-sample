package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.GetAspectsRequest;
import co.altruix.inwt.messages.GetAspectsResponse;
import co.altruix.inwt.model.EbayAspect;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import co.altruix.inwt.server.persistence.MongoPersistence;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetAspectsActionTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetAspectsActionTest.class);
    public static final String PRODUCT_CATEGORY_ID = "31331";
    public static final String ASPECT_BRAND = "Brand";
    public static final String ASPECT_MAIN_STONE = "Main stone";
    public static final String ASPECT_METAL = "Metal";

    @Test
    @Ignore
    public void test()
    {
        /**
         * Prepare
         */
        final MongoPersistence persistence = new MongoPersistence();
        persistence.init();

        final GetAspectsRequest request = new GetAspectsRequest();
        request.setUserEmail("dp@altruix.co");
        request.setProductCategoryId("50647");

        /**
         * Run method under test
         */
        try
        {
            final GetAspectsAction objectUnderTest =
                    new GetAspectsAction(request);

            persistence.runAction(objectUnderTest);
        } catch (InterruptedException e) {
            LOGGER.error("", e);
        } finally {
            persistence.shutdown();
        }

        /**
         * Verify
         */

    }

    @Test
    @Ignore
    public void testRunOnMongo()
    {
        /**
         * Prepare
         */
        final GetAspectsRequest request = mock(GetAspectsRequest.class);
        when(request.getUserEmail()).thenReturn("dp@altruix.co");
        when(request.getProductCategoryId()).thenReturn(PRODUCT_CATEGORY_ID);

        final GetAspectsAction objectUnderTest = new GetAspectsAction(request);

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        final BasicDBList productIds = mock(BasicDBList.class);
        doReturn(productIds).when(objectUnderTest).getProductIds(persistenceState);

        final BasicDBObject query = mock(BasicDBObject.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final Map<String,Object> data = (Map<String, Object>) invocation.getArguments()[0];

                assertThat(data.get("$in")).isEqualsToByComparingFields(new BasicDBObject("$in",
                        productIds));
                assertThat(data.get(InwtPersistenceAction.FIELD_EBAY_PRIMARY_CATEGORY_ID))
                        .isEqualTo(PRODUCT_CATEGORY_ID);
                return query;
            }
        }).when(objectUnderTest).createQuery(any(Map.class));

        final DBCollection userProducts = mock(DBCollection.class);
        doReturn(userProducts).when(objectUnderTest).getCollection(persistenceState,
                InwtPersistenceAction.COLLECTION_USER_PRODUCTS);

        final DBCursor cursor = mock(DBCursor.class);

        doReturn(cursor).when(userProducts).find(query);

        when(cursor.hasNext()).thenReturn(true).thenReturn(true).thenReturn(false);

        final BasicDBObject record1 = createRecord1();
        final BasicDBObject record2 = createRecord2();

        when(cursor.next()).thenReturn(record1).thenReturn(record2);

        /**
         * Run method under test
         */
        final GetAspectsResponse response = objectUnderTest.runOnMongo(persistenceState);

        /**
         * Verify
         */
        assertThat(response).isNotNull();
        assertThat(response.getAspects()).isNotNull();
        assertThat(response.getAspects().size()).isEqualTo(3);
        assertThat(response.getAspects()).contains(
                createEbayAspect(PRODUCT_CATEGORY_ID, ASPECT_BRAND),
                createEbayAspect(PRODUCT_CATEGORY_ID, ASPECT_MAIN_STONE),
                createEbayAspect(PRODUCT_CATEGORY_ID, ASPECT_METAL));
    }

    private EbayAspect createEbayAspect(final String aProductCategoryId, final String aName) {
        final EbayAspect aspect = new EbayAspect();
        aspect.setProductCategory(aProductCategoryId);
        aspect.setName(aName);

        return aspect;
    }

    private BasicDBObject createRecord2() {
        final BasicDBList aspects = new BasicDBList();
        final BasicDBObject aspect1 = new BasicDBObject(ASPECT_BRAND, "Handmade");
        final BasicDBObject aspect2 = new BasicDBObject(ASPECT_MAIN_STONE, "Diamond");

        aspects.add(aspect1);
        aspects.add(aspect2);

        return new BasicDBObject(InwtPersistenceAction.FIELD_EBAY_ASPECTS, aspects);
    }

    private BasicDBObject createRecord1() {
        final BasicDBList aspects = new BasicDBList();
        final BasicDBObject aspect1 = new BasicDBObject(ASPECT_BRAND, "Pandora");
        final BasicDBObject aspect2 = new BasicDBObject(ASPECT_METAL, "Silver");

        aspects.add(aspect1);
        aspects.add(aspect2);

        return new BasicDBObject(InwtPersistenceAction.FIELD_EBAY_ASPECTS, aspects);
    }

    @Test
    public void testConvertAspectNamesToAspectList()
    {
        /**
         * Prepare
         */
        final GetAspectsRequest request = mock(GetAspectsRequest.class);
        when(request.getProductCategoryId()).thenReturn(PRODUCT_CATEGORY_ID);

        final GetAspectsAction objectUnderTest = new GetAspectsAction(request);

        final List<String> aspectNames = new LinkedList<String>();
        aspectNames.add(ASPECT_BRAND);
        aspectNames.add(ASPECT_MAIN_STONE);
        aspectNames.add(ASPECT_METAL);

        /**
         * Run method under test
         */
        final List<EbayAspect> actualResult = objectUnderTest.convertAspectNamesToAspectList(
                aspectNames);

        /**
         * Verify
         */
        assertThat(actualResult).isNotNull();
        assertThat(actualResult.size()).isEqualTo(3);

        assertThat(actualResult).usingElementComparator(new EbayAspectComparator()).
                contains(createAspect(ASPECT_BRAND, PRODUCT_CATEGORY_ID));
        assertThat(actualResult).usingElementComparator(new EbayAspectComparator()).
                contains(createAspect
                (ASPECT_MAIN_STONE, PRODUCT_CATEGORY_ID));
        assertThat(actualResult).usingElementComparator(new EbayAspectComparator()).
                contains(createAspect
                (ASPECT_METAL, PRODUCT_CATEGORY_ID));
    }

    private EbayAspect createAspect(final String aAspectName, final String aProductCategoryId) {
        final EbayAspect result = new EbayAspect();

        result.setName(aAspectName);
        result.setProductCategory(aProductCategoryId);

        return result;
    }
}
