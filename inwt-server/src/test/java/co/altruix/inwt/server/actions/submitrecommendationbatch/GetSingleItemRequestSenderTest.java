package co.altruix.inwt.server.actions.submitrecommendationbatch;

import co.altruix.inwt.server.ebay.EbayDataReader;
import org.junit.Ignore;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class GetSingleItemRequestSenderTest {
    private final static String ITEM_ID = "151389560917";
    public static final String APPLICATION_ID = "XXXXXXXX-0000-0000-0000-000000000000";

    @Test
    @Ignore
    public void testSunnyDay()
    {
        final GetSingleItemRequestSender getSingleItem = new GetSingleItemRequestSender(
                new GetSingleItemResponseParser());

        getSingleItem.getEbayItemData("151389560917", EbayDataReader.APPLICATION_ID);
    }

    @Test
    public void test() throws IOException {
        final IEbayItemData parsingResult = mock(IEbayItemData.class);

        final GetSingleItemResponseParser responseParser = mock(GetSingleItemResponseParser.class);
        when(responseParser.parse(eq(GetSingleItemResponseParserTest.XML_RESPONSE))).thenReturn
                (parsingResult);

        final GetSingleItemRequestSender objectUnderTest = spy(
                new GetSingleItemRequestSender(responseParser));

        final HttpURLConnection connection = mock(HttpURLConnection.class);

        when(connection.getInputStream()).thenReturn(new ByteArrayInputStream
                (GetSingleItemResponseParserTest.XML_RESPONSE.getBytes()));

        final URL url = new URL("http://altruix.cc");

        doReturn(connection).when(objectUnderTest).openConnection(eq(url));

        doReturn(url).when(objectUnderTest).createUrl(eq(ITEM_ID), eq(APPLICATION_ID));

        /**
         * Invoke method under test
         */
        final IEbayItemData actualResult = objectUnderTest.getEbayItemData(ITEM_ID,
                APPLICATION_ID);

        /**
         * Verify
         */
        verify(objectUnderTest).openConnection(eq(url));
        verify(objectUnderTest).createUrl(eq(ITEM_ID), eq(APPLICATION_ID));
        verify(connection).getInputStream();
        verify(responseParser).parse(eq(GetSingleItemResponseParserTest.XML_RESPONSE));
        assertThat(actualResult).isSameAs(parsingResult);
    }

    @Test
    public void testCreateUrl() throws MalformedURLException {
        final GetSingleItemResponseParser responseParser = mock(GetSingleItemResponseParser.class);
        final GetSingleItemRequestSender objectUnderTest = spy(new GetSingleItemRequestSender(
                responseParser));


        assertThat(objectUnderTest.createUrl("151389560917", "myAppId").toString()).isEqualTo("http://open.api.ebay.com/shopping?callname=GetSingleItem&responseencoding=XML&appid=myAppId&siteid=0&version=515&ItemID=151389560917");
    }
}