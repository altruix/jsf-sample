package co.altruix.inwt.server.actions;

import co.altruix.inwt.model.EbayProductCategoryPopularityInfo;
import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EbayProductCategoryPopularityInfoComparatorTest {
    @Test
    public void test()
    {
        final EbayProductCategoryPopularityInfoComparator objectUnderTest = new
                EbayProductCategoryPopularityInfoComparator();

        final EbayProductCategoryPopularityInfo object1 = mock(
                EbayProductCategoryPopularityInfo.class);
        when(object1.getNumberOfLikedImages()).thenReturn(1);

        final EbayProductCategoryPopularityInfo object2 = mock(EbayProductCategoryPopularityInfo
                .class);
        when(object2.getNumberOfLikedImages()).thenReturn(2);

        assertThat(objectUnderTest.compare(object1, object2)).isGreaterThan(0);
        assertThat(objectUnderTest.compare(object2, object1)).isLessThan(0);
    }
}
