package co.altruix.inwt.server.actions.specifics;

import co.altruix.inwt.server.actions.InwtPersistenceAction;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_EBAY_ASPECTS;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_EBAY_ITEM_ID;
import static co.altruix.inwt.server.actions.specifics.AddSpecificsDataAction.EXISTS;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AddSpecificsDataActionTest {
    public static final String ITEM_ID = "261488387809";
    public static final ObjectId OBJECT_ID = new ObjectId("123456789012".getBytes());
    public static final String MAX = "Max";
    public static final String MORITZ = "Moritz";
    public static final String ROMEO = "Romeo";
    public static final String JULIET = "Juliet";

    @Test
    public void testRunOnMongo1()
    {
        /**
         * Prepare
         */
        final IEbayAspectDataReader ebayAspectDataReader = mock(IEbayAspectDataReader.class);
        final AddSpecificsDataAction objectUnderTest = spy(new AddSpecificsDataAction(
                ebayAspectDataReader));

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        final DBCollection userProducts = mock(DBCollection.class);
        doReturn(userProducts).when(objectUnderTest).getUserProductsCollection(persistenceState);

        final DBObject record = mock(DBObject.class);

        when(record.get(InwtPersistenceAction.FIELD_EBAY_ITEM_ID)).thenReturn(ITEM_ID);
        when(record.get("_id")).thenReturn(OBJECT_ID);

        final Map<String,String> aspectData = mock(Map.class);
        when(ebayAspectDataReader.getAspectData(ITEM_ID)).thenReturn(aspectData);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final BasicDBObject query = (BasicDBObject) invocation.getArguments()[0];

                assertThat(query.get(FIELD_EBAY_ASPECTS)).isEqualTo(new BasicDBObject(EXISTS,
                        FALSE));
                assertThat(query.get(FIELD_EBAY_ITEM_ID)).isEqualTo(new BasicDBObject(EXISTS,
                        TRUE));

                return record;
            }
        }).when(userProducts).findOne(isA(BasicDBObject.class));

        final ArrayList ebayAspects = mock(ArrayList.class);

        doReturn(ebayAspects).when(objectUnderTest).convertAspectDataToArrayList(aspectData);

        /**
         * Run method under test
         */
        final Boolean result = objectUnderTest.runOnMongo(persistenceState);

        /**
         * Verify
         */
        assertThat(result).isTrue();
        verify(objectUnderTest).addEbayAspectData(OBJECT_ID, ebayAspects, userProducts);
    }

    @Test
    public void testRunOnMongo2()
    {
        /**
         * Prepare
         */
        final IEbayAspectDataReader ebayAspectDataReader = mock(IEbayAspectDataReader.class);
        final AddSpecificsDataAction objectUnderTest = spy(new AddSpecificsDataAction(
                ebayAspectDataReader));

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        final DBCollection userProducts = mock(DBCollection.class);
        doReturn(userProducts).when(objectUnderTest).getUserProductsCollection(persistenceState);

        final Map<String,String> aspectData = mock(Map.class);
        when(ebayAspectDataReader.getAspectData(ITEM_ID)).thenReturn(aspectData);

        doReturn(null).when(userProducts).findOne(isA(BasicDBObject.class));

        final ArrayList ebayAspects = mock(ArrayList.class);

        doReturn(ebayAspects).when(objectUnderTest).convertAspectDataToArrayList(aspectData);

        /**
         * Run method under test
         */
        final Boolean result = objectUnderTest.runOnMongo(persistenceState);

        /**
         * Verify
         */
        assertThat(result).isFalse();
        verify(objectUnderTest, never()).addEbayAspectData(OBJECT_ID, ebayAspects, userProducts);
    }

    @Test
    public void testConvertAspectDataToArrayList()
    {
        /**
         * Prepare
         */
        final AddSpecificsDataAction objectUnderTest = new AddSpecificsDataAction();

        final Map<String,String> map = new HashMap<String, String>();

        map.put(MAX, MORITZ);
        map.put(ROMEO, JULIET);

        /**
         * Run method under test
         */
        final ArrayList result = objectUnderTest.convertAspectDataToArrayList(map);

        /**
         * Verify
         */
        assertThat(result).isNotNull();
        assertThat(result.size()).isEqualTo(2);
        assertThat(result).contains(new BasicDBObject(MAX, MORITZ));
        assertThat(result).contains(new BasicDBObject(ROMEO, JULIET));
    }

    @Test
    public void testAddEbayAspectData()
    {
        /**
         * Prepare
         */
        final AddSpecificsDataAction objectUnderTest = new AddSpecificsDataAction();

        final ObjectId id = mock(ObjectId.class);
        final ArrayList ebayAspects = mock(ArrayList.class);
        final DBCollection userProducts = mock(DBCollection.class);

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final BasicDBObject query = (BasicDBObject) invocation.getArguments()[0];

                assertThat(query.get("_id")).isEqualTo(id);

                final BasicDBObject update = (BasicDBObject) invocation.getArguments()[1];

                final BasicDBObject result1 = (BasicDBObject) update.get(
                        InwtPersistenceAction.SET);

                assertThat(result1).isNotNull();

                final ArrayList result2 = (ArrayList) result1.get(FIELD_EBAY_ASPECTS);

                assertThat(result2).isSameAs(ebayAspects);

                return mock(WriteResult.class);
            }
        }).when(userProducts).update(isA(BasicDBObject.class), isA(BasicDBObject.class));

        /**
         * Run method under test
         */
        objectUnderTest.addEbayAspectData(id, ebayAspects, userProducts);

        /**
         * Verify
         */
        verify(userProducts).update(isA(BasicDBObject.class), isA(BasicDBObject.class));
    }
}
