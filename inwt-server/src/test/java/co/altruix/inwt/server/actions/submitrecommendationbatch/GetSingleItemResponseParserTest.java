package co.altruix.inwt.server.actions.submitrecommendationbatch;

import org.junit.Test;

import java.math.BigDecimal;

import static org.fest.assertions.api.Assertions.assertThat;

public class GetSingleItemResponseParserTest {

    public static final String XML_RESPONSE = "<GetSingleItemResponse xmlns=\"urn:ebay:apis:eBLBaseComponents\">\n" +
            "   <Timestamp>2014-10-21T18:56:31.155Z</Timestamp>\n" +
            "   <Ack>Success</Ack>\n" +
            "   <Build>E889_CORE_APILW_17041759_R1</Build>\n" +
            "   <Version>889</Version>\n" +
            "   <Item>\n" +
            "    <ItemID>151389560917</ItemID>\n" +
            "    <EndTime>2014-11-20T06:56:12.000Z</EndTime>\n" +
            "    <ViewItemURLForNaturalSearch>http://www.ebay.com/itm/Fashion-Hot-Women-Sterling-Silver-Plated-Beads-Ball-Bangle-Cuff-Bracelet-Jewelry-/151389560917</ViewItemURLForNaturalSearch>\n" +
            "    <ListingType>FixedPriceItem</ListingType>\n" +
            "    <Location>HK</Location>\n" +
            "    <GalleryURL>http://thumbs2.ebaystatic.com/pict/1513895609178080_2.jpg</GalleryURL>\n" +
            "    <PictureURL>http://i.ebayimg.com/00/s/NTAwWDUwMA==/z/K5QAAOSwVFlT~U-6/$_1.JPG?set_id=8800005007</PictureURL>\n" +
            "    <PictureURL>http://i.ebayimg.com/00/s/ODAxWDgwMA==/z/wYkAAOSwRLZT9ulm/$_12.JPG?set_id=880000500F</PictureURL>\n" +
            "    <PictureURL>http://i.ebayimg.com/00/s/NjA2WDgwMA==/z/L2YAAOSwDk5T9ulp/$_12.JPG?set_id=880000500F</PictureURL>\n" +
            "    <PictureURL>http://i.ebayimg.com/00/s/NDM0WDgwMA==/z/HzMAAOSwd4tT9ulr/$_12.JPG?set_id=880000500F</PictureURL>\n" +
            "    <PictureURL>http://i.ebayimg.com/00/s/ODAwWDgwMA==/z/wg8AAOSwRLZT9ulx/$_12.JPG?set_id=880000500F</PictureURL>\n" +
            "    <PictureURL>http://i.ebayimg.com/00/s/ODAwWDgwMA==/z/rlEAAOSwQItT9ul9/$_12.JPG?set_id=880000500F</PictureURL>\n" +
            "    <PictureURL>http://i.ebayimg.com/00/s/ODAwWDgwMA==/z/32AAAOSwxCxT9umJ/$_12.JPG?set_id=880000500F</PictureURL>\n" +
            "    <PrimaryCategoryID>50637</PrimaryCategoryID>\n" +
            "    <PrimaryCategoryName>Jewelry &amp; Watches:Fashion Jewelry:Bracelets</PrimaryCategoryName>\n" +
            "    <BidCount>0</BidCount>\n" +
            "    <ConvertedCurrentPrice currencyID=\"USD\">1.98</ConvertedCurrentPrice>\n" +
            "    <ListingStatus>Active</ListingStatus>\n" +
            "    <TimeLeft>P29DT11H59M41S</TimeLeft>\n" +
            "    <Title>Fashion Hot Women Sterling Silver Plated Beads Ball Bangle Cuff Bracelet Jewelry</Title>\n" +
            "    <Country>HK</Country>\n" +
            "    <AutoPay>false</AutoPay>\n" +
            "    <QuantityAvailableHint>MoreThan</QuantityAvailableHint>\n" +
            "    <QuantityThreshold>10</QuantityThreshold>\n" +
            "   </Item>\n" +
            "  </GetSingleItemResponse>";

    @Test
    public void test()
    {
        final GetSingleItemResponseParser objectUnderTest = new GetSingleItemResponseParser();

        final IEbayItemData actualResult = objectUnderTest.parse(XML_RESPONSE);

        assertThat(actualResult).isNotNull();
        assertThat(actualResult.getConvertedCurrentPrice()).isNotNull();
        assertThat(actualResult.getConvertedCurrentPrice().getCurrencyId()).isEqualTo("USD");
        assertThat(actualResult.getConvertedCurrentPrice().getAmount()).isEqualTo(new BigDecimal
                ("1.98"));
        assertThat(actualResult.getItemId()).isEqualTo("151389560917");
        assertThat(actualResult.getViewItemURLForNaturalSearch()).isEqualTo("http://www.ebay.com/itm/Fashion-Hot-Women-Sterling-Silver-Plated-Beads-Ball-Bangle-Cuff-Bracelet-Jewelry-/151389560917");
        assertThat(actualResult.getListingType()).isEqualTo("FixedPriceItem");
        assertThat(actualResult.getPictureUrls()).isNotNull();
        assertThat(actualResult.getPictureUrls()).contains("http://i.ebayimg" +
                ".com/00/s/NTAwWDUwMA==/z/K5QAAOSwVFlT~U-6/$_1.JPG?set_id=8800005007",
                "http://i.ebayimg.com/00/s/ODAxWDgwMA==/z/wYkAAOSwRLZT9ulm/$_12" +
                        ".JPG?set_id=880000500F",
                "http://i.ebayimg.com/00/s/NjA2WDgwMA==/z/L2YAAOSwDk5T9ulp/$_12" +
                        ".JPG?set_id=880000500F",
                "http://i.ebayimg.com/00/s/NDM0WDgwMA==/z/HzMAAOSwd4tT9ulr/$_12" +
                        ".JPG?set_id=880000500F",
                "http://i.ebayimg.com/00/s/ODAwWDgwMA==/z/wg8AAOSwRLZT9ulx/$_12" +
                        ".JPG?set_id=880000500F",
                "http://i.ebayimg.com/00/s/ODAwWDgwMA==/z/rlEAAOSwQItT9ul9/$_12" +
                        ".JPG?set_id=880000500F",
                "http://i.ebayimg.com/00/s/ODAwWDgwMA==/z/32AAAOSwxCxT9umJ/$_12" +
                        ".JPG?set_id=880000500F");
        assertThat(actualResult.getPrimaryCategoryID()).isEqualTo("50637");
        assertThat(actualResult.getPrimaryCategoryName()).isEqualTo("Jewelry & " +
                "Watches:Fashion Jewelry:Bracelets");
        assertThat(actualResult.getTitle()).isEqualTo("Fashion Hot Women Sterling Silver Plated Beads Ball Bangle Cuff Bracelet Jewelry");
    }
}
