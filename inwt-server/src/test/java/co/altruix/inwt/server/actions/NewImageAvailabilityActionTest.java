package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.NewImageAvailabilityRequest;
import co.altruix.inwt.messages.NewImageAvailabilityResponse;
import co.altruix.inwt.messages.RecommendationResult;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static co.altruix.inwt.messages.RecommendationResult.NEW_IMAGE_AVAILABLE;
import static co.altruix.inwt.messages.RecommendationResult.NO_NEW_IMAGES;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NewImageAvailabilityActionTest {

    public static final String EMAIL = "dp@altruix.co";

    @Test
    public void testRunOnMongo()
    {
        runOnMongoTestLogic(true, NEW_IMAGE_AVAILABLE);
        runOnMongoTestLogic(false, NO_NEW_IMAGES);

    }

    private void runOnMongoTestLogic(final boolean aNewImagesThere,
                                     final RecommendationResult aExpectedResult) {
        /**
         * Prepare
         */
        final NewImageAvailabilityRequest request = mock(NewImageAvailabilityRequest.class);
        when(request.getUserEmail()).thenReturn(EMAIL);

        final NewImageAvailabilityAction objectUnderTest = spy(new NewImageAvailabilityAction
            (request));

        final IMongoPersistenceState persistenceState = mock(IMongoPersistenceState.class);

        final DBCursor cursor = mock(DBCursor.class);
        when(cursor.hasNext()).thenReturn(aNewImagesThere);

        final DBCollection collection = mock(DBCollection.class);

        doReturn(collection).when(objectUnderTest).getCollection(eq(persistenceState));

        doAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final BasicDBObject query = (BasicDBObject)invocation.getArguments()[0];

                assertThat(query).isNotNull();
                assertThat(query.get(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL)).
                        isEqualTo(EMAIL);
                assertThat(query.get(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED)).
                        isEqualTo(false);

                return cursor;
            }
        }).when(collection).find(isA(DBObject.class));

        /**
         * Run method under test
         */
        final NewImageAvailabilityResponse response = objectUnderTest.runOnMongo(
                persistenceState);

        /**
         * Verify
         */
        verify(collection).find(isA(DBObject.class));
        verify(cursor).close();
        assertThat(response.getResult()).isEqualTo(aExpectedResult);
    }
}
