package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.SignUpRequest;
import co.altruix.inwt.messages.SignUpResponse;
import co.altruix.inwt.messages.SignUpResult;
import co.altruix.inwt.server.actions.SignUpAction;
import co.altruix.inwt.server.email.IEmailSender;
import co.altruix.inwt.server.password.IPasswordManager;
import co.altruix.inwt.server.services.initialrecoms.IInitialRecomGenerator;
import co.altruix.inwt.server.services.signup.IBodyTextGenerator;
import co.altruix.inwt.server.services.signup.IEMailValidator;
import co.altruix.inwt.server.services.signup.IPasswordGenerator;
import org.junit.Test;
import ru.altruix.androidprototyping.server.persistence.IPersistence;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SignUpTest {
    private static final String EMAIL = "user@provider.com";

    @Test
    public void testCreationOfInitialRecommendations() throws InterruptedException {
        /**
         * Prepare
         */
        final IEMailValidator validator = mock(IEMailValidator.class);
        when(validator.isEmailValid(anyString())).thenReturn(true);

        final IEmailSender emailSender = mock(IEmailSender.class);
        final IPasswordGenerator passwordGenerator = mock(IPasswordGenerator.class);
        final IBodyTextGenerator bodyTextGenerator = mock(IBodyTextGenerator.class);
        final IPasswordManager passwordManager = mock(IPasswordManager.class);
        final IInitialRecomGenerator initialRecomGenerator = mock(IInitialRecomGenerator.class);

        final SignUp objectUnderTest = spy(new SignUp(validator, emailSender, passwordGenerator,
            bodyTextGenerator, passwordManager, initialRecomGenerator));

        final SignUpRequest request = mock(SignUpRequest.class);
        when(request.getEmail()).thenReturn(EMAIL);

        final SignUpResponse response = mock(SignUpResponse.class);
        when(response.getResult()).thenReturn(SignUpResult.INSERTED);

        final IPersistence persistence = mock(IPersistence.class);
        when(persistence.runAction(isA(SignUpAction.class))).thenReturn(response);

        when(objectUnderTest.getInwtPersistence()).thenReturn(persistence);

        /**
         * Run method under test
         */
        objectUnderTest.createResponse(request);

        /**
         * Verify
         */
        verify(initialRecomGenerator).createInitialRecommendations(persistence, EMAIL);
    }
}
