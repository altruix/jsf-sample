package co.altruix.inwt.server.actions;

import co.altruix.inwt.model.EbayAspectStatistic;
import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EbayAspectStatisticByPopularityComparatorTest {
    @Test
    public void testCompare()
    {
        final EbayAspectStatisticByPopularityComparator objectUnderTest = new
                EbayAspectStatisticByPopularityComparator();

        assertThat(objectUnderTest.compare(createStatistic(10),
                createStatistic(5))).isLessThan(0);
        assertThat(objectUnderTest.compare(createStatistic(5),
                createStatistic(10))).isGreaterThan(0);
        assertThat(objectUnderTest.compare(createStatistic(10),
                createStatistic(10))).isEqualTo(0);
    }

    private EbayAspectStatistic createStatistic(final int aNumberOfLikedImages)
    {
        final EbayAspectStatistic stat = mock(EbayAspectStatistic.class);

        when(stat.getNumberOfLikedImages()).thenReturn(aNumberOfLikedImages);

        return stat;
    }
}
