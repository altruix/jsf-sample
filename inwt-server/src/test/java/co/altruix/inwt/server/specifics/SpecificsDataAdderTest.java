package co.altruix.inwt.server.specifics;

import co.altruix.inwt.server.actions.specifics.AddSpecificsDataAction;
import org.junit.Test;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import ru.altruix.androidprototyping.server.persistence.IPersistence;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static co.altruix.inwt.server.ebay.EbayCrawler.PERSISTENCE;

public class SpecificsDataAdderTest {

    @Test
    public void testExecute() throws JobExecutionException, InterruptedException {
        /**
         * Prepare
         */
        final SpecificsDataAdder objectUnderTest = new SpecificsDataAdder();

        final IPersistence persistence = mock(IPersistence.class);
        when(persistence.runAction(isA(AddSpecificsDataAction.class))).thenReturn(Boolean.TRUE)
                .thenReturn(Boolean.FALSE);

        final JobDataMap jobDataMap = mock(JobDataMap.class);
        when(jobDataMap.get(PERSISTENCE)).thenReturn(persistence);

        final JobDetail jobDetail = mock(JobDetail.class);
        when(jobDetail.getJobDataMap()).thenReturn(jobDataMap);

        final JobExecutionContext ctx = mock(JobExecutionContext.class);

        when(ctx.getJobDetail()).thenReturn(jobDetail);

        /**
         * Run method under test
         */
        objectUnderTest.execute(ctx);

        /**
         * Verify
         */
        verify(persistence, times(2)).runAction(isA(AddSpecificsDataAction.class));
    }
}
