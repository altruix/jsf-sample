package co.altruix.inwt.server.services.initialrecoms;

import co.altruix.inwt.messages.SubmitRecommendationBatchRequest;
import co.altruix.inwt.server.actions.SubmitRecommendationBatchAction;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import ru.altruix.androidprototyping.server.persistence.IPersistence;

import static co.altruix.inwt.server.services.initialrecoms.InitialRecomGenerator.INITIAL_RECOM_GENERATOR_INWT_INFO;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;

public class InitialRecomGeneratorTest {
    public static final String EMAIL = "user@provider.com";

    @Test
    public void test() throws InterruptedException {
        /**
         * Prepare
         */
        final InitialRecomGenerator objectUnderTest = spy(new InitialRecomGenerator());

        final IPersistence persistence = mock(IPersistence.class);

        final SubmitRecommendationBatchAction action = mock(SubmitRecommendationBatchAction.class);

        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                final SubmitRecommendationBatchRequest request = (SubmitRecommendationBatchRequest)
                        invocation.getArguments()[0];

                assertThat(request.getSubmitterEmail()).isEqualTo(
                        INITIAL_RECOM_GENERATOR_INWT_INFO);
                assertThat(request.getRecommenationRecipientEmail()).isEqualTo(EMAIL);
                assertThat(request.getProductUrls().size()).isEqualTo(14);

                return action;
            }
        }).when(objectUnderTest).createAction(isA(SubmitRecommendationBatchRequest.class));

        /**
         * Run method under test
         */
        objectUnderTest.createInitialRecommendations(persistence, EMAIL);

        /**
         * Verify
         */
        verify(persistence).runAction(action);
    }
}
