package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.GetRecomQualityRequest;
import co.altruix.inwt.messages.GetRecomQualityResponse;
import co.altruix.inwt.messages.ImageRating;
import co.altruix.inwt.model.RecomQualityInfo;
import co.altruix.inwt.server.actions.ReadProposedRecommendationsAction;
import co.altruix.inwt.server.services.recomquality.Recom;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import org.joda.time.DateTime;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;
import ru.altruix.androidprototyping.server.persistence.IPersistence;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static java.util.Collections.shuffle;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GetRecomQualityTest {
    public static final String REQUEST = "request";
    public static final String USER_EMAIL = "user@provider.com";
    public static final String RECOMMENDER_EMAIL = "recommender@provider.com";
    public static final DateTime CREATION_DATETIME_1 = new DateTime(2014, 11, 7, 21, 34);

    @Test
    public void testGetRecomQuality() throws IOException, InterruptedException {
        /**
         * Prepare
         */
        final GetRecomQuality objectUnderTest = spy(new GetRecomQuality());

        final ObjectMapper objectMapper = mock(ObjectMapper.class);

        PowerMockito.doReturn(objectMapper).when(objectUnderTest).createObjectMapper();

        final GetRecomQualityRequest request = mock(GetRecomQualityRequest.class);
        when(request.getUserEmail()).thenReturn(USER_EMAIL);
        when(request.getRecommenderEmail()).thenReturn(RECOMMENDER_EMAIL);

        when(objectMapper.readValue(REQUEST, GetRecomQualityRequest.class)).thenReturn(request);

        final List<Recom> recoms = mock(List.class);

        final IPersistence persistence = mock(IPersistence.class);

        doReturn(persistence).when(objectUnderTest).getInwtPersistence();

        doReturn(recoms).when(objectUnderTest).readRecoms(USER_EMAIL,
                RECOMMENDER_EMAIL, persistence);

        final List<RecomQualityInfo> qualityTuples = mock(List.class);

        doReturn(qualityTuples).when(objectUnderTest).convertRecomsToRecomQualityTuples(recoms);

        final GetRecomQualityResponse response = mock(GetRecomQualityResponse.class);

        doReturn(response).when(objectUnderTest).createGetGetRecomQualityResponse();

        /**
         * Run method under test
         */
        objectUnderTest.getRecomQuality(REQUEST);

        /**
         * Verify
         */
        verify(response).setRecomQualityInfos(qualityTuples);
    }

    @Test
    public void testConvertRecomsToRecomQualityTuples()
    {
        /**
         * Prepare
         */
        final GetRecomQuality objectUnderTest = spy(new GetRecomQuality());
        final List<Recom> recoms = new LinkedList<Recom>();

        final DateTime creationTime1 = CREATION_DATETIME_1;
        final DateTime creationTime2 = new DateTime(2014, 11, 8, 21, 34);
        final DateTime creationTime3 = new DateTime(2014, 11, 9, 21, 34);
        final DateTime creationTime4 = new DateTime(2014, 11, 10, 0, 1);

        final DateTime ratingTime1 = new DateTime(2014, 11, 10, 21, 34);
        final DateTime ratingTime2 = new DateTime(2014, 11, 10, 21, 34);

        createRecomBatch1(recoms, creationTime1, ratingTime1, creationTime2);
        createRecomBatch2(recoms, creationTime3, ratingTime2, creationTime4);

        shuffle(recoms);

        /**
         * Run method under test
         */
        final List<RecomQualityInfo> recomQualityInfos = objectUnderTest.
                convertRecomsToRecomQualityTuples(recoms);

        /**
         * Verify
         */
        assertThat(recomQualityInfos).isNotNull();
        assertThat(recomQualityInfos.size()).isEqualTo(4);

        assertThat(recomQualityInfos.get(0)).isEqualsToByComparingFields(new RecomQualityInfo(
                new DateTime(2014, 11, 7, 0, 0, 0), new DateTime(2014, 11, 7, 23, 59, 59, 999),
                6, 1));

        assertThat(recomQualityInfos.get(1)).isEqualsToByComparingFields(new RecomQualityInfo(
                new DateTime(2014, 11, 8, 0, 0, 0), new DateTime(2014, 11, 8, 23, 59, 59, 999),
                4, 4));

        assertThat(recomQualityInfos.get(2)).isEqualsToByComparingFields(new RecomQualityInfo(
                new DateTime(2014, 11, 9, 0, 0, 0), new DateTime(2014, 11, 9, 23, 59, 59, 999),
                6, 3));
        assertThat(recomQualityInfos.get(3)).isEqualsToByComparingFields(new RecomQualityInfo(
                new DateTime(2014, 11, 10, 0, 0, 0), new DateTime(2014, 11, 10, 23, 59, 59, 999),
                4, 4));
    }

    protected void createRecomBatch1(final List<Recom> aRecoms, final DateTime aCreationTime,
                                     final DateTime aRatingTime, final DateTime aCreationTime2) {
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime, aRatingTime, ImageRating.NOT, "EbayCategory", "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime, aRatingTime.plusMinutes(1), ImageRating.NOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime, aRatingTime.plusMinutes(2), ImageRating.NOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime, aRatingTime.plusMinutes(3), ImageRating.NOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime, aRatingTime.plusMinutes(4), ImageRating.NOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime, aRatingTime.plusMinutes(5), ImageRating.HOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime2, aRatingTime.plusMinutes(6), ImageRating.HOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime2, aRatingTime.plusMinutes(7), ImageRating.HOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime2, aRatingTime.plusMinutes(8), ImageRating.HOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime2, aRatingTime.plusMinutes(9), ImageRating.HOT, "EbayCategory",
                "50637"));
    }

    protected void createRecomBatch2(final List<Recom> aRecoms, final DateTime aCreationTime,
                                     final DateTime aRatingTime, final DateTime aCreationTime2) {
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime, aRatingTime, ImageRating.NOT, "EbayCategory", "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime, aRatingTime.plusMinutes(1), ImageRating.NOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime, aRatingTime.plusMinutes(2), ImageRating.NOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime, aRatingTime.plusMinutes(3), ImageRating.HOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime, aRatingTime.plusMinutes(4), ImageRating.HOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime, aRatingTime.plusMinutes(5), ImageRating.HOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime2, aRatingTime.plusMinutes(6), ImageRating.HOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime2, aRatingTime.plusMinutes(7), ImageRating.HOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime2, aRatingTime.plusMinutes(8), ImageRating.HOT, "EbayCategory",
                "50637"));
        aRecoms.add(new Recom(USER_EMAIL, RECOMMENDER_EMAIL,
                aCreationTime2, aRatingTime.plusMinutes(9), ImageRating.HOT, "EbayCategory",
                "50637"));
    }

    @Test
    public void testReadRecoms() throws InterruptedException {
        /**
         * Prepare
         */
        final GetRecomQuality objectUnderTest = spy(new GetRecomQuality());

        final IPersistence persistence = mock(IPersistence.class);

        final ReadProposedRecommendationsAction action = mock(ReadProposedRecommendationsAction
                .class);

        doReturn(action).when(objectUnderTest).createReadProposedRecommendationsAction(USER_EMAIL,
                RECOMMENDER_EMAIL);

        /**
         * Run method under test
         */
        objectUnderTest.readRecoms(USER_EMAIL, RECOMMENDER_EMAIL, persistence);

        /**
         * Verify
         */
        verify(persistence).runAction(action);
    }

    @Test
    public void testSameDay()
    {
        /**
         * Prepare
         */
        final GetRecomQuality objectUnderTest = new GetRecomQuality();

        /**
         * Verify
         */
        assertThat(objectUnderTest.sameDay(new DateTime(2014, 11, 7, 0, 0,0),
                new DateTime(2014, 11, 7, 23, 23,59))).isTrue();
        assertThat(objectUnderTest.sameDay(new DateTime(2014, 11, 7, 0, 0,0),
                new DateTime(2014, 11, 8, 0, 0,0))).isFalse();

    }

    @Test
    public void testCreateObjectMapper()
    {
        final GetRecomQuality objectUnderTest = spy(new GetRecomQuality());

        final ObjectMapper objectMapper = mock(ObjectMapper.class);

        doReturn(objectMapper).when(objectUnderTest).creteVirginObjectMapper();

        objectUnderTest.createObjectMapper();

        verify(objectMapper).configure(FAIL_ON_UNKNOWN_PROPERTIES, false);
        verify(objectMapper).registerModule(isA(JodaModule.class));
    }
}
