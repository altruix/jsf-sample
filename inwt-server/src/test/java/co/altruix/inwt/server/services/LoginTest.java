package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.LoginRequest;
import co.altruix.inwt.messages.LoginResponse;
import co.altruix.inwt.server.password.IPasswordManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import ru.altruix.androidprototyping.server.persistence.IPersistence;
import ru.altruix.androidprototyping.server.persistence.IPersistenceAction;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isA;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LoginTest {

    public static final boolean PASSWORD_CHECK_RESULT = true;
    public static final String EMAIL = "user@provider.com";

    @Test
    public void testLogIsWritten() throws Exception {
        /**
         * Prepare
         */
        final IPasswordManager passwordManager = mock(IPasswordManager.class);
        when(passwordManager.check(anyString(),anyString())).thenReturn(PASSWORD_CHECK_RESULT);

        final Login objectUnderTest = spy(new Login(passwordManager));
        final LoginRequest request = mock(LoginRequest.class);
        when(request.getEmail()).thenReturn(EMAIL);

        doReturn(request).when(objectUnderTest).convertStringToRequest((String) isNull(),
                isA(ObjectMapper.class));

        final IPersistence persistence = mock(IPersistence.class);

        when(persistence.runAction(isA(IPersistenceAction.class))).thenReturn("passwordHash");

        when(objectUnderTest.getInwtPersistence()).thenReturn(persistence);

        doReturn("").when(objectUnderTest).writeValueAsString(isA(ObjectMapper.class), isA(
                LoginResponse.class));

        /**
         * Run method under test
         */
        objectUnderTest.login(null);

        /**
         * Verify
         */
        verify(objectUnderTest).logUserBehaviour(persistence, EMAIL, PASSWORD_CHECK_RESULT);
    }
}
