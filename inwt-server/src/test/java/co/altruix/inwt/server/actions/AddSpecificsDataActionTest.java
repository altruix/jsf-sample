package co.altruix.inwt.server.actions;

import co.altruix.inwt.server.actions.specifics.AddSpecificsDataAction;
import co.altruix.inwt.server.persistence.MongoPersistence;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddSpecificsDataActionTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(AddSpecificsDataActionTest.class);

    @Test
    @Ignore
    public void test()
    {
        /**
         * Prepare
         */
        final MongoPersistence persistence = new MongoPersistence();
        persistence.init();

        /**
         * Run method under test
         */
        try
        {
            final AddSpecificsDataAction objectUnderTest = new AddSpecificsDataAction();

            persistence.runAction(objectUnderTest);
        } catch (InterruptedException e) {
            LOGGER.error("", e);
        } finally {
            persistence.shutdown();
        }

        /**
         * Verify
         */



    }
}
