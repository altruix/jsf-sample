package co.altruix.inwt.server.services;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import co.altruix.inwt.messages.LoginRequest;
import co.altruix.inwt.messages.LoginResponse;
import co.altruix.inwt.server.actions.ReadPasswordHashAction;
import co.altruix.inwt.server.password.IPasswordManager;
import co.altruix.inwt.server.services.Login;
import ru.altruix.androidprototyping.server.persistence.IPersistence;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import static org.mockito.Matchers.isA;
import static org.mockito.Matchers.eq;


@RunWith(PowerMockRunner.class)
@PrepareForTest(Login.class)
public class LoginTest2 {
	@Test
	public void testSunnyDay() throws Exception
	{
		sunnyDayTestLogic(true);
		sunnyDayTestLogic(false);
	}

	private void sunnyDayTestLogic(boolean aPasswordCorrect) throws Exception, IOException,
			JsonParseException, JsonMappingException, InterruptedException {
		final IPasswordManager passwordManager = mock(IPasswordManager.class);
		
		when(passwordManager.check("Password", "passwordHash")).thenReturn(aPasswordCorrect);
		
		final Login objectUnderTest = new Login(passwordManager);
		
		final LoginRequest request = new LoginRequest();
		request.setEmail("Email");
		request.setPassword("Password");
		final ObjectMapper objectMapper = mock(ObjectMapper.class);
		
		when(objectMapper.readValue(isA(String.class), eq(LoginRequest.class))).thenReturn(request);
		
		final IPersistence persistence = mock(IPersistence.class);
		when(persistence.runAction(isA(ReadPasswordHashAction.class))).thenReturn("passwordHash");
		
		objectUnderTest.setInwtPersistence(persistence);
		
		PowerMockito.whenNew(ObjectMapper.class).withAnyArguments().thenReturn(objectMapper);
		
		final LoginResponse response = mock(LoginResponse.class);
		
		PowerMockito.whenNew(LoginResponse.class).withAnyArguments().thenReturn(response);
		
		objectUnderTest.login("bla-bla");
		
		verify(response).setSuccess(eq(aPasswordCorrect));
	}
}
