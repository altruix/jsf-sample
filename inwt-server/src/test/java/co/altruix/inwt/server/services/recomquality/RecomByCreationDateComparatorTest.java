package co.altruix.inwt.server.services.recomquality;

import org.joda.time.DateTime;
import org.junit.Test;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mock;

public class RecomByCreationDateComparatorTest {
    @Test
    public void test1()
    {
        /**
         * Prepare
         */
        final RecomByCreationDateComparator objectUnderTest = new RecomByCreationDateComparator();
        final Recom recom1 = mock(Recom.class);
        when(recom1.getCreationDateTimeUtc()).thenReturn(new DateTime(2014, 11, 10, 22, 35));

        final Recom recom2 = mock(Recom.class);
        when(recom2.getCreationDateTimeUtc()).thenReturn(new DateTime(2014, 11, 10, 22, 36));

        /**
         * Run method under test & verify
         */
        assertThat(objectUnderTest.compare(recom1, recom2)).isLessThan(0);
        assertThat(objectUnderTest.compare(recom2, recom1)).isGreaterThan(0);
    }

    @Test
    public void test2()
    {
        /**
         * Prepare
         */
        final DateTime creationTime = new DateTime(2014, 11, 10, 22, 35);
        final DateTime ratingTime1 = creationTime.plusDays(1);
        final DateTime ratingTime2 = creationTime.plusDays(2);

        final RecomByCreationDateComparator objectUnderTest = new RecomByCreationDateComparator();
        final Recom recom1 = mock(Recom.class);
        when(recom1.getCreationDateTimeUtc()).thenReturn(creationTime);
        when(recom1.getHotNotMarkingDateTimeUtc()).thenReturn(ratingTime1);

        final Recom recom2 = mock(Recom.class);
        when(recom2.getCreationDateTimeUtc()).thenReturn(creationTime);
        when(recom2.getHotNotMarkingDateTimeUtc()).thenReturn(ratingTime2);

        /**
         * Run method under test & verify
         */
        assertThat(objectUnderTest.compare(recom1, recom2)).isLessThan(0);
        assertThat(objectUnderTest.compare(recom2, recom1)).isGreaterThan(0);
    }

}
