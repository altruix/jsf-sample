package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.DeleteImageRequest;
import co.altruix.inwt.messages.DeleteImageResponse;
import co.altruix.inwt.server.actions.DeleteImageAction;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

@Path("/"+ DeleteImageRequest.SERVICE_NAME)
public class DeleteImage extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteImage.class);

    @POST
    @Produces("text/plain")
    public String deleteImage(@FormParam("request") final String aRequest) {
        try {

            LOGGER.debug("deleteImage request received at server side");

            final ObjectMapper mapper = new ObjectMapper();
            final DeleteImageRequest request = mapper.readValue(aRequest,
                    DeleteImageRequest.class);

            LOGGER.debug("request: " + request.getImageId());

            getInwtPersistence().runAction(new DeleteImageAction(request));

            return mapper.writeValueAsString(new DeleteImageResponse());
        } catch (final JsonParseException exception) {
            LOGGER.error(DeleteImageRequest.SERVICE_NAME, exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error(DeleteImageRequest.SERVICE_NAME, exception);
        } catch (final IOException exception) {
            LOGGER.error(DeleteImageRequest.SERVICE_NAME, exception);
        } catch (final InterruptedException exception) {
            LOGGER.error(DeleteImageRequest.SERVICE_NAME, exception);
        } catch (final Exception exception) {
            LOGGER.error(DeleteImageRequest.SERVICE_NAME, exception);
        }

        return "";
    }

}
