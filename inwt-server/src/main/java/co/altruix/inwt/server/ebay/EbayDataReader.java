package co.altruix.inwt.server.ebay;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebay.services.client.ClientConfig;
import com.ebay.services.client.FindingServiceClientFactory;
import com.ebay.services.finding.FindItemsByCategoryRequest;
import com.ebay.services.finding.FindItemsByCategoryResponse;
import com.ebay.services.finding.FindingServicePortType;
import com.ebay.services.finding.PaginationInput;
import com.ebay.services.finding.SearchResult;

public class EbayDataReader implements IEbayDataReader {
	public static final String APPLICATION_ID = "DmitriPi-4181-4c46-a8ff-cb6ad365117c";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(EbayDataReader.class);

	@Override
	public SearchResult findEbayItems(final String aCategoryId) {
		try {

			final ClientConfig config = new ClientConfig();

			config.setApplicationId(APPLICATION_ID);

			final FindingServicePortType serviceClient = FindingServiceClientFactory
					.getServiceClient(config);

			final FindItemsByCategoryRequest request = new FindItemsByCategoryRequest();
			request.getCategoryId().add(aCategoryId);

			final PaginationInput pi = new PaginationInput();
			pi.setEntriesPerPage(10);
			request.setPaginationInput(pi);

			final FindItemsByCategoryResponse result = serviceClient
					.findItemsByCategory(request);

			LOGGER.debug("Ack = " + result.getAck());
			LOGGER.debug("Find " + result.getSearchResult().getCount()
					+ " items.");
			return result.getSearchResult();

		} catch (final Exception exception) {
			LOGGER.error("", exception);
			return null;
		}
	}
}
