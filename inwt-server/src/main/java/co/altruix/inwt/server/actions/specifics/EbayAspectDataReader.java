package co.altruix.inwt.server.actions.specifics;

import com.ebay.sdk.ApiContext;
import com.ebay.sdk.ApiCredential;
import com.ebay.sdk.call.GetItemCall;
import com.ebay.soap.eBLBaseComponents.ItemType;
import com.ebay.soap.eBLBaseComponents.NameValueListArrayType;
import com.ebay.soap.eBLBaseComponents.NameValueListType;
import com.ebay.soap.eBLBaseComponents.SiteCodeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class EbayAspectDataReader implements IEbayAspectDataReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(EbayAspectDataReader.class);
    public static final String EBAY_TOKEN = "AgAAAA**AQAAAA**aAAAAA**ZeZsVA**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6AGmYGnCZeKqA6dj6x9nY+seQ**yGoCAA**AAMAAA**KlSsq31aVXHoot0vvcuxIuGSKi5jtqrxkFYO2rm5vs0rFmzlsVUSoKlTRrlrwChl5XRDqH0wYNPhqh+4H1qFFUD1IjOuQWji3uIAI8iQSy2COQgRTWIV2WsbFjOWu6xyB9eGIAIMaBv+yJgPFmFBfP3xBuWnkrMHu4WCHYJwIeQnTDK+05/SOiWTZyFtsnl1xY5SsfkPTW7DTppMZzjw4ocDbbfaYjT9RnHsKA/3JjzHpdz3JBkYhab62LsOeFHn+VtlgA0lXf0i9MYAlvMqAn5ne2Qu7tPSDtrbA/8NgyyUxxJzAQLJOaH1rm6zwiN4U5xDlcNYcjaUyAVFxb+i04e8v5FwtJkqz4RGZZS13Txis9h6WKumgcSJS5Ah9zRU+FQChYzFHZIY1Z01uvbJ4GExXfoCClRfxaEW9pmAgX6SpvdQDKcvNprdYHshtfQ4FmSjzdGuH+EslBXlJ3fxNTf6EoBH6RwJ3ecK5ui7o89CAmiM1xY5Q9CoLKNkeGpSiGJsJubUVahaXZKH4CQCmqg7qRQIaFeh4TRYbgpTw4zNcCAXzpZ5/Zs601Gz1JEBVyJdZmPiK0DoBwHw+alivVcTVgbRJvFgKVgtrH1IUEO2QvxeLf3AgnO4VsZcZGXdf81Nxz0gq3t+9TdY54b45fpsT2R77rzkGXoDtWrVOMkV9m9O7FH8KEOEfIiMba5OwJOfQipsW/jo1/NICarOHvSJ4uaxMK/Bs38fLpq4paOaCRg00rul28P5Jytqx0RP";
    public static final String API_SERVER_URL = "https://api.ebay.com/wsapi";

    @Override
    public Map<String, String> getAspectData(final String aEbayItemId) {
        final Map<String,String> result = new HashMap<String, String>();

        try
        {
            final ApiContext apiContext = createApiContext();

            final GetItemCall getItem = createGetItemCall(apiContext);
            getItem.setItemID(aEbayItemId);
            getItem.setIncludeItemSpecifics(Boolean.TRUE);

            final ItemType item = getItem.getItem();

            final NameValueListType[] nameValueList = getNameValueList(item);

            for (NameValueListType curSpecific : nameValueList) {
                final String[] values = curSpecific.getValue();

                boolean firstValueAdded = false;

                final StringBuilder valueBuilder = new StringBuilder();

                for (String curValue : values)
                {
                    if (!firstValueAdded)
                    {
                        firstValueAdded = true;
                    }
                    else
                    {
                        valueBuilder.append(", ");
                    }
                    valueBuilder.append(curValue);
                }

                result.put(curSpecific.getName(), valueBuilder.toString());
            }
        } catch (final Exception exception) {
            LOGGER.error("", exception);
        }

        return result;
    }

    protected NameValueListType[] getNameValueList(final ItemType aItem) {
        final NameValueListArrayType itemSpecifics = aItem.getItemSpecifics();

        if (itemSpecifics != null)
        {
            itemSpecifics.getNameValueList();
        }

        return new NameValueListType[0];
    }

    protected GetItemCall createGetItemCall(final ApiContext aApiContext) {
        return new GetItemCall(aApiContext);
    }

    protected ApiContext createApiContext() {
        ApiContext apiContext = new ApiContext();

        final ApiCredential apiCredential = new ApiCredential();
        apiCredential.seteBayToken(EBAY_TOKEN);

        apiContext.setApiCredential(apiCredential);
        apiContext.setApiServerUrl(API_SERVER_URL);
        apiContext.setSite(SiteCodeType.US);
        return apiContext;
    }
}
