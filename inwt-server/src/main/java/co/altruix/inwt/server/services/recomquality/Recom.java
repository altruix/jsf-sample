package co.altruix.inwt.server.services.recomquality;

import co.altruix.inwt.messages.ImageRating;
import org.joda.time.DateTime;

public class Recom {
    public Recom()
    {
        this(null, null, null, null, null, null, null);
    }

    public Recom(final String aRecipientEmail, final String aSubmitterEmail,
                 final DateTime aCreationDateTimeUtc, final DateTime aHotNotMarkingDateTimeUtc,
                 final ImageRating aHotOrNot, final String aEbayPrimaryCategoryName,
                 final String aEbayPrimaryCategoryID) {
        recipientEmail = aRecipientEmail;
        submitterEmail = aSubmitterEmail;
        creationDateTimeUtc = aCreationDateTimeUtc;
        hotNotMarkingDateTimeUtc = aHotNotMarkingDateTimeUtc;
        hotOrNot = aHotOrNot;
        ebayPrimaryCategoryName = aEbayPrimaryCategoryName;
        ebayPrimaryCategoryID = aEbayPrimaryCategoryID;
    }

    private String recipientEmail;
    private String submitterEmail;
    private DateTime creationDateTimeUtc;
    private DateTime hotNotMarkingDateTimeUtc;
    private ImageRating hotOrNot;
    private String ebayPrimaryCategoryName;
    private String ebayPrimaryCategoryID;

    public String getRecipientEmail() {
        return recipientEmail;
    }

    public void setRecipientEmail(final String aRecipientEmail) {
        recipientEmail = aRecipientEmail;
    }

    public String getSubmitterEmail() {
        return submitterEmail;
    }

    public void setSubmitterEmail(final String aSubmitterEmail) {
        submitterEmail = aSubmitterEmail;
    }

    public DateTime getCreationDateTimeUtc() {
        return creationDateTimeUtc;
    }

    public void setCreationDateTimeUtc(final DateTime aCreationDateTimeUtc) {
        creationDateTimeUtc = aCreationDateTimeUtc;
    }

    public DateTime getHotNotMarkingDateTimeUtc() {
        return hotNotMarkingDateTimeUtc;
    }

    public void setHotNotMarkingDateTimeUtc(final DateTime aHotNotMarkingDateTimeUtc) {
        hotNotMarkingDateTimeUtc = aHotNotMarkingDateTimeUtc;
    }

    public ImageRating getHotOrNot() {
        return hotOrNot;
    }

    public void setHotOrNot(final ImageRating aHotOrNot) {
        hotOrNot = aHotOrNot;
    }

    public String getEbayPrimaryCategoryName() {
        return ebayPrimaryCategoryName;
    }

    public void setEbayPrimaryCategoryName(final String aEbayPrimaryCategoryName) {
        ebayPrimaryCategoryName = aEbayPrimaryCategoryName;
    }

    public String getEbayPrimaryCategoryID() {
        return ebayPrimaryCategoryID;
    }

    public void setEbayPrimaryCategoryID(final String aEbayPrimaryCategoryID) {
        ebayPrimaryCategoryID = aEbayPrimaryCategoryID;
    }
}
