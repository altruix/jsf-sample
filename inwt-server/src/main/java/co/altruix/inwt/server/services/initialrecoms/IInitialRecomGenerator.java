package co.altruix.inwt.server.services.initialrecoms;

import ru.altruix.androidprototyping.server.persistence.IPersistence;

public interface IInitialRecomGenerator {
    void createInitialRecommendations(final IPersistence aPersistence, final String aNewUserEmail) throws
            InterruptedException;
}
