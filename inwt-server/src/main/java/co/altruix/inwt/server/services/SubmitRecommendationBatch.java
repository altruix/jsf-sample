package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.SubmitRecommendationBatchRequest;
import co.altruix.inwt.messages.SubmitRecommendationBatchResponse;
import co.altruix.inwt.server.actions.SubmitRecommendationBatchAction;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

@Path("/"+ SubmitRecommendationBatchRequest.SERVICE_NAME)
public class SubmitRecommendationBatch extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SubmitRecommendationBatch.class);

    @POST
    @Produces("text/plain")
    public String submitRecommendationBatch(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final SubmitRecommendationBatchRequest request = mapper.readValue(aRequest,
                    SubmitRecommendationBatchRequest.class);

            final SubmitRecommendationBatchResponse response =
                    getInwtPersistence().runAction(new SubmitRecommendationBatchAction(
                            request));

            return mapper.writeValueAsString(response);
        } catch (final JsonParseException exception) {
            LOGGER.error(SubmitRecommendationBatchRequest.SERVICE_NAME, exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error(SubmitRecommendationBatchRequest.SERVICE_NAME, exception);
        } catch (final IOException exception) {
            LOGGER.error(SubmitRecommendationBatchRequest.SERVICE_NAME, exception);
        } catch (final InterruptedException exception) {
            LOGGER.error(SubmitRecommendationBatchRequest.SERVICE_NAME, exception);
        } catch (final Exception exception) {
            LOGGER.error(SubmitRecommendationBatchRequest.SERVICE_NAME, exception);
        }

        return "";
    }
}
