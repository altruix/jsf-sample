package co.altruix.inwt.server.actions.submitrecommendationbatch;

import java.util.List;

public interface IEbayItemData {
    String getItemId();

    void setItemId(String aItemId);

    String getViewItemURLForNaturalSearch();

    void setViewItemURLForNaturalSearch(String aViewItemURLForNaturalSearch);

    String getListingType();

    void setListingType(String aListingType);

    List<String> getPictureUrls();

    void setPictureUrls(List<String> aPictureUrls);

    String getPrimaryCategoryID();

    void setPrimaryCategoryID(String aPrimaryCategoryID);

    String getPrimaryCategoryName();

    void setPrimaryCategoryName(String aPrimaryCategoryName);

    ConvertedCurrentPrice getConvertedCurrentPrice();

    void setConvertedCurrentPrice(ConvertedCurrentPrice aConvertedCurrentPrice);

    String getTitle();

    void setTitle(String aTitle);
}
