package co.altruix.inwt.server.services;

import co.altruix.inwt.model.UserBehaviourEvent;
import co.altruix.inwt.server.actions.LogUserBehaviourEventAction;
import ru.altruix.androidprototyping.server.persistence.IPersistence;

public final class InwtServicesUtils {
    private InwtServicesUtils() {
    }

    public static void logUserBehaviour(final IPersistence aPersistence, final String aUserEmail,
                                        final UserBehaviourEvent aEvent) throws
            InterruptedException {
        aPersistence.runAction(new LogUserBehaviourEventAction(aUserEmail,aEvent));
    }

    public static void logUserBehaviour(final IPersistence aPersistence, final String aUserEmail,
                                        final UserBehaviourEvent aEvent,
                                        final String aMessage) throws
            InterruptedException {
        aPersistence.runAction(new LogUserBehaviourEventAction(aUserEmail, aEvent, aMessage));
    }

}
