package co.altruix.inwt.server.actions.specifics;

import co.altruix.inwt.server.actions.InwtPersistenceAction;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Map;

import static co.altruix.inwt.server.actions.InwtPersistenceAction.SET;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class AddSpecificsDataAction extends InwtPersistenceAction<Boolean> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AddSpecificsDataAction.class);
    public static final String EXISTS = "$exists";

    private final IEbayAspectDataReader ebayAspectDataReader;

    public AddSpecificsDataAction()
    {
        this(new EbayAspectDataReader());
    }

    public AddSpecificsDataAction(final IEbayAspectDataReader aEbayAspectDataReader) {
        ebayAspectDataReader = aEbayAspectDataReader;
    }

    @Override
    protected Boolean runOnMongo(final IMongoPersistenceState aPersistenceState) {
        /**
         * Find one document, which has no aspect data
         */
        final BasicDBObject query = new BasicDBObject();
        query.put(FIELD_EBAY_ASPECTS, new BasicDBObject(EXISTS, FALSE));
        query.put(FIELD_EBAY_ITEM_ID, new BasicDBObject(EXISTS, TRUE));

        final DBCollection userProducts = getUserProductsCollection(aPersistenceState);

        final DBObject record = userProducts.findOne(query);

        if (record != null)
        {
            final Map<String,String> aspectData = ebayAspectDataReader.getAspectData(
                    (String) record.get(FIELD_EBAY_ITEM_ID));

            final ArrayList ebayAspects = convertAspectDataToArrayList(aspectData);

            addEbayAspectData((ObjectId)record.get("_id"), ebayAspects, userProducts);

            return TRUE;
        }

        return FALSE;
    }

    protected ArrayList convertAspectDataToArrayList(final Map<String, String> aAspectData) {
        final ArrayList ebayAspects = new ArrayList();

        for (final Map.Entry<String, String> curEntry : aAspectData.entrySet()) {
            ebayAspects.add(new BasicDBObject(curEntry.getKey(), curEntry.getValue()));
        }
        return ebayAspects;
    }

    protected DBCollection getUserProductsCollection(
            final IMongoPersistenceState aPersistenceState) {
        return aPersistenceState.getDb().getCollection(COLLECTION_USER_PRODUCTS);
    }

    protected void addEbayAspectData(final ObjectId aId, final ArrayList aEbayAspects,
                                   final DBCollection aUserProducts) {
        final BasicDBObject query = new BasicDBObject("_id", aId);

        final DBObject update = new BasicDBObject();

        update.put(SET, new BasicDBObject(FIELD_EBAY_ASPECTS, aEbayAspects));

        aUserProducts.update(query, update);
    }
}
