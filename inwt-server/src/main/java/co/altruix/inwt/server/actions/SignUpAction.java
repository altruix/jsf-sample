package co.altruix.inwt.server.actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import co.altruix.inwt.messages.SignUpRequest;
import co.altruix.inwt.messages.SignUpResponse;
import co.altruix.inwt.messages.SignUpResult;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;

public class SignUpAction extends InwtPersistenceAction<SignUpResponse> {
	private final SignUpRequest request;
	private final String saltedHash;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SignUpAction.class);

	public SignUpAction(final SignUpRequest aRequest, final String aSaltedHash) {
		request = aRequest;
		saltedHash = aSaltedHash;
	}

	@Override
	protected SignUpResponse runOnMongo(
			final IMongoPersistenceState aPersistenceState) {
		
		LOGGER.debug("aPersistenceState: " + aPersistenceState);
		LOGGER.debug("aPersistenceState.getDb(): " + aPersistenceState.getDb());
		LOGGER.debug("aPersistenceState.getCollection(): " + aPersistenceState.getDb().getCollection(
				COLLECTION_USERS));
		
		final DBCollection users = aPersistenceState.getDb().getCollection(
				COLLECTION_USERS);

		final BasicDBObject userExistsQuery = new BasicDBObject(
				FIELD_USERS_EMAIL, request.getEmail());

		final DBCursor userExistsCursor = users.find(userExistsQuery);

		if (userExistsCursor.hasNext()) {
			userExistsCursor.close();
			return new SignUpResponse(SignUpResult.USER_ALREADY_EXISTS);
		}

		userExistsCursor.close();

		final BasicDBObject userRecord = new BasicDBObject(FIELD_USERS_EMAIL,
				request.getEmail()).append(FIELD_USERS_PASSWORD_HASH, saltedHash);
		users.insert(userRecord);

		return new SignUpResponse(SignUpResult.INSERTED);
	}
}
