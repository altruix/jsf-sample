package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.NewImageAvailabilityRequest;
import co.altruix.inwt.messages.NewImageAvailabilityResponse;
import co.altruix.inwt.messages.RecommendationResult;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import ru.altruix.androidprototyping.server.persistence.IPersistenceAction;

import java.util.HashMap;
import java.util.Map;

import static co.altruix.inwt.messages.RecommendationResult.*;

public class NewImageAvailabilityAction extends
        InwtPersistenceAction<NewImageAvailabilityResponse> {
    private final NewImageAvailabilityRequest request;

    public NewImageAvailabilityAction(final NewImageAvailabilityRequest aRequest) {
        request = aRequest;
    }

    @Override
    protected NewImageAvailabilityResponse runOnMongo(
            final IMongoPersistenceState aPersistenceState) {
        final Map<String,Object> queryMap = new HashMap<String,Object>();
        queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL, request.getUserEmail());
        queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, false);

        final DBCollection userProducts = getCollection(aPersistenceState);

        final DBCursor cursor = userProducts.find(new BasicDBObject(queryMap));

        final NewImageAvailabilityResponse response = new NewImageAvailabilityResponse();

        response.setResult(cursor.hasNext() ? NEW_IMAGE_AVAILABLE : NO_NEW_IMAGES);

        cursor.close();

        return response;
    }

    protected DBCollection getCollection(final IMongoPersistenceState aPersistenceState) {
        return aPersistenceState.getDb().getCollection(
                COLLECTION_RECOMMENDATIONS);
    }
}
