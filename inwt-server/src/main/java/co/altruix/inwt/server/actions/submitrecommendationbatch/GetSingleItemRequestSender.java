package co.altruix.inwt.server.actions.submitrecommendationbatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class GetSingleItemRequestSender implements IGetSingleItemRequestSender {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(GetSingleItemRequestSender.class);

    private IGetSingleItemResponseParser responseParser;

    public GetSingleItemRequestSender()
    {
        this(new GetSingleItemResponseParser());
    }

    public GetSingleItemRequestSender(final GetSingleItemResponseParser aResponseParser) {
        responseParser = aResponseParser;
    }

    @Override
    public IEbayItemData getEbayItemData(final String aItemId, final String aAppId) {
        URL url;
        HttpURLConnection connection = null;
        IEbayItemData result = null;
        try {
            //Create connection
            url = createUrl(aItemId, aAppId);

            LOGGER.debug("url: " + url);

            connection = openConnection(url);

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }
            rd.close();

            final String responseText = response.toString();

            result = responseParser.parse(responseText.trim());
        } catch (final Exception exception) {
            LOGGER.error("", exception);
        } finally {
            if(connection != null) {
                connection.disconnect();
            }
        }

        return result;
    }

    protected HttpURLConnection openConnection(final URL aUrl) throws IOException {
        return (HttpURLConnection) aUrl.openConnection();
    }

    protected URL createUrl(final String aItemId, final String aAppId) throws
            MalformedURLException {
        return new URL("http://open.api.ebay" +
                ".com/shopping?callname=GetSingleItem&responseencoding=XML&appid=" +
                aAppId + "&siteid=0&version=515&ItemID=" + aItemId);
    }
}
