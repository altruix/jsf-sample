package co.altruix.inwt.server.services;

import java.io.IOException;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import co.altruix.inwt.model.UserBehaviourEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.altruix.inwt.messages.LoginRequest;
import co.altruix.inwt.messages.LoginResponse;
import co.altruix.inwt.server.actions.ReadPasswordHashAction;
import co.altruix.inwt.server.password.IPasswordManager;
import co.altruix.inwt.server.password.PasswordManager;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.altruix.androidprototyping.server.persistence.IPersistence;

import static co.altruix.inwt.model.UserBehaviourEvent.LOGIN_ATTEMPT;

@Path("/Login")
public class Login extends AbstractInwtService {
	private static final Logger LOGGER = LoggerFactory.getLogger(Login.class);
	
	private final IPasswordManager passwordManager;
	
	public Login()
	{
		this(new PasswordManager());
	}
	
	public Login(final IPasswordManager aPasswordManager)
	{
		passwordManager = aPasswordManager;
	}
	
	
	@POST
	@Produces("text/plain")
	public String login(@FormParam("request") final String aRequest) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
            final LoginRequest request = convertStringToRequest(aRequest, mapper);

            final String passwordHash = getInwtPersistence().runAction(
					new ReadPasswordHashAction(request.getEmail()));

			final boolean success = 
					passwordManager.check(request.getPassword(), passwordHash);
			
			final LoginResponse response = new LoginResponse();
			
			response.setSuccess(success);

            logUserBehaviour(getInwtPersistence(), request.getEmail(), success);

			return writeValueAsString(mapper, response);
		} catch (final JsonParseException exception) {
			LOGGER.error("signUp", exception);
		} catch (final JsonMappingException exception) {
			LOGGER.error("signUp", exception);
		} catch (final IOException exception) {
			LOGGER.error("signUp", exception);
		} catch (final InterruptedException exception) {
			LOGGER.error("signUp", exception);
		} catch (final Exception exception) {
			LOGGER.error("signUp", exception);
		}

		return "";
	}

    protected String writeValueAsString(final ObjectMapper aMapper,
                                      final LoginResponse aResponse) throws
            JsonProcessingException {
        return aMapper.writeValueAsString(aResponse);
    }

    protected LoginRequest convertStringToRequest(final String aRequest,
                                                final ObjectMapper aMapper) throws
            IOException {
        return aMapper.readValue(aRequest, LoginRequest.class);
    }

    protected void logUserBehaviour(final IPersistence aInwtPersistence, final String aEmail,
                                  final boolean aSuccess) {
        try {
            InwtServicesUtils.logUserBehaviour(aInwtPersistence, aEmail,
                    LOGIN_ATTEMPT, "Success: " + aSuccess);
        } catch (final InterruptedException exception) {
            LOGGER.error("", exception);
        }

    }
}
