package co.altruix.inwt.server.ebay;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.altruix.androidprototyping.server.persistence.IPersistence;

import com.ebay.services.finding.SearchResult;

@DisallowConcurrentExecution
public class EbayCrawler implements Job {
	private static final String CATEGORY_EARRINGS = "50647";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(EbayCrawler.class);
	public final static String PERSISTENCE = "persistence";
	private final IEbayDataReader ebayDataReader;
	private final IEbayDataSaver ebayDataSaver;

	public EbayCrawler() {
		this(new EbayDataReader(), new EbayDataSaver());
	}

	public EbayCrawler(final IEbayDataReader aEbayDataReader,
			final IEbayDataSaver aEbayDataSaver) {
		ebayDataReader = aEbayDataReader;
		ebayDataSaver = aEbayDataSaver;
	}

	@Override
	public void execute(final JobExecutionContext aContext)
			throws JobExecutionException {
		final IPersistence persistence = (IPersistence) aContext.getJobDetail()
				.getJobDataMap().get(PERSISTENCE);
		
		LOGGER.debug("persistence: " + persistence);
		
		final SearchResult searchResult = ebayDataReader
				.findEbayItems(CATEGORY_EARRINGS);
		ebayDataSaver.saveEbaySearchResults(persistence, searchResult);
	}
}
