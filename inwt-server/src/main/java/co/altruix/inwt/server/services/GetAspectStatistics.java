package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.GetAspectStatisticsRequest;
import co.altruix.inwt.messages.GetAspectStatisticsResponse;
import co.altruix.inwt.server.actions.GetAspectStatisticsAction;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

@Path("/"+ GetAspectStatisticsRequest.SERVICE_NAME)
public class GetAspectStatistics extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetAspectStatistics.class);

    @POST
    @Produces("text/plain")
    public String getAspectStatistics(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final GetAspectStatisticsRequest request = mapper.readValue(aRequest, GetAspectStatisticsRequest.class);

            final GetAspectStatisticsResponse response =
                    getInwtPersistence().runAction(new GetAspectStatisticsAction(request));

            return mapper.writeValueAsString(response);
        } catch (final JsonParseException exception) {
            logException(exception);
        } catch (final JsonMappingException exception) {
            logException(exception);
        } catch (final IOException exception) {
            logException(exception);
        } catch (final InterruptedException exception) {
            logException(exception);
        } catch (final Exception exception) {
            logException(exception);
        }

        return "";
    }

    protected void logException(final Exception exception) {
        LOGGER.error(GetAspectStatisticsRequest.SERVICE_NAME, exception);
    }
}
