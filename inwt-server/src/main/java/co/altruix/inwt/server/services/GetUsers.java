package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.GetUsersRequest;
import co.altruix.inwt.messages.GetUsersResponse;
import co.altruix.inwt.server.actions.GetUsersAction;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

@Path("/"+ GetUsersRequest.SERVICE_NAME)
public class GetUsers extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetUsers.class);

    @POST
    @Produces("text/plain")
    public String getUsers(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final GetUsersRequest request = mapper.readValue(aRequest,
                    GetUsersRequest.class);

            final GetUsersResponse response =
                    getInwtPersistence().runAction(new GetUsersAction(request));

            return mapper.writeValueAsString(response);
        } catch (final JsonParseException exception) {
            LOGGER.error(GetUsersRequest.SERVICE_NAME, exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error(GetUsersRequest.SERVICE_NAME, exception);
        } catch (final IOException exception) {
            LOGGER.error(GetUsersRequest.SERVICE_NAME, exception);
        } catch (final InterruptedException exception) {
            LOGGER.error(GetUsersRequest.SERVICE_NAME, exception);
        } catch (final Exception exception) {
            LOGGER.error(GetUsersRequest.SERVICE_NAME, exception);
        }

        return "";
    }

}
