package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.GetAspectsRequest;
import co.altruix.inwt.messages.GetAspectsResponse;
import co.altruix.inwt.model.EbayAspect;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class GetAspectsAction extends
        InwtPersistenceAction<GetAspectsResponse> {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetAspectsAction.class);
    private final GetAspectsRequest request;

    public GetAspectsAction(final GetAspectsRequest aRequest) {
        request = aRequest;
    }

    @Override
    protected GetAspectsResponse runOnMongo(final IMongoPersistenceState aPersistenceState) {
        LOGGER.debug("GetAspectsAction.runOnMongo");

        /**
         * Get product IDs of user
         */
        final BasicDBList productIds = getProductIds(aPersistenceState);

        LOGGER.debug("productIds.size(): " + productIds.size());

        /**
         * Get aspect information of product IDs, which belong to request.getProductCategoryId();
         */
        final Map<String,Object> queryData = new HashMap<String, Object>();
        queryData.put("_id", new BasicDBObject("$in", productIds));
        queryData.put(InwtPersistenceAction.FIELD_EBAY_PRIMARY_CATEGORY_ID,
                request.getProductCategoryId());
        final BasicDBObject query = createQuery(queryData);

        final DBCollection userProducts = getCollection(aPersistenceState,
                InwtPersistenceAction.COLLECTION_USER_PRODUCTS);

        final DBCursor cursor = userProducts.find(query);

        final List<String> aspectNames = new LinkedList<String>();

        LOGGER.debug("cursor.hasNext(): " + cursor.hasNext());

        while (cursor.hasNext())
        {
            final BasicDBObject curRecord = (BasicDBObject) cursor.next();

            final BasicDBList aspectList = (BasicDBList) curRecord.get(
                    InwtPersistenceAction.FIELD_EBAY_ASPECTS);

            for (int i=0; i < aspectList.size(); i++)
            {
                final BasicDBObject curAspecDataItem = (BasicDBObject) aspectList.get(i);

                final Object[] entries = curAspecDataItem.entrySet().toArray();

                if ((entries != null) && (entries.length > 0))
                {
                    final Map.Entry entry = (Map.Entry) entries[0];

                    final String curAspectName = (String) entry.getKey();

                    if (!aspectNames.contains(curAspectName))
                    {
                        aspectNames.add(curAspectName);
                    }

                }

            }
        }

        final List<EbayAspect> aspects = convertAspectNamesToAspectList(aspectNames);

        LOGGER.debug("aspects.size(): " + aspects.size());

        final GetAspectsResponse response = new GetAspectsResponse();
        response.setAspects(aspects);

        return response;
    }

    protected BasicDBObject createQuery(final Map<String, Object> aQueryData) {
        return new BasicDBObject(aQueryData);
    }

    protected BasicDBList getProductIds(final IMongoPersistenceState aPersistenceState) {
        final List<byte[]> productIdAsByteArrays = getProductIdAsByteArrays(
                getCollection(aPersistenceState, COLLECTION_RECOMMENDATIONS),
                request.getUserEmail());
        return convertByteArrayToObjectId(productIdAsByteArrays);
    }

    protected List<EbayAspect> convertAspectNamesToAspectList(final List<String> aAspectNames) {
        final List<EbayAspect> aspects = new LinkedList<EbayAspect>();
        for (final String curAspectName : aAspectNames)
        {
            final EbayAspect aspect = new EbayAspect();
            aspect.setName(curAspectName);
            aspect.setProductCategory(request.getProductCategoryId());

            aspects.add(aspect);
        }
        return aspects;
    }

    protected BasicDBList convertByteArrayToObjectId(final List<byte[]> aProductIdBytes) {
        return convertByteArrayToObjectId2(aProductIdBytes);
    }

    protected DBCollection getCollection(final IMongoPersistenceState aPersistenceState,
                                         final String aCollName) {
        return aPersistenceState.getDb().getCollection(aCollName);
    }

    protected List<byte[]> getProductIdAsByteArrays(final DBCollection aRecoms,
                                                    final String aUserEmail) {
        return getProductIdsLikedByUser(aRecoms, aUserEmail);
    }
}
