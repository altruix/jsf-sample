package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.GetAspectsRequest;
import co.altruix.inwt.messages.GetAspectsResponse;
import co.altruix.inwt.server.actions.GetAspectsAction;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

@Path("/"+ GetAspectsRequest.SERVICE_NAME)
public class GetAspects extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetAspects.class);

    @POST
    @Produces("text/plain")
    public String getAspects(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final GetAspectsRequest request = mapper.readValue(aRequest, GetAspectsRequest.class);

            final GetAspectsResponse response =
                    getInwtPersistence().runAction(new GetAspectsAction(request));

            return mapper.writeValueAsString(response);
        } catch (final JsonParseException exception) {
            LOGGER.error(GetAspectsRequest.SERVICE_NAME, exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error(GetAspectsRequest.SERVICE_NAME, exception);
        } catch (final IOException exception) {
            LOGGER.error(GetAspectsRequest.SERVICE_NAME, exception);
        } catch (final InterruptedException exception) {
            LOGGER.error(GetAspectsRequest.SERVICE_NAME, exception);
        } catch (final Exception exception) {
            LOGGER.error(GetAspectsRequest.SERVICE_NAME, exception);
        }

        return "";
    }
}
