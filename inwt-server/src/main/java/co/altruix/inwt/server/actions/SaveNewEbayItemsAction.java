package co.altruix.inwt.server.actions;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.bson.types.Binary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ebay.services.finding.Category;
import com.ebay.services.finding.SearchItem;
import com.ebay.services.finding.SearchResult;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

import co.altruix.inwt.server.persistence.IMongoPersistenceState;

public class SaveNewEbayItemsAction extends InwtPersistenceAction<Object> {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SaveNewEbayItemsAction.class);

	private final List<String> ebayProductIds;
	private final SearchResult searchResult;
	
	public SaveNewEbayItemsAction(final List<String> aEbayProductIds,
			final SearchResult aSearchResult) {
		ebayProductIds = aEbayProductIds;
		searchResult = aSearchResult;
	}

	@Override
	protected Object runOnMongo(final IMongoPersistenceState aPersistenceState) {
		final List<SearchItem> items = searchResult.getItem();
		
		final DBCollection collection = aPersistenceState.getDb().getCollection(
				FilterExistingEbayProductIdsAction.EBAY_PRODUCT);
		
		for (final SearchItem curItem : items)
		{
			if (ebayProductIds.contains(curItem.getViewItemURL()))
			{
				final String viewItemURL = curItem.getViewItemURL();
				BasicDBObject record = new BasicDBObject("url", viewItemURL);
				
				final Category primaryCategory = curItem.getPrimaryCategory();
				
				if (primaryCategory != null)
				{
					record = record.append("primaryCategoryId", primaryCategory.getCategoryId());
				}
								
				
				final Binary image = readImage(curItem.getGalleryURL());
				
				if (image != null)
				{
					record = record.append("image", image).append("galleryURL", curItem.getGalleryURL());
				}
				
				collection.insert(record);
			}
		}
		
		return null;
	}

	private Binary readImage(final String viewItemURL) {
		InputStream inputStream = null;
		Binary image = null;
		try
		{
			inputStream = new URL(viewItemURL).openStream();;
			byte bytes[] = new byte[inputStream.available()];
			inputStream.read(bytes);
			
			image = new Binary(bytes);
		}
		catch (final IOException exception)
		{
			LOGGER.error("", exception);
		}
		finally
		{
			IOUtils.closeQuietly(inputStream);
		}
		return image;
	}
}
