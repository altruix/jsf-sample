package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.RateProductImageRequest;
import co.altruix.inwt.messages.RateProductImageResponse;
import co.altruix.inwt.server.actions.RateProductImageAction;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.altruix.androidprototyping.server.persistence.IPersistence;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

import static co.altruix.inwt.model.UserBehaviourEvent.IMAGE_RATING;

@Path("/"+ RateProductImageRequest.SERVICE_NAME)
public class RateProductImage extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RateProductImage.class);

    @POST
    @Produces("text/plain")
    public String rateProductImage (@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = createObjectMapper();
            final RateProductImageRequest request = mapper.readValue(aRequest,
                    RateProductImageRequest.class);

            getInwtPersistence().runAction(new RateProductImageAction(request));

            final RateProductImageResponse response = new RateProductImageResponse();

            logUserBehaviour(getInwtPersistence(), request.getEmail());

            return mapper.writeValueAsString(response);
        } catch (final JsonParseException exception) {
            LOGGER.error(RateProductImageRequest.SERVICE_NAME, exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error(RateProductImageRequest.SERVICE_NAME, exception);
        } catch (final IOException exception) {
            LOGGER.error(RateProductImageRequest.SERVICE_NAME, exception);
        } catch (final InterruptedException exception) {
            LOGGER.error(RateProductImageRequest.SERVICE_NAME, exception);
        } catch (final Exception exception) {
            LOGGER.error(RateProductImageRequest.SERVICE_NAME, exception);
        }

        return "";
    }

    protected ObjectMapper createObjectMapper() {
        return new ObjectMapper();
    }

    protected void logUserBehaviour(final IPersistence aInwtPersistence, final String aEmail) {
        try {
            InwtServicesUtils.logUserBehaviour(aInwtPersistence, aEmail,
                    IMAGE_RATING);
        } catch (final InterruptedException exception) {
            LOGGER.error("", exception);
        }


    }

}
