package co.altruix.inwt.server.actions.submitrecommendationbatch;

public interface IGetSingleItemRequestSender {
    IEbayItemData getEbayItemData(final String aItemId, final String aAppId);
}
