package co.altruix.inwt.server.services.signup;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;


public class JavaMailValidator implements IEMailValidator {

	@Override
	public boolean isEmailValid(final String aEmail) {
		boolean result = true;
		try {
			final InternetAddress emailAddr = new InternetAddress(aEmail);
			emailAddr.validate();
		} catch (final AddressException exception) {
			result = false;
		}
		return result;
	}

}
