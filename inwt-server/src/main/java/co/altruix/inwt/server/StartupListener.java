package co.altruix.inwt.server;

import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import co.altruix.inwt.server.specifics.SpecificsDataAdder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.ee.servlet.QuartzInitializerListener;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import ru.altruix.androidprototyping.server.persistence.IPersistence;

import co.altruix.inwt.server.ebay.EbayCrawler;
import static org.quartz.DateBuilder.evenMinuteDate;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;


public class StartupListener implements ServletContextListener {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(StartupListener.class);
    private Scheduler scheduler;

    public StartupListener() {
	}

	public void contextInitialized(
			final ServletContextEvent aServletContextEvent) {
		
		LOGGER.debug("contextInitialized");
		
		final ServletContext context = aServletContextEvent.getServletContext();
		
        final SchedulerFactory factory = createSchedulerFactory();
        
        setupScheduledJobs(context, factory);
	}

    protected SchedulerFactory createSchedulerFactory() {
        return new StdSchedulerFactory();
    }

    protected void setupScheduledJobs(final ServletContext aContext,
            final SchedulerFactory aFactory) {
    	LOGGER.debug("setupScheduledJobs");
        try {
            scheduler = aFactory.getScheduler();
            final ApplicationContext appContext = getAppContext(aContext);
            final IPersistence persistence = (IPersistence) appContext.getBean("persistence");

            final JobDetail job = newJob(EbayCrawler.class)
                    .build();
            Trigger trigger = newTrigger()
                    .withIdentity("trigger1", "group1")
                    .startAt(new Date())
                    // .startAt(evenMinuteDate(new Date()))
                    .withSchedule(simpleSchedule().withIntervalInMinutes(2).repeatForever())
                    .build();


			job.getJobDataMap().put(EbayCrawler.PERSISTENCE, persistence);
			
            // job.getJobDataMap().put("emailSender", emailSender);
            // job.getJobDataMap().put("servletContext", aContext);

            // scheduler.scheduleJob(job, trigger);

            final JobDetail specificsDataAdderJob = createSpecificsDataAdderJob(persistence);
            final Trigger specificsDataAdderTrigger = createSpecificsDataAdderTrigger();
            scheduler.scheduleJob(specificsDataAdderJob, specificsDataAdderTrigger);

            // LOGGER.debug("job scheduled");
        } catch (final SchedulerException exception) {
            LOGGER.error("", exception);
        }

    }

    protected WebApplicationContext getAppContext(final ServletContext aContext) {
        return WebApplicationContextUtils.getWebApplicationContext(aContext);
    }

    protected Trigger createSpecificsDataAdderTrigger() {
        return newTrigger()
                .withIdentity("trigger1", "group1")
                .startAt(new Date())
                .withSchedule(simpleSchedule().withIntervalInMinutes(5).repeatForever())
                .build();
    }

    protected JobDetail createSpecificsDataAdderJob(final IPersistence aPersistence) {
        final JobDetail job = newJob(SpecificsDataAdder.class).build();
        job.getJobDataMap().put(EbayCrawler.PERSISTENCE, aPersistence);
        return job;
    }

    public void contextDestroyed(final ServletContextEvent aServletContextEvent) {
        if (scheduler != null)
        {
            try {
                scheduler.shutdown();
            } catch (final SchedulerException exception) {
                LOGGER.error("", exception);
            }
        }
	}
}
