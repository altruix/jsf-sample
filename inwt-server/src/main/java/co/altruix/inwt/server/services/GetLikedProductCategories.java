package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.GetLikedProductCategoriesRequest;
import co.altruix.inwt.messages.GetLikedProductCategoriesResponse;
import co.altruix.inwt.server.actions.GetLikedProductCategoriesAction;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

@Path("/"+ GetLikedProductCategoriesRequest.SERVICE_NAME)
public class GetLikedProductCategories extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetLikedProductCategories.class);

    @POST
    @Produces("text/plain")
    public String getLikedProductCategories(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final GetLikedProductCategoriesRequest request = mapper.readValue(aRequest,
                    GetLikedProductCategoriesRequest.class);

            final GetLikedProductCategoriesResponse response =
                    getInwtPersistence().runAction(new GetLikedProductCategoriesAction(request));

            return mapper.writeValueAsString(response);
        } catch (final JsonParseException exception) {
            LOGGER.error(GetLikedProductCategoriesRequest.SERVICE_NAME, exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error(GetLikedProductCategoriesRequest.SERVICE_NAME, exception);
        } catch (final IOException exception) {
            LOGGER.error(GetLikedProductCategoriesRequest.SERVICE_NAME, exception);
        } catch (final InterruptedException exception) {
            LOGGER.error(GetLikedProductCategoriesRequest.SERVICE_NAME, exception);
        } catch (final Exception exception) {
            LOGGER.error(GetLikedProductCategoriesRequest.SERVICE_NAME, exception);
        }

        return "";
    }

}
