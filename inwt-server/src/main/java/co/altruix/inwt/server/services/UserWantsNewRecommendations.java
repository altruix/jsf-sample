package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.UserWantsNewRecommendationsRequest;
import co.altruix.inwt.messages.UserWantsNewRecommendationsResponse;
import co.altruix.inwt.model.UserBehaviourEvent;
import co.altruix.inwt.server.actions.LogUserBehaviourEventAction;
import co.altruix.inwt.server.email.EmailSender;
import co.altruix.inwt.server.email.IEmailSender;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ServletContextAware;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

import static co.altruix.inwt.model.UserBehaviourEvent.REQUESTED_NEW_RECOMMENDATIONS;

@Path("/"+ UserWantsNewRecommendationsRequest.SERVICE_NAME)
public class UserWantsNewRecommendations extends AbstractInwtService implements
        ServletContextAware {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserWantsNewRecommendations.class);

    private final IEmailSender emailSender;
    private ServletContext servletContext;

    public UserWantsNewRecommendations()
    {
        this(new EmailSender());
    }

    public UserWantsNewRecommendations(final IEmailSender aEmailSender) {
        emailSender = aEmailSender;
    }

    @POST
    @Produces("text/plain")
    public String userWantsNewRecommendations(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final UserWantsNewRecommendationsRequest request = readRequest(aRequest, mapper);

            logUserBehaviour(request);

            sendEmail(request.getUserEmail());
            return mapper.writeValueAsString(new UserWantsNewRecommendationsResponse());
        } catch (final JsonParseException exception) {
            LOGGER.error(UserWantsNewRecommendationsRequest.SERVICE_NAME, exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error(UserWantsNewRecommendationsRequest.SERVICE_NAME, exception);
        } catch (final IOException exception) {
            LOGGER.error(UserWantsNewRecommendationsRequest.SERVICE_NAME, exception);
        } catch (final Exception exception) {
            LOGGER.error(UserWantsNewRecommendationsRequest.SERVICE_NAME, exception);
        }

        return "";
    }

    protected void logUserBehaviour(final UserWantsNewRecommendationsRequest aRequest) throws
            InterruptedException {
        InwtServicesUtils.logUserBehaviour(getInwtPersistence(), aRequest.getUserEmail(),
                REQUESTED_NEW_RECOMMENDATIONS);
    }

    protected UserWantsNewRecommendationsRequest readRequest(final String aRequest,
                                                           final ObjectMapper aMapper) throws
            IOException {
        return aMapper.readValue(aRequest,
                        UserWantsNewRecommendationsRequest.class);
    }

    protected void sendEmail(final String aUserEmail) {
        try {
            emailSender.sendEMail("User " + aUserEmail + " wants new recommendations",
                    "User " + aUserEmail + " wants new recommendations",
                    "dp@altruix.co", servletContext);
        } catch (final MessagingException exception) {
            LOGGER.error(UserWantsNewRecommendationsRequest.SERVICE_NAME, exception);
        }
    }

    @Override
    public void setServletContext(final ServletContext aServletContext) {
        servletContext = aServletContext;
    }
}
