package co.altruix.inwt.server.actions;

import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import co.altruix.inwt.server.services.recomquality.Recom;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import org.joda.time.DateTime;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static co.altruix.inwt.messages.ImageRating.HOT;
import static co.altruix.inwt.messages.ImageRating.NOT;
import static java.lang.Boolean.TRUE;

public class ReadProposedRecommendationsAction extends InwtPersistenceAction<List<Recom>> {
    private final String userEmail;
    private final String recommenderEmail;

    public ReadProposedRecommendationsAction(final String aUserEmail,
                                             final String aRecommenderEmail) {
        userEmail = aUserEmail;
        recommenderEmail = aRecommenderEmail;
    }

    @Override
    protected List<Recom> runOnMongo(final IMongoPersistenceState aPersistenceState) {
        final Map<String,Object> data = new HashMap<String, Object>();

        data.put(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL, userEmail);
        data.put(FIELD_COLLECTION_RECOMMENDATIONS_SUBMITTER_EMAIL, recommenderEmail);
        data.put(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, TRUE);

        final BasicDBObject query = createQuery(data);

        final DBCollection recoms = getCollection(aPersistenceState);
        final DBCursor cursor = recoms.find(query);
        final List<Recom> recomList = new LinkedList<Recom>();

        try
        {
            while (cursor.hasNext())
            {
                final BasicDBObject curRecord = (BasicDBObject) cursor.next();

                recomList.add(recordToRecom(curRecord));
            }
        }
        finally
        {
            cursor.close();
        }

        return recomList;
    }

    protected DBCollection getCollection(final IMongoPersistenceState aPersistenceState) {
        return aPersistenceState.getDb().getCollection(COLLECTION_RECOMMENDATIONS);
    }

    protected Recom recordToRecom(final BasicDBObject aRecord) {
        final Recom recom = new Recom();

        recom.setCreationDateTimeUtc(new DateTime(aRecord.getDate(
                FIELD_COLLECTION_RECOMMENDATIONS_CREATION_DATETIME_UTC)));
        recom.setHotNotMarkingDateTimeUtc(new DateTime(aRecord.getDate(
                FIELD_COLLECTION_RECOMMENDATIONS_HOT_NOT_MARKING_DATETIME_UTC)));
        recom.setHotOrNot(aRecord.getBoolean
                (FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER) ? HOT : NOT);

        return recom;
    }

    protected BasicDBObject createQuery(final Map<String, Object> aData) {
        return new BasicDBObject(aData);
    }
}
