package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.GetUserProductsResponse;
import co.altruix.inwt.model.UserProductInfo;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import java.util.LinkedList;
import java.util.List;

public class GetUserProductsAction extends InwtPersistenceAction<GetUserProductsResponse> {
    private final String authorEmail;

    public GetUserProductsAction(final String aAuthorEmail) {
        authorEmail = aAuthorEmail;
    }

    @Override
    protected GetUserProductsResponse runOnMongo(final IMongoPersistenceState aPersistenceState) {

        final DBCollection userProducts = aPersistenceState.getDb().getCollection(
                COLLECTION_USER_PRODUCTS);
        final BasicDBObject query = new BasicDBObject(FIELD_USER_PRODUCTS_CREATOR_EMAIL,
                authorEmail);

        final DBCursor cursor = userProducts.find(query);
        final List<UserProductInfo> productInfoList = new LinkedList<UserProductInfo>();

        try
        {
            while (cursor.hasNext())
            {
                final BasicDBObject curRecord = (BasicDBObject) cursor.next();

                final UserProductInfo userProductInfo = new UserProductInfo();
                userProductInfo.setProductId(curRecord.getObjectId("_id").toByteArray());
                userProductInfo.setName(curRecord.getString(FIELD_USER_PRODUCTS_NAME));
                userProductInfo.setDescription(
                        curRecord.getString(FIELD_USER_PRODUCTS_DESCRIPTION));
                userProductInfo.setAuthorEmail(
                        curRecord.getString(FIELD_USER_PRODUCTS_CREATOR_EMAIL));

                productInfoList.add(userProductInfo);
            }
        }
        finally
        {
            cursor.close();
        }

        final GetUserProductsResponse result = new GetUserProductsResponse();
        result.setUserProducts(productInfoList);
        return result;
    }
}
