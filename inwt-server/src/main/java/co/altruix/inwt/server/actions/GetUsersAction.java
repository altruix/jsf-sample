package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.GetUsersRequest;
import co.altruix.inwt.messages.GetUsersResponse;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import java.util.LinkedList;
import java.util.List;

public class GetUsersAction extends InwtPersistenceAction<GetUsersResponse> {
    public GetUsersAction(final GetUsersRequest aRequest) {
    }

    @Override
    protected GetUsersResponse runOnMongo(final IMongoPersistenceState aPersistenceState) {
        final DBCollection users = aPersistenceState.getDb().getCollection(COLLECTION_USERS);
        final DBCursor cursor = users.find();

        final List<String> userEmails = new LinkedList<String>();
        try
        {
            while (cursor.hasNext())
            {
                final BasicDBObject curRecord = (BasicDBObject) cursor.next();

                userEmails.add(curRecord.getString(FIELD_USERS_EMAIL));
            }
        }
        finally
        {
            cursor.close();
        }


        final GetUsersResponse response = new GetUsersResponse();
        response.setUserEmails(userEmails);

        return response;
    }
}
