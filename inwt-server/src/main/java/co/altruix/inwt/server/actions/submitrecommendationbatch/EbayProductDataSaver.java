package co.altruix.inwt.server.actions.submitrecommendationbatch;

import co.altruix.inwt.server.actions.CreateProductAction;
import co.altruix.inwt.server.actions.InwtPersistenceAction;
import co.altruix.inwt.server.ebay.EbayDataReader;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.WriteConcern;
import com.mongodb.WriteResult;
import org.apache.commons.io.IOUtils;
import org.bson.types.Binary;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_CREATOR_EMAIL;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_DATA;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_FILE_NAME;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_IS_DELETED;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_MIME_TYPE;
import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_PRODUCT_ID;

public class EbayProductDataSaver implements IEbayProductDataSaver {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(EbayProductDataSaver.class);
    public static final String CREATOR_EMAIL = "EbayProductDataSaver@inwt.info";
    public static final String MIME_TYPE_JPEG = "image/jpeg";
    public static final String MIME_TYPE_PNG = "image/png";
    public static final BigDecimal CENTS_IN_EURO = new BigDecimal("100");
    private final IGetSingleItemRequestSender getSingleItemRequestSender;

    public EbayProductDataSaver()
    {
        this(new GetSingleItemRequestSender());
    }

    public EbayProductDataSaver(final IGetSingleItemRequestSender aGetSingleItemRequestSender) {
        getSingleItemRequestSender = aGetSingleItemRequestSender;
    }

    @Override
    public byte[] importEbayProduct(final String aProductUrl, final IMongoPersistenceState
            aPersistenceState) {
        final String itemId = extractItemId(aProductUrl);

        final IEbayItemData ebayItemData = getSingleItemRequestSender.getEbayItemData(itemId,
                EbayDataReader.APPLICATION_ID);

        return saveProductData(ebayItemData, aPersistenceState);
    }

    protected byte[] saveProductData(final IEbayItemData aEbayItemData,
                                   final IMongoPersistenceState aPersistenceState) {
        final byte[] newProductId = saveProductDataExceptImages(aEbayItemData, aPersistenceState);

        saveProductImages(newProductId, aEbayItemData.getPictureUrls(), aPersistenceState);

        return newProductId;
    }

    protected void saveProductImages(final byte[] aNewProductId, final List<String> aPictureUrls,
                                   final IMongoPersistenceState aPersistenceState) {
        final DB db = aPersistenceState.getDb();
        final DBCollection productImagesColl = db.getCollection(
                InwtPersistenceAction.COLLECTION_USER_PRODUCT_IMAGES);

        for (final String curPictureUrl : aPictureUrls)
        {
            final Binary imageData = readImage(curPictureUrl);

            final Map<String,Object> map = new HashMap<String, Object>();

            map.put(FIELD_COLLECTION_USER_PRODUCT_IMAGES_CREATOR_EMAIL, CREATOR_EMAIL);
            map.put(FIELD_COLLECTION_USER_PRODUCT_IMAGES_PRODUCT_ID, aNewProductId);
            map.put(FIELD_COLLECTION_USER_PRODUCT_IMAGES_DATA, imageData);
            final String fileName = extractFileName(curPictureUrl);
            map.put(FIELD_COLLECTION_USER_PRODUCT_IMAGES_FILE_NAME, fileName);
            map.put(FIELD_COLLECTION_USER_PRODUCT_IMAGES_MIME_TYPE, getMimeType(fileName));
            map.put(FIELD_COLLECTION_USER_PRODUCT_IMAGES_IS_DELETED, Boolean.FALSE);

            productImagesColl.insert(WriteConcern.SAFE, createRecordObject(map));
        }
    }

    protected String getMimeType(final String aFileName) {
        final String lowerCaseFileName = aFileName.toLowerCase();

        if (lowerCaseFileName.endsWith(".jpg") || lowerCaseFileName.endsWith(".jpeg"))
        {
            return MIME_TYPE_JPEG;
        }
        else if (lowerCaseFileName.endsWith(".png"))
        {
            return MIME_TYPE_PNG;
        }

        return null;
    }

    protected String extractFileName(final String aCurPictureUrl) {
        final String[] parts1 = aCurPictureUrl.split("\\?");

        final String url = parts1[0];

        final String[] parts2 = url.split("/");

        final String fileName = parts2[parts2.length-1].replace("$", "");

        return fileName;
    }

    protected byte[] saveProductDataExceptImages(final IEbayItemData aEbayItemData,
                                               final IMongoPersistenceState aPersistenceState) {
        final DB db = aPersistenceState.getDb();
        final DBCollection productsColl = db.getCollection(
                InwtPersistenceAction.COLLECTION_USER_PRODUCTS);

        final Map<String,Object> valuesByKeys = new HashMap<String, Object>();
        valuesByKeys.put(InwtPersistenceAction.FIELD_USER_PRODUCTS_CREATOR_EMAIL, CREATOR_EMAIL);
        valuesByKeys.put(InwtPersistenceAction.FIELD_USER_PRODUCTS_NAME, aEbayItemData.getTitle());
        valuesByKeys.put(InwtPersistenceAction.FIELD_USER_PRODUCTS_CREATION_DATETIME_UTC,
                getCurrentUtcTime());
        valuesByKeys.put(InwtPersistenceAction.FIELD_USER_PRODUCTS_PRODUCT_URL,
                aEbayItemData.getViewItemURLForNaturalSearch());

        valuesByKeys.put(InwtPersistenceAction.FIELD_EBAY_ITEM_ID, aEbayItemData.getItemId());
        valuesByKeys.put(InwtPersistenceAction.FIELD_EBAY_VIEW_ITEM_URL_FOR_NATURAL_SEARCH,
                aEbayItemData.getViewItemURLForNaturalSearch());
        valuesByKeys.put(InwtPersistenceAction.FIELD_EBAY_LISTING_TYPE,
                aEbayItemData.getListingType());
        valuesByKeys.put(InwtPersistenceAction.FIELD_EBAY_PRIMARY_CATEGORY_ID,
                aEbayItemData.getPrimaryCategoryID());
        valuesByKeys.put(InwtPersistenceAction.FIELD_EBAY_PRIMARY_CATEGORY_NAME,
                aEbayItemData.getPrimaryCategoryName());
        final ConvertedCurrentPrice convertedCurrentPrice = aEbayItemData.getConvertedCurrentPrice();
        if (convertedCurrentPrice != null)
        {
            valuesByKeys.put(InwtPersistenceAction.FIELD_EBAY_CONVERTED_CURRENT_PRICE_AMOUNT,
                    bigDecimalToInt(convertedCurrentPrice.getAmount()));
            valuesByKeys.put(InwtPersistenceAction.FIELD_EBAY_CONVERTED_CURRENT_PRICE_CURRENCY,
                    convertedCurrentPrice.getCurrencyId());
        }

        final BasicDBObject newProduct = createRecordObject(valuesByKeys);

        final WriteResult writeResult = productsColl.insert(WriteConcern.SAFE, newProduct);

        LOGGER.debug("writeResult: " + writeResult);
        LOGGER.debug("writeResult.getUpsertedId(): " + writeResult.getUpsertedId());

        // newProduct.get("_id")

        // return ((ObjectId)writeResult.getUpsertedId()).toByteArray();
        return ((ObjectId)newProduct.get("_id")).toByteArray();
    }

    protected int bigDecimalToInt(final BigDecimal aAmount) {
        return aAmount.multiply(CENTS_IN_EURO).intValue();
    }

    protected BasicDBObject createRecordObject(final Map<String, Object> aValuesByKeys) {
        return new BasicDBObject(aValuesByKeys);
    }

    protected Date getCurrentUtcTime() {
        return CreateProductAction.getCurrentUtcTime();
    }


    protected String extractItemId(final String aUrl)
    {
        final String[] segments = aUrl.split("/");
        final String lastSegment = segments[segments.length - 1];

        if (lastSegment.contains("?"))
        {
            final String[] segmentsByQuestionMark = lastSegment.split("\\?");
            return segmentsByQuestionMark[0];
        }

        return lastSegment;
    }

    protected Binary readImage(final String viewItemURL) {
        InputStream inputStream = null;
        Binary result = null;

        try
        {
            inputStream = new URL(viewItemURL).openStream();
            result = new Binary(IOUtils.toByteArray(inputStream));
        }
        catch (final IOException exception) 
        {
            LOGGER.error("", exception);
        }
        finally
        {
            if (inputStream != null)
            {
                IOUtils.closeQuietly(inputStream);
            }
        }

        return result;
    }
}
