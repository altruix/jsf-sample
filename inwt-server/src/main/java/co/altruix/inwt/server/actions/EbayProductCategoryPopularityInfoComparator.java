package co.altruix.inwt.server.actions;

import co.altruix.inwt.model.EbayProductCategoryPopularityInfo;

import java.util.Comparator;

public class EbayProductCategoryPopularityInfoComparator
        implements Comparator<EbayProductCategoryPopularityInfo> {
    @Override
    public int compare(final EbayProductCategoryPopularityInfo aObject1,
                       final EbayProductCategoryPopularityInfo aObject2) {
        return aObject2.getNumberOfLikedImages()-aObject1.getNumberOfLikedImages();
    }
}
