package co.altruix.inwt.server.ebay;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.altruix.androidprototyping.server.persistence.IPersistence;

import co.altruix.inwt.server.actions.FilterExistingEbayProductIdsAction;
import co.altruix.inwt.server.actions.NewEbayProductIdInfo;
import co.altruix.inwt.server.actions.SaveNewEbayItemsAction;

import com.ebay.services.finding.SearchItem;
import com.ebay.services.finding.SearchResult;

public class EbayDataSaver implements IEbayDataSaver {
	private static final Logger LOGGER = LoggerFactory.getLogger(EbayDataSaver.class);

	@Override
	public void saveEbaySearchResults(final IPersistence aPersistence,
			final SearchResult aSearchResult) {
		final NewEbayProductIdInfo newEbayProductInfo = composeNewEbayProductIdInfo(aSearchResult);
		
		final FilterExistingEbayProductIdsAction action1 = new FilterExistingEbayProductIdsAction(newEbayProductInfo);
		
		try {
			final NewEbayProductIdInfo newEbayProductInfo2 = aPersistence.runAction(action1);
			
			final SaveNewEbayItemsAction action2 = new SaveNewEbayItemsAction(newEbayProductInfo2.getEbayProductIds(), aSearchResult);
			
			aPersistence.runAction(action2);
		} catch (final InterruptedException exception) {
			LOGGER.error("", exception);
		}
	}

	private NewEbayProductIdInfo composeNewEbayProductIdInfo(
			final SearchResult aSearchResult) {
		final List<String> ebayProductIds = new LinkedList<String>();

		final List<SearchItem> items = aSearchResult.getItem();
		
		for (final SearchItem curItem : items)
		{
			ebayProductIds.add(curItem.getViewItemURL());
		}
		
		final NewEbayProductIdInfo newEbayProductInfo = new NewEbayProductIdInfo();
		newEbayProductInfo.setEbayProductIds(ebayProductIds);
		return newEbayProductInfo;
	}
}
