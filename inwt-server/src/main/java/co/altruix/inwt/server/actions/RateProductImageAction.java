package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.ImageRating;
import co.altruix.inwt.messages.RateProductImageRequest;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Boolean.TRUE;

public class RateProductImageAction extends InwtPersistenceAction<Object> {
    private final RateProductImageRequest request;

    public RateProductImageAction(final RateProductImageRequest aRequest) {
        request = aRequest;
    }

    @Override
    protected Object runOnMongo(final IMongoPersistenceState aPersistenceState) {
        final Map<String, Object> queryMap = createDataMap(request);

        final DBCollection recommendationsColl = getCollection(aPersistenceState);
        final BasicDBObject query = createBasicDBObject(queryMap);

        final DBObject update = new BasicDBObject();
        update.put(SET, new BasicDBObject(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, TRUE).
                append(FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER, imageRatingToBoolean(request)).
                append(FIELD_COLLECTION_RECOMMENDATIONS_HOT_NOT_MARKING_DATETIME_UTC, getNow()));

        recommendationsColl.update(query, update);

        return null;
    }

    protected BasicDBObject createBasicDBObject(final Map<String, Object> aQueryMap) {
        return new BasicDBObject(aQueryMap);
    }

    protected DBCollection getCollection(final IMongoPersistenceState aPersistenceState) {
        return aPersistenceState.getDb().getCollection(
                InwtPersistenceAction.COLLECTION_RECOMMENDATIONS);
    }

    protected Map<String, Object> createDataMap(final RateProductImageRequest aRequest) {
        final Map<String,Object> dataMap = new HashMap<String, Object>();

        dataMap.put(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL, aRequest.getEmail());
        dataMap.put(FIELD_COLLECTION_RECOMMENDATIONS_USER_IMAGE_ID, request.getImageId());
        dataMap.put(FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID, request.getProductId());

        /**
        dataMap.put(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, Boolean.TRUE);
        dataMap.put(FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER,
                imageRatingToBoolean(request));
        dataMap.put(FIELD_COLLECTION_RECOMMENDATIONS_HOT_NOT_MARKING_DATETIME_UTC, getNow());
         */

        return dataMap;
    }

    protected Date getNow() {
        return CreateProductAction.getCurrentUtcTime();
    }

    protected Boolean imageRatingToBoolean(final RateProductImageRequest aRequest) {
        return ImageRating.HOT.equals(aRequest.getRating()) ? TRUE : Boolean.FALSE;
    }
}
