package co.altruix.inwt.server.persistence;

import com.mongodb.DB;

import ru.altruix.androidprototyping.server.persistence.IPersistenceState;

public interface IMongoPersistenceState extends IPersistenceState {
	DB getDb();
}
