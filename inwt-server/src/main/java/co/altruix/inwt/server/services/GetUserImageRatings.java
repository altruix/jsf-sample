package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.GetUserImageRatingsRequest;
import co.altruix.inwt.messages.GetUserImageRatingsResponse;
import co.altruix.inwt.messages.GetUserProductImagesRequest;
import co.altruix.inwt.messages.GetUserProductImagesResponse;
import co.altruix.inwt.server.actions.GetUserImageRatingsAction;
import co.altruix.inwt.server.actions.GetUserProductImagesAction;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

@Path("/"+ GetUserImageRatingsRequest.SERVICE_NAME)
public class GetUserImageRatings extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetUserImageRatings.class);

    @POST
    @Produces("text/plain")
    public String getUserImageRatings(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final GetUserImageRatingsRequest request = mapper.readValue(aRequest,
                    GetUserImageRatingsRequest.class);

            final GetUserImageRatingsResponse response =
                    getInwtPersistence().runAction(new GetUserImageRatingsAction(request));

            return mapper.writeValueAsString(response);
        } catch (final JsonParseException exception) {
            LOGGER.error(GetUserProductImagesRequest.SERVICE_NAME, exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error(GetUserProductImagesRequest.SERVICE_NAME, exception);
        } catch (final IOException exception) {
            LOGGER.error(GetUserProductImagesRequest.SERVICE_NAME, exception);
        } catch (final InterruptedException exception) {
            LOGGER.error(GetUserProductImagesRequest.SERVICE_NAME, exception);
        } catch (final Exception exception) {
            LOGGER.error(GetUserProductImagesRequest.SERVICE_NAME, exception);
        }

        return "";
    }

}
