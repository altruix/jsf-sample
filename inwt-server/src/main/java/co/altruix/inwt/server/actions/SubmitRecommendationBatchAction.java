package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.SubmitRecommendationBatchRequest;
import co.altruix.inwt.messages.SubmitRecommendationBatchResponse;
import co.altruix.inwt.server.actions.submitrecommendationbatch.EbayProductDataSaver;
import co.altruix.inwt.server.actions.submitrecommendationbatch.IEbayProductDataSaver;
import co.altruix.inwt.server.actions.submitrecommendationbatch.ImpressionCountMode;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static co.altruix.inwt.server.actions.InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_SUBMITTER_EMAIL;
import static co.altruix.inwt.server.actions.submitrecommendationbatch.ImpressionCountMode.REJECTIONS;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class SubmitRecommendationBatchAction
        extends InwtPersistenceAction<SubmitRecommendationBatchResponse> {
    private final IEbayProductDataSaver ebayProductDataSaver;
    private final SubmitRecommendationBatchRequest request;

    public SubmitRecommendationBatchAction(final SubmitRecommendationBatchRequest aRequest)
    {
        this(aRequest, new EbayProductDataSaver());
    }

    public SubmitRecommendationBatchAction(final SubmitRecommendationBatchRequest aRequest,
                                           final IEbayProductDataSaver aEbayProductDataSaver) {
        ebayProductDataSaver = aEbayProductDataSaver;
        request = aRequest;
    }

    @Override
    protected SubmitRecommendationBatchResponse runOnMongo(
            final IMongoPersistenceState aPersistenceState) {
        /**
         * What can we do next?
         *
         * The purpose of this thing is to convert a list of URLs to one or more recommendation
         * batches.
         */

        final List<Recommendation> recommendations = convertUrlsToRecommendations(request
                .getProductUrls(), aPersistenceState);

        saveRecommendations(recommendations, aPersistenceState,
                request.getRecommenationRecipientEmail(), request.getSubmitterEmail());

        return new SubmitRecommendationBatchResponse();
    }

    protected void saveRecommendations(final List<Recommendation> aRecommendations,
                                       final IMongoPersistenceState aMongoPersistenceState,
                                       final String aRecipientEmail, final String aSubmitterEmail) {
        final List<Recommendation> recommendationsToAdd = makeSureEachProductHasOnly2Images(
                aRecommendations);

        insertRecommendations(recommendationsToAdd, aMongoPersistenceState, aRecipientEmail,
                aSubmitterEmail);
    }

    protected void insertRecommendations(final List<Recommendation> aRecommendationsToAdd,
                                         final IMongoPersistenceState aMongoPersistenceState,
                                         final String aRecipientEmail, final String aSubmitterEmail) {
        final DBCollection recom = getCollection(aMongoPersistenceState,
                COLLECTION_RECOMMENDATIONS);

        for (final Recommendation curRecom : aRecommendationsToAdd)
        {
            final BasicDBObject record = createRecomRecord(curRecom, aRecipientEmail,
                    aSubmitterEmail);
            recom.insert(record);
        }
    }

    protected BasicDBObject createRecomRecord(final Recommendation aCurRecom,
                                              final String aRecipienEmail,
                                              final String aSubmitterEmail) {
        final Map<String,Object> data = new HashMap<String, Object>();

        data.put(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL, aRecipienEmail);
        data.put(FIELD_COLLECTION_RECOMMENDATIONS_USER_IMAGE_ID, aCurRecom.getImageId());
        data.put(FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID, aCurRecom.getProductId());
        data.put(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, FALSE);
        data.put(FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER, FALSE);
        data.put(FIELD_COLLECTION_RECOMMENDATIONS_CREATION_DATETIME_UTC, this.getTimeStampUtc());
        data.put(FIELD_COLLECTION_RECOMMENDATIONS_SUBMITTER_EMAIL, aSubmitterEmail);

        return createBasicDBObject(data);
    }

    protected List<Recommendation> makeSureEachProductHasOnly2Images(
            final List<Recommendation> aRecommendations) {
        byte[] curProductId = null;
        int curProductRecommendations = 0;

        final List<Recommendation> recommendationsToAdd = new LinkedList<Recommendation>();

        for (final Recommendation curRecom : aRecommendations)
        {
            if (curProductId == null)
            {
                curProductId = curRecom.getProductId();
                recommendationsToAdd.add(curRecom);
                curProductRecommendations++;
            }
            else if (curProductId.equals(curRecom.getProductId()))
            {
                if (curProductRecommendations < 2)
                {
                    recommendationsToAdd.add(curRecom);
                    curProductRecommendations++;
                }
            }
            else if (!curProductId.equals(curRecom.getProductId()))
            {
                curProductId = curRecom.getProductId();
                curProductRecommendations = 1;

                recommendationsToAdd.add(curRecom);
            }
        }
        return recommendationsToAdd;
    }

    protected List<Recommendation> convertUrlsToRecommendations(final List<String>
                                                                        aProductUrls,
                                                                final IMongoPersistenceState
                                                                        aPersistenceState) {
        final List<Recommendation> recommendations = new LinkedList<Recommendation>();

        for (int i=0; i < aProductUrls.size(); i++)
        {
            processProductUrl(aProductUrls.get(i), recommendations, aPersistenceState);
        }

        return recommendations;
    }

    protected void processProductUrl(final String aProductUrl,
                                     final List<Recommendation> aRecommendations,
                                     final IMongoPersistenceState aPersistenceState) {
        // Check the number of rejections of the product by the user
        byte[] productId = getProductId(aProductUrl, aPersistenceState);

        double numberOfImpressions = getNumberOfImpressions(productId,
                request.getRecommenationRecipientEmail(), aPersistenceState);
        double numberOfRejections = getNumberOfRejections(productId,
                request.getRecommenationRecipientEmail(), aPersistenceState);

        double rejectionRatio = numberOfRejections / numberOfRejections;

        if ((numberOfImpressions > 2) && (rejectionRatio > 0.5))
        {
            saveRecommendationLog(aProductUrl, request.getRecommenationRecipientEmail(),
                    request.getSubmitterEmail(), getTimeStampUtc(), "Product rejected because it " +
                    "was presented " + numberOfImpressions + " times and rejected " +
                    numberOfRejections + " times.", aPersistenceState);

            return;
        }

        // Check the validity of URL
        if (urlInvalid(aProductUrl))
        {
            saveRecommendationLog(aProductUrl, request.getRecommenationRecipientEmail(),
                    request.getSubmitterEmail(), getTimeStampUtc(),
                    "Product rejected because the product URL is invalid.", aPersistenceState);

            return;
        }

        // Get list of images of the product, which were not presented to the user yet
        final List<byte[]> imageIds = getImagesOfProductNotPresentedToUserYet(productId,
                request.getRecommenationRecipientEmail(), aPersistenceState);

        for (final byte[] curImageId : imageIds)
        {
            aRecommendations.add(new Recommendation(curImageId, productId));
        }
    }

    protected byte[] getProductId(final String aProductUrl,
                                  final IMongoPersistenceState aPersistenceState) {
        final DBCollection productColl = aPersistenceState.getDb().getCollection(
                COLLECTION_USER_PRODUCTS);
        final BasicDBObject query = new BasicDBObject(FIELD_USER_PRODUCTS_PRODUCT_URL, aProductUrl);

        final DBObject product = productColl.findOne(query);

        if (product != null)
        {
            return ((ObjectId)product.get("_id")).toByteArray();
        }

        return ebayProductDataSaver.importEbayProduct(aProductUrl, aPersistenceState);
    }

    protected List<byte[]> getImagesOfProductNotPresentedToUserYet(
            final byte[] aProductId, final String aRecommenationRecipientEmail,
            final IMongoPersistenceState aPersistenceState) {
        final List<byte[]> productImageIds = getProductImageIds(aProductId, aPersistenceState);
        final List<byte[]> shownProductImageIds = getShownProductImageIds(aProductId,
                aRecommenationRecipientEmail, aPersistenceState);

        productImageIds.removeAll(shownProductImageIds);

        return productImageIds;
    }

    protected List<byte[]> getShownProductImageIds(final byte[] aProductId,
                                                   final String aRecommenationRecipientEmail,
                                                   final IMongoPersistenceState aPersistenceState) {
        final Map<String,Object> queryMap = new HashMap<String, Object>();

        queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL, aRecommenationRecipientEmail);
        queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID, aProductId);
        queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, TRUE);

        final BasicDBObject query = createBasicDBObject(queryMap);

        final DBCollection recomCollection = getCollection(aPersistenceState,
                COLLECTION_RECOMMENDATIONS);

        final DBCursor cursor = recomCollection.find(query);
        final List<byte[]> imageIds = new LinkedList<byte[]>();

        try
        {
            while (cursor.hasNext())
            {
                final BasicDBObject curRecord = (BasicDBObject) cursor.next();

                imageIds.add(
                        (byte[]) curRecord.get(FIELD_COLLECTION_RECOMMENDATIONS_USER_IMAGE_ID));
            }
        }
        finally
        {
            cursor.close();
        }

        return imageIds;
    }

    protected DBCollection getCollection(final IMongoPersistenceState aPersistenceState,
                                         final String aName) {
        return aPersistenceState.getDb().getCollection(aName);
    }

    protected List<byte[]> getProductImageIds(final byte[] aProductId,
                                              final IMongoPersistenceState aPersistenceState) {
        final Map<String,Object> queryData = new HashMap<String, Object>();

        queryData.put(FIELD_COLLECTION_USER_PRODUCT_IMAGES_PRODUCT_ID, aProductId);

        final BasicDBObject query = createBasicDBObject(queryData);

        final DBCollection productImages = getCollection(aPersistenceState,
                COLLECTION_USER_PRODUCT_IMAGES);

        final DBCursor cursor = productImages.find(query);

        final List<byte[]> result = new LinkedList<byte[]>();

        try
        {
            while (cursor.hasNext())
            {
                final DBObject curRecord = cursor.next();

                result.add(((ObjectId)curRecord.get("_id")).toByteArray());
            }
        }
        finally {
            cursor.close();
        }

        return result;
    }

    protected boolean urlInvalid(final String aProductUrl) {
        if (StringUtils.isEmpty(aProductUrl))
        {
            return true;
        }

        if (!aProductUrl.contains(".ebay."))
        {
            return true;
        }

        if (!(aProductUrl.startsWith("http://") || aProductUrl.startsWith("https://")))
        {
            return true;
        }

        return false;
    }

    protected Date getTimeStampUtc() {
        return new DateTime(DateTimeZone.UTC).toDate();
    }

    protected void saveRecommendationLog(final String aProductUrl,
                                         final String aRecommenationRecipientEmail,
                                         final String aSubmitterEmail, final Date aDate,
                                         final String aMessage,
                                         final IMongoPersistenceState aPersistenceState) {
        final Map<String,Object> data = new HashMap<String, Object>();

        data.put(FIELD_COLLECTION_RECOMMENDATION_LOG_SUBMITTER_EMAIL, aSubmitterEmail);
        data.put(FIELD_COLLECTION_RECOMMENDATION_LOG_RECIPIENT_EMAIL, aRecommenationRecipientEmail);
        data.put(FIELD_COLLECTION_RECOMMENDATION_LOG_TIMESTAMP, aDate);
        data.put(FIELD_COLLECTION_RECOMMENDATION_LOG_MESSAGE, aMessage);
        data.put(FIELD_COLLECTION_RECOMMENDATION_LOG_PRODUCT_URL, aProductUrl);

        final BasicDBObject record = createBasicDBObject(data);

        final DBCollection recomLog = getCollection(aPersistenceState,
                COLLECTION_RECOMMENDATION_LOG);
        recomLog.insert(record);
    }

    /**
     * Returns the number of times product with URL aProductUrl was rejected (not liked) by user
     * aRecommenationRecipientEmail.
     */
    protected int getNumberOfRejections(final byte[] aProductId,
                                        final String aRecommenationRecipientEmail,
                                        final IMongoPersistenceState aPersistenceState) {
        return countImpressions(aProductId, aRecommenationRecipientEmail, aPersistenceState,
                REJECTIONS);
    }

    private int countImpressions(final byte[] aProductId, final String aRecommenationRecipientEmail,
                                 final IMongoPersistenceState aPersistenceState,
                                 final ImpressionCountMode aMode) {
        final Map<String,Object> queryMap = new HashMap<String, Object>();

        queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL,
                aRecommenationRecipientEmail);
        queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID, aProductId);
        queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, TRUE);

        if (REJECTIONS.equals(aMode))
        {
            queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER, FALSE);
        }

        final BasicDBObject query = createBasicDBObject(queryMap);

        final DBCollection collection = getCollection(aPersistenceState, COLLECTION_RECOMMENDATIONS);

        return collection.find(query).count();
    }

    protected BasicDBObject createBasicDBObject(final Map<String, Object> aData) {
        return new BasicDBObject(aData);
    }

    /**
     * Returns the number of times product with URL aProductUrl was presented to user
     * aRecommenationRecipientEmail.
     */
    protected int getNumberOfImpressions(final byte[] aProductId,
                                         final String aRecommenationRecipientEmail,
                                         final IMongoPersistenceState aPersistenceState) {
        return countImpressions(aProductId, aRecommenationRecipientEmail, aPersistenceState,
                ImpressionCountMode.ALL);
    }
}
