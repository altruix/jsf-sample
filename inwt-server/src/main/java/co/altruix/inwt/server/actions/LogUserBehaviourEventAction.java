package co.altruix.inwt.server.actions;

import co.altruix.inwt.model.UserBehaviourEvent;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.WriteConcern;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class LogUserBehaviourEventAction extends
        InwtPersistenceAction<Object> {
    private final String userEmail;
    private final UserBehaviourEvent userBehaviourEvent;
    private final String message;

    public LogUserBehaviourEventAction(final String aUserEmail,
                                       final UserBehaviourEvent aUserBehaviourEvent)
    {
        this(aUserEmail, aUserBehaviourEvent, null);
    }

    public LogUserBehaviourEventAction(final String aUserEmail,
                                       final UserBehaviourEvent aUserBehaviourEvent,
                                       final String aMessage) {
        userEmail = aUserEmail;
        userBehaviourEvent = aUserBehaviourEvent;
        message = aMessage;
    }

    @Override
    protected Object runOnMongo(final IMongoPersistenceState aPersistenceState) {
        final DBCollection userBehaviourLog = getCollection(aPersistenceState);

        final Map<String,Object> data = new HashMap<String, Object>();

        data.put(InwtPersistenceAction.FIELD_COLLECTION_USER_BEHAVIOUR_LOG_TIMESTAMP_UTC,
                getCurrentUtcTime());
        data.put(InwtPersistenceAction.FIELD_COLLECTION_USER_BEHAVIOUR_LOG_USER_EMAIL, userEmail);
        data.put(InwtPersistenceAction.FIELD_COLLECTION_USER_BEHAVIOUR_LOG_EVENT_TYPE,
                userBehaviourEvent.toString());

        if (message != null)
        {
            data.put(InwtPersistenceAction.FIELD_COLLECTION_USER_BEHAVIOUR_LOG_MESSAGE,
                    message);
        }

        final DBObject record = createRecord(data);

        userBehaviourLog.insert(WriteConcern.SAFE, record);

        return null;
    }

    protected DBCollection getCollection(final IMongoPersistenceState aPersistenceState) {
        return aPersistenceState.getDb().getCollection
                (InwtPersistenceAction.COLLECTION_USER_BEHAVIOUR_LOG);
    }

    protected DBObject createRecord(final Map<String, Object> aData) {
        return new BasicDBObject(aData);
    }

    protected Date getCurrentUtcTime() {
        return CreateProductAction.getCurrentUtcTime();
    }
}
