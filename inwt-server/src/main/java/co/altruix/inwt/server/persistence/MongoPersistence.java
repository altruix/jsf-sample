package co.altruix.inwt.server.persistence;

import java.net.UnknownHostException;
import java.util.Arrays;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

import ru.altruix.androidprototyping.server.persistence.AbstractThreadSafePersistence;
import ru.altruix.androidprototyping.server.persistence.IPersistence;
import ru.altruix.androidprototyping.server.persistence.IPersistenceAction;
import ru.altruix.androidprototyping.server.persistence.IPersistenceState;

public class MongoPersistence extends AbstractThreadSafePersistence<MongoPersistenceState> implements IPersistence {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(MongoPersistence.class);
	private boolean busy = false;

	private IPersistenceState state;
	
	private MongoClient mongoClient;
	private DB db;
	
	public MongoPersistence()
	{
	}
	
	public <T> T runAction(final IPersistenceAction<T> aAction) throws InterruptedException
	{
		T returnValue = null;
				
		synchronized (this) {
			waitUntilIdle();
			busy = true;

			returnValue = aAction.run(state);

			busy = false;

			notifyAll();
		}
		
		return returnValue;
	}
	
	@PostConstruct
	public void init() {
		try {
			final ServerAddress serverAddress = new ServerAddress("95.85.28.148", 27017);
			
			final MongoCredential credential = MongoCredential.createMongoCRCredential("inwtAdmin", 
					"inwt", "2C6wGOVox136JU6V1a2QOjsj0P7WIJTUoF4J6rL1".toCharArray());
			
			mongoClient = new MongoClient(serverAddress, Arrays.asList(credential));
			
			db = mongoClient.getDB("inwt");
			state = getPersistenceState();
		} catch (final UnknownHostException exception) {
			LOGGER.error("", exception);
		}
	}

	@PreDestroy
	public void shutdown()
	{
		mongoClient.close();
	}
	
	private void waitUntilIdle() throws InterruptedException {
		while (busy) {
			wait();
		}
	}

	@Override
	protected MongoPersistenceState getPersistenceState() {
		return new MongoPersistenceState(db);
	}
}
