package co.altruix.inwt.server.actions.submitrecommendationbatch;

public enum ImpressionCountMode {
    ALL,
    REJECTIONS
}
