package co.altruix.inwt.server.actions.submitrecommendationbatch;

import co.altruix.inwt.server.persistence.IMongoPersistenceState;

public interface IEbayProductDataSaver {

    byte[] importEbayProduct(final String aProductUrl, final IMongoPersistenceState
            aPersistenceState);
}
