package co.altruix.inwt.server.email;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;

public class EmailSender implements IEmailSender {
	public static final String MAIL_USERNAME = "mail:username";
	public static final String MAIL_PASSWORD = "mail:password";
	public static final String MAIL_SERVER_NAME = "mail:server-name";
	public static final String MAIL_PORT = "mail:port";
	public static final String MAIL_SMTP_PORT = "mail.smtp.port";
	public static final String MAIL_SMTP_HOST = "mail.smtp.host";
	public static final String MAIL_SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
	public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
	public static final String NOREPLY_ALTRUIX_CO = "INWT <dp@altruix.co>";

	public void sendEMail(final String aSubject, final String aBody,
			final String aRecipient, final ServletContext aContext)
			throws AddressException, MessagingException {

		final String username = aContext.getInitParameter(MAIL_USERNAME);
		final String password = aContext.getInitParameter(MAIL_PASSWORD);

		final Properties props = new Properties();
		props.put(MAIL_SMTP_AUTH, "true");
		props.put(MAIL_SMTP_STARTTLS_ENABLE, "true");
		props.put(MAIL_SMTP_HOST, aContext.getInitParameter(MAIL_SERVER_NAME));
		props.put(MAIL_SMTP_PORT, aContext.getInitParameter(MAIL_PORT));

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});

		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(NOREPLY_ALTRUIX_CO));
		message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(aRecipient));
		message.setSubject(aSubject);
		message.setText(aBody);

		Transport.send(message);
	}
}
