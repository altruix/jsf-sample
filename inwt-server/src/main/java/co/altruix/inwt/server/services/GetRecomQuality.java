package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.GetRecomQualityRequest;
import co.altruix.inwt.messages.GetRecomQualityResponse;
import co.altruix.inwt.messages.ImageRating;
import co.altruix.inwt.model.RecomQualityInfo;
import co.altruix.inwt.server.actions.ReadProposedRecommendationsAction;
import co.altruix.inwt.server.services.recomquality.Recom;
import co.altruix.inwt.server.services.recomquality.RecomByCreationDateComparator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.altruix.androidprototyping.server.persistence.IPersistence;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static java.util.Collections.sort;

@Path("/"+ GetRecomQualityRequest.SERVICE_NAME)
public class GetRecomQuality extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetRecomQuality.class);
    public static final int RECOMS_IN_QUALITY_MEASUREMENT_BATCH = 10;

    @POST
    @Produces("text/plain")
    public String getRecomQuality(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = createObjectMapper();
            final GetRecomQualityRequest request = mapper.readValue(aRequest,
                    GetRecomQualityRequest.class);

            final List<Recom> recoms = readRecoms(request.getUserEmail(),
                    request.getRecommenderEmail(), getInwtPersistence());

            final List<RecomQualityInfo> qualityTuples = convertRecomsToRecomQualityTuples(recoms);

            final GetRecomQualityResponse response = createGetGetRecomQualityResponse();

            response.setRecomQualityInfos(qualityTuples);

            return mapper.writeValueAsString(response);
        } catch (final JsonParseException exception) {
            LOGGER.error(GetRecomQualityRequest.SERVICE_NAME, exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error(GetRecomQualityRequest.SERVICE_NAME, exception);
        } catch (final IOException exception) {
            LOGGER.error(GetRecomQualityRequest.SERVICE_NAME, exception);
        } catch (final Exception exception) {
            LOGGER.error(GetRecomQualityRequest.SERVICE_NAME, exception);
        }

        return "";
    }

    protected GetRecomQualityResponse createGetGetRecomQualityResponse() {
        return new GetRecomQualityResponse();
    }

    protected ObjectMapper createObjectMapper() {
        final ObjectMapper mapper = creteVirginObjectMapper();

        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
                false);
        mapper.registerModule(new JodaModule());

        return mapper;
    }

    protected ObjectMapper creteVirginObjectMapper() {
        return new ObjectMapper();
    }

    protected List<RecomQualityInfo> convertRecomsToRecomQualityTuples(final List<Recom> aRecoms) {
        sort(aRecoms, new RecomByCreationDateComparator());

        final List<RecomQualityInfo> result = new LinkedList<RecomQualityInfo>();
        RecomQualityInfo curQualityInfo = new RecomQualityInfo();
        DateTime firstRecomCreationDateTime = null;

        for (final Recom curRecom : aRecoms)
        {
            if (firstRecomCreationDateTime == null)
            {
                firstRecomCreationDateTime = curRecom.getCreationDateTimeUtc()
                        .withTimeAtStartOfDay();
                curQualityInfo.setFirstRecomCreationDateTimeUtc(firstRecomCreationDateTime);

                curQualityInfo.setLastRecomCreationDateTimeUtc(firstRecomCreationDateTime
                        .withTime(23, 59, 59, 999));
            }

            if (!sameDay(firstRecomCreationDateTime, curRecom.getCreationDateTimeUtc()))
            {
                result.add(curQualityInfo);
                curQualityInfo = new RecomQualityInfo();

                firstRecomCreationDateTime = curRecom.getCreationDateTimeUtc()
                        .withTimeAtStartOfDay();
                curQualityInfo.setFirstRecomCreationDateTimeUtc(firstRecomCreationDateTime);

                curQualityInfo.setLastRecomCreationDateTimeUtc(firstRecomCreationDateTime
                        .withTime(23, 59, 59, 999));
            }
            if (ImageRating.HOT.equals(curRecom.getHotOrNot()))
            {
                incrementHotCount(curQualityInfo);
            }
            incrementNumberOfRecoms(curQualityInfo);
        }

        if (curQualityInfo.getNumberOfRecoms() > 0)
        {
            result.add(curQualityInfo);
        }

        return result;
    }

    protected boolean sameDay(final DateTime aDateTime1, final DateTime aDateTime2) {
        final DateTime start = aDateTime1;
        final DateTime stop = start.plusDays( 1 ).withTimeAtStartOfDay();

        final Interval interval = new Interval( start, stop );

        return interval.contains(aDateTime2);
    }

    private void incrementNumberOfRecoms(final RecomQualityInfo aCurQualityInfo) {
        aCurQualityInfo.setNumberOfRecoms(aCurQualityInfo.getNumberOfRecoms()+1);
    }

    private void incrementHotCount(final RecomQualityInfo aCurQualityInfo) {
        aCurQualityInfo.setNumberOfHotRecoms(aCurQualityInfo.getNumberOfHotRecoms()+1);
    }

    protected List<Recom> readRecoms(final String aUserEmail, final String aRecommenderEmail,
                                     final IPersistence aPersistence) throws InterruptedException {
        return aPersistence.runAction(
                createReadProposedRecommendationsAction(aUserEmail, aRecommenderEmail));
    }

    protected ReadProposedRecommendationsAction createReadProposedRecommendationsAction(
            final String aUserEmail, final String aRecommenderEmail) {
        return new ReadProposedRecommendationsAction(aUserEmail, aRecommenderEmail);
    }
}
