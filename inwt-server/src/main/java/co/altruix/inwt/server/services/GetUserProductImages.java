package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.GetUserProductImagesRequest;
import co.altruix.inwt.messages.GetUserProductImagesResponse;
import co.altruix.inwt.server.actions.GetUserProductImagesAction;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

@Path("/"+ GetUserProductImagesRequest.SERVICE_NAME)
public class GetUserProductImages extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetUserProductImages.class);

    @POST
    @Produces("text/plain")
    public String getUserProductImages(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final GetUserProductImagesRequest request = mapper.readValue(aRequest,
                    GetUserProductImagesRequest.class);

            final GetUserProductImagesResponse response =
                    getInwtPersistence().runAction(new GetUserProductImagesAction(request));

            return mapper.writeValueAsString(response);
        } catch (final JsonParseException exception) {
            LOGGER.error(GetUserProductImagesRequest.SERVICE_NAME, exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error(GetUserProductImagesRequest.SERVICE_NAME, exception);
        } catch (final IOException exception) {
            LOGGER.error(GetUserProductImagesRequest.SERVICE_NAME, exception);
        } catch (final InterruptedException exception) {
            LOGGER.error(GetUserProductImagesRequest.SERVICE_NAME, exception);
        } catch (final Exception exception) {
            LOGGER.error(GetUserProductImagesRequest.SERVICE_NAME, exception);
        }

        return "";
    }

}
