package co.altruix.inwt.server.ebay;

import com.ebay.services.finding.SearchResult;

public interface IEbayDataReader {
	SearchResult findEbayItems(final String aCategoryId);
}
