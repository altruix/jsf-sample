package co.altruix.inwt.server.actions;

import java.util.List;

public class RecommendationBatch {
    private String recommenationRecipientEmail;
    private List<Recommendation> recommendations;

    public String getRecommenationRecipientEmail() {
        return recommenationRecipientEmail;
    }

    public void setRecommenationRecipientEmail(final String aRecommenationRecipientEmail) {
        recommenationRecipientEmail = aRecommenationRecipientEmail;
    }

    public List<Recommendation> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(final List<Recommendation> aRecommendations) {
        recommendations = aRecommendations;
    }
}
