package co.altruix.inwt.server.persistence;

import com.mongodb.DB;

public class MongoPersistenceState implements IMongoPersistenceState {
	private final DB db;
	
	public MongoPersistenceState(final DB aDb)
	{
		db = aDb;
	}
	
	@Override
	public DB getDb() {
		return db;
	}	
}
