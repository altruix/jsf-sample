package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.GetUserImageRatingsRequest;
import co.altruix.inwt.messages.GetUserImageRatingsResponse;
import co.altruix.inwt.messages.ImageRating;
import co.altruix.inwt.model.UserProductImage;
import co.altruix.inwt.model.UserProductImageRating;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class GetUserImageRatingsAction extends InwtPersistenceAction<GetUserImageRatingsResponse> {
    private final GetUserImageRatingsRequest request;

    public GetUserImageRatingsAction(final GetUserImageRatingsRequest aRequest) {
        request = aRequest;
    }

    @Override
    protected GetUserImageRatingsResponse runOnMongo(
            final IMongoPersistenceState aPersistenceState) {

        final Map<String,Object> queryMap = new HashMap<String, Object>();

        queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL, request.getUserEmail());
        queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, Boolean.TRUE);

        final BasicDBObject query = new BasicDBObject(queryMap);

        final DBCollection recommendations = aPersistenceState.getDb().getCollection
                (InwtPersistenceAction.COLLECTION_RECOMMENDATIONS);

        final DBCursor cursor = recommendations.find(query);

        final List<UserProductImageRating> ratings = new LinkedList<UserProductImageRating>();

        while (cursor.hasNext())
        {
            final UserProductImageRating curRating = new UserProductImageRating();

            final DBObject curRecord = cursor.next();

            final Boolean hot = (Boolean) curRecord.get
                    (FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER);

            curRating.setRating(hot ? ImageRating.HOT : ImageRating.NOT);

            final byte[] imageId = (byte[])curRecord.get
                    (FIELD_COLLECTION_RECOMMENDATIONS_USER_IMAGE_ID);

            final byte[] userProductId = (byte[])curRecord.get
                    (FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID);

            curRating.setImage(loadImage(aPersistenceState, imageId, userProductId));

            ratings.add(curRating);
        }

        cursor.close();

        final GetUserImageRatingsResponse response = new GetUserImageRatingsResponse();

        response.setRatings(ratings);

        return response;
    }

    private UserProductImage loadImage(final IMongoPersistenceState aPersistenceState,
                                       final byte[] aImageId, final byte[] aUserProductId) {
        final DBCollection imagesColl = aPersistenceState.getDb().getCollection
                (COLLECTION_USER_PRODUCT_IMAGES);

        final DBObject imageRecod = imagesColl.findOne(new BasicDBObject("_id",
                new ObjectId(aImageId)));

        if (imageRecod != null)
        {
            return UserProductImageUtils.convertTupleToUserProductImage((BasicDBObject) imageRecod,
                    aImageId,
                    aUserProductId);
        }

        return null;
    }
}
