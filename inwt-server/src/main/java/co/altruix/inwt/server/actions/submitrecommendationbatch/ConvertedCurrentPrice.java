package co.altruix.inwt.server.actions.submitrecommendationbatch;

import java.math.BigDecimal;

public class ConvertedCurrentPrice {
    private BigDecimal amount;
    private String currencyId;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal aAmount) {
        amount = aAmount;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(final String aCurrencyId) {
        currencyId = aCurrencyId;
    }

}
