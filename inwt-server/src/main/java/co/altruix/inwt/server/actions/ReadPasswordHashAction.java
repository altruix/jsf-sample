package co.altruix.inwt.server.actions;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import co.altruix.inwt.server.persistence.IMongoPersistenceState;

public class ReadPasswordHashAction extends InwtPersistenceAction<String> {
	private final String email;
	
	public ReadPasswordHashAction(final String aEmail)
	{
		email = aEmail;
	}
	
	@Override
	protected String runOnMongo(final IMongoPersistenceState aPersistenceState) {
		String result = "";
		final DBCollection users = aPersistenceState.getDb().getCollection(
				COLLECTION_USERS);
		
		final BasicDBObject query = new BasicDBObject(FIELD_USERS_EMAIL, email);
		
		final DBCursor cursor = users.find(query);
		
		try
		{
			if (cursor.hasNext())
			{
				result = (String) cursor.next().get(FIELD_USERS_PASSWORD_HASH);
			}
		}
		finally
		{
			cursor.close();
		}
		
		return result;
	}	
}
