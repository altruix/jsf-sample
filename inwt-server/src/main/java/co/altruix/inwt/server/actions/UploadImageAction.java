package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.UploadImageRequest;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

import java.util.HashMap;
import java.util.Map;

public class UploadImageAction extends InwtPersistenceAction<Boolean> {
    private final UploadImageRequest request;

    public UploadImageAction(final UploadImageRequest aRequest) {
        request = aRequest;
    }

    @Override
    protected Boolean runOnMongo(final IMongoPersistenceState aPersistenceState) {
        final DBCollection userProductImages = aPersistenceState.getDb().getCollection(
                COLLECTION_USER_PRODUCT_IMAGES);

        final Map newProductData = new HashMap();

        newProductData.put(FIELD_COLLECTION_USER_PRODUCT_IMAGES_CREATOR_EMAIL,
                request.getCreatorEmail());
        newProductData.put(FIELD_COLLECTION_USER_PRODUCT_IMAGES_PRODUCT_ID,
                request.getUserProductId());
        newProductData.put(FIELD_COLLECTION_USER_PRODUCT_IMAGES_DATA, request.getData());
        newProductData.put(FIELD_COLLECTION_USER_PRODUCT_IMAGES_FILE_NAME, request.getFileName());
        newProductData.put(FIELD_COLLECTION_USER_PRODUCT_IMAGES_MIME_TYPE, request.getMimeType());
        newProductData.put(FIELD_COLLECTION_USER_PRODUCT_IMAGES_IS_DELETED, Boolean.FALSE);

        final BasicDBObject newProduct = new BasicDBObject(newProductData);
        userProductImages.insert(newProduct);

        return Boolean.TRUE;
    }
}
