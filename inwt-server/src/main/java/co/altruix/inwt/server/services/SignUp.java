package co.altruix.inwt.server.services;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.ServletContext;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import co.altruix.inwt.server.services.initialrecoms.IInitialRecomGenerator;
import co.altruix.inwt.server.services.initialrecoms.InitialRecomGenerator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ServletContextAware;

import co.altruix.inwt.messages.SignUpRequest;
import co.altruix.inwt.messages.SignUpResponse;
import co.altruix.inwt.messages.SignUpResult;
import co.altruix.inwt.server.actions.SignUpAction;
import co.altruix.inwt.server.email.EmailSender;
import co.altruix.inwt.server.email.IEmailSender;
import co.altruix.inwt.server.password.IPasswordManager;
import co.altruix.inwt.server.password.PasswordManager;
import co.altruix.inwt.server.services.signup.BodyTextGenerator;
import co.altruix.inwt.server.services.signup.IBodyTextGenerator;
import co.altruix.inwt.server.services.signup.IEMailValidator;
import co.altruix.inwt.server.services.signup.IPasswordGenerator;
import co.altruix.inwt.server.services.signup.JavaMailValidator;
import co.altruix.inwt.server.services.signup.PasswordGenerator;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static co.altruix.inwt.messages.SignUpResult.INSERTED;

@Path("/SignUp")
public class SignUp extends AbstractInwtService implements ServletContextAware {
	private static final Logger LOGGER = LoggerFactory.getLogger(SignUp.class);
	private final IEMailValidator emailValidator;
	private final IEmailSender emailSender;
	private final IPasswordGenerator passwordGenerator;
	private final IBodyTextGenerator bodyTextGenerator;
	private final IPasswordManager passwordManager;
	private ServletContext servletContext;
    private final IInitialRecomGenerator initialRecomGenerator;

	public SignUp() {
		this(new JavaMailValidator(), new EmailSender(),
				new PasswordGenerator(), new BodyTextGenerator(),
				new PasswordManager(), new InitialRecomGenerator());
	}

	public SignUp(final IEMailValidator aValidator,
                  final IEmailSender aEmailSender,
                  final IPasswordGenerator aPasswordGenerator,
                  final IBodyTextGenerator aBodyTextGenerator,
                  final IPasswordManager aPasswordManager,
                  final IInitialRecomGenerator aInitialRecomGenerator) {
		emailValidator = aValidator;
		emailSender = aEmailSender;
		passwordGenerator = aPasswordGenerator;
		bodyTextGenerator = aBodyTextGenerator;
		passwordManager = aPasswordManager;
        initialRecomGenerator = aInitialRecomGenerator;
    }

	@POST
	@Produces("text/plain")
	public String signUp(@FormParam("request") final String aRequest) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			final SignUpRequest request = mapper.readValue(aRequest,
					SignUpRequest.class);

			final SignUpResponse response = createResponse(request);

			return mapper.writeValueAsString(response);
		} catch (final JsonParseException exception) {
			LOGGER.error("signUp", exception);
		} catch (final JsonMappingException exception) {
			LOGGER.error("signUp", exception);
		} catch (final IOException exception) {
			LOGGER.error("signUp", exception);
		} catch (InterruptedException exception) {
			LOGGER.error("signUp", exception);
		}

		return "";
	}

	protected SignUpResponse createResponse(final SignUpRequest request)
			throws InterruptedException {
		if (StringUtils.isEmpty(request.getEmail())) {
			return new SignUpResponse(SignUpResult.EMPTY_USER_NAME);
		}

		if (!emailValidator.isEmailValid(request.getEmail())) {
			return new SignUpResponse(SignUpResult.EMPTY_USER_NAME);
		}

		final String password = passwordGenerator.generatePassword();
		String saltedHash = null;
		
		try
		{
			saltedHash = passwordManager.getSaltedHash(password);			
		}
		catch (final Exception exception)
		{
			LOGGER.error("Error generating salted hash", exception);
			return new SignUpResponse(SignUpResult.CREDENTIALS_GENERATING_FAILURE);
		}
		
		
		final SignUpResponse response = getInwtPersistence().runAction(
				new SignUpAction(request, saltedHash));

		if (INSERTED.equals(response.getResult())) {
            initialRecomGenerator.createInitialRecommendations(getInwtPersistence(),
                    request.getEmail());

			try {
				emailSender.sendEMail("Your INWT password",
						bodyTextGenerator.composeBodyText(password),
						request.getEmail(), servletContext);
				response.setResult(SignUpResult.SUCCESS);
			} catch (final AddressException exception) {
				processEMailSendingException(request, response, exception);
			} catch (final MessagingException exception) {
				processEMailSendingException(request, response, exception);
			}
		}

		return response;
	}

	private void processEMailSendingException(final SignUpRequest request,
			final SignUpResponse response, final Exception exception) {
		LOGGER.error("Error sending e-mail to '" + request.getEmail() + "'",
				exception);
		response.setResult(SignUpResult.EMAIL_SENDING_FAILURE);
	}

	@Override
	public void setServletContext(final ServletContext aServletContext) {
		servletContext = aServletContext;
	}
}
