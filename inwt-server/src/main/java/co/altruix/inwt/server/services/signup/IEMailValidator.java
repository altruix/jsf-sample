package co.altruix.inwt.server.services.signup;

public interface IEMailValidator {
	boolean isEmailValid(final String aEmail);
}
