package co.altruix.inwt.server.actions.submitrecommendationbatch;

public interface IGetSingleItemResponseParser {
    IEbayItemData parse(String aGetSingleItemXmlResponse);
}
