package co.altruix.inwt.server.actions.submitrecommendationbatch;

import com.sun.org.apache.xerces.internal.dom.DeferredElementImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class GetSingleItemResponseParser implements IGetSingleItemResponseParser {
    private static final Logger LOGGER = LoggerFactory
            .getLogger(GetSingleItemResponseParser.class);

    @Override
    public IEbayItemData parse(final String aGetSingleItemXmlResponse)
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        // factory.setNamespaceAware(true);
        DocumentBuilder builder;
        Document doc = null;
        final EbayItemData result = new EbayItemData();

        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse(new ByteArrayInputStream(aGetSingleItemXmlResponse.getBytes()));

            // Create XPathFactory object
            XPathFactory xpathFactory = XPathFactory.newInstance();

            // Create XPath object
            XPath xpath = xpathFactory.newXPath();

            result.setConvertedCurrentPrice(getConvertedCurrentPrice(doc, xpath));
            result.setItemId(getNodeText(doc, xpath, "ItemID"));
            result.setViewItemURLForNaturalSearch(getNodeText(doc, xpath,
                    "ViewItemURLForNaturalSearch"));
            result.setListingType(getNodeText(doc, xpath, "ListingType"));
            result.setPictureUrls(getPictureUrls(doc, xpath));
            result.setPrimaryCategoryID(getNodeText(doc, xpath, "PrimaryCategoryID"));
            result.setPrimaryCategoryName(getNodeText(doc, xpath, "PrimaryCategoryName"));
            result.setTitle(getNodeText(doc, xpath, "Title"));


        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    private List<String> getPictureUrls(final Document aDoc, final XPath aXpath)
    {
        try
        {
            final XPathExpression expr = aXpath.compile("/GetSingleItemResponse/Item/PictureURL");
            final NodeList nodes = (NodeList) expr.evaluate(aDoc, XPathConstants.NODESET);

            final List<String> result = new LinkedList<String>();

            for (int i=0; i < nodes.getLength(); i++)
            {
                final Node curNode = nodes.item(i);

                result.add(((DeferredElementImpl) curNode).getTextContent());
            }

            return result;
        }
        catch (final XPathExpressionException exception) {
            LOGGER.error("", exception);
        }
        return null;

    }

    private String getNodeText(final Document aDoc, final XPath aXpath, final String aFieldName) {
        try
        {
            final XPathExpression expr = aXpath.compile("/GetSingleItemResponse/Item/" +
                    aFieldName);
            final Node node = (Node) expr.evaluate(aDoc, XPathConstants.NODE);

            return node.getTextContent();
        }
        catch (final XPathExpressionException exception) {
            LOGGER.error("", exception);
        }
        return null;
    }

    private ConvertedCurrentPrice getConvertedCurrentPrice(final Document aDoc,
                                                           final XPath aXpath) {
        try
        {
            final XPathExpression expr = aXpath.compile("/GetSingleItemResponse/Item/ConvertedCurrentPrice");
            final Node node = (Node) expr.evaluate(aDoc, XPathConstants.NODE);

            final ConvertedCurrentPrice result = new ConvertedCurrentPrice();

            result.setCurrencyId(((DeferredElementImpl) node).getAttribute("currencyID"));
            result.setAmount(new BigDecimal(node.getTextContent()));

            return result;
        }
        catch (final XPathExpressionException exception) {
            LOGGER.error("", exception);
        }
        return null;
    }
}
