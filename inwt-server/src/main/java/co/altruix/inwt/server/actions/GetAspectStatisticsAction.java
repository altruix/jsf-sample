package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.GetAspectStatisticsRequest;
import co.altruix.inwt.messages.GetAspectStatisticsResponse;
import co.altruix.inwt.model.EbayAspect;
import co.altruix.inwt.model.EbayAspectStatistic;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetAspectStatisticsAction extends
        InwtPersistenceAction<GetAspectStatisticsResponse> {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetAspectStatisticsAction.class);
    public static final EbayAspectStatisticByPopularityComparator COMPARATOR = new EbayAspectStatisticByPopularityComparator();
    private final GetAspectStatisticsRequest request;

    public GetAspectStatisticsAction(final GetAspectStatisticsRequest aRequest) {
        request = aRequest;
    }

    @Override
    protected GetAspectStatisticsResponse runOnMongo(
            final IMongoPersistenceState aPersistenceState) {
        LOGGER.debug("GetAspectStatisticsAction.runOnMongo");

        // Get product IDs, which
        // a) belong to request.getProductCategoryId() and
        // b) were rated by user request.getUserEmail().

        final List<ObjectId> productIds = getProductIds(aPersistenceState.getDb().getCollection
                (COLLECTION_RECOMMENDATIONS), aPersistenceState.getDb().getCollection
                (COLLECTION_USER_PRODUCTS), request.getUserEmail(), request.getProductCategoryId());

        LOGGER.debug("productIds.size(): " + productIds.size());

        final GetAspectStatisticsResponse response = new GetAspectStatisticsResponse();

        final List<EbayAspectStatistic> statistics = composeStatistics(productIds,
                aPersistenceState,
                request.getUserEmail(), request.getAspectName(), request.getProductCategoryId());

        LOGGER.debug("statistics.size(): " + statistics.size());

        Collections.sort(statistics, COMPARATOR);

        response.setStatistics(statistics);

        return response;
    }

    protected List<EbayAspectStatistic> composeStatistics(final List<ObjectId> aProductIds,
                                                          final IMongoPersistenceState
                                                                  aPersistenceState,
                                                          final String aUserEmail,
                                                          final String aAspectName,
                                                          final String aProductCategoryId) {
        /**
         * Find all recommendations for user X, for which the product ID is among aProductIds
         */
        // TODO: Test this

        final BasicDBList rawByteArraysList = convertObjectIdListToByteArrayList(aProductIds);

        final BasicDBObject inClause = new BasicDBObject("$in",
                rawByteArraysList);

        final Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL, aUserEmail);
        queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, Boolean.TRUE);
        queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID, inClause);

        final BasicDBObject query = new BasicDBObject(queryMap);

        final DBCollection recoms = getRecomsCollection(aPersistenceState);

        final DBCursor cursor = recoms.find(query);

        final Map<String,EbayAspectStatistic> statsByAspectName = new HashMap<String,
                EbayAspectStatistic>();

        LOGGER.debug("cursor.hasNext(): " + cursor.hasNext());

        while (cursor.hasNext())
        {
            final BasicDBObject curRecord = (BasicDBObject) cursor.next();

            final BasicDBList aspects = getProductAspects((byte[]) curRecord.get
                    (FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID), aPersistenceState);

            final String aspectValue = getAspectValue(aspects, aAspectName);

            if (aspectValue != null)
            {
                final EbayAspectStatistic curStatistic = getCurStatistic(statsByAspectName,
                        aAspectName, aspectValue, aProductCategoryId);

                final boolean hot = curRecord.getBoolean
                        (FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER);

                if (hot)
                {
                    curStatistic.incrementNumberOfLikedImages();
                }
                else
                {
                    curStatistic.incrementNumberOfDisklikedImages();
                }
            }
        }

        LOGGER.debug("statsByAspectName.values().size(): " + statsByAspectName.values().size());

        return new ArrayList<EbayAspectStatistic>(statsByAspectName.values());
    }

    protected BasicDBList getProductAspects(final byte[] aProductId,
                                          final IMongoPersistenceState aPersistenceState) {
        final ObjectId productId = new ObjectId(aProductId);
        final BasicDBObject query = new BasicDBObject("_id", productId);

        final DBCollection userProductsColl = getUserProductsCollection(aPersistenceState);

        final DBObject product = userProductsColl.findOne(query);

        return (BasicDBList) product.get(FIELD_EBAY_ASPECTS);
    }

    protected DBCollection getUserProductsCollection(final IMongoPersistenceState
                                                           aPersistenceState) {
        return aPersistenceState.getDb().getCollection(COLLECTION_USER_PRODUCTS);
    }

    protected DBCollection getRecomsCollection(final IMongoPersistenceState aPersistenceState) {
        return aPersistenceState.getDb().getCollection(
                COLLECTION_RECOMMENDATIONS);
    }

    protected BasicDBList convertObjectIdListToByteArrayList(final List<ObjectId> aProductIds) {
        final BasicDBList rawByteArraysList = new BasicDBList();

        for (final ObjectId curId : aProductIds)
        {
            rawByteArraysList.add(curId.toByteArray());
        }
        return rawByteArraysList;
    }

    protected EbayAspectStatistic getCurStatistic(
            final Map<String, EbayAspectStatistic> aStatsByAspectValue,
            final String aAspectName, final String aAspectValue, final String aProductCategoryId) {
        EbayAspectStatistic statistic = aStatsByAspectValue.get(aAspectValue);

        if (statistic == null)
        {
            final EbayAspect aspect = new EbayAspect();
            aspect.setName(aAspectName);
            aspect.setProductCategory(aProductCategoryId);

            statistic = new EbayAspectStatistic();
            statistic.setAspect(aspect);

            statistic.setValue(aAspectValue);

            aStatsByAspectValue.put(aAspectValue, statistic);
        }

        return statistic;
    }

    protected String getAspectValue(final BasicDBList aAspects, final String aAspectName) {
        String aspectValue = null;

        for (int i=0; (i < aAspects.size()) && (aspectValue == null); i++)
        {
            final BasicDBObject curNameValuePair = (BasicDBObject) aAspects.get(i);
            final Object[] entries = curNameValuePair.entrySet().toArray();

            if ((entries != null) && (entries.length > 0))
            {
                final Map.Entry entry = (Map.Entry) entries[0];

                final String curAspectName = (String) entry.getKey();

                if (aAspectName.equals(curAspectName))
                {
                    aspectValue = (String) entry.getValue();
                }
            }
        }

        return aspectValue;
    }

    protected BasicDBList convertListToBasicDBList(final List<ObjectId> aProductIds) {
        final BasicDBList result = new BasicDBList();

        result.addAll(aProductIds);

        return result;
    }

    protected List<ObjectId> getProductIds(final DBCollection aRecoms,
                                           final DBCollection aProducts,
                                           final String aUserEmail,
                                           final String aProductCategoryId) {
        // Get product IDs, which
        // a) belong to request.getProductCategoryId() and
        // b) were rated by user request.getUserEmail().

        final BasicDBList ratedProductIds = convertByteArrayToObjectId2(
                getRatedProductIds(aRecoms, aUserEmail));

        final Map<String,Object> queryData = new HashMap<String, Object>();
        queryData.put("_id", new BasicDBObject("$in", ratedProductIds));
        queryData.put(InwtPersistenceAction.FIELD_EBAY_PRIMARY_CATEGORY_ID,
                aProductCategoryId);

        final List<ObjectId> ratedProductsFromSelectedCategory =
                aProducts.distinct("_id", new BasicDBObject(queryData));

        return ratedProductsFromSelectedCategory;
    }

    protected List<byte[]> getRatedProductIds(final DBCollection aRecoms, final String aUserEmail) {
        final BasicDBObject query = new BasicDBObject(InwtPersistenceAction
                .FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL,
                aUserEmail);
        query.append(InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED,
                Boolean.TRUE);

        return aRecoms.distinct(InwtPersistenceAction
                .FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID,
                query);
    }

}
