package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.GetUserProductCategoriesRequest;
import co.altruix.inwt.messages.GetUserProductCategoriesResponse;
import co.altruix.inwt.server.actions.GetUserProductCategoriesAction;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

@Path("/"+ GetUserProductCategoriesRequest.SERVICE_NAME)
public class GetUserProductCategories extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetUserProductCategories.class);

    @POST
    @Produces("text/plain")
    public String getUserProductCategories(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final GetUserProductCategoriesRequest request = mapper.readValue(aRequest,
                    GetUserProductCategoriesRequest.class);

            final GetUserProductCategoriesResponse response =
                    getInwtPersistence().runAction(new GetUserProductCategoriesAction(request));

            return mapper.writeValueAsString(response);
        } catch (final JsonParseException exception) {
            LOGGER.error(GetUserProductCategoriesRequest.SERVICE_NAME, exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error(GetUserProductCategoriesRequest.SERVICE_NAME, exception);
        } catch (final IOException exception) {
            LOGGER.error(GetUserProductCategoriesRequest.SERVICE_NAME, exception);
        } catch (final InterruptedException exception) {
            LOGGER.error(GetUserProductCategoriesRequest.SERVICE_NAME, exception);
        } catch (final Exception exception) {
            LOGGER.error(GetUserProductCategoriesRequest.SERVICE_NAME, exception);
        }

        return "";
    }

}
