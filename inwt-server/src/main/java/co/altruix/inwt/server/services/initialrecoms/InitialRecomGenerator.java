package co.altruix.inwt.server.services.initialrecoms;

import co.altruix.inwt.messages.SubmitRecommendationBatchRequest;
import co.altruix.inwt.server.actions.SubmitRecommendationBatchAction;
import ru.altruix.androidprototyping.server.persistence.IPersistence;

import java.util.LinkedList;

public class InitialRecomGenerator implements IInitialRecomGenerator {

    public static final String INITIAL_RECOM_GENERATOR_INWT_INFO = "InitialRecomGenerator@inwt.info";

    @Override
    public void createInitialRecommendations(final IPersistence aPersistence,
                                             final String aNewUserEmail) throws
            InterruptedException {
        final SubmitRecommendationBatchRequest request = new SubmitRecommendationBatchRequest();
        request.setRecommenationRecipientEmail(aNewUserEmail);
        request.setSubmitterEmail(INITIAL_RECOM_GENERATOR_INWT_INFO);

        final LinkedList<String> productUrls = new LinkedList<String>();

        /**
         * Category 1 - anklets
         */
        productUrls.add("http://www.ebay.com/itm/Hot-Fashion-Barefoot-Sandal-Beach-Pearl-Foot-Jewelry-Anklet-Ring-Chain-Bracelet-/131303524560?pt=Fashion_Jewelry&hash=item1e924cc8d0");
        productUrls.add("http://www.ebay.com/itm/New-Beach-Fashion-Multi-Tassel-Toe-Ring-Bracelet-Chain-Link-Foot-Jewelry-Anklet-/331222913750?pt=Fashion_Jewelry&hash=item4d1e6c92d6");

        /**
         * Category 3 - bracelets
         */
        productUrls.add("http://www.ebay.com/itm/Ops-Objects-Original-Love-Heart-Tag-Iconic-Bracelet-/271660647020?pt=Fashion_Jewelry&hash=item3f403c826c");
        productUrls.add("http://www.ebay.com/itm/Bracciale-MORELLATO-INCANTO-cristalli-Turchese-Viola-Azzurro-Acciaio-Donna-DD-/131332876952?pt=Bigiotteria&var=&hash=item1e940caa98");

        /**
         * Category 4 - heir and head jewelry
         */
        productUrls.add("http://www.ebay.com/itm/Hot-New-Fashion-Rhinestone-Head-Chain-Jewelry-Headband-Head-Piece-Hair-Band-Free-/400767771988?pt=Fashion_Jewelry&hash=item5d4f9ee554");
        productUrls.add("http://www.ebay.com/itm/ART-NOUVEAU-LEAF-Oberon-Design-PEWTER-BARRETTE-jewelry-hair-clip-hand-poured-USA-/381046051115?pt=Fashion_Jewelry&hash=item58b81d512b");

        /**
         * Category 5 - necklaces and pendants
         */
        productUrls.add("http://www.ebay.com/itm/amber-amber-jewelry-baltic-amber-jewelry-/231378857363?pt=Fashion_Jewelry&hash=item35df412d93");
        productUrls.add("http://www.ebay.com/itm/Beautiful-Swarovski-Butterfly-Pin-/161477102103?pt=Fashion_Jewelry&hash=item2598c90a17");

        /**
         * Category 6 - pins & brooches
         */
        productUrls.add("http://www.ebay.com/itm/Gold-tone-LEOPARD-Pin-Rhinestone-Eyes-Collar-/201213229228?pt=Fashion_Jewelry&hash=item2ed93e38ac");
        productUrls.add("http://www.ebay.com/itm/NEW-Vivienne-westwood-PURPLE-Heart-Orb-Brooch-with-dust-bag-FREE-POSTAGE-/271661653819?pt=UK_JewelleryWatches_WomensJewellery_Rings_SR&hash=item3f404bdf3b");

        /**
         * Category 7 - rings
         */
        productUrls.add("http://www.ebay.com/itm/White-Sapphire-925-Sterling-Silver-Engagement-Wedding-Ring-Size-6-7-8-9-10-F664-/201136096131?pt=Gemstone_Rings&var=&hash=item2ed4a54383");
        productUrls.add("http://www.ebay.com/itm/Morganite-Blue-Topaz-Garnet-Amethyst-Sapphire-925-Silver-Ring-Size-6-7-8-9-10-11-/201034263897?pt=AU_FashionJewellery&var=&hash=item2ece936d59");

        /**
         * Category 8 - wristbands
         */
        productUrls.add("http://www.ebay.com/itm/Leather-Bracelet-with-Tibetan-Silver-Enameled-Yin-Yang-Charm-Friendship-GCPS034-/110913510620?pt=UK_JewelleryWatches_WomensJewellery_Rings_SR&hash=item19d2f5dcdc");
        productUrls.add("http://www.ebay.com/itm/Leather-White-Rhinestone-Wrist-Watch-Wristband-Cuff-Charm-Bracelet-Bangle-469-/301348935968?pt=LH_DefaultDomain_0&hash=item4629cbd920");


        request.setProductUrls(productUrls);

        aPersistence.runAction(createAction(request));
    }

    protected SubmitRecommendationBatchAction createAction(
            final SubmitRecommendationBatchRequest aRequest) {
        return new SubmitRecommendationBatchAction(aRequest);
    }
}
