package co.altruix.inwt.server.actions;

import java.util.LinkedList;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import co.altruix.inwt.server.persistence.IMongoPersistenceState;

public class FilterExistingEbayProductIdsAction extends
		InwtPersistenceAction<NewEbayProductIdInfo> {
	public static final String EBAY_PRODUCT = "ebayProducts";
	private NewEbayProductIdInfo newEbayProductIdInfo;

	public FilterExistingEbayProductIdsAction(
			final NewEbayProductIdInfo aNewEbayProductIdInfo) {
		newEbayProductIdInfo = aNewEbayProductIdInfo;
	}

	@Override
	protected NewEbayProductIdInfo runOnMongo(
			final IMongoPersistenceState aPersistenceState) {
		
		final BasicDBObject query = new BasicDBObject("url", new BasicDBObject("$in", 
				newEbayProductIdInfo.getEbayProductIds()));
		
		final DBCollection ebayProducts = aPersistenceState.getDb().getCollection(EBAY_PRODUCT);
		
		final List<String> existingUrls = new LinkedList<String>();
		
		final DBCursor cursor = ebayProducts.find(query);
		
		try
		{
			while (cursor.hasNext())
			{
				existingUrls.add((String) cursor.next().get("url"));
			}
		}
		finally
		{
			cursor.close();
		}
		
		newEbayProductIdInfo.getEbayProductIds().removeAll(existingUrls);
		
		return newEbayProductIdInfo;
	}
}
