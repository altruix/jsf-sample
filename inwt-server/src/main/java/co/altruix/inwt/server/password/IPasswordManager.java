package co.altruix.inwt.server.password;

public interface IPasswordManager {
	String getSaltedHash(String password) throws Exception;
	boolean check(String password, String stored) throws Exception;
}
