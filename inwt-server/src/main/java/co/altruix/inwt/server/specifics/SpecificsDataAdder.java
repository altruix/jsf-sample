package co.altruix.inwt.server.specifics;

import co.altruix.inwt.server.actions.specifics.AddSpecificsDataAction;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.altruix.androidprototyping.server.persistence.IPersistence;

import static co.altruix.inwt.server.ebay.EbayCrawler.PERSISTENCE;

@DisallowConcurrentExecution
public class SpecificsDataAdder implements Job {
    private static final Logger LOGGER = LoggerFactory.getLogger(SpecificsDataAdder.class);

    @Override
    public void execute(final JobExecutionContext aJobExecutionContext) throws
            JobExecutionException {
        final IPersistence persistence = (IPersistence) aJobExecutionContext.getJobDetail()
                .getJobDataMap().get(PERSISTENCE);

        try {
            Boolean productsWithoutSpecificsPresent = Boolean.TRUE;

            while (productsWithoutSpecificsPresent)
            {
                productsWithoutSpecificsPresent = persistence.runAction(new
                        AddSpecificsDataAction());
            }
        } catch (final InterruptedException exception) {
            LOGGER.error("", exception);
        }
    }
}
