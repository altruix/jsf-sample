package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.DeleteImageRequest;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import org.bson.types.ObjectId;

public class DeleteImageAction extends InwtPersistenceAction<Object> {
    private final DeleteImageRequest request;

    public DeleteImageAction(final DeleteImageRequest aRequest) {
        request = aRequest;
    }

    @Override
    protected Object runOnMongo(final IMongoPersistenceState aPersistenceState) {
        final DBCollection userProductImages = aPersistenceState.getDb().getCollection(
                COLLECTION_USER_PRODUCT_IMAGES);

        final BasicDBObject query = new BasicDBObject();
        query.put("_id", new ObjectId(request.getImageId()));

        // final DBObject dbObj = userProductImages.findOne(query);


        final BasicDBObject newDocument = new BasicDBObject();
        newDocument.append("$set", new BasicDBObject().append(
                FIELD_COLLECTION_USER_PRODUCT_IMAGES_IS_DELETED, Boolean.TRUE));

        userProductImages.update(query, newDocument);

        return null;
    }
}
