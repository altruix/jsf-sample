package co.altruix.inwt.server.services.recomquality;

import java.util.Comparator;

public class RecomByCreationDateComparator implements Comparator<Recom> {
    @Override
    public int compare(final Recom aRecom1, final Recom aRecom2) {
        final int equalityByCreationTime = aRecom1.getCreationDateTimeUtc().compareTo(
                aRecom2.getCreationDateTimeUtc());
        if (equalityByCreationTime != 0)
        {
            return equalityByCreationTime;
        }


        return aRecom1.getHotNotMarkingDateTimeUtc().compareTo(
                aRecom2.getHotNotMarkingDateTimeUtc());
    }
}
