package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.GetLikedProductCategoriesRequest;
import co.altruix.inwt.messages.GetLikedProductCategoriesResponse;
import co.altruix.inwt.model.EbayProductCategoryPopularityInfo;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.sort;

public class GetLikedProductCategoriesAction extends
        InwtPersistenceAction<GetLikedProductCategoriesResponse> {
    class RawProductPopularityInfo
    {
        private final byte[] productId;
        private int numberOfLikedImages = 0;

        RawProductPopularityInfo(final byte[] aProductId) {
            productId = aProductId;
        }

        public void incrementNumberOfLikedImages()
        {
            numberOfLikedImages++;
        }

        public int getNumberOfLikedImages() {
            return numberOfLikedImages;
        }

        public byte[] getProductId() {
            return productId;
        }
    }

    class ProductCategoryData
    {
        private String categoryId;
        private String categoryName;

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(final String aCategoryId) {
            categoryId = aCategoryId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(final String aCategoryName) {
            categoryName = aCategoryName;
        }
    }

    private final GetLikedProductCategoriesRequest request;

    public GetLikedProductCategoriesAction(final GetLikedProductCategoriesRequest aRequest) {
        request = aRequest;
    }

    @Override
    protected GetLikedProductCategoriesResponse runOnMongo(
            final IMongoPersistenceState aPersistenceState) {
        final Map<byte[], RawProductPopularityInfo> rawProductPopularityInfoByProductId =
                generateRawProductPopularityInfoMap(aPersistenceState);

        final Map<String,EbayProductCategoryPopularityInfo> popularityDataByCategoryId = new
                HashMap<String, EbayProductCategoryPopularityInfo>();

        for (final RawProductPopularityInfo curRecord : rawProductPopularityInfoByProductId
                .values())
        {
            final ProductCategoryData categoryData = getProductCategoryData(curRecord.getProductId
                    (), aPersistenceState);

            EbayProductCategoryPopularityInfo popularityInfo = popularityDataByCategoryId.get
                    (categoryData.getCategoryId());

            if (popularityInfo == null)
            {
                popularityInfo = new EbayProductCategoryPopularityInfo();
                popularityInfo.setCategoryId(categoryData.getCategoryId());

                popularityDataByCategoryId.put(popularityInfo.getCategoryId(), popularityInfo);
            }

            popularityInfo.appendCategoryName(categoryData.getCategoryName());

            popularityInfo.incrementNumberOfLikedImages(curRecord.getNumberOfLikedImages());
        }

        final ArrayList result = new ArrayList(popularityDataByCategoryId.values());
        sort(result, new EbayProductCategoryPopularityInfoComparator());

        final GetLikedProductCategoriesResponse response = new GetLikedProductCategoriesResponse();
        response.setPopularityData(result);

        return response;
    }

    private Map<byte[], RawProductPopularityInfo> generateRawProductPopularityInfoMap(
            final IMongoPersistenceState aPersistenceState) {
        final Map<String,Object> data = new HashMap<String, Object>();

        data.put(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL, request.getUserEmail());
        data.put(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, Boolean.TRUE);

        final DBCollection recommendations = aPersistenceState.getDb().getCollectionFromString(
                COLLECTION_RECOMMENDATIONS);

        final BasicDBObject query = new BasicDBObject(data);

        final DBCursor cursor = recommendations.find(query);

        final Map<byte[],RawProductPopularityInfo> rawProductPopularityInfoByProductId =
                new HashMap<byte[], RawProductPopularityInfo>();

        while (cursor.hasNext())
        {
            final BasicDBObject record = (BasicDBObject) cursor.next();

            final byte[] productId = (byte[]) record.get(
                    FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID);

            RawProductPopularityInfo statRecord = rawProductPopularityInfoByProductId.get
                    (productId);

            if (statRecord == null)
            {
                statRecord = new RawProductPopularityInfo(productId);
                rawProductPopularityInfoByProductId.put(productId, statRecord);
            }

            statRecord.incrementNumberOfLikedImages();
        }

        cursor.close();
        return rawProductPopularityInfoByProductId;
    }

    private ProductCategoryData getProductCategoryData(final byte[] aProductId,
                                                       final IMongoPersistenceState aPersistenceState) {
        final Map<String,Object> data = new HashMap<String, Object>();

        data.put("_id", new ObjectId(aProductId));


        final DBCollection userProducts = aPersistenceState.getDb().getCollection
                (COLLECTION_USER_PRODUCTS);

        final BasicDBObject record = (BasicDBObject) userProducts.findOne(new BasicDBObject(data));

        if (record != null)
        {
            final ProductCategoryData result = new ProductCategoryData();

            result.setCategoryId(record.getString(FIELD_EBAY_PRIMARY_CATEGORY_ID));
            result.setCategoryName(record.getString(FIELD_EBAY_PRIMARY_CATEGORY_NAME));

            return result;
        }

        // TODO: Test
        return null;
    }
}
