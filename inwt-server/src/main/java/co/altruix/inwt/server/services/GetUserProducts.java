package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.GetUserProductsRequest;
import co.altruix.inwt.messages.GetUserProductsResponse;
import co.altruix.inwt.server.actions.GetUserProductsAction;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

@Path("/"+ GetUserProductsRequest.SERVICE_NAME)
public class GetUserProducts extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetUserProducts.class);

    @POST
    @Produces("text/plain")
    public String getUserProducts(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final GetUserProductsRequest request = mapper.readValue(aRequest,
                    GetUserProductsRequest.class);

            final GetUserProductsResponse response = getInwtPersistence().runAction(
                    new GetUserProductsAction(request.getUserEmail()));

            return mapper.writeValueAsString(response);
        } catch (final JsonParseException exception) {
            LOGGER.error("signUp", exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error("signUp", exception);
        } catch (final IOException exception) {
            LOGGER.error("signUp", exception);
        } catch (final InterruptedException exception) {
            LOGGER.error("signUp", exception);
        } catch (final Exception exception) {
            LOGGER.error("signUp", exception);
        }

        return "";
    }

}
