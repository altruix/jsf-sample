package co.altruix.inwt.server.email;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.ServletContext;

public interface IEmailSender {
    void sendEMail(final String aSubject, final String aBody,
            final String aRecipient, final ServletContext aContext) throws AddressException, MessagingException;

}
