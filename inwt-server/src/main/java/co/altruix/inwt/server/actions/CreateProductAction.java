package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.CreateProductRequest;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CreateProductAction extends InwtPersistenceAction<Boolean> {
    private final CreateProductRequest request;

    public CreateProductAction(final CreateProductRequest aRequest) {
        request = aRequest;
    }

    @Override
    protected Boolean runOnMongo(final IMongoPersistenceState aPersistenceState) {
        final DBCollection userProducts = aPersistenceState.getDb().getCollection(
                COLLECTION_USER_PRODUCTS);

        final Map newProductData = new HashMap();

        newProductData.put(FIELD_USER_PRODUCTS_CREATOR_EMAIL, request.getCreatorEmail());
        newProductData.put(FIELD_USER_PRODUCTS_NAME, request.getName());
        newProductData.put(FIELD_USER_PRODUCTS_DESCRIPTION, request.getDescription());
        newProductData.put(FIELD_USER_PRODUCTS_CREATION_DATETIME_UTC,
                getCurrentUtcTime());

        final BasicDBObject newProduct = new BasicDBObject(newProductData);
        userProducts.insert(newProduct);

        return Boolean.TRUE;
    }

    public static Date getCurrentUtcTime() {
        return new DateTime(DateTimeZone.UTC).toDate();
    }
}
