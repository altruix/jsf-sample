package co.altruix.inwt.server.actions;

import co.altruix.inwt.model.EbayAspectStatistic;

import java.util.Comparator;

class EbayAspectStatisticByPopularityComparator implements
        Comparator<EbayAspectStatistic> {
    @Override
    public int compare(final EbayAspectStatistic o1, final EbayAspectStatistic o2) {
        return o2.getNumberOfLikedImages() - o1.getNumberOfLikedImages();
    }
}
