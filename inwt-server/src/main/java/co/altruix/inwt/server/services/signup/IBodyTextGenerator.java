package co.altruix.inwt.server.services.signup;

public interface IBodyTextGenerator {
	String composeBodyText(final String aPassword);
}
