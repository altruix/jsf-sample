package co.altruix.inwt.server.services.signup;

public class BodyTextGenerator implements IBodyTextGenerator {

	@Override
	public String composeBodyText(final String aPassword) {
		final StringBuilder builder = new StringBuilder();
		
		builder.append("Hello!");
		builder.append(System.getProperty("line.separator"));
		builder.append("Recently, someone (probably you) registered with INWT, a novel way to discover costume jewelry and fashion accessories.");
		builder.append(System.getProperty("line.separator"));
		builder.append("Please copy the password and paste it into the 'Password' field in the INWT mobile app.");
		builder.append(System.getProperty("line.separator"));
		builder.append(System.getProperty("line.separator"));
		builder.append(aPassword);
		builder.append(System.getProperty("line.separator"));
		builder.append(System.getProperty("line.separator"));
		builder.append("If you did not register with INWT, please just ignore this message.");
		builder.append(System.getProperty("line.separator"));
		builder.append(System.getProperty("line.separator"));
		builder.append("Best regards");
		builder.append(System.getProperty("line.separator"));
		builder.append(System.getProperty("line.separator"));
		builder.append("Dmitri Pisarenko");
		return builder.toString();
	}
}
