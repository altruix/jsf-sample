package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.NewImageAvailabilityRequest;
import co.altruix.inwt.messages.NewImageAvailabilityResponse;
import co.altruix.inwt.server.actions.NewImageAvailabilityAction;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

import static co.altruix.inwt.messages.NewImageAvailabilityRequest.SERVICE_NAME;

@Path("/"+ SERVICE_NAME)
public class NewImageAvailability extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(NewImageAvailability.class);

    @POST
    @Produces("text/plain")
    public String newImageAvailability(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final NewImageAvailabilityRequest request = mapper.readValue(aRequest,
                    NewImageAvailabilityRequest.class);

            final NewImageAvailabilityResponse response =
                    getInwtPersistence().runAction(new NewImageAvailabilityAction(request));

            return mapper.writeValueAsString(response);
        } catch (final JsonParseException exception) {
            LOGGER.error(SERVICE_NAME, exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error(SERVICE_NAME, exception);
        } catch (final IOException exception) {
            LOGGER.error(SERVICE_NAME, exception);
        } catch (final InterruptedException exception) {
            LOGGER.error(SERVICE_NAME, exception);
        } catch (final Exception exception) {
            LOGGER.error(SERVICE_NAME, exception);
        }

        return "";
    }

}
