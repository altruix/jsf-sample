package co.altruix.inwt.server.actions;

import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import org.bson.types.ObjectId;
import ru.altruix.androidprototyping.server.persistence.IPersistenceAction;
import ru.altruix.androidprototyping.server.persistence.IPersistenceState;

import java.util.List;

public abstract class InwtPersistenceAction<T> implements IPersistenceAction<T> {
    public static final String TIME_STAMP_UTC = "timeStampUtc";

    /**
     * Collection userBehaviourLog (start)
     */
    public static final String COLLECTION_USER_BEHAVIOUR_LOG = "userBehaviourLog";
    public static final String FIELD_COLLECTION_USER_BEHAVIOUR_LOG_TIMESTAMP_UTC =
            TIME_STAMP_UTC;
    public static final String FIELD_COLLECTION_USER_BEHAVIOUR_LOG_USER_EMAIL =
            "userEmail";
    public static final String FIELD_COLLECTION_USER_BEHAVIOUR_LOG_EVENT_TYPE =
            "eventType";
    public static final String FIELD_COLLECTION_USER_BEHAVIOUR_LOG_MESSAGE =
            "message";

    /**
     * Collection userBehaviourLog (end)
     */

    /**
     * Collection recommendationLog (start)
     */
    public static final String COLLECTION_RECOMMENDATION_LOG = "recommendationLog";
    public static final String FIELD_COLLECTION_RECOMMENDATION_LOG_SUBMITTER_EMAIL =
            "submitterEmail";
    public static final String FIELD_COLLECTION_RECOMMENDATION_LOG_RECIPIENT_EMAIL =
            "recipientEmail";
    public static final String FIELD_COLLECTION_RECOMMENDATION_LOG_TIMESTAMP =
            TIME_STAMP_UTC;
    public static final String FIELD_COLLECTION_RECOMMENDATION_LOG_MESSAGE =
            "message";
    public static final String FIELD_COLLECTION_RECOMMENDATION_LOG_PRODUCT_URL = "productUrl";

    /**
     * Collection recommendationLog (end)
     */

    /**
     * Collection recommendations (start)
     */
    public static final String COLLECTION_RECOMMENDATIONS = "recommendations";
    public static final String FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL = "recipientEmail";
    public static final String FIELD_COLLECTION_RECOMMENDATIONS_USER_IMAGE_ID = "userImageId";
    public static final String FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID = "productId";
    public static final String FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED = "wasProposed";
    public static final String FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER =
            "markedAsHotByUser";
    public static final String FIELD_COLLECTION_RECOMMENDATIONS_CREATION_DATETIME_UTC =
            "creationDateTimeUtc";
    public static final String FIELD_COLLECTION_RECOMMENDATIONS_HOT_NOT_MARKING_DATETIME_UTC =
            "hotNotMarkingDateTimeUtc";
    public static final String FIELD_COLLECTION_RECOMMENDATIONS_SUBMITTER_EMAIL = "submitterEmail";

    /**
     * Collection recommendations (end)
     */

    /**
     * Collection userProductImages (start)
     */
    public static final String COLLECTION_USER_PRODUCT_IMAGES = "userProductImages";
    public static final String FIELD_COLLECTION_USER_PRODUCT_IMAGES_CREATOR_EMAIL = "creatorEmail";
    public static final String FIELD_COLLECTION_USER_PRODUCT_IMAGES_PRODUCT_ID = "userProductId";
    public static final String FIELD_COLLECTION_USER_PRODUCT_IMAGES_DATA = "imageData";
    public static final String FIELD_COLLECTION_USER_PRODUCT_IMAGES_FILE_NAME = "fileName";
    public static final String FIELD_COLLECTION_USER_PRODUCT_IMAGES_MIME_TYPE = "mimeType";
    public static final String FIELD_COLLECTION_USER_PRODUCT_IMAGES_IS_DELETED = "isDeleted";
    /**
     * Collection userProductImages (end)
     */


    /**
     * Collection userProducts (start)
     */
    public static final String COLLECTION_USER_PRODUCTS = "userProducts";
    public static final String FIELD_USER_PRODUCTS_CREATOR_EMAIL = "creatorEmail";
    public static final String FIELD_USER_PRODUCTS_NAME = "name";
    public static final String FIELD_USER_PRODUCTS_DESCRIPTION = "description";
    public static final String FIELD_USER_PRODUCTS_CREATION_DATETIME_UTC = "creationDateTimeUtc";
    public static final String FIELD_USER_PRODUCTS_PRODUCT_URL = "productUrl";

    /**
     * E-bay fields (start)
     */
    public static final String FIELD_EBAY_ITEM_ID = "ebayItemId";
    public static final String FIELD_EBAY_VIEW_ITEM_URL_FOR_NATURAL_SEARCH =
            "ebayViewItemURLForNaturalSearch";
    public static final String FIELD_EBAY_LISTING_TYPE = "ebayListingType";
    public static final String FIELD_EBAY_PRIMARY_CATEGORY_ID =
            "ebayPrimaryCategoryID";
    public static final String FIELD_EBAY_PRIMARY_CATEGORY_NAME =
            "ebayPrimaryCategoryName";
    public static final String
            FIELD_EBAY_CONVERTED_CURRENT_PRICE_AMOUNT =
            "ebayConvertedCurrentPriceAmount";
    public static final String
            FIELD_EBAY_CONVERTED_CURRENT_PRICE_CURRENCY =
            "ebayConvertedCurrentPriceCurrency";
    public static final String FIELD_EBAY_ASPECTS = "ebayAspects";
    /**
     * E-bay fields (start)
     */
    /**
     * Collection userProducts (end)
     */


    /**
     * Collection users (start)
     */
    public static final String COLLECTION_USERS = "users";
	public static final String FIELD_USERS_PASSWORD_HASH = "password_hash";
	public static final String FIELD_USERS_EMAIL = "email";
    public static final String SET = "$set";

    public static List<byte[]> getProductIdsLikedByUser(final DBCollection aRecoms,
                                                        final String aUserEmail) {
        final BasicDBObject query = new BasicDBObject(InwtPersistenceAction
                .FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL,
                aUserEmail);
        query.append(InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED,
                Boolean.TRUE);
        query.append(InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_MARKED_AS_HOT_BY_USER,
                Boolean.TRUE);

        return aRecoms.distinct(InwtPersistenceAction.FIELD_COLLECTION_RECOMMENDATIONS_PRODUCT_ID,
                query);
    }

    public static BasicDBList convertByteArrayToObjectId2(final List<byte[]> aProductIdBytes) {
        final BasicDBList productIds = new BasicDBList();
        for (final byte[] curProductId : aProductIdBytes)
        {
            productIds.add(new ObjectId(curProductId));
        }
        return productIds;
    }

    /**
     * Collection users (end)
     */

	@Override
	public T run(final IPersistenceState aPersistenceState) {
		return runOnMongo((IMongoPersistenceState) aPersistenceState);
	}

	protected abstract T runOnMongo(IMongoPersistenceState aPersistenceState);
}
