package co.altruix.inwt.server.actions;

public class Recommendation {
    private byte[] imageId;
    private byte[] productId;

    public Recommendation(final byte[] aImageId, final byte[] aProductId) {
        imageId = aImageId;
        productId = aProductId;
    }

    public byte[] getImageId() {
        return imageId;
    }

    public void setImageId(final byte[] aImageId) {
        imageId = aImageId;
    }

    public byte[] getProductId() {
        return productId;
    }

    public void setProductId(final byte[] aProductId) {
        productId = aProductId;
    }

}
