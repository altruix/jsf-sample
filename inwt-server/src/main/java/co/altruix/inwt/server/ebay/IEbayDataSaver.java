package co.altruix.inwt.server.ebay;

import ru.altruix.androidprototyping.server.persistence.IPersistence;

import com.ebay.services.finding.SearchResult;

public interface IEbayDataSaver {
	void saveEbaySearchResults(final IPersistence aPersistence, final SearchResult aSearchResult);
}
