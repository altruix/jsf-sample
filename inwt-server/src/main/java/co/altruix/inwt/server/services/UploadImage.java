package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.CreateProductRequest;
import co.altruix.inwt.messages.CreateProductResponse;
import co.altruix.inwt.messages.UploadImageRequest;
import co.altruix.inwt.messages.UploadImageResponse;
import co.altruix.inwt.server.actions.CreateProductAction;
import co.altruix.inwt.server.actions.UploadImageAction;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

@Path("/"+ UploadImageRequest.SERVICE_NAME)
public class UploadImage extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UploadImage.class);

    @POST
    @Produces("text/plain")
    public String uploadImage(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final UploadImageRequest request = mapper.readValue(aRequest,
                    UploadImageRequest.class);

            getInwtPersistence().runAction(new UploadImageAction(request));

            final UploadImageResponse response = new UploadImageResponse();

            return mapper.writeValueAsString(response);
        } catch (final JsonParseException exception) {
            LOGGER.error(CreateProductRequest.SERVICE_NAME, exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error(CreateProductRequest.SERVICE_NAME, exception);
        } catch (final IOException exception) {
            LOGGER.error(CreateProductRequest.SERVICE_NAME, exception);
        } catch (final InterruptedException exception) {
            LOGGER.error(CreateProductRequest.SERVICE_NAME, exception);
        } catch (final Exception exception) {
            LOGGER.error(CreateProductRequest.SERVICE_NAME, exception);
        }

        return "";
    }

}
