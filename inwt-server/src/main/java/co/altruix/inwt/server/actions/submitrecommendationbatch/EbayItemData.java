package co.altruix.inwt.server.actions.submitrecommendationbatch;

import java.util.List;

public class EbayItemData implements IEbayItemData {
    private String itemId;
    private String viewItemURLForNaturalSearch;
    private String listingType;
    private List<String> pictureUrls;
    private String primaryCategoryID;
    private String primaryCategoryName;
    private ConvertedCurrentPrice convertedCurrentPrice;
    private String title;

    @Override
    public String getItemId() {
        return itemId;
    }

    @Override
    public void setItemId(final String aItemId) {
        itemId = aItemId;
    }

    @Override
    public String getViewItemURLForNaturalSearch() {
        return viewItemURLForNaturalSearch;
    }

    @Override
    public void setViewItemURLForNaturalSearch(final String aViewItemURLForNaturalSearch) {
        viewItemURLForNaturalSearch = aViewItemURLForNaturalSearch;
    }

    @Override
    public String getListingType() {
        return listingType;
    }

    @Override
    public void setListingType(final String aListingType) {
        listingType = aListingType;
    }

    @Override
    public List<String> getPictureUrls() {
        return pictureUrls;
    }

    @Override
    public void setPictureUrls(final List<String> aPictureUrls) {
        pictureUrls = aPictureUrls;
    }

    @Override
    public String getPrimaryCategoryID() {
        return primaryCategoryID;
    }

    @Override
    public void setPrimaryCategoryID(final String aPrimaryCategoryID) {
        primaryCategoryID = aPrimaryCategoryID;
    }

    @Override
    public String getPrimaryCategoryName() {
        return primaryCategoryName;
    }

    @Override
    public void setPrimaryCategoryName(final String aPrimaryCategoryName) {
        primaryCategoryName = aPrimaryCategoryName;
    }

    @Override
    public ConvertedCurrentPrice getConvertedCurrentPrice() {
        return convertedCurrentPrice;
    }

    @Override
    public void setConvertedCurrentPrice(final ConvertedCurrentPrice aConvertedCurrentPrice) {
        convertedCurrentPrice = aConvertedCurrentPrice;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(final String aTitle) {
        title = aTitle;
    }
}
