package co.altruix.inwt.server.actions.specifics;

import java.util.Map;

public interface IEbayAspectDataReader {
    Map<String,String> getAspectData(final String aEbayItemId);
}
