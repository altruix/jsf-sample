package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.GetProductImageRequest;
import co.altruix.inwt.messages.GetProductImageResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

@Path("/GetProductImage")
public class GetProductImage extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetProductImage.class);

    public GetProductImage()
    {

    }

    @POST
    @Produces("text/plain")
    public String getProductImage(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final GetProductImageRequest request = mapper.readValue(aRequest,
                    GetProductImageRequest.class);

            final GetProductImageResponse response = processRequest(request);

            return mapper.writeValueAsString(response);
        } catch (final JsonParseException exception) {
            LOGGER.error("getProductImage", exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error("getProductImage", exception);
        } catch (final IOException exception) {
            LOGGER.error("getProductImage", exception);
        }

        return "";
    }

    private GetProductImageResponse processRequest(final GetProductImageRequest aRequest) {
        return null;
    }
}
