package co.altruix.inwt.server.actions;

import co.altruix.inwt.model.UserProductImage;
import com.mongodb.BasicDBObject;

public class UserProductImageUtils {
    private UserProductImageUtils()
    {

    }

    public static UserProductImage convertTupleToUserProductImage(final BasicDBObject aCurRecord,
                                                            final byte[] aUserProductImageId,
                                                            final byte[] aUserProductId) {
        final UserProductImage userProductImage = new UserProductImage();

        userProductImage.setUserProductImageId(aUserProductImageId);
        userProductImage.setUserProductId(aUserProductId);
        userProductImage.setFileName(aCurRecord.getString(
                InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_FILE_NAME));
        userProductImage.setMimeType(aCurRecord.getString(
                InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_MIME_TYPE));
        userProductImage.setCreatorEmail(aCurRecord.getString(
                InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_CREATOR_EMAIL));
        userProductImage.setImageData((byte[]) aCurRecord.get
            (InwtPersistenceAction.FIELD_COLLECTION_USER_PRODUCT_IMAGES_DATA));
        return userProductImage;
    }
}
