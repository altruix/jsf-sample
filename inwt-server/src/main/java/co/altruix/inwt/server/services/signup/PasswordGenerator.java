package co.altruix.inwt.server.services.signup;

import org.apache.commons.lang3.RandomStringUtils;

public class PasswordGenerator implements IPasswordGenerator {

	@Override
	public String generatePassword() {
		return RandomStringUtils.randomAlphanumeric(30);
	}

}
