package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.GetProductImageRequest;
import co.altruix.inwt.messages.GetProductImageResponse;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

public class GetProductImageAction extends InwtPersistenceAction<GetProductImageResponse> {
    private final GetProductImageRequest request;

    public GetProductImageAction(final GetProductImageRequest aRequest) {
        request = aRequest;
    }

    @Override
    protected GetProductImageResponse runOnMongo(final IMongoPersistenceState aPersistenceState) {
        final DBCollection userRatings = aPersistenceState.getDb().getCollection(
                "userRatings");
        final DBCollection ebayProducts = aPersistenceState.getDb().getCollection(
                FilterExistingEbayProductIdsAction.EBAY_PRODUCT);



        // final BasicDBObject query = new BasicDBObject("", )

        final DBObject productInfo = ebayProducts
                .findOne();




        // Find first product image, which was not yet shown to the user X

        // Two collections:
        //
        // 1) ebayProducts
        // 2) userRatings



        request.getEmail();

        return null;
    }
}
