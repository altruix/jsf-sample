package co.altruix.inwt.server.services;

import ru.altruix.androidprototyping.server.persistence.IPersistence;

public abstract class AbstractInwtService {

	private IPersistence inwtPersistence;

	public IPersistence getInwtPersistence() {
		return inwtPersistence;
	}

	public void setInwtPersistence(final IPersistence aPersistence) {
		this.inwtPersistence = aPersistence;
	}

}
