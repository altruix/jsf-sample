package co.altruix.inwt.server.actions;

import java.util.List;

public class NewEbayProductIdInfo {
	private List<String> ebayProductIds;

	public List<String> getEbayProductIds() {
		return ebayProductIds;
	}

	public void setEbayProductIds(final List<String> aEbayProductIds) {
		this.ebayProductIds = aEbayProductIds;
	}
}
