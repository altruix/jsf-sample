package co.altruix.inwt.server.services;

import co.altruix.inwt.messages.GetNextRecommendedImageRequest;
import co.altruix.inwt.messages.GetNextRecommendedImageResponse;
import co.altruix.inwt.server.actions.GetNextRecommendedImageAction;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.IOException;

@Path("/"+ GetNextRecommendedImageRequest.SERVICE_NAME)
public class GetNextRecommendedImage extends AbstractInwtService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GetNextRecommendedImage.class);

    @POST
    @Produces("text/plain")
    public String getNextRecommendedImage(@FormParam("request") final String aRequest) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final GetNextRecommendedImageRequest request = mapper.readValue(aRequest,
                    GetNextRecommendedImageRequest.class);

            final GetNextRecommendedImageResponse response =
                    getInwtPersistence().runAction(new GetNextRecommendedImageAction(request));

            return mapper.writeValueAsString(response);
        } catch (final JsonParseException exception) {
            LOGGER.error(GetNextRecommendedImageRequest.SERVICE_NAME, exception);
        } catch (final JsonMappingException exception) {
            LOGGER.error(GetNextRecommendedImageRequest.SERVICE_NAME, exception);
        } catch (final IOException exception) {
            LOGGER.error(GetNextRecommendedImageRequest.SERVICE_NAME, exception);
        } catch (final InterruptedException exception) {
            LOGGER.error(GetNextRecommendedImageRequest.SERVICE_NAME, exception);
        } catch (final Exception exception) {
            LOGGER.error(GetNextRecommendedImageRequest.SERVICE_NAME, exception);
        }

        return "";
    }

}
