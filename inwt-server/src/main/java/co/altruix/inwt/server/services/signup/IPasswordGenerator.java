package co.altruix.inwt.server.services.signup;

public interface IPasswordGenerator {
	String generatePassword();
}
