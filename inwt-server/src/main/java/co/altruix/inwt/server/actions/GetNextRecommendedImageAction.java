package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.GetNextRecommendedImageRequest;
import co.altruix.inwt.messages.GetNextRecommendedImageResponse;
import co.altruix.inwt.messages.RecommendationResult;
import co.altruix.inwt.model.UserProductImage;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.Map;

public class GetNextRecommendedImageAction extends
        InwtPersistenceAction<GetNextRecommendedImageResponse> {
    private final GetNextRecommendedImageRequest request;

    public GetNextRecommendedImageAction(final GetNextRecommendedImageRequest aRequest) {
        request = aRequest;
    }

    @Override
    protected GetNextRecommendedImageResponse runOnMongo(
            final IMongoPersistenceState aPersistenceState) {

        final Map<String,Object> queryMap = new HashMap<String,Object>();
        queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_RECIPIENT_EMAIL, request.getUserEmail());
        queryMap.put(FIELD_COLLECTION_RECOMMENDATIONS_WAS_PROPOSED, false);

        final DBCollection userProducts = aPersistenceState.getDb().getCollection(
                COLLECTION_RECOMMENDATIONS);

        final DBCursor cursor = userProducts.find(new BasicDBObject(queryMap));

        final GetNextRecommendedImageResponse response = new GetNextRecommendedImageResponse();

        if (cursor.hasNext())
        {
            final BasicDBObject curRecord = (BasicDBObject) cursor.next();

            final byte[] userImageId = (byte[]) curRecord.get
                    (FIELD_COLLECTION_RECOMMENDATIONS_USER_IMAGE_ID);

            response.setResult(RecommendationResult.NEW_IMAGE_AVAILABLE);

            response.setImage(loadImage(aPersistenceState.getDb(), userImageId));
        }
        else
        {
            response.setResult(RecommendationResult.NO_NEW_IMAGES);
        }

        cursor.close();

        return response;
    }

    private UserProductImage loadImage(final DB aDb, final byte[] aUserImageId) {
        final DBCollection userProductImagesColl = aDb.getCollection(
                COLLECTION_USER_PRODUCT_IMAGES);
        final BasicDBObject query = new BasicDBObject(
                "_id", new ObjectId(aUserImageId));

        final DBCursor cursor = userProductImagesColl.find(query);

        final UserProductImage result;

        try
        {
            if (cursor.hasNext())
            {
                final BasicDBObject curRecord = (BasicDBObject) cursor.next();

                result =
                        UserProductImageUtils.convertTupleToUserProductImage(curRecord,
                                curRecord.getObjectId("_id").toByteArray(),
                                (byte[])curRecord.get(
                                        FIELD_COLLECTION_USER_PRODUCT_IMAGES_PRODUCT_ID));
            }
            else
            {
                result = null;
            }
        }
        finally
        {
            cursor.close();
        }

        return result;
    }
}
