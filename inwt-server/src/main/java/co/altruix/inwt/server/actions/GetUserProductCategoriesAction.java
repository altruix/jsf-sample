package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.GetUserProductCategoriesRequest;
import co.altruix.inwt.messages.GetUserProductCategoriesResponse;
import co.altruix.inwt.model.EbayProductCategory;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class GetUserProductCategoriesAction extends
        InwtPersistenceAction<GetUserProductCategoriesResponse> {
    private final GetUserProductCategoriesRequest request;

    public GetUserProductCategoriesAction(final GetUserProductCategoriesRequest aRequest) {
        request = aRequest;
    }

    @Override
    protected GetUserProductCategoriesResponse runOnMongo(
            final IMongoPersistenceState aPersistenceState) {
        final DBCollection recoms = getCollection(aPersistenceState, COLLECTION_RECOMMENDATIONS);

        final List<byte[]> productIdBytes = getProductIdAsByteArrays(recoms,
                request.getUserEmail());

        final BasicDBList productIds = convertByteArrayToObjectId(productIdBytes);

        final BasicDBObject query = createQuery(productIds);

        final DBCollection userProducts = getCollection(aPersistenceState,
                COLLECTION_USER_PRODUCTS);

        final DBCursor cursor = userProducts.find(query);

        final Map<String,EbayProductCategory> categoriesByIds = new HashMap<String,
                EbayProductCategory>();

        while (cursor.hasNext())
        {
            final BasicDBObject curRecord = (BasicDBObject) cursor.next();
            final String categoryId = curRecord.getString(FIELD_EBAY_PRIMARY_CATEGORY_ID);
            final String categoryName = curRecord.getString(FIELD_EBAY_PRIMARY_CATEGORY_NAME);

            if (StringUtils.isNotBlank(categoryId))
            {
                EbayProductCategory category = categoriesByIds.get(categoryId);

                if (category == null)
                {
                    category = new EbayProductCategory();
                    category.setCategoryId(categoryId);

                    category.setCategoryNames(new LinkedList<String>());

                    categoriesByIds.put(categoryId, category);
                }

                if (!category.getCategoryNames().contains(categoryName))
                {
                    category.getCategoryNames().add(categoryName);
                }
            }
        }

        final GetUserProductCategoriesResponse response = new GetUserProductCategoriesResponse();
        response.setUserProductCategories(
                new LinkedList<EbayProductCategory>(categoriesByIds.values()));

        return response;
    }

    protected BasicDBObject createQuery(final BasicDBList aProductIds) {
        DBObject inClause = new BasicDBObject("$in", aProductIds);

        final BasicDBObject query = new BasicDBObject();
        query.put("_id", inClause);
        return query;
    }

    protected BasicDBList convertByteArrayToObjectId(final List<byte[]> aProductIdBytes) {
        return convertByteArrayToObjectId2(aProductIdBytes);
    }

    protected List<byte[]> getProductIdAsByteArrays(final DBCollection aRecoms,
                                                    final String aUserEmail) {
        return getProductIdsLikedByUser(aRecoms, aUserEmail);
    }

    protected DBCollection getCollection(final IMongoPersistenceState aPersistenceState,
                                         final String aCollName) {
        return aPersistenceState.getDb().getCollection(aCollName);
    }
}
