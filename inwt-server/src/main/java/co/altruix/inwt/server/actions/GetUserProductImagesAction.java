package co.altruix.inwt.server.actions;

import co.altruix.inwt.messages.GetUserProductImagesRequest;
import co.altruix.inwt.messages.GetUserProductImagesResponse;
import co.altruix.inwt.model.UserProductImage;
import co.altruix.inwt.server.persistence.IMongoPersistenceState;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

import java.util.LinkedList;
import java.util.List;

public class GetUserProductImagesAction extends
        InwtPersistenceAction<GetUserProductImagesResponse> {
    private final GetUserProductImagesRequest request;

    public GetUserProductImagesAction(final GetUserProductImagesRequest aRequest) {
        request = aRequest;
    }

    @Override
    protected GetUserProductImagesResponse runOnMongo(
            final IMongoPersistenceState aPersistenceState) {
        final DBCollection userProductImagesColl = aPersistenceState.getDb().getCollection(
                COLLECTION_USER_PRODUCT_IMAGES);
        final BasicDBObject query = new BasicDBObject(
                FIELD_COLLECTION_USER_PRODUCT_IMAGES_PRODUCT_ID, request.getUserProductId()).
                append(FIELD_COLLECTION_USER_PRODUCT_IMAGES_IS_DELETED, Boolean.FALSE);

        final DBCursor cursor = userProductImagesColl.find(query);
        final List<UserProductImage> userProductImages = new LinkedList<UserProductImage>();

        try {
            while (cursor.hasNext()) {
                final BasicDBObject curRecord = (BasicDBObject) cursor.next();

                final UserProductImage userProductImage =
                        UserProductImageUtils.convertTupleToUserProductImage(curRecord,
                        curRecord.getObjectId("_id").toByteArray(), request.getUserProductId());

                userProductImages.add(userProductImage);
            }
        } finally {
            cursor.close();
        }

        final GetUserProductImagesResponse result = new GetUserProductImagesResponse();
        result.setUserImages(userProductImages);

        return result;
    }
}
